/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;

public class CalConsequencesGrid extends DataGridView<RptConsequenceDTO> {

    public CalConsequencesGrid(DataProvider<RptConsequenceDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<RptConsequenceDTO>> columnList = new ArrayList<DataColumn<RptConsequenceDTO>>();
        columnList.add(createAssetColumn());
        columnList.add(createThreatColumn());
        columnList.add(createConsequenceColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<RptConsequenceDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<RptConsequenceDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<RptConsequenceDTO> createAssetColumn() {
        Column<RptConsequenceDTO, String> column = new Column<RptConsequenceDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptConsequenceDTO object) {
                return String.valueOf(object.getAsset());
            }
        };	
        Comparator<RptConsequenceDTO> comparator = new Comparator<RptConsequenceDTO>(){

			@Override
			public int compare(RptConsequenceDTO o1, RptConsequenceDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptConsequenceDTO>("Asset", column, comparator, 80);
    }

    private DataColumn<RptConsequenceDTO> createThreatColumn() {
        Column<RptConsequenceDTO, String> column = new Column<RptConsequenceDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptConsequenceDTO object) {
                return String.valueOf(object.getThreat());
            }
        };	
        Comparator<RptConsequenceDTO> comparator = new Comparator<RptConsequenceDTO>(){

			@Override
			public int compare(RptConsequenceDTO o1, RptConsequenceDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptConsequenceDTO>("Threat", column, comparator, 80);
    }
    
    private DataColumn<RptConsequenceDTO> createConsequenceColumn() {
        Column<RptConsequenceDTO, String> column = new Column<RptConsequenceDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(RptConsequenceDTO object) {
                return String.valueOf(object.getConsequence());
            }
        };	
        Comparator<RptConsequenceDTO> comparator = new Comparator<RptConsequenceDTO>(){

			@Override
			public int compare(RptConsequenceDTO o1, RptConsequenceDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<RptConsequenceDTO>("Consequence", column, comparator, 80);
    }


}
