/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import org.parre.client.rpt.presenter.EditOptionPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ViewUtil;
import org.parre.shared.ReportInvestmentOptionDTO;

public class EditOptionView extends BaseModelView<ReportInvestmentOptionDTO>  implements EditOptionPresenter.Display {
	private Widget viewWidget;
    private TextBox investmentOption;
    private TextBox countermeasure;
	private TextBox sitesApplied;
	private TextBox siteCost;
	private TextBox totalInvestment;
    private Button saveButton;
    private Long optionId;

	public EditOptionView() {
        viewWidget = buildView();
	}

	private Panel buildView() {
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);
        int rowNum = -1;
        investmentOption = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Option");
        investmentOption.setWidth("200px");
        countermeasure = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Countermeasures");
        countermeasure.setWidth("200px");
        sitesApplied = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "No. of Sites Applied");
        sitesApplied.setWidth("200px");
        siteCost = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Cost($) per Site");
        siteCost.setWidth("200px");
        totalInvestment = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Total Investment Cost($)");
        totalInvestment.setWidth("200px");
        saveButton = ViewUtil.createButton("Save");
        panel.add(inputTable);
        panel.add(ViewUtil.layoutRight(saveButton));
        panel.setStyleName("inputPanel");
        return panel;
    }
	

	public ReportInvestmentOptionDTO getOptionValues() {
		ReportInvestmentOptionDTO dto = new ReportInvestmentOptionDTO();
		dto.setInvestOption(ViewUtil.getText(investmentOption));
		dto.setCountermeasure(ViewUtil.getText(countermeasure));
		dto.setSitesApplied(ViewUtil.getText(sitesApplied));
		dto.setSiteCost(ViewUtil.getText(siteCost));
		dto.setTotalInvestment(ViewUtil.getText(totalInvestment));
		dto.setOptionId(optionId);
		return dto;
    }
	
	public void displayOptionValues(ReportInvestmentOptionDTO dto){
		 ViewUtil.setText(investmentOption, dto.getInvestOption());
	     ViewUtil.setText(countermeasure, dto.getCountermeasure());
	     ViewUtil.setText(sitesApplied, dto.getSitesApplied());
	     ViewUtil.setText(siteCost, dto.getSiteCost());
	     ViewUtil.setText(totalInvestment, dto.getTotalInvestment());
	     optionId = dto.getId();	     
	}
	
	/*public void clearOptionValues(){
		ViewUtil.setText(investmentOption, "");
	    ViewUtil.setText(countermeasure, "");
	    ViewUtil.setText(sitesApplied, "");
	    ViewUtil.setText(siteCost, "");
	    ViewUtil.setText(totalInvestment, "");
	    optionId = null;
	}*/
	
	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getOptionSaveButton() {
        return saveButton;
    }
    

	@Override
	public boolean validate() {
		return false;
	}

	@Override
	public void updateModel() {
	}
	
	@Override
	public void updateView() {
	}

	}
