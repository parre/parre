/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.view;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.google.gwt.user.cellview.client.Column;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.ReportDataGridView;
import org.parre.shared.ReportInvestmentOptionDTO;

public class ReportInvestmentOptionGrid extends ReportDataGridView<ReportInvestmentOptionDTO> {

    public ReportInvestmentOptionGrid(DataProvider<ReportInvestmentOptionDTO> dataProvider) {
        super(dataProvider);
        List<DataColumn<ReportInvestmentOptionDTO>> columnList = new ArrayList<DataColumn<ReportInvestmentOptionDTO>>();
        columnList.add(createInvOptionColumn());
        columnList.add(createCounterColumn());
        columnList.add(createSitesAppliedColumn());
        columnList.add(createSiteCostColumn());
        columnList.add(createTotInvestmentColumn());
        addColumns(columnList);
    }

    private DoubleClickableTextCell<ReportInvestmentOptionDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<ReportInvestmentOptionDTO>(getSelectionModel(), getSelectionSource());
    }

    private DataColumn<ReportInvestmentOptionDTO> createInvOptionColumn() {
        Column<ReportInvestmentOptionDTO, String> column = new Column<ReportInvestmentOptionDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportInvestmentOptionDTO object) {
                return String.valueOf(object.getInvestOption());
            }
        };	
        Comparator<ReportInvestmentOptionDTO> comparator = new Comparator<ReportInvestmentOptionDTO>(){

			@Override
			public int compare(ReportInvestmentOptionDTO o1, ReportInvestmentOptionDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportInvestmentOptionDTO>("Option", column, comparator, 48);
    }

    private DataColumn<ReportInvestmentOptionDTO> createCounterColumn() {
        Column<ReportInvestmentOptionDTO, String> column = new Column<ReportInvestmentOptionDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportInvestmentOptionDTO object) {
                return String.valueOf(object.getCountermeasure());
            }
        };	
        Comparator<ReportInvestmentOptionDTO> comparator = new Comparator<ReportInvestmentOptionDTO>(){

			@Override
			public int compare(ReportInvestmentOptionDTO o1, ReportInvestmentOptionDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportInvestmentOptionDTO>("Countermeasures", column, comparator, 48);
    }
    
    private DataColumn<ReportInvestmentOptionDTO> createSitesAppliedColumn() {
        Column<ReportInvestmentOptionDTO, String> column = new Column<ReportInvestmentOptionDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportInvestmentOptionDTO object) {
                return String.valueOf(object.getSitesApplied());
            }
        };	
        Comparator<ReportInvestmentOptionDTO> comparator = new Comparator<ReportInvestmentOptionDTO>(){

			@Override
			public int compare(ReportInvestmentOptionDTO o1, ReportInvestmentOptionDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportInvestmentOptionDTO>("No. of Sites Applied", column, comparator, 48);
    }
    
    private DataColumn<ReportInvestmentOptionDTO> createSiteCostColumn() {
        Column<ReportInvestmentOptionDTO, String> column = new Column<ReportInvestmentOptionDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportInvestmentOptionDTO object) {
                return String.valueOf(object.getSiteCost());
            }
        };	
        Comparator<ReportInvestmentOptionDTO> comparator = new Comparator<ReportInvestmentOptionDTO>(){

			@Override
			public int compare(ReportInvestmentOptionDTO o1, ReportInvestmentOptionDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportInvestmentOptionDTO>("Cost($) Per Site", column, comparator, 48);
    }
    
    private DataColumn<ReportInvestmentOptionDTO> createTotInvestmentColumn() {
        Column<ReportInvestmentOptionDTO, String> column = new Column<ReportInvestmentOptionDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ReportInvestmentOptionDTO object) {
                return String.valueOf(object.getTotalInvestment());
            }
        };	
        Comparator<ReportInvestmentOptionDTO> comparator = new Comparator<ReportInvestmentOptionDTO>(){

			@Override
			public int compare(ReportInvestmentOptionDTO o1, ReportInvestmentOptionDTO o2) {
				return 0;
			}        	
        };
            return new DataColumn<ReportInvestmentOptionDTO>("Totla Investment Cost($)", column, comparator, 48);
    }
    
    


}
