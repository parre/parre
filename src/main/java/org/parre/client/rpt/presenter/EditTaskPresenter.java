/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rpt.presenter;

import java.util.List;

import org.jfree.util.Log;

import com.google.gwt.core.shared.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.web.bindery.event.shared.EventBus;
import org.parre.client.ClientSingleton;
import org.parre.client.messaging.ReportsServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.rpt.event.EditReportAssessmentTaskGridEvent;
import org.parre.client.rpt.event.ReportAssessmentTaskGridEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.shared.ReportAssessmentTaskDTO;

public class EditTaskPresenter implements Presenter {
	private EventBus eventBus;
    private Display display;
    private ReportsServiceAsync service;
    private BusyHandler busyHandler;
    
    public interface Display extends ModelView<ReportAssessmentTaskDTO> {
        HasClickHandlers getSaveButton();
        ReportAssessmentTaskDTO getTaskValues();
		void displayTaskValues(ReportAssessmentTaskDTO editDTO);
    }
    
    public EditTaskPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getReportsService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

	public void go() {
		bind();
		
	}
	
	private void bind() {
        display.getSaveButton().addClickHandler(new ClickHandler() {
        	  public void onClick(ClickEvent event) {
                ExecutableAsyncCallback<List<ReportAssessmentTaskDTO>> exec = new ExecutableAsyncCallback<List<ReportAssessmentTaskDTO>>(busyHandler, new BaseAsyncCommand<List<ReportAssessmentTaskDTO>>() {
                    public void execute(AsyncCallback<List<ReportAssessmentTaskDTO>> callback) {
                    	ServiceFactory.getInstance().getReportsService().saveAddTaskPopup(ClientSingleton.getParreAnalysisId(), display.getTaskValues(), callback);
                    }

                    @Override
                    public void handleResult(List<ReportAssessmentTaskDTO> result) {
                    	eventBus.fireEvent(ReportAssessmentTaskGridEvent.create(result));
                    }

                    @Override
                    public boolean handleError(Throwable t) {
                        MessageDialog.popup(t.getMessage(), true);
                        return true;
                    }
                });
                exec.makeCall();
            }
        });
        
        eventBus.addHandler(EditReportAssessmentTaskGridEvent.TYPE, new EditReportAssessmentTaskGridEvent.Handler() {
			public void onEvent(EditReportAssessmentTaskGridEvent event) {
				ReportAssessmentTaskDTO editDTO = event.getTaskDTO();
				//GWT.log(" Task Id = " + editDTO.getId() + " and Report Id = " + editDTO.getReportId());
                display.displayTaskValues(editDTO);
			}
        });
        
       }

}
