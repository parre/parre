/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.nt.presenter.EditNaturalThreatPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ProbabilityValueValidator;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AmountDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.PhysicalResourceDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class EditNaturalThreatView.
 */
public class EditNaturalThreatView extends BaseModelView<NaturalThreatDTO>  implements EditNaturalThreatPresenter.Display {
	protected List<FieldValidationHandler> requiredFieldHandlers;
	protected List<FieldValidationHandler> requiredCalculationFieldHandlers;
	protected Label header;
	protected Widget viewWidget;
	protected TextBox assetId;
	protected TextBox threatName;
	protected TextBox fatalities;
	protected TextBox seriousInjuries;
	protected TextBox financialImpact;
    protected TextBox economicImpact;
	protected TextBox vulnerability;
	protected TextBox lot;
	protected TextBox operationLoss;
	
	protected TextBox duration;
	protected TextBox severity;
	
	protected TextBox replacementCost;
	protected TextBox damageFactor;
	protected TextBox financialTotal;
	protected TextBox totalRisk;
	
	protected Button calculateButton;
    protected Button manualCalculateButton;
	
	protected Button saveButton;
	protected Button cancelButton;
	
	protected Widget calculationView;
	protected CheckBox useManualEntry;
	
	protected PhysicalResourceDTO physicalResource;
	protected NaturalThreatCalculationDTO calculation;
	
	final protected String calculatorWidgetWidth = "120px";
	
	protected Long lastParreAnalysisId;
	
	private boolean hasCalculator = true;
    
	public EditNaturalThreatView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
		requiredCalculationFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
	}

	private Widget buildView() {
		header = new Label("Edit Natural Threat");
        header.setStyleName("inputPanelHeader");
        
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);

        int rowNum = -1;

        final String width = "120px";
        assetId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset ID", requiredFieldHandlers);;
        assetId.setWidth(width);
        assetId.setEnabled(false);

        threatName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat", requiredFieldHandlers);
        threatName.setWidth(width);
        threatName.setEnabled(false);

        fatalities = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Fatalities", requiredFieldHandlers);
        fatalities.setWidth(width);

        seriousInjuries = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Serious Injuries", requiredFieldHandlers);
        seriousInjuries.setWidth(width);
        
        duration = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Duration (days)");
    	duration.setWidth(width);
    	
    	severity = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Severity (MGD)");
    	severity.setWidth(width);

        economicImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Economic Impact", requiredFieldHandlers);
        economicImpact.setWidth(width);

        useManualEntry = new CheckBox("Manually enter risk totals.");
        
        useManualEntry.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				toggleManualEntry();
			}
		});
        
        ViewUtil.addFormEntry(inputTable, ++rowNum, "", useManualEntry);
        
        financialImpact = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Financial Impact", requiredFieldHandlers);
        financialImpact.setWidth(width);
        
        vulnerability = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Vulnerability", requiredFieldHandlers, ProbabilityValueValidator.INSTANCE);
        makeProbabilityBox(vulnerability);
        vulnerability.setWidth(width);

        lot = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat Likelihood", requiredFieldHandlers, ProbabilityValueValidator.INSTANCE);
        makeProbabilityBox(lot);
        lot.setWidth(width);

        manualCalculateButton = ViewUtil.createButton("Manually Calculate");
        ViewUtil.addFormEntry(inputTable, ++rowNum, "", manualCalculateButton);
        manualCalculateButton.setEnabled(false);
        
        saveButton = ViewUtil.createButton("Save");
        cancelButton = ViewUtil.createButton("Cancel");
        
        VerticalPanel viewPanel = new VerticalPanel();
        viewPanel.setWidth("100%");
        viewPanel.setSpacing(5);
        viewPanel.setStyleName("inputPanel");

        viewPanel.add(header);
        
        VerticalPanel calculationPanel = new VerticalPanel();
        calculationPanel.setBorderWidth(1);
        Label calcPanelLabel = new Label("Calculator");
        calcPanelLabel.setStyleName("inputPanelHeader");
        calculationPanel.add(calcPanelLabel);
        calculationPanel.add(createCalculationView());
        viewPanel.add(ViewUtil.layoutCenter(inputTable, calculationPanel));
        viewPanel.add(ViewUtil.layoutCenter(createTotalsTable()));
        viewPanel.add(ViewUtil.layoutCenter(saveButton, cancelButton));
        return viewPanel;

    }
	
	protected void toggleManualEntry() {
		calculateButton.setEnabled(!useManualEntry.getValue());
		financialImpact.setEnabled(useManualEntry.getValue());
		vulnerability.setEnabled(useManualEntry.getValue());
		lot.setEnabled(useManualEntry.getValue());
        manualCalculateButton.setEnabled(useManualEntry.getValue());
	}
	
	protected Widget createCalculationView() {
		FlexTable inputTable = new FlexTable();
    	calculateButton = ViewUtil.createButton("Calculate");
    	
    	calculateButton.setSize("150px", "30px");
    	
    	addCalculationWidgets(inputTable);
    	
    	ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "", calculateButton);
    	
    	calculationView = inputTable;
    	calculationView.setVisible(true);
    	
    	return inputTable;
	}
	
	protected void addCalculationWidgets(FlexTable inputTable) {
		int currentRow = inputTable.getRowCount() - 1;
		
		if (usesLossAndReplacement()) {
			operationLoss = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Operational Loss ($)");
			operationLoss.setWidth(calculatorWidgetWidth);
			
	    	replacementCost = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Replacement Cost ($)");
	    	replacementCost.setWidth(calculatorWidgetWidth);
	    	replacementCost.setEnabled(false);
		}
		
    	if (usesDamageFactor()) {
	    	damageFactor = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Damage Factor");
	    	damageFactor.setWidth(calculatorWidgetWidth);
	    	damageFactor.setEnabled(false);
    	}
	}
	
	protected Widget createTotalsTable() {
		FlexTable inputTable = new FlexTable();
    	int currentRow = -1;
    	
    	final String width = "120px";
    	
    	financialTotal = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Financial Total ($)");
    	financialTotal.setWidth(width);
    	financialTotal.setEnabled(false);
    	
    	totalRisk = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Total Risk ($)");
    	totalRisk.setWidth(width);
    	totalRisk.setEnabled(false);
    	
    	return inputTable;
	}
	
	public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers) && validateCalculator();
	}
	
	public boolean validateCalculator() {
		return !useManualEntry.getValue() || ViewUtil.validate(requiredCalculationFieldHandlers);
	}

    @Override
    public TextBox getEconomicImpact() {
        return economicImpact;
    }

    @Override
    public TextBox getOperationalLoss() {
        return null;
    }

    @Override
    public TextBox getFinancialImpact() {
        return financialImpact;
    }

    public void updateModel() {
        NaturalThreatDTO model = getModel();
        model.setFatalities(ViewUtil.getQuantity(fatalities));
        model.setSeriousInjuries(ViewUtil.getQuantity(seriousInjuries));
        model.setFinancialImpactDTO(new AmountDTO(ViewUtil.getBigDecimal(financialImpact)));
        model.setEconomicImpactDTO(new AmountDTO(ViewUtil.getBigDecimal(economicImpact)));
        model.setVulnerability(getProbability(vulnerability));
        model.setLot(getProbability(lot));
        model.setManuallyEntered(useManualEntry.getValue());
        model.setDurationDays(getBigDecimal(duration));
    	model.setSeverityMgd(getBigDecimal(severity));
        model.setFinancialTotalDTO(new AmountDTO(getBigDecimal(financialTotal)));
        model.setTotalRiskDTO(new AmountDTO(ViewUtil.getBigDecimal(totalRisk)));
        if (!useManualEntry.getValue() && usesLossAndReplacement()) {
        	model.setOperationalLossDTO(new AmountDTO(ViewUtil.getBigDecimal(operationLoss)));
        }
    }

	public void updateView() {
        NaturalThreatDTO model = getModel();
        useManualEntry.setValue(model.getManuallyEntered() || !hasCalculator);
        setText(assetId, model.getAssetId());
        setText(threatName, model.getThreatName());
        setQuantity(fatalities, model.getFatalities());
        setQuantity(seriousInjuries, model.getSeriousInjuries());
        financialImpact.setText(ViewUtil.toDisplayValue(model.getFinancialImpactDTO().getQuantity()));
        economicImpact.setText(ViewUtil.toDisplayValue(model.getEconomicImpactDTO().getQuantity()));
        setProbability(vulnerability, model.getVulnerability());
        setProbability(lot, model.getLot());
        
        setBigDecimal(duration, model.getDurationDays());
        setBigDecimal(severity, model.getSeverityMgd());
        
        if (usesLossAndReplacement()) {
        	if (hasCalculator) {
        		operationLoss.setText(ViewUtil.toDisplayValue(model.getOperationalLossDTO().getQuantity()));
        	} else {
        		operationLoss.setEnabled(false);
        	}
        }
        
        if (model.getTotalRiskDTO() != null) {
        	totalRisk.setText(ViewUtil.toDisplayValue(model.getTotalRiskDTO().getQuantity()));
        }
        
        if (model.getFinancialTotalDTO() != null) {
        	financialTotal.setText(ViewUtil.toDisplayValue(model.getFinancialTotalDTO().getQuantity()));
        }
        
        toggleManualEntry();
	}

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }
    
    public HasText getHeader() {
        return header;
    }

	@Override
	public HasClickHandlers getCancelButton() {
		return cancelButton;
	}
	
	@Override
	public HasClickHandlers getCalculateButton() {
		return calculateButton;
	}

	@Override
	public void setPhysicalResource(PhysicalResourceDTO dto) {
		this.physicalResource = dto;
		if (usesLossAndReplacement()) {
            replacementCost.setText(ViewUtil.toDisplayValue(physicalResource.getReplacementCostDTO().getQuantity()));
		}
		
        if (usesDamageFactor()) {
        	setBigDecimal(damageFactor, dto.getDamageFactor());
        }
	}

    @Override
    public PhysicalResourceDTO getPhysicalResourceDTO() {
        return physicalResource;
    }

    @Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		if (!validateCalculator()) {
			return null;
		}
		calculation = new NaturalThreatCalculationDTO();
		calculation.setAsset(physicalResource);
		calculation.getAsset().setParreAnalysisId(ClientSingleton.getParreAnalysisId());
		calculation.setAnalysis(new NaturalThreatDTO());
		calculation.getAnalysis().setFatalities(ViewUtil.getQuantity(fatalities));
		calculation.getAnalysis().setSeriousInjuries(ViewUtil.getQuantity(seriousInjuries));
		calculation.getAnalysis().setDurationDays(getBigDecimal(duration));
		calculation.getAnalysis().setSeverityMgd(getBigDecimal(severity));
		if (usesLossAndReplacement()) {
			calculation.getAnalysis().setOperationalLossDTO(new AmountDTO(ViewUtil.getBigDecimal(operationLoss)));
		}
		return calculation;
	}

	@Override
	public void updateCalculationResults(NaturalThreatCalculationDTO calculation) {
		this.calculation = calculation;
		
		if (!calculation.getCalculationSuccess()) {
			MessageDialog.popup(calculation.getResultMessage());
			return;
		}
		
		financialImpact.setText(ViewUtil.toDisplayValue(calculation.getAnalysis().getFinancialImpactDTO().getQuantity()));
		setProbability(vulnerability, calculation.getAnalysis().getVulnerability());
		setProbability(lot, calculation.getAnalysis().getLot());
		financialTotal.setText(ViewUtil.toDisplayValue(calculation.getAnalysis().getFinancialTotalDTO().getQuantity()));
		totalRisk.setText(ViewUtil.toDisplayValue(calculation.getAnalysis().getTotalRiskDTO().getQuantity()));
	}
	
	@Override
	public HasClickHandlers getAddProfileButton() {
		return null;
	}

	@Override
	public HasClickHandlers getEditProfileButton() {
		return null;
	}
	
	@Override
	public Long getCurrentProfileId() {
		return null;
	}
	
	@Override
	public void updateProfileList() {
		lastParreAnalysisId = ClientSingleton.getCurrentParreAnalysis().getId();
	}

	protected boolean usesDamageFactor() {
		return true;
	}
	
	protected boolean usesLossAndReplacement() {
		return true;
	}
	
	@Override
	public Long getLastParreAnalysisId() {
		return lastParreAnalysisId;
	}

    @Override
    public void setStateList(List<LabelValueDTO> stateList) {
    }
    
    @Override
    public void setHasCalculator(boolean value) {
    	this.hasCalculator = value;
    }

    @Override
    public void selectHurricaneProfile(Long hurricaneProfileId) {}

    @Override
    public void selectEarthquakeProfile(Long hurricaneProfileId) {}

    @Override
    public HasClickHandlers getManualCalculateButton() {
        return manualCalculateButton;
    }

    @Override
    public void manuallyCalculate() {
        BigDecimal newFinancialTotal = ViewUtil.getBigDecimal(financialImpact);
        if(ClientSingleton.getCurrentParreAnalysis().getUseStatValues()) {
            newFinancialTotal = newFinancialTotal.add(ViewUtil.getBigDecimal(fatalities)
                    .multiply(ClientSingleton.getStatValues().getFatalityQuantity()
                            .multiply(new BigDecimal(1000000))));
            newFinancialTotal = newFinancialTotal.add(ViewUtil.getBigDecimal(seriousInjuries)
                    .multiply(ClientSingleton.getStatValues().getSeriousInjuryQuantity()
                            .multiply(new BigDecimal(1000000))));
        }
        financialTotal.setText(ViewUtil.toDisplayValue(newFinancialTotal));
        ClientLoggingUtil.info("Financial total = " + newFinancialTotal);
        BigDecimal totalrisk = newFinancialTotal
                .multiply(ViewUtil.getBigDecimal(vulnerability))
                .multiply(ViewUtil.getBigDecimal(lot));
        totalRisk.setText(ViewUtil.toDisplayValue(totalrisk));
    }

    @Override
    public Boolean getTornadoLocationOverride() {
        return null;
    }

    @Override
    public ListBox getTornadoCountyName() {
        return null;
    }

    @Override
    public void setTornadoStateList(List<LabelValueDTO> stateList) {}

    @Override
    public ListBox getStateCode() {
        return null;
    }
    
    @Override
    public CheckBox getUseManualEntry() {
    	return useManualEntry;
    }

    @Override
    public CheckBox getOverrideLocation() {
        return null;
    }

    @Override
    public void toggleOverride() {}
}
