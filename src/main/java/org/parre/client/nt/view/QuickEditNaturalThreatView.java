/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.nt.presenter.QuickEditNaturalThreatPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AmountDTO;
import org.parre.shared.NaturalThreatDTO;

/**
 * The Class QuickEditNaturalThreatView.
 */
public class QuickEditNaturalThreatView extends BaseModelView<NaturalThreatDTO>  implements QuickEditNaturalThreatPresenter.Display {
	private List<FieldValidationHandler> requiredFieldHandlers;
	private Widget viewWidget;
    private TextBox assetId;
    private TextBox threatName;
	private TextBox fatalities;
    private TextBox seriousInjuries;
    private TextBox financialImpact;
    private TextBox vulnerability;
    private TextBox lot;
    private Button saveButton;

	
	
	public QuickEditNaturalThreatView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
		
	}

	private Widget buildView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);

        int colNum = -1;

        final String width = "120px";
        assetId = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Asset ID", ++colNum, requiredFieldHandlers);;
        assetId.setWidth(width);
        assetId.setEnabled(false);

        threatName = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Threat", ++colNum, requiredFieldHandlers);
        threatName.setWidth(width);
        threatName.setEnabled(false);

        fatalities = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Fatalities", ++colNum, requiredFieldHandlers);
        fatalities.setWidth(width);

        seriousInjuries = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Serious Injuries", ++colNum, requiredFieldHandlers);
        seriousInjuries.setWidth(width);

        financialImpact = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Financial Impact", ++colNum, requiredFieldHandlers);
        financialImpact.setWidth(width);

        vulnerability = ViewUtil.addTextBoxWithHeader(inputTable, 0, "Vulnerability", ++colNum, requiredFieldHandlers);
        makeProbabilityBox(vulnerability);

        lot = ViewUtil.addTextBoxWithHeader(inputTable, 0, "LOT", ++colNum, requiredFieldHandlers);
        makeProbabilityBox(lot);

        saveButton = ViewUtil.createButton("Save");
        inputTable.setWidget(1, ++colNum, saveButton);
        inputTable.setStyleName("inputPanel");
        return inputTable;

    }
	

	public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers);
	}

	public void updateModel() {
        NaturalThreatDTO model = getModel();
        model.setFatalities(ViewUtil.getQuantity(fatalities));
        model.setSeriousInjuries(ViewUtil.getQuantity(seriousInjuries));
        model.setFinancialImpactDTO(new AmountDTO(ViewUtil.getBigDecimal(financialImpact)));
        //model.setEconomicImpactDTO(new AmountDTO(ViewUtil.getBigDecimal(economicImpact)));
        model.setVulnerability(getProbability(vulnerability));
        model.setLot(getProbability(lot));
    }

	public void updateView() {
        NaturalThreatDTO model = getModel();
        setText(assetId, model.getAssetId());
        setText(threatName, model.getThreatName());
        setQuantity(fatalities, model.getFatalities());
        setQuantity(seriousInjuries, model.getSeriousInjuries());
        setBigDecimal(financialImpact, model.getFinancialImpactDTO().getQuantity());
        setProbability(vulnerability, model.getVulnerability());
        setProbability(lot, model.getLot());
	}

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }
}
