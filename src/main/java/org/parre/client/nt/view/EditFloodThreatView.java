/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.view;

import org.parre.client.util.ViewUtil;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;

import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.TextBox;

/**
 * The Class EditFloodThreatView.
 */
public class EditFloodThreatView extends EditNaturalThreatView {
	private CheckBox floodRisk;
	
	@Override
	protected void addCalculationWidgets(FlexTable inputTable) {
		super.addCalculationWidgets(inputTable);
		floodRisk = new CheckBox("");
		ViewUtil.addFormEntry(inputTable, inputTable.getRowCount(), "Flood Risk", floodRisk);
	}
	
	@Override
	public void updateModel() {
		super.updateModel();
		getModel().setFloodRisk(floodRisk.getValue());
	}
	
	@Override
	public void updateView() {
		super.updateView();
		NaturalThreatDTO model = getModel();
		floodRisk.setValue(model.getFloodRisk() != null && model.getFloodRisk() == true);
	}
	
	@Override
	public NaturalThreatCalculationDTO prepareCalculationDTO() {
		calculation = super.prepareCalculationDTO();
		calculation.getAnalysis().setFloodRisk(floodRisk.getValue());
		return calculation;
	}

	@Override
	protected boolean usesDamageFactor() {
		return false;
	}

    @Override
    public TextBox getOperationalLoss() {
        return operationLoss;
    }
	
	@Override
	protected void toggleManualEntry() {
 		super.toggleManualEntry();
 		
 		boolean isManualEntry = useManualEntry.getValue();
 		operationLoss.setEnabled(!isManualEntry);
 		floodRisk.setEnabled(!isManualEntry);
	}

	
}
