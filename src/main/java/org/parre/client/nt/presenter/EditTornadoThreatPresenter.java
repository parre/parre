/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nt.presenter;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.nt.event.GetTornadoCountyDataEvent;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.ViewUtil;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.types.NaturalThreatType;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The Class EditTornadoThreatPresenter.
 */
public class EditTornadoThreatPresenter extends EditNaturalThreatPresenter {

	public EditTornadoThreatPresenter(Display display) {
		super(display);
        bind();
        setStateCodeList();
	}

    private void bind() {
        display.getOperationalLoss().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent changeEvent) {
                display.getOperationalLoss().setText(ViewUtil.toDisplayValue(
                        ViewUtil.getBigDecimal(display.getOperationalLoss())));
            }
        });

        display.getStateCode().addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                display.getModel().setStateCodeOverride(ViewUtil.getSelectedValue(display.getStateCode()));
                ClientLoggingUtil.info("State selected = " + display.getModel().getStateCodeOverride());
                getTornadoCountyData();
            }
        });

        display.getOverrideLocation().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                display.updateModel();
                display.toggleOverride();
                if(!display.getOverrideLocation().getValue()) {
                    getTornadoCountyData();
                }
            }
        });

        eventBus.addHandler(GetTornadoCountyDataEvent.TYPE, new GetTornadoCountyDataEvent.Handler() {
            @Override
            public void onEvent(GetTornadoCountyDataEvent event) {
                getTornadoCountyData();
            }
        });
    }

	@Override
	protected void calculate() {
		ExecutableAsyncCallback<NaturalThreatCalculationDTO> exec = new ExecutableAsyncCallback<NaturalThreatCalculationDTO>(busyHandler, new BaseAsyncCommand<NaturalThreatCalculationDTO>() {
            public void execute(AsyncCallback<NaturalThreatCalculationDTO> asyncCallback) {
                service.calculateTornadoData(display.prepareCalculationDTO(), asyncCallback);
            }

            @Override
            public void handleResult(NaturalThreatCalculationDTO result) {
                display.updateCalculationResults(result);
            }

            @Override
            public boolean handleError(Throwable t) {
                MessageDialog.popup(t.getMessage(), true);
                return true;
            }
        });
        exec.makeCall();
	}

	@Override
	protected boolean handlesNaturalThreatType(NaturalThreatType type) {
		return NaturalThreatType.TORNADO == type;
	}

    protected void setStateCodeList() {
        display.setTornadoStateList(ClientSingleton.getStateList());
    }

    protected void getTornadoCountyData() {
        ExecutableAsyncCallback<List<String>> exec = new ExecutableAsyncCallback<List<String>>(busyHandler, new BaseAsyncCommand<List<String>>() {
            @Override
            public void execute(AsyncCallback<List<String>> aSyncCallback) {
                if(display.getTornadoLocationOverride()) {
                    service.getTornadoCountyData(display.getStateCode().getValue(display.getStateCode().getSelectedIndex()), aSyncCallback);
                }
                else {
                    service.getTornadoCountyData(display.getPhysicalResourceDTO().getStateCode(), aSyncCallback);
                }
            }

            public void handleResult(List<String> results) {
                display.getTornadoCountyName().clear();
                display.getTornadoCountyName().addItem("Select", "");
                for(String result : results) {
                    display.getTornadoCountyName().addItem(result);
                }
                display.updateView();
            }
        });
        exec.makeCall();
    }
	
}
