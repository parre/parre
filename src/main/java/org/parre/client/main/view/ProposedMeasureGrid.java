/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.main.view;

import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridHeaders;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.ProposedMeasureDTO;

/**
 * The Class ProposedMeasureGrid.
 */
public class ProposedMeasureGrid extends DataGridView<ProposedMeasureDTO> {
    public ProposedMeasureGrid(DataProvider<ProposedMeasureDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<ProposedMeasureDTO>> columnList = new ArrayList<DataColumn<ProposedMeasureDTO>>();
		columnList.add(createNameColumn());
		columnList.add(createDescriptionColumn());
        columnList.add(createTypeColumn());
        columnList.add(createCapitalCostColumn());
        columnList.add(createEffectiveLifeColumn());
        columnList.add(createSalvageValueColumn());
        columnList.add(createInflationRateColumn());
        addColumns(columnList);
	}

    private DataColumn<ProposedMeasureDTO> createNameColumn() {
        Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(ProposedMeasureDTO object) {
                return object.getName();
            }
        };
        column.setSortable(true);

        Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
            public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        return new DataColumn<ProposedMeasureDTO>("Name", column, comparator, 20);
    }


    private DataColumn<ProposedMeasureDTO> createDescriptionColumn() {
    	Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ProposedMeasureDTO object) {
				return	object.getDescription();
			}
		};
		column.setSortable(true);

		Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
			public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
				return o1.getDescription().compareTo(o2.getDescription());
			}
		};
		return new DataColumn<ProposedMeasureDTO>("Description", column, comparator, 25);
	}



	private DataColumn<ProposedMeasureDTO> createTypeColumn() {
		Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(ProposedMeasureDTO object) {
				return	object.getType().getValue();
			}
		};
		column.setSortable(true);

		Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
			public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
				return o1.getType().compareTo(o2.getType());
			}
		};
		return new DataColumn<ProposedMeasureDTO>("Type", column, comparator, 15);
	}


    private DataColumn<ProposedMeasureDTO> createCapitalCostColumn() {
    	Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ProposedMeasureDTO object) {
    			return ViewUtil.toDisplayValue(object.getCapitalCostQuantity());
    		}
    	};
    	column.setSortable(true);

    	Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
    		public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
    			return o1.getCapitalCostQuantity().compareTo(o2.getCapitalCostQuantity());
    		}
    	};
    	return new DataColumn<ProposedMeasureDTO>(DataGridHeaders.createHeader("Capital", "Cost"), column, comparator, 10);
    }
    private DataColumn<ProposedMeasureDTO> createEffectiveLifeColumn() {
    	Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ProposedMeasureDTO object) {
    			return String.valueOf(object.getEffectiveLife());
    		}
    	};
    	column.setSortable(true);

    	Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
    		public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
    			return o1.getEffectiveLife().compareTo(o2.getEffectiveLife());
    		}
    	};
    	return new DataColumn<ProposedMeasureDTO>(DataGridHeaders.createHeader("Effective", "Life"), column, comparator, 10);
    }

    private DataColumn<ProposedMeasureDTO> createInflationRateColumn() {
    	Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(ProposedMeasureDTO object) {
                return ViewUtil.toDisplayValue(object.getInflationRate());
    		}
    	};
    	column.setSortable(true);
    	
    	Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
    		public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
    			return o1.getInflationRate().compareTo(o2.getInflationRate());
    		}
    	};
    	return new DataColumn<ProposedMeasureDTO>(DataGridHeaders.createHeader("Inflation", "Rate"), column, comparator, 10);
    }

    private DataColumn<ProposedMeasureDTO> createSalvageValueColumn() {
        Column<ProposedMeasureDTO, String> column = new Column<ProposedMeasureDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(ProposedMeasureDTO object) {
                return ViewUtil.toDisplayValue(object.getSalvageValueQuantity());
            }
        };
        column.setSortable(true);

        Comparator<ProposedMeasureDTO> comparator = new Comparator<ProposedMeasureDTO>() {
            public int compare(ProposedMeasureDTO o1, ProposedMeasureDTO o2) {
                return o1.getSalvageValueQuantity().compareTo(o2.getSalvageValueQuantity());
            }
        };
        return new DataColumn<ProposedMeasureDTO>(DataGridHeaders.createHeader("Salvage", "Value"), column, comparator, 10);

    }

    private DoubleClickableTextCell<ProposedMeasureDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<ProposedMeasureDTO>(getSelectionModel(), getSelectionSource());
    }

}
