/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.util.List;

import org.parre.shared.*;

/*
	Date			Author				Changes
    12/1/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface LotService.
 */
@RemoteServiceRelativePath("lot")
public interface LotService extends RemoteService {
    LotDTO save(LotDTO vulnerabilityDTO);

    List<LotDTO> getAssetSearchResults(AssetSearchDTO dto, Long parreAnalysisId);

    List<LotDTO> getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId);

    List<LabelValueDTO> getAnalysisTypes();

    LotAnalysisDTO getCurrentAnalysis(Long parreAnalysisId);

    LotAnalysisDTO startAnalysis(LotAnalysisDTO dto);

    LotDTO saveWithProxyIndication(LotDTO lotDTO);

    List<LabelValueDTO> getProxyCities();

    List<LabelValueDTO> getProxyTargetTypes();

    ProxyIndicationDTO getProxyIndication(Long lotId);

    ProxyIndicationDTO saveProxyIndication(ProxyIndicationDTO proxyIndicationDTO);

    List<ProxyIndicationDTO> getExistingProxyIndications(Long parreAnalysisId);

    ProxyIndicationDTO associateProxyIndication(Long lotId, Long proxyIndicationId, boolean makeCopy);

    /**
     * Utility/Convenience class.
     * Use VulnerabilityService.App.getInstance() to access static instance of VulnerabilityServiceAsync
     */
    public static class App {
        private static final LotServiceAsync ourInstance = (LotServiceAsync) GWT.create(LotService.class);

        public static LotServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
