/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import java.util.List;

import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.ZipLookupDataDTO;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>WarrantyServService</code>.
 */
public interface AssetServiceAsync {

    void getAssetSearchResults(AssetSearchDTO searchDTO, AsyncCallback<List<AssetDTO>> callback);

    void getStateList(AsyncCallback<List<LabelValueDTO>> callback);

    void saveAsset(AssetDTO assetDTO, Long parreAnalysisId, AsyncCallback<AssetDTO> callback);

    void getAssetThreatLevels(Long parreAnalysisId, AsyncCallback<List<AssetThreatLevelsDTO>> asyncCallback);

    void getActiveThreats(Long parreAnalysisId, AsyncCallback<List<ThreatDTO>> callback);

    void deleteAsset(AssetDTO assetDTO, Long parreAnalysisId, AsyncCallback<AssetDTO> callback);

    void getZipLookupData(String zipCode, AsyncCallback<ZipLookupDataDTO> callback);

    void hasAssets(Long parreAnalysisId, AsyncCallback<Boolean> async);

    void hasActiveThreats(Long parreAnalysis, AsyncCallback<Boolean> async);
    
    void getSystemTypes(Long parreAnalysisId, AsyncCallback<List <String>> callback);
}
