/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.shared.*;

/*
    Date			Author				Changes
    2/15/12	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface RiskResilienceServiceAsync.
 */
public interface RiskResilienceServiceAsync {
    void getAnalysis(Long parreAnalysisId, AsyncCallback<RiskResilienceAnalysisDTO> callback);

    void getRiskResilienceDTOs(Long riskResilienceAnalysisId, Long baselineId, AsyncCallback<List<RiskResilienceDTO>> callback);

    void saveRiskResilience(RiskResilienceDTO riskResilienceDTO, AsyncCallback<RiskResilienceDTO> callback);

    void getUriResponseDTOs(Long riskResilienceAnalysisId, AsyncCallback<List<UriResponseDTO>> callback);

    void saveUriResponses(List<UriResponseDTO> uriResponseDTOs, AsyncCallback<Void> callback);

    void getUriQuestionDTOs(AsyncCallback<List<UriQuestionDTO>> callback);

    void updateUnitPrice(Long riskResilienceId, UnitPriceDTO unitPriceDTO, AsyncCallback<Void> callback);
}
