/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.messaging;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.google.gwt.core.client.GWT;

import java.util.List;

import org.parre.shared.*;

/*
	Date			Author				Changes
    2/15/12	Swarn S. Dhaliwal	Created
*/
/**
 * The Interface RiskResilienceService.
 */
@RemoteServiceRelativePath("riskResilience")
public interface RiskResilienceService extends RemoteService {
    RiskResilienceAnalysisDTO getAnalysis(Long parreAnalysisId);

    List<RiskResilienceDTO> getRiskResilienceDTOs(Long riskResilienceAnalysisId, Long baselineId);

    RiskResilienceDTO saveRiskResilience(RiskResilienceDTO riskResilienceDTO);

    List<UriResponseDTO> getUriResponseDTOs(Long riskResilienceAnalysisId);

    void saveUriResponses(List<UriResponseDTO> uriResponseDTOs);

    List<UriQuestionDTO> getUriQuestionDTOs();

    void updateUnitPrice(Long riskResilienceId, UnitPriceDTO unitPriceDTO);

    /**
     * Utility/Convenience class.
     * Use RraService.App.getInstance() to access static instance of RraServiceAsync
     */
    public static class App {
        private static final RiskResilienceServiceAsync ourInstance = (RiskResilienceServiceAsync) GWT.create(RiskResilienceService.class);

        public static RiskResilienceServiceAsync getInstance() {
            return ourInstance;
        }
    }
}
