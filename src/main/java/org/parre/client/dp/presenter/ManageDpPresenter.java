/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.presenter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.asset.event.ClearAssetSearchEvent;
import org.parre.client.common.ClientCalculationUtil;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.ManageAnalysisPresenter;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.dp.event.DpSearchResultsEvent;
import org.parre.client.dp.event.DpUpdatedEvent;
import org.parre.client.dp.event.EditDpEvent;
import org.parre.client.dp.event.ManageDpNavEvent;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.NaturalThreatNavEvent;
import org.parre.client.nav.event.RiskResilienceNavEvent;
import org.parre.client.threat.event.ThreatSearchEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.Presenter;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AmountDTO;
import org.parre.shared.DpAnalysisDTO;
import org.parre.shared.DpDTO;
import org.parre.shared.HasFinancialTotal;
import org.parre.shared.ThreatDTO;
import org.parre.shared.types.AssetThreatAnalysisType;
import org.parre.shared.types.DpType;
import org.parre.shared.types.MoneyUnitType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageDpPresenter extends ManageAnalysisPresenter implements Presenter {
    private DpDTO selectedDTO;
    private ThreatSearchEvent searchEvent = ThreatSearchEvent.create(AssetThreatSearchType.DEPENDENCY_PROXIMITY);
    private BusyHandler busyHandler;
    private DpAnalysisDTO currentAnalysis;


    /**
     * The Interface Display.
     */
    public static interface Display extends AnalysisView {
        SingleSelectionModel<DpDTO> getSelectionModel();

        ActionSource<DpDTO> getSelectionSource();

        HasClickHandlers getSearchButton();

        HasClickHandlers getClearButton();

        void setData(List<DpDTO> dtos);

        void refreshSearchResultsView();

        HasCloseHandlers<PopupPanel> getEditPopup();

        void showEditPopup();

        void hideEditAssetPopup();

        List<DpDTO> getData();

        void setSearchResultsViewDisabled(boolean value);

        HasClickHandlers getAddCatButton();

        void setAddCategoryButtonEnabled(boolean value);

        void showCreateSubCatPopup(DpDTO dpDTO);

        void hideCreateSubCatPopup();

        HasClickHandlers getSaveSubCatButton();

        DpDTO getValidatedSubCat();

        HasClickHandlers getRemoveCatButton();

        HasClickHandlers getConfirmRemoveCatButton();

        void showConfirmRemoveCatPopup();

        void hideConfirmRemoveCatPopup();

        void setRemoveCatButtonEnabled(boolean value);
    }

    private EventBus eventBus;
    private Display display;

    public ManageDpPresenter(Display display) {
        super(display);
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
        init();
    }

    private void bind() {
        bindEvents();

        bindActions();
    }

    private void bindActions() {
        display.getAddCatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                DpDTO subCat = new DpDTO();
                subCat.setParentId(selectedDTO.getId());
                display.showCreateSubCatPopup(subCat);
            }
        });
        display.getSaveSubCatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                DpDTO validatedSubCat = display.getValidatedSubCat();
                if (validatedSubCat != null) {
                    addSubCategory(validatedSubCat);
                }
            }
        });
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearAssetSearchEvent.create());
                display.setData(Collections.<DpDTO>emptyList());
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(searchEvent);
            }
        });
        display.getSelectionSource().setCommand(new ActionSource.ActionCommand<DpDTO>() {
            public void execute(DpDTO dto) {
                if (!dto.getType().isMainWithSub())
                edit(dto);
            }
        });
        display.getEditPopup().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                display.setSearchResultsViewDisabled(false);
            }
        });

        display.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<DpDTO> selectionModel = (SingleSelectionModel<DpDTO>) event.getSource();
                selectedDTO = selectionModel.getSelectedObject();
                selected(selectedDTO);
            }
        });
        display.getRemoveCatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.showConfirmRemoveCatPopup();
            }
        });
        display.getConfirmRemoveCatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideConfirmRemoveCatPopup();
                removeSelectedCat();
            }
        });
        display.getPreviousButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(NaturalThreatNavEvent.create());
            }
        });
        display.getNextButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(RiskResilienceNavEvent.create());
            }
        });
    }

    private void bindEvents() {
        eventBus.addHandler(ManageDpNavEvent.TYPE, new ManageDpNavEvent.Handler() {
            public void onEvent(ManageDpNavEvent event) {
                getCurrentAnalysis();
            }
        });
        eventBus.addHandler(DpUpdatedEvent.TYPE, new DpUpdatedEvent.Handler() {
            public void onEvent(DpUpdatedEvent event) {
                display.hideEditAssetPopup();
                DpDTO updated = event.getDpDTO();
                List<DpDTO> currentResults = display.getData();
                int index = currentResults.indexOf(updated);
                currentResults.set(index, updated);
                updateFinancialTotal(updated);
                updateTotalRisk(updated, currentResults);
                display.refreshSearchResultsView();

            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                display.setData(Collections.<DpDTO>emptyList());
            }
        });
        eventBus.addHandler(DpSearchResultsEvent.TYPE, new DpSearchResultsEvent.Handler() {
            public void onEvent(DpSearchResultsEvent event) {
                List<DpDTO> dpList = arrange(event.getResults());
                updateFinancialTotal(dpList);
                updateTotalRisk(dpList);
                display.setData(dpList);
                display.getSelectionModel().setSelected(null, true);
                display.refreshView();
            }
        });
    }

    private void edit(DpDTO dto) {
        if (ParreClientUtil.handleAnalysisBaselined()) {
            return;
        }

        eventBus.fireEvent(EditDpEvent.create(dto));
        display.showEditPopup();
    }

    private void updateFinancialTotal(HasFinancialTotal updated) {
        if (ClientSingleton.getCurrentParreAnalysis().getUseStatValues()) {
            ClientCalculationUtil.updateFinancialTotal(updated);
        }
    }

    private void updateFinancialTotal(List<DpDTO> updated) {
        if (ClientSingleton.getCurrentParreAnalysis().getUseStatValues()) {
            ClientCalculationUtil.updateFinancialTotal(updated);
        }
    }

    private void updateTotalRisk(DpDTO updated, List<DpDTO> currentResults) {
        AmountDTO totalRiskDTO = calculateTotalRisk(updated);
        updated.setTotalRiskDTO(totalRiskDTO);
        if (updated.getType().isSub()) {
            updateParentTotalRisk(updated, currentResults);
        }
    }

    private void updateParentTotalRisk(DpDTO updated, List<DpDTO> currentResults) {
        for (DpDTO currentResult : currentResults) {
            if (currentResult.isParentOf(updated)) {
                AmountDTO amountDTO = calculateSubCatTotalFor(currentResult, currentResults);
                currentResult.setTotalRiskDTO(amountDTO);
                break;
            }
        }
    }

    private AmountDTO calculateTotalRisk(DpDTO updated) {
        Boolean useStatValues = ClientSingleton.getCurrentParreAnalysis().getUseStatValues();
        AmountDTO financialValue = useStatValues ? updated.getFinancialTotalDTO() :
                updated.getFinancialImpactDTO();
        BigDecimal finQuantity = financialValue.getQuantity();
        BigDecimal riskQ = finQuantity.multiply(updated.getLot()).multiply(updated.getVulnerability());
        return new AmountDTO(riskQ, financialValue.getUnit());
    }

    private void updateTotalRisk(List<DpDTO> dpDTOs) {
        for (DpDTO dpDTO : dpDTOs) {
            AmountDTO totalRiskDTO = calculateTotalRisk(dpDTO);
            dpDTO.setTotalRiskDTO(totalRiskDTO);
        }
        for (DpDTO dpDTO : dpDTOs) {
            if (dpDTO.getType().isMainWithSub()) {
                AmountDTO totalRisk = calculateSubCatTotalFor(dpDTO, dpDTOs);
                dpDTO.setTotalRiskDTO(totalRisk);
            }
        }
    }

    private AmountDTO calculateSubCatTotalFor(DpDTO dpDTO, List<DpDTO> dpDTOs) {
        BigDecimal total = BigDecimal.ZERO;
        MoneyUnitType unit = MoneyUnitType.DOLLAR;
        //int numOfSubs = 0;
        for (DpDTO dto : dpDTOs) {
            if (dpDTO.isParentOf(dto)) {
                AmountDTO totalRiskDTO = dto.getTotalRiskDTO();
                total = total.add(totalRiskDTO.getQuantity());
                unit = totalRiskDTO.getUnit();
                //numOfSubs++;
            }
        }
        //BigDecimal temp = total.divide(BigDecimal.valueOf(numOfSubs));
        return new AmountDTO(total, unit);
    }

    private void removeSelectedCat() {
        final DpDTO catDTO = selectedDTO;
        ExecutableAsyncCallback<Void> exec = 
                new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
                    public void execute(AsyncCallback<Void> callback) {
                        ServiceFactory.getInstance().getDpService().removeCategory(catDTO.getId(), callback);
                    }
        
                    @Override
                    public void handleResult(Void results) {
                        categoryRemoved(catDTO);
                    }
                });
        exec.makeCall();
        
    }

    private void categoryRemoved(DpDTO catDTO) {
        List<DpDTO> dpList = display.getData();
        dpList.remove(catDTO);
        DpDTO parentDTO = null;
        for (DpDTO dpDTO : dpList) {
            if (dpDTO.isParentOf(catDTO)) {
                parentDTO = dpDTO;
                break;
            }
        }
        boolean hasSubCats = false;
        for (DpDTO dpDTO : dpList) {
            if (parentDTO.isParentOf(dpDTO)) {
                hasSubCats = true;
                break;
            }
        }
        if (!hasSubCats) {
            parentDTO.setType(DpType.MAIN);
            updateTotalRisk(parentDTO, dpList);
        }
        else {
            updateParentTotalRisk(catDTO, dpList);
        }
        display.refreshSearchResultsView();
    }

    private List<DpDTO> arrange(List<DpDTO> results) {
        List<DpDTO> arrangedList = new ArrayList<DpDTO>(results.size());
        for (DpDTO dpDTO : results) {
            DpType type = dpDTO.getType();
            if (!type.isSub()) {
                arrangedList.add(dpDTO);
            }
            if (type.isMainWithSub()) {
                for (DpDTO result : results) {
                    if (dpDTO.isParentOf(result)) {
                        arrangedList.add(result);
                    }
                }
            }
        }
        return arrangedList;
    }

    private void addSubCategory(final DpDTO subCat) {
        ExecutableAsyncCallback<DpDTO> exec =
                new ExecutableAsyncCallback<DpDTO>(busyHandler, new BaseAsyncCommand<DpDTO>() {
                    public void execute(AsyncCallback<DpDTO> callback) {
                        ServiceFactory.getInstance().getDpService().addSubCategory(subCat, callback);
                    }

                    @Override
                    public void handleResult(DpDTO results) {
                        updateSubCat(results);
                    }
                });
        exec.makeCall();

    }

    private void updateSubCat(DpDTO subCatDTO) {
        List<DpDTO> searchResults = display.getData();
        DpDTO parentDTO = null;
        for (DpDTO dpDTO : searchResults) {
            if (dpDTO.isParentOf(subCatDTO)) {
                parentDTO = dpDTO;
                parentDTO.setType(DpType.MAIN_WITH_SUB);
                break;
            }
        }
        int index = searchResults.indexOf(parentDTO);
        int insertionPoint = index + 1;
        for (int i = insertionPoint; i < searchResults.size(); i++) {
            DpDTO dpDTO = searchResults.get(i);
            if (parentDTO.getId().equals(dpDTO.getParentId())) {
                insertionPoint++;
            }
            else {
                break;
            }
        }
        searchResults.add(insertionPoint, subCatDTO);
        display.refreshSearchResultsView();
    }


    public void selected(DpDTO dto) {
        boolean isBaseline = ClientSingleton.isCurrentAnalysisBaselined();
        boolean canAddCategories = dto != null && dto.getType() != DpType.SUB &&
                ("D(S)".equals(dto.getName()) || "D(U)".equals(dto.getName()));
        display.setAddCategoryButtonEnabled(!isBaseline && canAddCategories);
        display.setRemoveCatButtonEnabled(!isBaseline && dto != null && dto.getType() == DpType.SUB);
    }

    @Override
    protected AssetThreatAnalysisType getAnalysisType() {
        return AssetThreatAnalysisType.DP;
    }

    @Override
    protected void startAnalysis() {
        final DpAnalysisDTO analysisDTO = new DpAnalysisDTO();
        analysisDTO.setParreAnalysisId(ClientSingleton.getParreAnalysisId());
        ExecutableAsyncCallback<DpAnalysisDTO> exec =
                new ExecutableAsyncCallback<DpAnalysisDTO>(busyHandler, new BaseAsyncCommand<DpAnalysisDTO>() {
                    public void execute(AsyncCallback<DpAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getDpService().
                                startAnalysis(analysisDTO, callback);
                    }

                    @Override
                    public void handleResult(DpAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }

    private void initCurrentAnalysis(DpAnalysisDTO dto) {
        this.currentAnalysis = dto;
        super.initCurrentAnalysis(dto);
        display.setCurrentAnalysis(dto);
        if (dto != null) {
            new DpSearchCommand().execute(new ThreatDTO(), display);
        }
    }

    @Override
    protected void getCurrentAnalysis() {
        ExecutableAsyncCallback<DpAnalysisDTO> exec =
                new ExecutableAsyncCallback<DpAnalysisDTO>(busyHandler, new BaseAsyncCommand<DpAnalysisDTO>() {
                    public void execute(AsyncCallback<DpAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getDpService().
                                getCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(DpAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }

}
