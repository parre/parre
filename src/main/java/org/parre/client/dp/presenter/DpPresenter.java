/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.dp.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.dp.event.ManageDpNavEvent;
import org.parre.client.dp.view.ManageDpView;
import org.parre.client.nav.event.DpNavEvent;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewContainer;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class DpPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class DpPresenter implements Presenter {
    private Integer manageDpViewIndex;

    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

        void showView(int viewIndex);
    }

    public DpPresenter(Display display) {
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
    }

    private void initViewLoaders() {
        ManageDpView managerDpView = ViewFactory.createManagerDpView();
        managerDpView.getPresenter().go();
        manageDpViewIndex = display.addAndShowView(managerDpView.asWidget());
    }

    private void bind() {
        eventBus.addHandler(DpNavEvent.TYPE, new DpNavEvent.Handler() {
            public void onEvent(DpNavEvent event) {
                eventBus.fireEvent(ManageDpNavEvent.create());
            }
        });
    }

}
