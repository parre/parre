/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 4/14/12
 * Time: 3:10 PM
 */
public class ConfirmDialog {

    private static HTML messageText;
    private static ActionSource confirmActionSource;
    private static DialogBox popup;

    static {
        init();
    }

    private static DialogBox init() {
        confirmActionSource = new ActionSource();
        popup = new ClosablePopup("Confirm", true);
        popup.setModal(true);
        VerticalPanel inputPanel = new VerticalPanel();
        inputPanel.setStyleName("inputPanel");
        messageText = new HTML();
        inputPanel.add(messageText);
        Button okButton = ViewUtil.createButton("OK");
        okButton.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                ConfirmDialog.confirmActionSource = confirmActionSource;
                ConfirmDialog.confirmActionSource.fire();
                confirmActionSource.setCommand(null);
                popup.hide();
            }
        });
        inputPanel.add(ViewUtil.layoutCenter(okButton));
        popup.setWidget(inputPanel);
        return popup;
    }

    public static void show(String message, ActionSource.ActionCommand confirmCommand) {
        confirmActionSource.setCommand(confirmCommand);
        messageText.setText(message);
        popup.center();
    }
}
