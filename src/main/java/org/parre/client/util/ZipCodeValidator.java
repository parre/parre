/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.ui.HasValue;

/**
 * User: keithjones
 * Date: 3/14/13
 * Time: 4:11 PM
*/
public class ZipCodeValidator implements Validator {
    @Override
    public boolean validate(Object source) {
        String value = ((HasValue<String>) source).getValue();
        if(value.equals("")) {
            return true;
        }
        boolean valid = value.trim().length() == 5;
        if(valid) {
            try {
                Integer.parseInt(value);
            } catch (NumberFormatException e) {
                valid = false;
            }
        }
        return valid;
    }

    @Override
    public String getMessage() {
        return "Must be a 5 digit number.";
    }
}
