/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.ui.*;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/22/11
 * Time: 12:05 AM
 */
public class DeckComposite extends Composite implements ViewContainer {
    private DeckLayoutPanel deckLayoutPanel;

    public DeckComposite() {
        this.deckLayoutPanel = new DeckLayoutPanel();
    }

    public DeckLayoutPanel getDeckLayoutPanel() {
        return deckLayoutPanel;
    }


    public int addAndShowView(Widget viewWidget) {
        int viewIndex = addView(viewWidget);
        showView(viewIndex);
        return viewIndex;
    }

    public void showView(int viewIndex) {
        deckLayoutPanel.showWidget(viewIndex);
    }

    public void showView(int viewIndex, String viewTitle) {
        deckLayoutPanel.showWidget(viewIndex);
    }
    
    @Override
	public void showView(int viewIndex, String viewTitle, String helpAnchor) {
    	showView(viewIndex, viewTitle);
	}

    public int addView(Widget viewWidget) {
        deckLayoutPanel.add(viewWidget);
        return deckLayoutPanel.getWidgetIndex(viewWidget);
    }

    public void setEnabled(boolean  value) {

    }

    public void refreshView() {

    }
}
