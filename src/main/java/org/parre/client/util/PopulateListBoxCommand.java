/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;

import java.util.List;

import org.parre.shared.LabelValueDTO;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/4/11
 * Time: 3:30 PM
 */
public abstract class PopulateListBoxCommand implements Command {
    private ListBox listBox;
    private BusyHandler busyHandler;
    public PopulateListBoxCommand(ListBox listBox, BusyHandler busyHandler) {
        this.listBox = listBox;
        this.busyHandler = busyHandler;
    }

    protected abstract void getListBoxContents(AsyncCallback<List<LabelValueDTO>> asyncCallback);

    protected String getFirstEntryLabel() {
        return "Select";
    }
    protected String getFirstEntryValue() {
        return "";
    }

    public void execute() {

        new ExecutableAsyncCallback<List<LabelValueDTO>>(busyHandler, new BaseAsyncCommand<List<LabelValueDTO>>() {
            public void execute(AsyncCallback<List<LabelValueDTO>> callback) {
                getListBoxContents(callback);
            }
            @Override
            public void handleResult(List<LabelValueDTO> results) {
                listBox.addItem(getFirstEntryLabel(), getFirstEntryValue());
                for (LabelValueDTO result : results) {
                    listBox.addItem(result.getLabel(), result.getValue());
                };
            }
        }).makeCall();

    }
}
