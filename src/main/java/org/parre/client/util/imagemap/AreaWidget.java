/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.imagemap;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.UIObject;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/3/11
 * Time: 10:23 AM
 */
public class AreaWidget extends UIObject {
    private Command command;

    public AreaWidget() {
        setElement(DOM.createElement("area"));
        DOM.setElementAttribute(getElement(), "href", "#");
    }

    public AreaWidget(String shape, String coords, String alt, Command command) {
        this();
        setShape(shape);
        setCoords(coords);
        setAlt(alt);
        this.command = command;
    }

    Command getCommand() {
        return command;
    }

    void setAlt(String alt) {
        DOM.setElementAttribute(getElement(), "alt", (alt == null) ? "area"
                : alt);
    }

    void setCoords(String coords) {
        DOM.setElementAttribute(getElement(), "coords", (coords == null) ? "" : coords);
    }

    void setShape(String shape) {
        DOM.setElementAttribute(getElement(), "shape", (shape == null) ? "" : shape);
    }
}
