/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.Image;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/11/11
 * Time: 12:21 PM
 */
public class ImageCursorHandler {
    public static final String INACTIVE_ATT_NAME = "inActive";
    private static final MouseOverHandler MOUSE_OVER_HANDLER = new MouseOverHandler() {
        public void onMouseOver(MouseOverEvent event) {
            String inActive = ((Image) event.getSource()).getElement().getAttribute(INACTIVE_ATT_NAME);
            boolean  isInActive = inActive != null && "true".equals(inActive);
            if (!isInActive) {
                GWTUtil.setPointerCursor((Image)event.getSource());
            }
        }
    };

    private static final MouseOutHandler MOUSE_OUT_HANDLER = new MouseOutHandler() {
        public void onMouseOut(MouseOutEvent event) {
            GWTUtil.setNormalCursor((Image)event.getSource());
        }
    };

    public static void add(Image image) {
        image.addMouseOverHandler(MOUSE_OVER_HANDLER);
        image.addMouseOutHandler(MOUSE_OUT_HANDLER);
    }
}
