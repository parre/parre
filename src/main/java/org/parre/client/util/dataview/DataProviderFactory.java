/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util.dataview;

import org.parre.shared.BaseDTO;

import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.ProvidesKey;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/2/11
 * Time: 2:16 AM
 */
public class DataProviderFactory {
    public static <T> DataProvider<T> createDataProvider() {
        ProvidesKey<T> keyProvider = new ProvidesKey<T>() {
            public Object getKey(T item) {
                return ((BaseDTO)item).getId();
            }
        };
        return new DataProvider<T>(keyProvider, new ListDataProvider<T>());
    }
}
