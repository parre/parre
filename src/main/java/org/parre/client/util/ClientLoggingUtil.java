/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.core.client.GWT;

/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 11:58 AM
*/
public class ClientLoggingUtil {
    public static void debug(String message) {
        GWT.log(message);
    }

    public static void debug(String message, Throwable throwable) {
        GWT.log(message, throwable);
    }

    public static void info(String message) {
        GWT.log(message);
    }

    public static void info(String message, Throwable throwable) {
        GWT.log(message, throwable);
    }

    public static void warn(String message) {
        GWT.log(message);
    }

    public static void warn(String message, Throwable throwable) {
        GWT.log(message, throwable);
    }

    public static void error(String message) {
        GWT.log(message);
    }

    public static void error(String message, Throwable throwable) {
        GWT.log(message, throwable);
    }
}
