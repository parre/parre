/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.core.client.GWT;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 10/22/11
 * Time: 12:22 AM
 */
public abstract class AsyncViewLoader extends ErrorHandlingRunAsyncCallback {
    private ViewContainer viewContainer;
    private java.lang.Integer viewIndex;
    private String viewTitle;
    private String helpAnchor;
    private View view;

    public AsyncViewLoader(ViewContainer viewContainer) {
        this(viewContainer, "", "");
    }
    
    protected AsyncViewLoader(ViewContainer viewContainer, String viewTitle) {
    	this.viewContainer = viewContainer;
    	this.viewTitle = viewTitle;
    }

    protected AsyncViewLoader(ViewContainer viewContainer, String viewTitle, String helpAnchor) {
        this.viewContainer = viewContainer;
        this.viewTitle = viewTitle;
        this.helpAnchor = helpAnchor;
    }

    protected abstract View createView();

    public View getView() {
        return view;
    }

    public java.lang.Integer getViewIndex() {
        return viewIndex;
    }

    protected void viewLoaded(java.lang.Integer viewIndex) {

    }

    public void onSuccess() {
        ClientLoggingUtil.info("Loading View");
        view = createView();
        view.getPresenter().go();
        viewIndex = viewContainer.addView(view.asWidget());
        viewContainer.showView(viewIndex, viewTitle, helpAnchor);
        ClientLoggingUtil.info("View Loaded, viewIndex=" + viewIndex);
        viewLoaded(viewIndex);
    }

    public void showView() {
        showView(viewTitle, helpAnchor);
    }

    public void showView(String viewTitle) {
        if (viewIndex != null) {
            viewContainer.showView(viewIndex, viewTitle);
        }
        else {
            GWT.runAsync(this);
        }
    }
    
    public void showView(String viewTitle, String helpAnchor) {
    	if (viewIndex != null) {
    		viewContainer.showView(viewIndex, viewTitle, helpAnchor);
    	}
    	else {
    		GWT.runAsync(this);
    	}
    }
}
