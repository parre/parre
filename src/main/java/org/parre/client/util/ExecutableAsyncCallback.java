/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import org.parre.client.ClientSingleton;
import org.parre.client.event.DisplayLoginEvent;

import com.google.gwt.user.client.rpc.AsyncCallback;


/**
 * The Class ExecutableAsyncCallback.
 *
 * @param <T> the generic type
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ExecutableAsyncCallback<T> implements AsyncCallback<T> {
    private BusyHandler busyHandler;
    private AsyncCommand<T> asyncCommand;

    public ExecutableAsyncCallback() {
    }

    public ExecutableAsyncCallback(BusyHandler busyHandler, AsyncCommand<T> asyncCommand) {
        this.busyHandler = busyHandler;
        this.asyncCommand = asyncCommand;
    }

    public void onFailure(Throwable caught) {
        String message = caught.getMessage();
        message = message != null ? message = message.trim() : "";
        if (busyHandler != null) {
            busyHandler.setBusy(false);
        }
        GWTUtil.setNormalCursor();
        ModalProgressDialog.hide();

       if (message.startsWith("403")) {
            ClientLoggingUtil.info("Message : '" + message + "'");
            ClientSingleton.getEventBus().fireEvent(DisplayLoginEvent.create());
            return;
        }
        if (!asyncCommand.handleError(caught)) {
            MessageDialog.popup("Unexpected error : " + message);
            caught.printStackTrace();
        }
    }

    public void onSuccess(T result) {
        busyHandler.setBusy(false);
        asyncCommand.handleResult(result);
    }

    public void makeCall() {
        busyHandler.setBusy(true);
        asyncCommand.execute(this);
    }
}
