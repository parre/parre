/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.util;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.HasValue;

import java.util.Date;

/**
 * User: keithjones
 * Date: 3/14/13
 * Time: 3:38 PM
*/
public class YearValidator implements Validator{
    @Override
    public boolean validate(Object source) {
        Date date = new Date();
        DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy");
        String year = dtf.format(date);

        String value = ((HasValue<String>) source).getValue();
        if(value.equals("")) {
            return false;
        }
        boolean valid = value.trim().length() == 4;
        if (valid) {
            try {
                valid = Integer.parseInt(value) <= Integer.parseInt(year);
            } catch (NumberFormatException e) {
                valid = false;
            }
        }
        return valid;
    }

    @Override
    public String getMessage() {
        return "Must be a year with 4 digits that isn't in the future.";
    }
}
