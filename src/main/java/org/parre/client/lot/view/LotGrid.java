/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.view;

import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.LotDTO;

/**
 * The Class LotGrid.
 */
public class LotGrid extends DataGridView<LotDTO> {
    public LotGrid(DataProvider<LotDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<LotDTO>> columnList = new ArrayList<DataColumn<LotDTO>>();
		columnList.add(createAssetColumn());
		columnList.add(createThreatNameColumn());
        columnList.add(createAnalysisTypeColumn());
        columnList.add(createProbabilityColumn());
        addColumns(columnList);
	}



    private DataColumn<LotDTO> createAssetColumn() {
        Column<LotDTO, String> column = new Column<LotDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(LotDTO object) {
                return object.getAssetId() + " - " + object.getAssetName();
            }
        };
        column.setSortable(true);

        Comparator<LotDTO> comparator = new Comparator<LotDTO>() {
            public int compare(LotDTO o1, LotDTO o2) {
                return o1.getAssetId().compareTo(o2.getAssetId());
            }
        };
        return new DataColumn<LotDTO>("Asset", column, comparator, 30);
    }



    private DataColumn<LotDTO> createThreatNameColumn() {
    	Column<LotDTO, String> column = new Column<LotDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(LotDTO object) {
				return	object.getThreatName() + " - " + object.getThreatDescription();
			}
		};
		column.setSortable(true);

		Comparator<LotDTO> comparator = new Comparator<LotDTO>() {
			public int compare(LotDTO o1, LotDTO o2) {
				return o1.getThreatName().compareTo(o2.getThreatName());
			}
		};
		return new DataColumn<LotDTO>("Threat", column, comparator, 30);
	}



	private DataColumn<LotDTO> createAnalysisTypeColumn() {
		Column<LotDTO, String> column = new Column<LotDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(LotDTO object) {
				return	object.getLotAnalysisType().getValue();
			}
		};
		column.setSortable(true);

		Comparator<LotDTO> comparator = new Comparator<LotDTO>() {
			public int compare(LotDTO o1, LotDTO o2) {
				return o1.getLotAnalysisType().compareTo(o2.getLotAnalysisType());
			}
		};
		return new DataColumn<LotDTO>("Analysis Type", column, comparator, 25);
	}


    private DataColumn<LotDTO> createProbabilityColumn() {
    	Column<LotDTO, String> column = new Column<LotDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(LotDTO object) {
    			return ViewUtil.toDisplayValue(object.getLot());
    		}
    	};
    	column.setSortable(true);

    	Comparator<LotDTO> comparator = new Comparator<LotDTO>() {
    		public int compare(LotDTO o1, LotDTO o2) {
    			return o1.getLot().compareTo(o2.getLot());
    		}
    	};
    	return new DataColumn<LotDTO>("Probability", column, comparator, 15);
    }

    private DoubleClickableTextCell<LotDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<LotDTO>(getSelectionModel(), getSelectionSource());
    }


}
