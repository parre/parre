/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.lot.view;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.lot.presenter.EditLotPresenter;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.ViewUtil;
import org.parre.shared.LotDTO;
import org.parre.shared.types.LotAnalysisType;

/**
 * The Class EditLotView.
 */
public class EditLotView extends BaseModelView<LotDTO>  implements EditLotPresenter.Display {
	private List<FieldValidationHandler> requiredFieldHandlers;
	private Widget viewWidget;
    private TextBox assetId;
    private TextBox threatName;
    private TextBox probability;
    private Button saveButton;

	public EditLotView() {
		requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
	}

	private Widget buildView() {
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(5);

        int rowNum = -1;

        assetId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset ID");
        assetId.setWidth("50px");
        assetId.setEnabled(false);

        threatName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Threat Code");
        threatName.setWidth("50px");
        threatName.setEnabled(false);

        probability = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Probability", requiredFieldHandlers);
        makeProbabilityBox(probability);

        saveButton = ViewUtil.createButton("Save");
        panel.add(ViewUtil.layoutCenter(inputTable));
        panel.add(ViewUtil.layoutRight(saveButton));
        panel.setStyleName("inputPanel");
        return panel;
    }

	public boolean validate() {
		return ViewUtil.validate(requiredFieldHandlers);
	}

	public void updateModel() {
        LotDTO model = getModel();
        model.setLot(getProbability(probability));
    }

	public void updateView() {
        LotDTO model = getModel();
        setText(assetId, model.getAssetId());
        setText(threatName, model.getThreatName());
        setProbability(probability, model.getLot());
        if(model.getLotAnalysisType().equals(LotAnalysisType.CONDITIONAL_RISK)) {
            probability.setEnabled(false);
        }
        else {
            probability.setEnabled(true);
        }
	}

	public Widget asWidget() {
		return viewWidget;
	}

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    @Override
    public void setProbabilty(String value) {
        probability.setText(value);
    }

    @Override
    public void setProbabilityEnabled(Boolean value) {
        probability.setEnabled(value);
    }
}