/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.login.presenter;

import org.parre.client.util.Presenter;
import org.parre.client.util.View;

import com.google.web.bindery.event.shared.EventBus;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/10/11
 * Time: 5:07 AM
 */
public class LoginPresenter implements Presenter {
    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View {
    }

    public LoginPresenter(EventBus eventBus, Display display) {
        this.eventBus = eventBus;
        this.display = display;
    }

    public void go() {
        bind();

    }

    private void bind() {
    }
}
