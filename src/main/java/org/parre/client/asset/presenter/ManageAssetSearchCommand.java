/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.AssetSearchResultsEvent;
import org.parre.client.common.presenter.AssetSearchCommand;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.util.*;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;

/**
 * The Class ManageAssetSearchCommand.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/27/11
 */
public class ManageAssetSearchCommand implements AssetSearchCommand {

    public void execute(final AssetSearchDTO dto, final View view) {
        ExecutableAsyncCallback<List<AssetDTO>> exec = new ExecutableAsyncCallback<List<AssetDTO>>(new BusyHandler(view), new BaseAsyncCommand<List<AssetDTO>>() {
            public void execute(AsyncCallback<List<AssetDTO>> callback) {
                ServiceFactory.getInstance().getAssetService().getAssetSearchResults(dto, callback);
            }

            @Override
            public void handleResult(List<AssetDTO> results) {
                ClientSingleton.getEventBus().fireEvent(AssetSearchResultsEvent.create(results));
            }
        });
        exec.makeCall();
    }
}
