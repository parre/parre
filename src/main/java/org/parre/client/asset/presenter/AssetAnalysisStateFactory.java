/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.presenter;

import org.parre.client.common.presenter.AnalysisStateFactory;
import org.parre.client.util.View;

/**
 * A factory for creating AssetAnalysisState objects.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class AssetAnalysisStateFactory implements AnalysisStateFactory<AssetAnalysisState> {
    private static AnalysisStateFactory<AssetAnalysisState> instance =
            new AssetAnalysisStateFactory();

    public static AnalysisStateFactory<AssetAnalysisState> getInstance() {
        return instance;
    }

    public AssetAnalysisState createActiveAnalysisState(View view) {
        return new ActiveAssetAnalysisState((ManageAssetPresenter.Display) view);
    }

    public AssetAnalysisState createLockedAnalysisState(View view) {
        return new LockedAssetAnalysisState((ManageAssetPresenter.Display) view);
    }
}
