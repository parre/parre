/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.presenter.QuickEditAssetPresenter;
import org.parre.client.common.ClientNoteUtil;
import org.parre.client.common.view.NoteRowView;
import org.parre.client.util.BaseModelView;
import org.parre.client.util.FieldValidationHandler;
import org.parre.client.util.NoteDialog;
import org.parre.client.util.ViewUtil;
import org.parre.shared.AssetDTO;
import org.parre.shared.NoteDTO;

import static org.parre.client.common.ClientNoteUtil.setNotesPanel;

/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 9/5/11
 * Time: 7:55 AM
 */
public class QuickEditAssetView extends BaseModelView<AssetDTO> implements QuickEditAssetPresenter.Display {
    private List<FieldValidationHandler> requiredFieldHandlers;
    private ListBox humanPl;
    private ListBox financialPl;
    private ListBox economicPl;
    private TextBox assetName;
    private TextBox assetId;
    private ListBox critical;
    private Button saveButton;
    private Widget viewWidget;
    private List<NoteRowView> noteViews = new ArrayList<NoteRowView>();
    private VerticalPanel notesPanel;
    private FlexTable notesTable = new FlexTable();

    public QuickEditAssetView() {
        requiredFieldHandlers = new ArrayList<FieldValidationHandler>();
        viewWidget = buildView();
    }

    private Widget buildView() {
        notesPanel = new VerticalPanel();
        VerticalPanel panel = new VerticalPanel();
        FlexTable inputTable = new FlexTable();
        inputTable.setCellSpacing(1);

        int rowNum = -1;

        assetId = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "ID");
        assetId.setWidth("100px");
        assetId.setEnabled(false);
        assetName = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Asset Name", requiredFieldHandlers);
        assetName.setWidth("200px");
        assetName.setEnabled(false);
        critical = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Is Critical?", requiredFieldHandlers);
        critical.addItem("Select", "");
        critical.addItem("Yes", "true");
        critical.addItem("No", "false");

        humanPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Human PL", requiredFieldHandlers);
        financialPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Financial PL", requiredFieldHandlers);
        economicPl = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Economic PL", requiredFieldHandlers);
        populateListBox(humanPl);
        populateListBox(financialPl);
        populateListBox(economicPl);

        saveButton = ViewUtil.createButton("Save");

        panel.add(ViewUtil.layoutCenter(inputTable));

        ViewUtil.addLabelFormEntry(notesTable, 0, "Notes");
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.setStyleName("inputPanel");

        panel.add(ViewUtil.layoutCenter(notesPanel));
        panel.add(ViewUtil.layoutRight(saveButton));

        panel.setStyleName("inputPanel");
        return panel;
    }

    private void populateListBox(ListBox listBox) {
        listBox.addItem("Select", "");
        for (int i = 1; i <= 5; i++) {
            listBox.addItem(String.valueOf(i), String.valueOf(i));
        }
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public void updateView() {
        AssetDTO model = getModel();
        assetId.setText(model.getAssetId());
        assetName.setText(model.getName());
        ViewUtil.setSelectedValue(critical, Boolean.toString(model.getCritical()));
        ViewUtil.setSelectedValue(humanPl, String.valueOf(model.getHumanPl()));
        ViewUtil.setSelectedValue(financialPl, String.valueOf(model.getFinancialPl()));
        ViewUtil.setSelectedValue(economicPl, String.valueOf(model.getEconomicPl()));
        notesPanel.clear();
        noteViews = new ArrayList<NoteRowView>();
        for(NoteDTO dto : model.getNoteDTOs()) {
            noteViews.add(new NoteRowView(dto));
        }
        notesPanel.add(ViewUtil.layoutCenter(notesTable));
        notesPanel.add(setNotesPanel(noteViews));
    }

    public void updateModel() {
        AssetDTO model = getModel();
        model.setName(ViewUtil.getText(assetName));
        model.setCritical(Boolean.valueOf(ViewUtil.getSelectedValue(critical)));
        model.setHumanPl(Integer.valueOf(ViewUtil.getSelectedValue(humanPl)));
        model.setFinancialPl(Integer.valueOf(ViewUtil.getSelectedValue(financialPl)));
        model.setEconomicPl(Integer.valueOf(ViewUtil.getSelectedValue(economicPl)));
        model.setNoteDTOs(ClientNoteUtil.getNoteDTOs(noteViews));
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public boolean validate() {
        return ViewUtil.validate(requiredFieldHandlers);
    }
    
   
}
