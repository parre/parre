/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.asset.view;

import java.math.BigDecimal;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.*;
import org.parre.shared.AmountDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.types.PhysicalAssetType;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

/**
 * The Class EditPhysicalResourceView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/7/11
 */
public class EditPhysicalResourceView extends EditAssetView {
    private TextBox yearBuilt;
    private TextBox zipCode;
    private TextBox replacementCost;
    private ListBox damageFactor;
    private TextBox latitude;
    private TextBox longitude;
    private Button getGISCoordinatesButton;
    private TextBox systemTypeText;
    private ListBox systemTypeList;

    public EditPhysicalResourceView() {
    	super();
    }

    @Override
    protected int addInputFields(FlexTable inputTable, int rowNum, List<FieldValidationHandler> fieldHandlers) {
        yearBuilt = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Year Built", fieldHandlers, new YearValidator());
        systemTypeText = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "System Type");
        systemTypeList = ViewUtil.addListBoxFormEntry(inputTable, 4,"System Type", 0, 2);
        zipCode = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Zip Code", fieldHandlers, new ZipCodeValidator());
        latitude = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Latitude", fieldHandlers, new NumberValidator());
        longitude = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Longitude", fieldHandlers, new NumberValidator());
        replacementCost = ViewUtil.addTextBoxFormEntry(inputTable, ++rowNum, "Replacement Cost($)", fieldHandlers, new NumericRangeValidator(0, 99999999999999d));
        damageFactor = ViewUtil.addListBoxFormEntry(inputTable, ++rowNum, "Asset Type");
        getGISCoordinatesButton = ViewUtil.createButton("Get GIS Coordinates");
        getGISCoordinatesButton.setSize("150px", "30px");
        inputTable.setWidget(5,2, getGISCoordinatesButton);
        return rowNum;
    }

    @Override
    public void updateView() {
        super.updateView();
        PhysicalResourceDTO model = (PhysicalResourceDTO) getModel();
        yearBuilt.setText(model.getYearBuilt() != null ? String.valueOf(model.getYearBuilt().intValue()) : "");
        
        ViewUtil.setText(systemTypeText, model.getSystemType());
        
        ViewUtil.setBigDecimal(replacementCost, model.getReplacementCostDTO().getQuantity());
        //ViewUtil.setSelectedValue(damageFactor, model.getDamageFactor().stripTrailingZeros().toString());
        ViewUtil.setSelectedValue(damageFactor, model.getPhysicalAssetType() != null ? model.getPhysicalAssetType().getDisplayName() : "0");
       
        if (model.isNew() && ClientSingleton.getCurrentParreAnalysis().getUseDefaultLocation()) {
        	ViewUtil.setText(zipCode, ClientSingleton.getCurrentParreAnalysis().getDefaultPostalCode());
        	ViewUtil.setBigDecimal(latitude, ClientSingleton.getCurrentParreAnalysis().getDefaultLatitude());
        	ViewUtil.setBigDecimal(longitude, ClientSingleton.getCurrentParreAnalysis().getDefaultLongitude());
        } else {
	        ViewUtil.setText(zipCode, model.getPostalCode());
            ViewUtil.setBigDecimal(latitude, model.getLatitude());
            ViewUtil.setBigDecimal(longitude, model.getLongitude());
        }
    }
    
    @Override
    public void updateModel() {
        super.updateModel();
        PhysicalResourceDTO model = (PhysicalResourceDTO) getModel();
        model.setYearBuilt(ViewUtil.getQuantity(yearBuilt));
        String systemTypeValue = ViewUtil.getText(systemTypeText);
        if(systemTypeValue != null && !systemTypeValue.equals("")){
        	model.setSystemType(systemTypeValue);
        } else {
        	model.setSystemType("Physical Asset");
        }
        
        model.setPostalCode(ViewUtil.getText(zipCode));
        model.setReplacementCostDTO(new AmountDTO(getBigDecimal(replacementCost)));
        
        String selectedPhysicalAssetType = damageFactor.getValue(damageFactor.getSelectedIndex());
        if ("0".equalsIgnoreCase(selectedPhysicalAssetType)) {
        	model.setDamageFactor(BigDecimal.ZERO);
        	model.setPhysicalAssetType(null);
        } else {
        	PhysicalAssetType physicalAssetType = PhysicalAssetType.lookupByDisplayName(selectedPhysicalAssetType);
        	BigDecimal damageFactorValue = new BigDecimal(physicalAssetType.getDamageFactor());
        	model.setDamageFactor(damageFactorValue.stripTrailingZeros());
        	model.setPhysicalAssetType(physicalAssetType);
        }
        
        try {
            model.setLatitude(getBigDecimal(latitude));
            model.setLongitude(getBigDecimal(longitude));
        } catch(NumberFormatException e) {
            ClientLoggingUtil.error("No values input for latitude or longitude.");
            model.setLatitude(null);
            model.setLongitude(null);
        }

    }
    @Override
    public ListBox getDamageFactor() {
        return damageFactor;
    }
    
    @Override
    public ListBox getSystemTypeList() {
        return systemTypeList;
    }
    
    @Override
    public HasClickHandlers getGISCoordinatesButton() {
        return getGISCoordinatesButton;
    }
    @Override
    public HasText getZipCode(){
        return zipCode;
    }
    @Override
    public void setGISCoordinates(String latitude, String longitude) {
        this.latitude.setText(latitude);
        this.longitude.setText(longitude);
    }
    
    @Override
   	public void setSystemTypeList(List<String> systemTypes) {
    	this.systemTypeList.clear();
        this.systemTypeList.addItem("Select", "0");
        for (String systemType : systemTypes) {
        	this.systemTypeList.addItem(systemType, systemType);
        }
        
    }
    
    @Override
	public void setSystemTypeText(String systemType) {
		this.systemTypeText.setText(systemType);
	}
	

    @Override
    public TextBox getReplacementCost() {
        return replacementCost;
    }

    @Override
    public void setReplacementCost(String replacementCost) {
        this.replacementCost.setText(replacementCost);
    }
}
