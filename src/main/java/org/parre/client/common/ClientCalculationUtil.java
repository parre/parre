/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common;


import java.math.BigDecimal;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.shared.*;

/**
 * The Class ClientCalculationUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/13/12
 */
public class ClientCalculationUtil {

    public static AmountDTO calculateFinancialTotal(Integer fatalities, Integer seriousInjuries, AmountDTO financialImpactDTO,
                                                    StatValueQuantities statValueQuantities) {
        BigDecimal financialImpactQuantity = financialImpactDTO.getQuantity();
        BigDecimal total = SharedCalculationUtil.calculateFinancialTotal(fatalities, seriousInjuries, financialImpactQuantity, statValueQuantities);

        return new AmountDTO(total, financialImpactDTO.getUnit());
    }

    public static void updateFinancialTotal(List<? extends HasFinancialTotal> dtos) {

        for (HasFinancialTotal dto : dtos) {
            updateFinancialTotal(dto);
        }
    }

    public static void updateFinancialTotal(HasFinancialTotal dto) {
        Integer fatalities = dto.getFatalities();
        Integer seriousInjuries = dto.getSeriousInjuries();
        AmountDTO financialImpactDTO = dto.getFinancialImpactDTO();
        StatValuesDTO statValuesDTO = ClientSingleton.getAppInitData().getParreAnalysisDTO().getStatValuesDTO();
        AmountDTO totalAmount = calculateFinancialTotal(fatalities, seriousInjuries, financialImpactDTO, statValuesDTO);
        dto.setFinancialTotalDTO(totalAmount);
    }
}
