/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import org.apache.log4j.Logger;
import org.parre.client.common.presenter.DisplayTogglePresenter;
import org.parre.client.util.BaseView;
import org.parre.client.util.ViewUtil;

/**
 * The Class DisplayToggleView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class DisplayToggleView extends BaseView implements DisplayTogglePresenter.Display {

    private Label headerLabel;

    /**
     * The Interface DisplayToggleCommand.
     */
    public interface DisplayToggleCommand {
        void toggleDisplay(boolean show);
    }
    private Widget viewWidget;
    private String label;
    private Image collapseButton;
    private Image expandButton;
    private DisplayToggleCommand displayToggleCommand;

    public DisplayToggleView() {
        viewWidget = createView();
    }

    private Widget createView() {
        headerLabel = new Label(label);
        headerLabel.setStyleName("inputPanelHeader");
        collapseButton = ViewUtil.createImageButton("toggle-collapse.png", "Hide", false);
        expandButton = ViewUtil.createImageButton("toggle-expand.png", "Show", false);
        expandButton.setVisible(false);
        FlexTable headerPanel = new FlexTable();
        headerPanel.setWidth("99%");
        headerPanel.getElement().getStyle().setBackgroundColor("#abd1fa");
        headerPanel.setWidget(0, 0, headerLabel);
        headerPanel.setWidget(0, 1, ViewUtil.horizontalPanel(collapseButton, expandButton));
        headerPanel.getFlexCellFormatter().setWidth(0, 1, "1%");
        return headerPanel;
    }

    public HasClickHandlers getCollapseButton() {
        return collapseButton;
    }

    public HasClickHandlers getExpandButton() {
        return expandButton;
    }

    public void setDisplayVisible(boolean value) {
        collapseButton.setVisible(value);
        expandButton.setVisible(!value);
        if (displayToggleCommand != null) {
            displayToggleCommand.toggleDisplay(value);
        }
    }

    public void setDisplayToggleCommand(DisplayToggleCommand displayToggleCommand) {
        this.displayToggleCommand = displayToggleCommand;
    }

    public void setText(String text) {
        headerLabel.setText(text);
    }

    public Widget asWidget() {
        return viewWidget;
    }
}
