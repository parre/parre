/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;
import java.util.List;

import org.parre.client.util.*;
import org.parre.shared.NameDescriptionDTO;

/**
 * The Class NameDescriptionView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/8/11
 */
public class NameDescriptionView extends BaseModelView<NameDescriptionDTO> implements ModelView<NameDescriptionDTO>{
    private List<FieldValidationHandler> fieldValidationHandlers = new ArrayList<FieldValidationHandler>();
    private Label header;
    private TextBox name;
    private TextBox description;
    private Button saveButton;
    private Widget viewWidget;
    private DialogBox popup;

    public NameDescriptionView() {
        viewWidget = createView();
        initPopup(viewWidget);
    }

    private FlexTable createView() {
        FlexTable inputTable = new FlexTable();
        inputTable.setWidth("100%");
        inputTable.setStyleName("inputPanel");
        header = new Label("");
        header.setStyleName("inputPanelHeader");
        int currentRow = -1;
        inputTable.setWidget(++currentRow, 0, header);
        alignCenter(currentRow, inputTable);
        name = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Name", fieldValidationHandlers);
        description = ViewUtil.addTextBoxFormEntry(inputTable, ++currentRow, "Description");
        saveButton = ViewUtil.createButton("Save");
        inputTable.setWidget(++currentRow, 0, saveButton);
        alignCenter(currentRow, inputTable);
        return inputTable;
    }

    private void initPopup(Widget widget) {
        popup = new ClosablePopup("Name Description", true);
        popup.setModal(true);
        popup.setWidget(widget);
    }

    private void alignCenter(int currentRow, FlexTable inputTable) {
        inputTable.getFlexCellFormatter().setColSpan(currentRow, 0, 2);
        inputTable.getFlexCellFormatter().setHorizontalAlignment(currentRow, 0, HasHorizontalAlignment.ALIGN_CENTER);
    }

    public HasClickHandlers getSaveButton() {
        return saveButton;
    }

    public void setHeader(String headerText) {
        this.header.setText(headerText);
    }

    public void updateModel() {
        NameDescriptionDTO model = getModel();
        model.setName(ViewUtil.getText(name));
        model.setDescription(ViewUtil.getText(description));
    }

    public void updateView() {
        NameDescriptionDTO model = getModel();
        ViewUtil.setText(name, model.getName());
        ViewUtil.setText(description, model.getDescription());
    }

    public boolean validate() {
        return ViewUtil.validate(fieldValidationHandlers);
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public DialogBox getPopup() {
        return popup;
    }
}
