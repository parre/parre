/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.view;

import org.parre.client.util.ModelView;
import org.parre.client.util.View;
import org.parre.shared.NameDescriptionDTO;

import com.google.gwt.event.dom.client.HasClickHandlers;

/**
 * The Interface AnalysisView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 12/8/11
 */
public interface AnalysisView extends View {
    void setCurrentAnalysis(NameDescriptionDTO currentAnalysis);

    HasClickHandlers getStartAnalysisButton();

    HasClickHandlers getSaveAnalysisButton();

    ModelView<NameDescriptionDTO> getStartAnalysisView();

    void showStartAnalysisPopup();

    void hideStartAnalysisPopup();

    HasClickHandlers getConfirmStartAnalysisButton();

    void showConfirmStartAnalysisPopup(Integer count);

    void hideConfirmStartAnalysisPopup();

    HasClickHandlers getPreviousButton();

    HasClickHandlers getNextButton();
}
