/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.common.presenter;

import org.parre.client.util.ModelView;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;

/**
 * The Class DisplayTogglePresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class DisplayTogglePresenter implements Presenter {
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View {
        HasClickHandlers getCollapseButton();

        HasClickHandlers getExpandButton();

        void setDisplayVisible(boolean value);
    }

    public DisplayTogglePresenter(Display display) {
        this.display = display;
    }

    public void go() {
        bind();
    }

    private void bind() {
        display.getCollapseButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.setDisplayVisible(false);
            }
        });
        display.getExpandButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.setDisplayVisible(true);
            }
        });

    }
}
