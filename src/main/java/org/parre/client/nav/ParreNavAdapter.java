/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.nav;

import org.apache.log4j.Logger;
import org.parre.client.ClientSingleton;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.nav.NavAdapter;
import org.parre.client.util.nav.NavEvent;
import org.parre.client.util.nav.NavEventHandler;

/**
 * The Class ParreNavAdapter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/16/12
 */
public class ParreNavAdapter implements NavAdapter {
    private static ParreNavAdapter instance = new ParreNavAdapter();
    private String startView;
    private String entryPointHash;
    private NavEvent entryPointNavEvent;
    private NavEventHandler navEventHandler;

    public static ParreNavAdapter getInstance() {
        return instance;
    }

    public boolean hasEntryPointHash() {
        return entryPointHash != null && entryPointHash.length() > 0;
    }

    public String getEntryPointHashValue() {
        return !hasEntryPointHash() ? "" : entryPointHash.substring(1);
    }

    public void onNavEvent(NavEvent navEvent) {
        if (navEventHandler != null) {
            navEventHandler.onNavEvent(navEvent);
        }
        ClientSingleton.getEventBus().fireEvent(navEvent);
    }

    public void handleEntryPointNavEvent(NavEvent navEvent) {
        ClientLoggingUtil.info("EntryPoint Nav Event : " + navEvent.getClass());
        this.entryPointNavEvent = navEvent;
        onNavEvent(navEvent);
    }

    public String getEntryPointHash() {
        return entryPointHash;
    }

    public void setEntryPointHash(String entryPointHash) {
        this.entryPointHash = entryPointHash;
    }

    public NavEvent getEntryPointNavEvent() {
        return entryPointNavEvent;
    }

    public boolean enteredVia(Class<? extends NavEvent> navEventClass) {
        Class<? extends NavEvent> aClass = entryPointNavEvent.getClass();
        return entryPointNavEvent != null && aClass.getName().equals(navEventClass.getName());
    }

    public String getStartView() {
        return startView;
    }

    public void setStartView(String startView) {
        this.startView = startView;
    }

    public NavEventHandler getNavEventHandler() {
        return navEventHandler;
    }

    public void setNavEventHandler(NavEventHandler navEventHandler) {
        this.navEventHandler = navEventHandler;
    }


}
