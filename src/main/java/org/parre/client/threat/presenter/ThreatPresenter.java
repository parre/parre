/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.presenter;

import org.parre.client.ClientSingleton;
import org.parre.client.ViewFactory;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.messaging.ThreatServiceAsync;
import org.parre.client.nav.event.ThreatNavEvent;
import org.parre.client.threat.event.*;
import org.parre.client.threat.view.EditThreatView;
import org.parre.client.threat.view.ManageThreatView;
import org.parre.client.util.*;

import com.google.gwt.event.shared.EventBus;

/**
 * The Class ThreatPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class ThreatPresenter implements Presenter {
    private Integer manageThreatViewIndex;
    private Integer editThreatViewIndex;
    private ThreatServiceAsync service;
    private EventBus eventBus;
    private Display display;

    /**
     * The Interface Display.
     */
    public interface Display extends View, ViewContainer {

    }

    public ThreatPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getThreatService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
    }

    public void go() {
        initViewLoaders();
        bind();
        display.showView(manageThreatViewIndex);
    }

    private void initViewLoaders() {
        ManageThreatView manageThreatView = ViewFactory.createManageThreatView();
        manageThreatView.getPresenter().go();
        manageThreatViewIndex = display.addView(manageThreatView.asWidget());

        EditThreatView editThreatView = ViewFactory.createEditThreatView();
        editThreatView.getPresenter().go();
        editThreatViewIndex = display.addView(editThreatView.asWidget());
    }

    private void bind() {
        eventBus.addHandler(ThreatNavEvent.TYPE, new ThreatNavEvent.Handler() {
            public void onEvent(ThreatNavEvent event) {
                display.showView(manageThreatViewIndex);
                eventBus.fireEvent(ManageThreatNavEvent.create());
            }
        });
        eventBus.addHandler(ManageThreatNavEvent.TYPE, new ManageThreatNavEvent.Handler() {
            public void onEvent(ManageThreatNavEvent event) {
                display.showView(manageThreatViewIndex);
            }
        });

        eventBus.addHandler(CreateThreatEvent.TYPE, new CreateThreatEvent.Handler() {
            public void onEvent(CreateThreatEvent event) {
                display.showView(editThreatViewIndex);
            }
        });

        eventBus.addHandler(EditThreatEvent.TYPE, new EditThreatEvent.Handler() {
            public void onEvent(EditThreatEvent event) {
                display.showView(editThreatViewIndex);
            }
        });

        eventBus.addHandler(EditThreatCancelEvent.TYPE, new EditThreatCancelEvent.Handler() {
            public void onEvent(EditThreatCancelEvent event) {
                display.showView(manageThreatViewIndex);
            }
        });
        eventBus.addHandler(ThreatUpdatedEvent.TYPE, new ThreatUpdatedEvent.Handler() {
            public void onEvent(ThreatUpdatedEvent event) {
                display.showView(manageThreatViewIndex);
            }
        });

    }


}
