/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasEnabled;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.parre.client.ClientSingleton;
import org.parre.client.asset.event.AssetSearchResultsEvent;
import org.parre.client.asset.presenter.ManageAssetSearchCommand;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.AnalysisStateHost;
import org.parre.client.common.presenter.AnalysisStateInitializer;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.messaging.ThreatServiceAsync;
import org.parre.client.nav.event.AssetNavEvent;
import org.parre.client.nav.event.ThreatAssetMatrixNavEvent;
import org.parre.client.threat.event.*;
import org.parre.client.util.*;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.ThreatDTO;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageThreatPresenter implements Presenter, AnalysisStateHost<ThreatAnalysisState> {
    private ThreatDTO selectedThreatDTO;
    private ThreatDTO editedThreatDTO;
    private int assetCount;

    public static final Map<Integer, GwtEvent> viewEventMap = new HashMap<Integer, GwtEvent>(2);
    private ThreatServiceAsync service;
    private AnalysisStateInitializer<ThreatAnalysisState> handlerInitializer;

    public void setHandler(ThreatAnalysisState handler) {
        this.handler = handler;
    }

    public View getView() {
        return display;
    }


    /**
     * The Interface Display.
     */
    public static interface Display extends View {
        HasClickHandlers getEditThreatButton();
        SingleSelectionModel<ThreatDTO> getSearchResultsSelectionModel();

        ActionSource<ThreatDTO> getThreatSelectionSource();
        HasClickHandlers getSearchButton();
        HasClickHandlers getClearButton();
        void setSearchResults(List<ThreatDTO> dtos);
        void refreshSearchResultsView();
        HasClickHandlers getCreateThreatButton();
        HasEnabled getEditThreatEnabled();
        ActionSource<ThreatDTO> getActiveStatusChangedSource();

        void showNoteDialog();

        void hideNoteDialog();

        HasClickHandlers getSaveNoteButton();

        HasText getNoteText();

        HasCloseHandlers<PopupPanel> getNoteDialog();

        HasClickHandlers getAssetThreatMatrixButton();

        HasClickHandlers getDeleteThreatButton();

        HasClickHandlers getManageAssetsButton();

        HasEnabled getDeleteThreatEnabled();

        void updateThreat(ThreatDTO threatDTO);

        void removeThreat(ThreatDTO selectedThreatDTO);
    }

    private EventBus eventBus;
    private Display display;
    private ThreatAnalysisState handler;

    public ManageThreatPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getThreatService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.handlerInitializer = new AnalysisStateInitializer<ThreatAnalysisState>(this,
                ThreatAnalysisStateFactory.getInstance());
    }
    public void go() {
        handlerInitializer.go();
        bind();
        getAllThreats();
        getAssetCount();
    }


    private void bind() {
        bindEvents();
        bindActions();
    }

    private void bindEvents() {
        eventBus.addHandler(ManageThreatNavEvent.TYPE, new ManageThreatNavEvent.Handler() {
            public void onEvent(ManageThreatNavEvent event) {
                getAllThreats();
                getAssetCount();
            }
        });
        eventBus.addHandler(ThreatUpdatedEvent.TYPE, new ThreatUpdatedEvent.Handler() {
            public void onEvent(ThreatUpdatedEvent event) {
                display.refreshSearchResultsView();
            }
        });
        eventBus.addHandler(ThreatUpdatedEvent.TYPE, new ThreatUpdatedEvent.Handler() {
            public void onEvent(ThreatUpdatedEvent event) {
                display.updateThreat(event.getThreatDTO());
            }
        });

        eventBus.addHandler(ThreatSearchResultsEvent.TYPE, new ThreatSearchResultsEvent.Handler() {
            public void onEvent(ThreatSearchResultsEvent event) {
                //display.setSearchCriteriaVisible(false);
                display.setSearchResults(event.getResults());
            }
        });
    }

    private void bindActions() {
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearThreatSearchEvent.create());
                display.setSearchResults(Collections.<ThreatDTO>emptyList());
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ThreatSearchEvent.create(AssetThreatSearchType.THREAT));
            }
        });
        display.getThreatSelectionSource().setCommand(new ActionSource.ActionCommand<ThreatDTO>() {
            public void execute(ThreatDTO dto) {
                //selectedThreatDTO = dto;
                handler.editThreat(selectedThreatDTO);
                //display.getEditThreatEnabled().setEnabled(selectedThreatDTO != null);
            }
        });
        display.getEditThreatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.editThreat(selectedThreatDTO);
            }
        });

        display.getDeleteThreatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.deleteThreat(selectedThreatDTO);
            }
        });
        display.getCreateThreatButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                handler.createThreat();
            }
        });
        display.getManageAssetsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                NavManager.performNav(AssetNavEvent.create());
            }
        });
        display.getAssetThreatMatrixButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                NavManager.performNav(ThreatAssetMatrixNavEvent.create());
            }
        });
        display.getSearchResultsSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<ThreatDTO> selectionModel = (SingleSelectionModel<ThreatDTO>) event.getSource();
                selectedThreatDTO = selectionModel.getSelectedObject();
                handler.threatSelected(selectedThreatDTO);
            }
        });
        display.getActiveStatusChangedSource().setCommand(new ActionSource.ActionCommand<ThreatDTO>() {
            public void execute(ThreatDTO threatDTO) {
                editedThreatDTO = threatDTO;
                handler.changeThreatActiveStatus(threatDTO);
            }
        });

        display.getSaveNoteButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideNoteDialog();
                editedThreatDTO.setInactiveReason(display.getNoteText().getText());
                handler.updateThreatActive(editedThreatDTO);
            }
        });

        display.getNoteDialog().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                boolean editCancelled = popupPanelCloseEvent.isAutoClosed();
                ClientLoggingUtil.info("Edit Cancelled : " + editCancelled);
                if (editCancelled) {
                    display.getSearchResultsSelectionModel().setSelected(editedThreatDTO, true);
                    editedThreatDTO.setActive(true);
                    display.refreshSearchResultsView();
                }
            }
        });
    }


    private void getAllThreats() {
        new ManageThreatSearchCommand().execute(new ThreatDTO(), display);
    }

    private int getAssetCount() {
        ExecutableAsyncCallback<List<AssetDTO>> exec = new ExecutableAsyncCallback<List<AssetDTO>>(new BusyHandler(display), new BaseAsyncCommand<List<AssetDTO>>() {
            public void execute(AsyncCallback<List<AssetDTO>> callback) {
                ServiceFactory.getInstance().getAssetService().getAssetSearchResults(new AssetSearchDTO(ClientSingleton.getParreAnalysisId()), callback);
            }

            @Override
            public void handleResult(List<AssetDTO> results) {
                assetCount = results.size();
            }
        });
        exec.makeCall();
        return assetCount;
    }


}
