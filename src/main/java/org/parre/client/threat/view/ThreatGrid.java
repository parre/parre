/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.threat.view;

import com.google.gwt.cell.client.Cell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.util.ActionSource;
import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.ThreatDTO;

/**
 * User: Swarn S. Dhaliwal
 * Date: 7/10/11
 * Time: 5:38 PM
*/
public class ThreatGrid extends DataGridView<ThreatDTO> {
    private static final SafeHtml INPUT_CHECKED = SafeHtmlUtils.fromSafeConstant("<input disabled=\"true\" type=\"checkbox\" tabindex=\"-1\" checked/>");

    /**
     * An html string representation of an unchecked input box.
     */
    private static final SafeHtml INPUT_UNCHECKED = SafeHtmlUtils.fromSafeConstant("<input disabled=\"true\" type=\"checkbox\" tabindex=\"-1\"/>");

    private ActionSource<ThreatDTO> activeStatusChangedSource = new ActionSource<ThreatDTO>();

    public ThreatGrid(DataProvider<ThreatDTO> searchResultsProvider) {
        super(searchResultsProvider);
        List<DataColumn<ThreatDTO>> columnList = new ArrayList<DataColumn<ThreatDTO>>();
        columnList.add(createThreatSelectedColumn());
        columnList.add(createThreatCodeColumn());
        columnList.add(createThreatDescription());
        columnList.add(createHazardTypeColumn());
        columnList.add(createThreatCategoryColumn());
        addColumns(columnList);
    }


    private DataColumn<ThreatDTO> createThreatSelectedColumn() {
        final CheckboxCell cell = new CheckboxCell(true, false);
        Column<ThreatDTO, Boolean> column = new Column<ThreatDTO, Boolean>(
                cell) {
            @Override
            public Boolean getValue(ThreatDTO object) {
                //LoggingUtil.info("Is Active " + object.getId() + ":" + object.getActive());
                return object.getActive();
            }

            @Override
            public void render(Cell.Context context, ThreatDTO object, SafeHtmlBuilder sb) {
                if (!ClientSingleton.isCurrentAnalysisLocked()) {
                    super.render(context, object, sb);
                }
                else {
                    sb.append(object.getActive() ? INPUT_CHECKED : INPUT_UNCHECKED);
                }
                if (!object.getActive()) {
                    sb.appendHtmlConstant("&nbsp;").appendEscaped(object.getInactiveReason());
                }
            }
        };
        column.setSortable(true);
        column.setFieldUpdater(new FieldUpdater<ThreatDTO, Boolean>() {
            public void update(int index, ThreatDTO object, Boolean value) {
                object.setActive(value);
                if (activeStatusChangedSource.getCommand() != null) {
                    activeStatusChangedSource.getCommand().execute(object);
                }
            }
        });
        Comparator<ThreatDTO> comparator = new Comparator<ThreatDTO>() {
            public int compare(ThreatDTO o1, ThreatDTO o2) {
                return o1.getActive().compareTo(o2.getActive());
            }
        };
        return new DataColumn<ThreatDTO>("Threat Active", column, comparator, 20);

    }

    private DataColumn<ThreatDTO> createThreatCategoryColumn() {
        Column<ThreatDTO, String> column = new Column<ThreatDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ThreatDTO object) {
                return object.getThreatCategory().getValue();
            }
        };
        column.setSortable(true);

        Comparator<ThreatDTO> comparator = new Comparator<ThreatDTO>() {
            public int compare(ThreatDTO o1, ThreatDTO o2) {
                return o1.getThreatCategory().compareTo(o2.getThreatCategory());
            }
        };
        return new DataColumn<ThreatDTO>("Threat Category", column, comparator, 20);

    }

    private DataColumn<ThreatDTO> createThreatCodeColumn() {
        Column<ThreatDTO, String> column = new Column<ThreatDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ThreatDTO object) {
                return object.getName();
            }
        };
        column.setSortable(true);

        Comparator<ThreatDTO> comparator = new Comparator<ThreatDTO>() {
            public int compare(ThreatDTO o1, ThreatDTO o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };
        return new DataColumn<ThreatDTO>("Hazard Code", column, comparator, 20);
    }

    private DataColumn<ThreatDTO> createHazardTypeColumn() {
        Column<ThreatDTO, String> column = new Column<ThreatDTO, String> (createDoubleClickableTextCell()) {
            @Override
            public String getValue(ThreatDTO object) {
                return object.getHazardType() ;
            }
        };
        column.setSortable(true);

        Comparator<ThreatDTO> comparator = new Comparator<ThreatDTO>() {
            public int compare(ThreatDTO o1, ThreatDTO o2) {
                return o1.getHazardType().compareTo(o2.getHazardType());
            }
        };
        return new DataColumn<ThreatDTO>("Hazard Type", column, comparator, 20);

    }

    private DataColumn<ThreatDTO> createThreatDescription() {
        Column<ThreatDTO, String> column = new Column<ThreatDTO, String>(createDoubleClickableTextCell()) {
            @Override
            public String getValue(ThreatDTO object) {
                return object.getDescription();
            }
        };
        column.setSortable(true);

        Comparator<ThreatDTO> comparator = new Comparator<ThreatDTO>() {
            public int compare(ThreatDTO o1, ThreatDTO o2) {
                return o1.getDescription().compareTo(o2.getDescription());
            }
        };
        return new DataColumn<ThreatDTO>("Hazard Description", column, comparator, 20);

    }

    public ActionSource<ThreatDTO> getActiveStatusChangedSource() {
        return activeStatusChangedSource;
    }

    private DoubleClickableTextCell<ThreatDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<ThreatDTO>(getSelectionModel(), getSelectionSource());
    }
}
