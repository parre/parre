/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.view;

import com.google.gwt.user.cellview.client.Column;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.parre.client.util.DoubleClickableTextCell;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.dataview.DataColumn;
import org.parre.client.util.dataview.DataGridView;
import org.parre.client.util.dataview.DataProvider;
import org.parre.shared.UriResponseDTO;

/**
 * The Class UriResponseGrid.
 */
public class UriResponseGrid extends DataGridView<UriResponseDTO> {
    public UriResponseGrid(DataProvider<UriResponseDTO> dataProvider) {
		super(dataProvider);
		List<DataColumn<UriResponseDTO>> columnList = new ArrayList<DataColumn<UriResponseDTO>>();
		columnList.add(createQuestionNameColumn());
		columnList.add(createOptionNameColumn());
        columnList.add(createValueColumn());
        columnList.add(createWeightColumn());
        addColumns(columnList);
	}



    private DataColumn<UriResponseDTO> createQuestionNameColumn() {
        Column<UriResponseDTO, String> column = new Column<UriResponseDTO, String>(
                createDoubleClickableTextCell()) {
            @Override
            public String getValue(UriResponseDTO object) {
                return object.getQuestionName();
            }
        };
        column.setSortable(true);

        Comparator<UriResponseDTO> comparator = new Comparator<UriResponseDTO>() {
            public int compare(UriResponseDTO o1, UriResponseDTO o2) {
                return o1.getQuestionName().compareTo(o2.getQuestionName());
            }
        };
        return new DataColumn<UriResponseDTO>("Question", column, comparator, 30);
    }



    private DataColumn<UriResponseDTO> createOptionNameColumn() {
    	Column<UriResponseDTO, String> column = new Column<UriResponseDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(UriResponseDTO object) {
				return	object.getOptionName();
			}
		};
		column.setSortable(true);

		Comparator<UriResponseDTO> comparator = new Comparator<UriResponseDTO>() {
			public int compare(UriResponseDTO o1, UriResponseDTO o2) {
				return o1.getOptionName().compareTo(o2.getOptionName());
			}
		};
		return new DataColumn<UriResponseDTO>("Option", column, comparator, 30);
	}



	private DataColumn<UriResponseDTO> createValueColumn() {
		Column<UriResponseDTO, String> column = new Column<UriResponseDTO, String>(
				createDoubleClickableTextCell()) {
			@Override
			public String getValue(UriResponseDTO object) {
				return	ViewUtil.toDisplayUriValue(object.getValue());
			}
		};
		column.setSortable(true);

		Comparator<UriResponseDTO> comparator = new Comparator<UriResponseDTO>() {
			public int compare(UriResponseDTO o1, UriResponseDTO o2) {
				return o1.getValue().compareTo(o2.getValue());
			}
		};
		return new DataColumn<UriResponseDTO>("Value", column, comparator, 25);
	}


    private DataColumn<UriResponseDTO> createWeightColumn() {
    	Column<UriResponseDTO, String> column = new Column<UriResponseDTO, String>(
    			createDoubleClickableTextCell()) {
    		@Override
    		public String getValue(UriResponseDTO object) {
    			return ViewUtil.toDisplayUriValue(object.getWeight());
    		}
    	};
    	column.setSortable(true);

    	Comparator<UriResponseDTO> comparator = new Comparator<UriResponseDTO>() {
    		public int compare(UriResponseDTO o1, UriResponseDTO o2) {
    			return o1.getWeight().compareTo(o2.getWeight());
    		}
    	};
    	return new DataColumn<UriResponseDTO>("Weight", column, comparator, 15);
    }

    private DoubleClickableTextCell<UriResponseDTO> createDoubleClickableTextCell() {
        return new DoubleClickableTextCell<UriResponseDTO>(getSelectionModel(), getSelectionSource());
    }


}
