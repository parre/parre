/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rra.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.view.client.SingleSelectionModel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.messaging.RiskResilienceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.rra.event.EditUriResponseEvent;
import org.parre.client.rra.event.ManageRraDisplayEvent;
import org.parre.client.rra.event.ManageUriNavEvent;
import org.parre.client.rra.event.UriResponseUpdatedEvent;
import org.parre.client.util.*;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.UriQuestionDTO;
import org.parre.shared.UriResponseDTO;
import org.parre.shared.types.UriQuestionType;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageUriPresenter implements Presenter {

    private RiskResilienceServiceAsync service;
    private BusyHandler busyHandler;
    private RiskResilienceAnalysisDTO currentAnalysis;
    private Map<Long, UriQuestionDTO> uriQuestionMap = new HashMap<Long, UriQuestionDTO>(15);
    private boolean friValid = true;
    private boolean oriValid = true;

    /**
     * The Interface Display.
     */
    public static interface Display extends View {

        void setCurrentAnalysis(RiskResilienceAnalysisDTO dto);

        void setFriResponses(List<UriResponseDTO> responseDTOs);

        void refreshFriResponseGrid();

        SingleSelectionModel<UriResponseDTO> getFriSelectionModel();

        ActionSource<UriResponseDTO> getFriSelectionSource();

        void showEditPopup();

        void hideEditPopup();

        List<UriResponseDTO> getFriData();

        List<UriResponseDTO> getOriData();

        HasClickHandlers getUpdateFriResponsesButton();

        void showUriQuestionPopup();

        HasClickHandlers getSelectQuestionButton();

        void hideUriQuestionsPopup();

        ListBox getUriQuestions();

        HasClickHandlers getBackButton();

        void setOriResponses(List<UriResponseDTO> responseDTOs);

        void refreshOriResponseGrid();

        void updateFriResponse(UriResponseDTO uriResponseDTO);

        void updateOriResponse(UriResponseDTO uriResponseDTO);

        void setUpdateFriEnabled(boolean enabled);

        void setUpdateOriEnabled(boolean enabled);

        SingleSelectionModel<UriResponseDTO> getOriSelectionModel();

        ActionSource<UriResponseDTO> getOriSelectionSource();

        HasClickHandlers getUpdateOriResponsesButton();

        void setCalculatedUri(BigDecimal uriValue);

        void setFriNeedsSaving(boolean value);

        void setOriNeedsSaving(boolean value);
    }

    private EventBus eventBus;
    private Display display;

    public ManageUriPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getRiskResilienceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
    }


    private void bind() {
        bindEvents();
        bindActions();
    }

    private void bindEvents() {
        eventBus.addHandler(ManageUriNavEvent.TYPE, new ManageUriNavEvent.Handler() {
            public void onEvent(ManageUriNavEvent event) {
                currentAnalysis = event.getAnalysisDTO();
                if (uriQuestionMap.isEmpty()) {
                    getUriQuestionDTOs();
                } else {
                    getUriResponseDTOs();
                }
            }
        });
        eventBus.addHandler(UriResponseUpdatedEvent.TYPE, new UriResponseUpdatedEvent.Handler() {
            public void onEvent(UriResponseUpdatedEvent event) {
                display.hideEditPopup();
                UriResponseDTO uriResponseDTO = event.getUriResponseDTO();
                boolean friResponse = isFriResponse(uriResponseDTO);
                if (friResponse) {
                    display.updateFriResponse(uriResponseDTO);
                    updateFriState();
                } else {
                    display.updateOriResponse(uriResponseDTO);
                    updateOriState();
                }
                updateCalculatedUri();
            }
        });
    }

    private void updateCalculatedUri() {
        if (!(friValid || oriValid)) {
            return;
        }
        BigDecimal friSum = calculateSumOfProduct(display.getFriData());
        BigDecimal oriSum = calculateSumOfProduct(display.getOriData());
        BigDecimal calculatedValue = friSum.add(oriSum);
        display.setCalculatedUri(calculatedValue);
    }

    private BigDecimal calculateSumOfProduct(List<UriResponseDTO> data) {
        BigDecimal value = BigDecimal.ZERO;
        for (UriResponseDTO uriResponseDTO : data) {
            value = value.add(uriResponseDTO.getValue().multiply(uriResponseDTO.getWeight()));
        }
        return value;
    }

    private void updateFriState() {
        List<UriResponseDTO> data = display.getFriData();
        BigDecimal total = calculateWeightTotal(data);
        friValid = (total.doubleValue() == 22);
        display.setUpdateFriEnabled(friValid);
    }

    private void updateOriState() {
        List<UriResponseDTO> data = display.getOriData();
        BigDecimal total = calculateWeightTotal(data);
        oriValid = (total.doubleValue() == 80);
        display.setUpdateOriEnabled(oriValid);
    }

    private BigDecimal calculateWeightTotal(List<UriResponseDTO> friData) {
        BigDecimal total = BigDecimal.ZERO;
        for (UriResponseDTO uriResponseDTO : friData) {
            total = total.add(uriResponseDTO.getWeight());
        }
        return total;
    }

    private void bindActions() {
        display.getFriSelectionSource().setCommand(new ActionSource.ActionCommand<UriResponseDTO>() {
            public void execute(UriResponseDTO dto) {
                editResponse(dto);
            }
        });
        display.getOriSelectionSource().setCommand(new ActionSource.ActionCommand<UriResponseDTO>() {
            public void execute(UriResponseDTO uriResponseDTO) {
                editResponse(uriResponseDTO);
            }
        });
        display.getUpdateFriResponsesButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                saveResponses(display.getFriData(), UriQuestionType.FRI);
            }
        });

        display.getUpdateOriResponsesButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                saveResponses(display.getOriData(), UriQuestionType.ORI);
            }
        });

        display.getSelectQuestionButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.hideUriQuestionsPopup();
                Long selectedId = ViewUtil.getSelectedId(display.getUriQuestions());
                addResponse(findUriQuestion(selectedId));
            }
        });
        display.getBackButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ManageRraDisplayEvent.create());
            }
        });
    }

    private void saveResponses(List<UriResponseDTO> dtos, final UriQuestionType questionType) {
        if (ParreClientUtil.handleAnalysisBaselined()) {
            return;
        }

        final List<UriResponseDTO> toSave = new ArrayList<UriResponseDTO>();
        for (UriResponseDTO dto : dtos) {
            dto.setUriQuestionDTO(null);
            toSave.add(dto);
        }
        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getRiskResilienceService().saveUriResponses(toSave, callback);
            }
            @Override
            public void handleResult(Void results) {
                if (questionType.isFri()) {
                    display.setFriNeedsSaving(false);
                    display.setUpdateFriEnabled(false);
                    display.getFriSelectionModel().setSelected(null, true);
                }
                else {
                    display.setOriNeedsSaving(false);
                    display.setUpdateOriEnabled(false);
                    display.getOriSelectionModel().setSelected(null, true);
                }
            }
        }).makeCall();

    }

    private void editResponse(UriResponseDTO dto) {
        if (ParreClientUtil.handleAnalysisBaselined()) {
            return;
        }
        UriQuestionDTO uriQuestion = findUriQuestion(dto.getUriQuestionId());
        dto.setUriQuestionDTO(uriQuestion);
        eventBus.fireEvent(EditUriResponseEvent.create(dto));
        display.showEditPopup();
    }

    private UriQuestionDTO findUriQuestion(Long selectedId) {
        return uriQuestionMap.get(selectedId);
    }

    private void addResponse(UriQuestionDTO uriQuestionDTO) {
        UriResponseDTO dto = new UriResponseDTO(currentAnalysis.getId(), uriQuestionDTO);
        eventBus.fireEvent(EditUriResponseEvent.create(dto));
        display.showEditPopup();
    }

    private void getUriQuestionDTOs() {
        ExecutableAsyncCallback<List<UriQuestionDTO>> exec =
                new ExecutableAsyncCallback<List<UriQuestionDTO>>(busyHandler, new BaseAsyncCommand<List<UriQuestionDTO>>() {
                    public void execute(AsyncCallback<List<UriQuestionDTO>> callback) {
                        ServiceFactory.getInstance().getRiskResilienceService().getUriQuestionDTOs(callback);
                    }

                    @Override
                    public void handleResult(List<UriQuestionDTO> results) {
                        for (UriQuestionDTO result : results) {
                            uriQuestionMap.put(result.getId(), result);
                        }
                        ListBox uriQuestions = display.getUriQuestions();
                        uriQuestions.addItem("Select");
                        for (UriQuestionDTO uriQuestionDTO : results) {
                            uriQuestions.addItem(uriQuestionDTO.getName(), String.valueOf(uriQuestionDTO.getId()));
                        }
                        getUriResponseDTOs();
                    }
                });
        exec.makeCall();

    }

    private void getUriResponseDTOs() {
        new ExecutableAsyncCallback<List<UriResponseDTO>>(busyHandler, new BaseAsyncCommand<List<UriResponseDTO>>() {
            public void execute(AsyncCallback<List<UriResponseDTO>> callback) {
                ServiceFactory.getInstance().getRiskResilienceService().getUriResponseDTOs(currentAnalysis.getId(), callback);
            }

            @Override
            public void handleResult(List<UriResponseDTO> results) {
                List<UriResponseDTO> friResponses = new ArrayList<UriResponseDTO>();
                List<UriResponseDTO> uriResponses = new ArrayList<UriResponseDTO>();
                for (UriResponseDTO result : results) {
                    boolean isFri = isFriResponse(result);
                    if (isFri) {
                        friResponses.add(result);
                    } else {
                        uriResponses.add(result);
                    }
                }
                display.setFriResponses(friResponses);
                display.setUpdateFriEnabled(false);
                display.setOriResponses(uriResponses);
                display.setUpdateOriEnabled(false);
                updateCalculatedUri();
            }
        }).makeCall();
    }

    private boolean isFriResponse(UriResponseDTO result) {
        UriQuestionDTO uriQuestion = findUriQuestion(result.getUriQuestionId());
        return uriQuestion.getType() == UriQuestionType.FRI;
    }
}
