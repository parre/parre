/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.rrm.view;

import org.apache.log4j.Logger;

import java.math.BigDecimal;

/**
 * The Class QuantityMetricRenderer.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/20/12
 */
public class QuantityMetricRenderer implements MetricRenderer {
    public static MetricRenderer INSTANCE = new QuantityMetricRenderer();
    public String render(BigDecimal value) {
        try {
            return String.valueOf(value.intValue());
        } catch (NullPointerException e) {
            return "0";
        }

    }
}
