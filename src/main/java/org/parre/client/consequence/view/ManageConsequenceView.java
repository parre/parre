/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.view;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.view.client.SingleSelectionModel;

import java.util.List;

import org.parre.client.ClientSingleton;
import org.parre.client.common.view.*;
import org.parre.client.consequence.presenter.ManageConsequencePresenter;
import org.parre.client.util.*;
import org.parre.client.util.dataview.DataProvider;
import org.parre.client.util.dataview.DataProviderFactory;
import org.parre.shared.ConsequenceDTO;
import org.parre.shared.NameDescriptionDTO;

/**
 * The Class ManageConsequenceView.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/16/11
 */
public class ManageConsequenceView extends BaseAnalysisView implements ManageConsequencePresenter.Display {
    private ConsequenceGrid consequenceGrid;
    private DeckLayoutPanel searchCriteriaDeck;
    private AssetCriteriaView assetCriteriaView;
    private ThreatCriteriaView threatCriteriaView;
    private Image searchButton;
    private Image clearButton;
    private Image threatCriteriaButton;
    private Image assetCriteriaButton;
    private VerticalPanel searchPanel;
    private Widget viewWidget;
    private DialogBox editPopup;
    private EditConsequenceView editAssetView;
    private DisplayToggleView displayToggleView;
    private Button previousButton;
    private Button nextButton;

    public ManageConsequenceView(AssetCriteriaView assetCriteriaView, ThreatCriteriaView threatCriteriaView,
                                 EditConsequenceView editConsequenceView, DisplayToggleView displayToggleView) {
        super(new AnalysisCommandPanel(), new NameDescriptionView());
        this.assetCriteriaView = assetCriteriaView;
        this.threatCriteriaView = threatCriteriaView;
        this.editAssetView = editConsequenceView;
        this.displayToggleView = displayToggleView;
        viewWidget = createView();
    }

    private Widget createView() {
        searchPanel = new VerticalPanel();
        searchPanel.setWidth("100%");
        searchButton = ViewUtil.createImageButton("search-button.png", "Perform Search");
        clearButton = ViewUtil.createImageButton("clear-button.jpg", "Clear Search Results");
        threatCriteriaButton = ViewUtil.createImageButton("threat.jpg", "Search Using Threats");
        assetCriteriaButton = ViewUtil.createImageButton("asset.jpg", "Search Using Assets");

        getCommandPanel().setToolTips("Back to Asset and Threat Characterization", "Forward to Vulnerability Analysis");

        searchCriteriaDeck = new DeckLayoutPanel();
        searchCriteriaDeck.setStyleName("inputPanel");
        searchCriteriaDeck.setHeight("100px");
        searchCriteriaDeck.add(assetCriteriaView.asWidget());
        searchCriteriaDeck.add(threatCriteriaView.asWidget());
        searchPanel.add(searchCriteriaDeck);

        searchPanel.add(ViewUtil.layoutCenter(searchButton, assetCriteriaButton, threatCriteriaButton, clearButton));

        DataProvider<ConsequenceDTO> dataProvider = DataProviderFactory.createDataProvider();
        consequenceGrid = new ConsequenceGrid(dataProvider);

        searchCriteriaDeck.showWidget(0);

        displayToggleView.setText("Search Criteria");
        displayToggleView.setDisplayToggleCommand(new DisplayToggleView.DisplayToggleCommand() {
            public void toggleDisplay(boolean show) {
                setSearchVisible(show);
            }
        });
        displayToggleView.setDisplayVisible(false);
        //assetButtonPanel.setWidth("75%");
        VerticalPanel viewPanel = new VerticalPanel();

        viewPanel.setWidth("100%");
        viewPanel.setHeight("100%");
        viewPanel.add(displayToggleView.asWidget());
        viewPanel.add(searchPanel);
        viewPanel.add(getCommandPanel().asWidget());
        viewPanel.add(consequenceGrid.asWidget());
        viewPanel.setSpacing(5);
        consequenceGrid.asWidget().setVisible(false);
        initEditConsequencePopup();
        return viewPanel;
    }


    public void setSearchVisible(boolean value) {
        searchPanel.setVisible(value);
        consequenceGrid.setHeight(value ? "500px" : "650px");
    }

    private void initEditConsequencePopup() {
        editPopup = new ClosablePopup("Edit Consequence", true);
        editPopup.setModal(true);
        Widget widget = editAssetView.asWidget();
        widget.setWidth("300px");
        editPopup.setWidget(widget);
    }

    public SingleSelectionModel<ConsequenceDTO> getSearchResultsSelectionModel() {
        return consequenceGrid.getSelectionModel();
    }

    public ActionSource<ConsequenceDTO> getConsequenceSelectionSource() {
        return consequenceGrid.getSelectionSource();
    }

    public HasClickHandlers getSearchButton() {
        return searchButton;
    }

    public HasClickHandlers getClearButton() {
        return clearButton;
    }

    public HasClickHandlers getThreatCriteriaButton() {
        return threatCriteriaButton;
    }

    public HasClickHandlers getAssetCriteriaButton() {
        return assetCriteriaButton;
    }

    public void showSearchView(int searchViewIndex) {
        searchCriteriaDeck.showWidget(searchViewIndex);
        if (ManageConsequencePresenter.ASSET_SEARCH == searchViewIndex) {
            assetCriteriaButton.setVisible(false);
            threatCriteriaButton.setVisible(true);
        }
        else {
            assetCriteriaButton.setVisible(true);
            threatCriteriaButton.setVisible(false);
        }
    }

    public void setSearchResults(List<ConsequenceDTO> results) {
        consequenceGrid.asWidget().setVisible(true);
        getCommandPanel().setAnalysisCount(results.size());
        consequenceGrid.setData(results);
        consequenceGrid.getSelectionModel().setSelected(null, true);
    }

    public void refreshSearchResultsView() {
        consequenceGrid.refresh();
    }

    public Widget asWidget() {
        return viewWidget;
    }

    public HasCloseHandlers<PopupPanel> getEditPopup() {
        return editPopup;
    }

    public void setSearchResultsViewDisabled(boolean value) {
        if (value) {
            consequenceGrid.asWidget().getElement().setAttribute("disabled", "true");
        }
        else {
            consequenceGrid.asWidget().getElement().removeAttribute("disabled");
        }
    }

    public void showEditPopup() {
        //editAssetPopup.showRelativeTo(consequenceGrid.asWidget());
        editPopup.center();
        editPopup.show();
        setSearchResultsViewDisabled(true);
    }

    public void hideEditAssetPopup() {
        editPopup.hide();
    }

    public List<ConsequenceDTO> getSearchResults() {
        return consequenceGrid.getData();
    }

    public void setCurrentAnalysis(NameDescriptionDTO currentAnalysis) {
        super.setCurrentAnalysis(currentAnalysis);
        boolean haveAnalysis = currentAnalysis != null;
        consequenceGrid.asWidget().setVisible(haveAnalysis);
        consequenceGrid.setUseStatValue(ClientSingleton.getCurrentParreAnalysis().getUseStatValues());
    }

    @Override
    public HasClickHandlers getPreviousButton() {
        return getCommandPanel().getPreviousButton();
    }

    @Override
    public HasClickHandlers getNextButton() {
        return getCommandPanel().getNextButton();
    }

    @Override
    public void refreshView() {
        consequenceGrid.forceLayout();
    }
}
