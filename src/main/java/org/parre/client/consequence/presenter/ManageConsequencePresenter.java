/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.consequence.presenter;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.parre.client.ClientSingleton;
import org.parre.client.ParreClientUtil;
import org.parre.client.asset.event.AssetSearchEvent;
import org.parre.client.asset.event.ClearAssetSearchEvent;
import org.parre.client.common.ClientCalculationUtil;
import org.parre.client.common.event.AssetThreatSearchType;
import org.parre.client.common.presenter.ManageAnalysisPresenter;
import org.parre.client.common.view.AnalysisView;
import org.parre.client.consequence.event.ConsequenceSearchResultsEvent;
import org.parre.client.consequence.event.ConsequenceUpdatedEvent;
import org.parre.client.consequence.event.EditConsequenceEvent;
import org.parre.client.consequence.event.ManageConsequenceNavEvent;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.event.LogoutEvent;
import org.parre.client.messaging.ConsequenceServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.ThreatAssetMatrixNavEvent;
import org.parre.client.nav.event.VulnerabilityNavEvent;
import org.parre.client.threat.event.ThreatSearchEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.Presenter;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.ConsequenceDTO;
import org.parre.shared.DirectedThreatAnalysisDTO;
import org.parre.shared.types.AssetThreatAnalysisType;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;


/**
 * User: Swarn S. Dhaliwal
 * Date: 8/21/11
 * Time: 5:03 PM
*/
public class ManageConsequencePresenter extends ManageAnalysisPresenter implements Presenter {
    private ConsequenceDTO selectedConsequenceDTO;
    public static final int ASSET_SEARCH = 0;
    public static final int THREAT_SEARCH = 1;

    public static final Map<Integer, GwtEvent> viewEventMap = new HashMap<Integer, GwtEvent>(2);

    static {
        viewEventMap.put(ASSET_SEARCH, AssetSearchEvent.create(AssetThreatSearchType.CONSEQUENCE));
        viewEventMap.put(THREAT_SEARCH, ThreatSearchEvent.create(AssetThreatSearchType.CONSEQUENCE));
    }

    private DirectedThreatAnalysisDTO currentAnalysis;
    private List<ConsequenceDTO> consequenceList;
    private ConsequenceServiceAsync service;
    private BusyHandler busyHandler;

    /**
     * The Interface Display.
     */
    public static interface Display extends AnalysisView {
        SingleSelectionModel<ConsequenceDTO> getSearchResultsSelectionModel();

        ActionSource<ConsequenceDTO> getConsequenceSelectionSource();

        HasClickHandlers getSearchButton();

        HasClickHandlers getClearButton();

        HasClickHandlers getThreatCriteriaButton();

        void setSearchResults(List<ConsequenceDTO> dtos);

        void refreshSearchResultsView();

        void showSearchView(int searchViewIndex);

        HasClickHandlers getAssetCriteriaButton();

        HasCloseHandlers<PopupPanel> getEditPopup();

        void showEditPopup();

        void hideEditAssetPopup();

        List<ConsequenceDTO> getSearchResults();

        void setSearchResultsViewDisabled(boolean value);

    }

    private int currentSearchView = ASSET_SEARCH;
    private EventBus eventBus;
    private Display display;

    public ManageConsequencePresenter(Display display) {
        super(display);
        this.service = ServiceFactory.getInstance().getConsequenceService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.busyHandler = new BusyHandler(display);
    }

    public void go() {
        bind();
        display.showSearchView(currentSearchView);
        init();
    }


    private GwtEvent getSearchEvent() {
        return viewEventMap.get(currentSearchView);
    }

    private void bind() {
        eventBus.addHandler(ManageConsequenceNavEvent.TYPE, new ManageConsequenceNavEvent.Handler() {
            public void onEvent(ManageConsequenceNavEvent event) {
                getCurrentAnalysis();
            }
        });
        eventBus.addHandler(ConsequenceUpdatedEvent.TYPE, new ConsequenceUpdatedEvent.Handler() {
            public void onEvent(ConsequenceUpdatedEvent event) {
                display.hideEditAssetPopup();
                ConsequenceDTO updated = event.getConsequenceDTO();
                updateFinancialTotal(updated);
                List<ConsequenceDTO> currentResults = display.getSearchResults();
                int index = currentResults.indexOf(updated);
                currentResults.set(index, updated);
                index = consequenceList.indexOf(updated);
                consequenceList.set(index, updated);
                display.refreshSearchResultsView();
            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                currentSearchView = ASSET_SEARCH;
                display.showSearchView(currentSearchView);
                display.setSearchResults(Collections.<ConsequenceDTO>emptyList());
            }
        });
        display.getClearButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                eventBus.fireEvent(ClearAssetSearchEvent.create());
                display.setSearchResults(Collections.<ConsequenceDTO>emptyList());
            }
        });
        display.getThreatCriteriaButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                switchSearchView(THREAT_SEARCH);
            }
        });
        display.getAssetCriteriaButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                switchSearchView(ASSET_SEARCH);
            }
        });
        display.getSearchButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (currentAnalysis != null) {
                    eventBus.fireEvent(getSearchEvent());
                } else {
                    MessageDialog.popup("Please create an Analysis");
                }
            }
        });
        display.getConsequenceSelectionSource().setCommand(new ActionSource.ActionCommand<ConsequenceDTO>() {
            public void execute(ConsequenceDTO dto) {
                edit(dto);
            }
        });
        display.getEditPopup().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                display.setSearchResultsViewDisabled(false);
            }
        });

        display.getSearchResultsSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<ConsequenceDTO> selectionModel = (SingleSelectionModel<ConsequenceDTO>) event.getSource();
                consequenceSelected(selectionModel.getSelectedObject());
            }
        });
        display.getPreviousButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(ThreatAssetMatrixNavEvent.create());
            }
        });
        display.getNextButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent clickEvent) {
                NavManager.performNav(VulnerabilityNavEvent.create());
            }
        });
        eventBus.addHandler(ConsequenceSearchResultsEvent.TYPE, new ConsequenceSearchResultsEvent.Handler() {
            public void onEvent(ConsequenceSearchResultsEvent event) {
                consequenceList = event.getResults();
                updateFinancialTotal();
                display.setSearchResults(consequenceList);
                display.getSearchResultsSelectionModel().setSelected(null, true);
                //display.refreshSearchResultsView();
                display.refreshView();
            }
        });
        eventBus.addHandler(LogoutEvent.TYPE, new LogoutEvent.Handler() {
            public void onEvent(LogoutEvent event) {
                switchSearchView(ASSET_SEARCH);
            }
        });
    }

    private void edit(ConsequenceDTO dto) {
        if (ParreClientUtil.handleAnalysisBaselined()) {
            return;
        }
        eventBus.fireEvent(EditConsequenceEvent.create(dto));
        display.showEditPopup();
    }

    private void updateFinancialTotal(ConsequenceDTO updated) {
        if (ClientSingleton.getCurrentParreAnalysis().getUseStatValues()) {
            ClientCalculationUtil.updateFinancialTotal(updated);
        }
    }

    private void updateFinancialTotal() {
        if (ClientSingleton.getCurrentParreAnalysis().getUseStatValues()) {
            ClientCalculationUtil.updateFinancialTotal(consequenceList);
        }
    }

    @Override
    protected AssetThreatAnalysisType getAnalysisType() {
        return AssetThreatAnalysisType.CONSEQUENCE;
    }

    protected void startAnalysis() {
        final DirectedThreatAnalysisDTO analysisDTO = new DirectedThreatAnalysisDTO();
        analysisDTO.setParreAnalysisId(ClientSingleton.getParreAnalysisId());
        ExecutableAsyncCallback<DirectedThreatAnalysisDTO> exec =
                new ExecutableAsyncCallback<DirectedThreatAnalysisDTO>(busyHandler, new BaseAsyncCommand<DirectedThreatAnalysisDTO>() {
                    public void execute(AsyncCallback<DirectedThreatAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getConsequenceService().
                                startAnalysis(analysisDTO, callback);
                    }

                    @Override
                    public void handleResult(DirectedThreatAnalysisDTO results) {
                        initCurrentAnalysis(results);
                    }
                });
        exec.makeCall();

    }


    private void switchSearchView(int viewIndex) {
        currentSearchView = viewIndex;
        display.showSearchView(viewIndex);
        display.setSearchResults(Collections.<ConsequenceDTO>emptyList());
    }


    public void consequenceSelected(ConsequenceDTO dto) {
        selectedConsequenceDTO = dto;
    }

    protected void getCurrentAnalysis() {
        ExecutableAsyncCallback<DirectedThreatAnalysisDTO> exec =
                new ExecutableAsyncCallback<DirectedThreatAnalysisDTO>(busyHandler, new BaseAsyncCommand<DirectedThreatAnalysisDTO>() {

                    public void execute(AsyncCallback<DirectedThreatAnalysisDTO> callback) {
                        ServiceFactory.getInstance().getConsequenceService().
                                getCurrentAnalysis(ClientSingleton.getParreAnalysisId(), callback);
                    }

                    @Override
                    public void handleResult(DirectedThreatAnalysisDTO analysis) {
                        initCurrentAnalysis(analysis);
                    }
                });
        exec.makeCall();

    }

    private void initCurrentAnalysis(DirectedThreatAnalysisDTO analysis) {
        currentAnalysis = analysis;
        super.initCurrentAnalysis(analysis);
        if (currentAnalysis != null) {
            new ConsequenceAssetSearchCommand().execute(new AssetSearchDTO(), display);
        }
        display.setCurrentAnalysis(currentAnalysis);
    }


}
