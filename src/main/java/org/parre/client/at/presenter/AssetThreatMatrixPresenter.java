/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.presenter;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.parre.client.ClientSingleton;
import org.parre.client.common.presenter.AnalysisStateHost;
import org.parre.client.common.presenter.AnalysisStateInitializer;
import org.parre.client.event.GoHomeEvent;
import org.parre.client.event.ThreatListChangedEvent;
import org.parre.client.messaging.AssetServiceAsync;
import org.parre.client.messaging.ServiceFactory;
import org.parre.client.nav.event.AssetNavEvent;
import org.parre.client.nav.event.ConsequenceNavEvent;
import org.parre.client.nav.event.ThreatAssetMatrixNavEvent;
import org.parre.client.nav.event.ThreatNavEvent;
import org.parre.client.util.ActionSource;
import org.parre.client.util.BaseAsyncCommand;
import org.parre.client.util.BusyHandler;
import org.parre.client.util.ClientLoggingUtil;
import org.parre.client.util.ExecutableAsyncCallback;
import org.parre.client.util.MessageDialog;
import org.parre.client.util.Presenter;
import org.parre.client.util.View;
import org.parre.client.util.ViewUtil;
import org.parre.client.util.nav.NavManager;
import org.parre.shared.AssetThreatLevelDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.ThreatDTO;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.CloseEvent;
import com.google.gwt.event.logical.shared.CloseHandler;
import com.google.gwt.event.logical.shared.HasCloseHandlers;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasText;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;

/**
 * The Class AssetThreatMatrixPresenter.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 7/19/11
 */
public class AssetThreatMatrixPresenter implements Presenter, AnalysisStateHost<AssetThreatAnalysisState> {
    private AssetServiceAsync service;
    private EventBus eventBus;
    private Display display;
    private AssetThreatLevelsDTO selectedAssetThreatLevelsDTO;
    private List<AssetThreatLevelsDTO> assetThreatLevelsDTOs;
    private Set<AssetThreatLevelDTO> toSaveAssetThreatLevels = new HashSet<AssetThreatLevelDTO>();
    private boolean refreshThreatList = true;
    private Integer assetThreatThreshold;
    private BusyHandler busyHandler;
    private AssetThreatAnalysisState handler;
    private AnalysisStateInitializer<AssetThreatAnalysisState> handlerInitializer;
    private Timer timer;

    public void setAssetThreatThreshold(Integer assetThreatThreshold) {
        this.assetThreatThreshold = assetThreatThreshold;
    }

    public void setHandler(AssetThreatAnalysisState handler) {
        this.handler = handler;
    }

    public View getView() {
        return display;
    }


    /**
     * The Interface Display.
     */
    public interface Display extends View {
        void setAssetThreatMatrixData(List<AssetThreatLevelsDTO> assetThreatLevelsDTOs);

        List<HasText> getAssetLabels();

        ActionSource<AssetThreatLevelsDTO> getSelectionSource();

        SingleSelectionModel<AssetThreatLevelsDTO> getSelectionModel();

        HasClickHandlers getChangeThresholdButton();

        void refreshThreatList(List<ThreatDTO> result);

        HasClickHandlers getChangeActiveThreatsButton();

        ListBox getAssetThreatThreshold();

        HasText getAssetThreatCount();

        ActionSource<AssetThreatLevelDTO> getAssetThreatUpdateSource();

        void refreshAssetThreatMatrix();

        void setAssetThreatThreshold(Integer assetThreatThreshold);

        void showInvalidValuePopup(String message);

        HasCloseHandlers<PopupPanel> getInvalidMessageDialog();

        void updateAssetThreatMatrixData(AssetThreatLevelsDTO assetThreatLevelsDTO);

        HasClickHandlers getAssetThreatDataUpdateButton();

        void setAssetThreatDataUpdateButtonEnabled(boolean value);

        HasClickHandlers getNextPageButton();
    }

    public AssetThreatMatrixPresenter(Display display) {
        this.service = ServiceFactory.getInstance().getAssetService();
        this.eventBus = ClientSingleton.getEventBus();
        this.display = display;
        this.handlerInitializer = new AnalysisStateInitializer<AssetThreatAnalysisState>(this,
                AssetThreatAnalysisStateFactory.getInstance());
    }

    public void go() {
        handlerInitializer.go();
        bind();
    }

    private void bind() {
        busyHandler = new BusyHandler(display);
        display.setAssetThreatThreshold(assetThreatThreshold);
        bindEvents();
        bindActions();
        refreshAssetThreatMatrix();

        timer = new Timer() {
            @Override
            public void run() {
                assetThreatThreshold = getCurrentThresholdValue(display.getAssetThreatThreshold());
                handler.updateAssetThreatData(assetThreatThreshold, toSaveAssetThreatLevels);
                display.setAssetThreatDataUpdateButtonEnabled(false);
            }
        };
    }

    private void bindEvents() {
        eventBus.addHandler(ThreatListChangedEvent.TYPE, new ThreatListChangedEvent.Handler() {
            public void onEvent(ThreatListChangedEvent event) {
                refreshThreatList = true;
            }
        });
        eventBus.addHandler(ThreatAssetMatrixNavEvent.TYPE, new ThreatAssetMatrixNavEvent.Handler() {
            public void onEvent(ThreatAssetMatrixNavEvent eventAsset) {
                display.setAssetThreatThreshold(assetThreatThreshold);
                refreshAssetThreatMatrix();
            }
        });
        eventBus.addHandler(GoHomeEvent.TYPE, new GoHomeEvent.Handler() {
            public void onEvent(GoHomeEvent event) {
                display.setAssetThreatMatrixData(Collections.<AssetThreatLevelsDTO>emptyList());
            }
        });
    }

    private void bindActions() {
        display.getChangeThresholdButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.setAssetThreatMatrixData(Collections.<AssetThreatLevelsDTO>emptyList());
                NavManager.performNav(AssetNavEvent.create());
            }
        });

        display.getChangeActiveThreatsButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                display.setAssetThreatMatrixData(Collections.<AssetThreatLevelsDTO>emptyList());
                NavManager.performNav(ThreatNavEvent.create());
            }
        });
        display.getSelectionModel().addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                SingleSelectionModel<AssetThreatLevelsDTO> selectionModel = (SingleSelectionModel<AssetThreatLevelsDTO>) event.getSource();
                selectedAssetThreatLevelsDTO = selectionModel.getSelectedObject();
                String displayValue = selectedAssetThreatLevelsDTO.getAssetId() + "-" + selectedAssetThreatLevelsDTO.getAssetName();
                List<HasText> assetLabels = display.getAssetLabels();
                for (HasText assetLabel : assetLabels) {
                    assetLabel.setText(displayValue);
                }
            }
        });
        display.getSelectionSource().setCommand(new ActionSource.ActionCommand<AssetThreatLevelsDTO>() {
            public void execute(AssetThreatLevelsDTO assetThreatLevelsDTO) {
                timer.schedule(Integer.MAX_VALUE);
            }
        });
        display.getAssetThreatThreshold().addFocusHandler(new FocusHandler() {
            @Override
            public void onFocus(FocusEvent event) {
                timer.schedule(Integer.MAX_VALUE);
            }
        });

        display.getAssetThreatThreshold().addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                updateAssetThreatCount();
                display.refreshAssetThreatMatrix();
                display.setAssetThreatDataUpdateButtonEnabled(true);
                timer.schedule(3000);
            }
        });
        display.getAssetThreatDataUpdateButton().addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                assetThreatThreshold = getCurrentThresholdValue(display.getAssetThreatThreshold());
                handler.updateAssetThreatData(assetThreatThreshold, toSaveAssetThreatLevels);
                timer.schedule(0);
            }
        });
        display.getInvalidMessageDialog().addCloseHandler(new CloseHandler<PopupPanel>() {
            public void onClose(CloseEvent<PopupPanel> popupPanelCloseEvent) {
                display.getSelectionModel().setSelected(selectedAssetThreatLevelsDTO, true);
                display.refreshAssetThreatMatrix();
            }
        });
        display.getNextPageButton().addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                if(display.getAssetThreatCount().getText().equals("0")) {
                    MessageDialog.popup("Must rate some asset-threat pairs to continue.");
                }
                else {
                    NavManager.performNav(ConsequenceNavEvent.create());
                }

            }
        });
        display.getAssetThreatUpdateSource().setCommand(new ActionSource.ActionCommand<AssetThreatLevelDTO>() {
            public void execute(final AssetThreatLevelDTO levelDTO) {
                final Integer level = levelDTO.getLevel();
                display.refreshAssetThreatMatrix();
                if (level > 0 && level < 6) {
                    updateAssetThreatLevel(levelDTO);
                } else {
                    Scheduler.ScheduledCommand command = new Scheduler.ScheduledCommand() {
                        public void execute() {
                            display.showInvalidValuePopup("Invalid Value = " + level);
                        }
                    };
                    Scheduler.get().scheduleDeferred(command);
                }
            }
        });
    }

    private void updateAssetThreatCount() {
        ListBox listBox = display.getAssetThreatThreshold();
        int thresholdValue = getCurrentThresholdValue(listBox);
        int count = 0;
        for (AssetThreatLevelsDTO assetThreatLevelsDTO : assetThreatLevelsDTOs) {
            Collection<Integer> levels = assetThreatLevelsDTO.getThreatLevels();
            for (Integer level : levels) {
                if (level >= thresholdValue) {
                    count++;
                }
            }
        }
        display.getAssetThreatCount().setText(String.valueOf(count));
    }

    private int getCurrentThresholdValue(ListBox listBox) {
        return Integer.valueOf(listBox.getValue(listBox.getSelectedIndex()));
    }


    private void updateAssetThreatLevel(final AssetThreatLevelDTO levelDTO) {
        Integer oldLevel = null;
        Integer newLevel = levelDTO.getLevel();
        for (AssetThreatLevelsDTO assetThreatLevelsDTO : assetThreatLevelsDTOs) {
            if (assetThreatLevelsDTO.getId().equals(levelDTO.getAssetId())) {
                oldLevel = assetThreatLevelsDTO.getThreatLevel(levelDTO.getThreatId());
                if (oldLevel.equals(newLevel)) {
                    return;
                }
                assetThreatLevelsDTO.putThreatLevel(levelDTO.getThreatId(), newLevel);
                break;
            }
        }
        final Integer change = calculateChange(oldLevel, newLevel);

        toSaveAssetThreatLevels.add(levelDTO);
        if (change != 0) {
            Integer currentCount = Integer.valueOf(display.getAssetThreatCount().getText());
            display.getAssetThreatCount().setText(String.valueOf(currentCount + change));
        }
        display.setAssetThreatDataUpdateButtonEnabled(true);
        //saveAssetThreatLevel(levelDTO, change);
        timer.schedule(3000);
    }

    private void saveAssetThreatLevel(final AssetThreatLevelDTO levelDTO, final Integer change) {
        new ExecutableAsyncCallback<Void>(busyHandler, new BaseAsyncCommand<Void>() {
            public void execute(AsyncCallback<Void> callback) {
                ServiceFactory.getInstance().getParreService().
                        updateAssetThreatLevel(levelDTO, ClientSingleton.getParreAnalysisId(), callback);
            }

            @Override
            public void handleResult(Void results) {
                if (change != 0) {
                    Integer currentCount = Integer.valueOf(display.getAssetThreatCount().getText());
                    display.getAssetThreatCount().setText(String.valueOf(currentCount + change));
                }
            }
        }).makeCall();
    }

    private Integer calculateChange(Integer oldLevel, Integer newLevel) {
        Integer threshold = Integer.valueOf(ViewUtil.getSelectedValue(display.getAssetThreatThreshold()));
        Integer change;
        if (oldLevel < threshold && newLevel >= threshold) {
            change = 1;
        } else if (oldLevel >= threshold && newLevel < threshold) {
            change = -1;
        } else {
            change = 0;
        }
        return change;
    }

    private void refreshAssetThreatMatrix() {
        //if (refreshThreatList) {
            populateThreatList();
        //} else {
        //    populateAssetThreatMatrix();
        //}
    }

    private void populateThreatList() {
        ClientLoggingUtil.info("Populating ThreatList");
        ExecutableAsyncCallback<List<ThreatDTO>> exec =
                new ExecutableAsyncCallback<List<ThreatDTO>>(busyHandler, new BaseAsyncCommand<List<ThreatDTO>>() {
                    public void execute(AsyncCallback<List<ThreatDTO>> callback) {
                        ServiceFactory.getInstance().getAssetService().getActiveThreats(ClientSingleton.getBaselineId(), callback);
                    }

                    @Override
                    public void handleResult(List<ThreatDTO> results) {
                        refreshThreatList = false;
                        display.refreshThreatList(results);
                        populateAssetThreatMatrix();
                    }
                });
        exec.makeCall();
    }

    private void populateAssetThreatMatrix() {
        ClientLoggingUtil.info("Populating AssetThreatMatrix");

        new ExecutableAsyncCallback<List<AssetThreatLevelsDTO>>(busyHandler, new BaseAsyncCommand<List<AssetThreatLevelsDTO>>() {
            public void execute(AsyncCallback<List<AssetThreatLevelsDTO>> callback) {
                ServiceFactory.getInstance().getAssetService().getAssetThreatLevels(ClientSingleton.getBaselineId(), callback);
            }
            @Override
            public void handleResult(List<AssetThreatLevelsDTO> results) {
                ClientLoggingUtil.info("Number of AssetThreatLevel DTOs : " + results.size());
                assetThreatLevelsDTOs = results;
                display.setAssetThreatMatrixData(results);
                updateAssetThreatCount();
            }
        }).makeCall();

    }

}
