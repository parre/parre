/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.client.at.presenter;


import java.util.Set;

import org.parre.client.ParreClientUtil;
import org.parre.shared.AssetThreatLevelDTO;

/**
 * The Class LockedAssetThreatAnalysisState.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/28/12
 */
public class LockedAssetThreatAnalysisState implements AssetThreatAnalysisState {
    private AssetThreatMatrixPresenter.Display display;

    public LockedAssetThreatAnalysisState(AssetThreatMatrixPresenter.Display display) {
        this.display = display;
    }

    public void updateAssetThreatData(Integer thresholdValue, Set<AssetThreatLevelDTO> levelDTO) {
        ParreClientUtil.showCurrentAnalysisLockedMessage();
    }
}
