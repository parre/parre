/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * The Class FilterUtil.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 10/11/11
 */
public class FilterUtil {

    public static boolean startsWithIgnoreCase(String s1, String s2) {
        return s1.toLowerCase().startsWith(s2.toLowerCase());
    }

    public static boolean equalsIgnoreCase(String s1, String s2) {
        return s1.toLowerCase().startsWith(s2.toLowerCase());
    }

    public static boolean compareValues(String criteriaValue, String targetValue) {
        if (criteriaValue == null || criteriaValue.length() == 0) {
            return true;
        }
        if (criteriaValue.endsWith("*")) {
            return startsWithIgnoreCase(targetValue, criteriaValue.substring(0, criteriaValue.length() - 1));
        }
        else {
            return equalsIgnoreCase(targetValue, criteriaValue);
        }
    }

    public static <T> List<T> filter(SearchSpec<T> searchSpec, Collection<T> values) {
        List<T> results = new ArrayList<T>();
        for (T value : values) {
            if (searchSpec.isSatisfiedBy(value)) {
                results.add(value);
            }
        }
        return results;
    }
}
