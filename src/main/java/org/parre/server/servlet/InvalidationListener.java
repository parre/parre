/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.servlet;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 * The listener interface for receiving invalidation events.
 * The class that is interested in processing a invalidation
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addInvalidationListener<code> method. When
 * the invalidation event occurs, that object's appropriate
 * method is invoked.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/8/12
 */
public class InvalidationListener implements Serializable, HttpSessionBindingListener {
	private final static transient Logger log = Logger.getLogger(InvalidationListener.class);

    public void valueBound(HttpSessionBindingEvent httpSessionBindingEvent) {
        if (log.isInfoEnabled()) { log.info("Last Accessed Time : " + httpSessionBindingEvent.getSession().getLastAccessedTime()); }
    }

    public void valueUnbound(HttpSessionBindingEvent httpSessionBindingEvent) {
        if (log.isInfoEnabled()) { log.info("Is Timed Out : " + isTimedOut(httpSessionBindingEvent.getSession())); }
    }

    public boolean isTimedOut(HttpSession httpSession) {
        long lastAccessedTime = httpSession.getLastAccessedTime();
        long timeoutMillis = httpSession.getMaxInactiveInterval() * 60 * 1000;
        return (System.currentTimeMillis() - lastAccessedTime) > timeoutMillis;
    }
}
