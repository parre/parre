/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AttackStepDTO;
import org.parre.shared.CounterMeasureDTO;
import org.parre.shared.types.AnalysisPathStepType;

import java.math.BigDecimal;

/**
 * The Class AttackStep.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/26/12
 */
@Entity
@Table(name = "ATTACK_STEP")
public class AttackStep extends BaseEntity {
    @Column(name = "TYPE") @Enumerated(EnumType.STRING)
    private AnalysisPathStepType type;
    @Column(name = "TRAVEL_TIME", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal travelTime;
    @Column(name = "OVERCOME_TIME", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal overcomeTime;
    @Column(name = "STEP_NUMBER")
    private Integer stepNumber;
    @OneToOne(targetEntity = CounterMeasure.class)
    @JoinColumn(name = "COUNTER_MEASURE_ID")
    private CounterMeasure counterMeasure;
    @Column(name = "DETECTED")
    private Boolean detected;

    public AnalysisPathStepType getType() {
        return type;
    }

    public void setType(AnalysisPathStepType type) {
        this.type = type;
    }

    public BigDecimal getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(BigDecimal travelTime) {
        this.travelTime = travelTime;
    }

    public BigDecimal getOvercomeTime() {
        return overcomeTime;
    }

    public void setOvercomeTime(BigDecimal overcomeTime) {
        this.overcomeTime = overcomeTime;
    }

    public Integer getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(Integer stepNumber) {
        this.stepNumber = stepNumber;
    }

    public CounterMeasure getCounterMeasure() {
        return counterMeasure;
    }

    public void setCounterMeasure(CounterMeasure counterMeasure) {
        this.counterMeasure = counterMeasure;
    }

    public Boolean getDetected() {
        return detected;
    }

    public void setDetected(Boolean detected) {
        this.detected = detected;
    }

    @Override
    public <T> T copyInto(T t) {
        AttackStepDTO dto = (AttackStepDTO) super.copyInto(t);
        try {
            dto.setCounterMeasureDTO(getCounterMeasure().copyInto(new CounterMeasureDTO()));
        } catch (NullPointerException e) {
            dto.setCounterMeasureDTO(null);
        }

        return t;
    }

    public AttackStep makeCopy() {
        AttackStep copy = new AttackStep();
        copy.copyFrom(this);
        copy.setId(null);
        return copy;
    }
}
