/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.Collections;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.parre.shared.NaturalThreatAnalysisDTO;


/**
 * The Class NaturalThreatAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
@Entity
@Table(name = "NATURAL_THREAT_ANALYSIS")
public class NaturalThreatAnalysis extends Analysis {
    @OneToMany(mappedBy = "analysis")
    private Set<NaturalThreat> naturalThreats  = Collections.<NaturalThreat>emptySet();

    public NaturalThreatAnalysis(String name, String description) {
        super(name, description);
    }

    public NaturalThreatAnalysis() {
    }

    public NaturalThreatAnalysis(NaturalThreatAnalysisDTO dto) {
        super(dto);
    }


    @Override
    public Set<? extends AssetThreatEntity> getAssetThreats() {
        return naturalThreats;
    }

    @Override
    public void addAssetThreat(AssetThreatEntity assetThreat) {
        addNaturalThreat((NaturalThreat) assetThreat);
    }

    public void addNaturalThreat(NaturalThreat naturalThreat) {
        naturalThreats = add(naturalThreat, naturalThreats);
        naturalThreat.setAnalysis(this);
    }

    public Set<NaturalThreat> getNaturalThreats() {
        return naturalThreats;
    }

    public void setNaturalThreats(Set<NaturalThreat> naturalThreats) {
        this.naturalThreats = naturalThreats;
    }

	@Override
	public String toString() {
		return "NaturalThreatAnalysis [name:" + this.getName() + ", id:" + this.getId() + 
				", parreAnalsysId: " + this.getParreAnalysis().getId() + "]";
	}
    
    

}
