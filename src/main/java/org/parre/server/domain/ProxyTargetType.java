/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.ProxyTargetTypeDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class ProxyTargetType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/6/12
 */
@Entity
@Table(name = "PROXY_TARGET_TYPE")
public class ProxyTargetType extends BaseEntity {
    @Column(name = "CODE")
    private Integer code;
    @Column(name = "NAME")
    private String name;
    @Column(name = "PROBABILITY", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal probability;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static List<ProxyTargetTypeDTO> createDTOs(List<ProxyTargetType> entities) {
        List<ProxyTargetTypeDTO> dtos = new ArrayList<ProxyTargetTypeDTO>(entities.size());
        for (ProxyTargetType entity : entities) {
            dtos.add(entity.copyInto(new ProxyTargetTypeDTO()));
        }
        return dtos;

    }
}
