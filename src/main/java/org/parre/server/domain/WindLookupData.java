/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.WindLookupDataDTO;


/**
 * User: keithjones
 * Date: 2/22/12
 * Time: 3:33 PM
*/
@Entity
@Table(name = "WIND_LOOKUP_DATA")
public class WindLookupData extends BaseEntity {
    @Column(name = "LONGITUDE_AND_LATITUDE")
    private String longitudeAndLatitude;
    @Column(name = "LATITUDE")
    private String latitude;
    @Column(name = "LONGITUDE")
    private String longitude;
    @Column(name = "INTERVAL1", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval1;   //0 to 4.47
    @Column(name = "INTERVAL2", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval2;   //4.47 to 13.4
    @Column(name = "INTERVAL3", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval3;   //13.4 to 22.4
    @Column(name = "INTERVAL4", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval4;   //22.4 to 31.3
    @Column(name = "INTERVAL5", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval5;   //31.3 to 40.2
    @Column(name = "INTERVAL6", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval6;   //greater than 40.2

    public String getLongitudeAndLatitude() {
        return longitudeAndLatitude;
    }

    public void setLongitudeAndLatitude(String longitudeAndLatitude) {
        this.longitudeAndLatitude = longitudeAndLatitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public BigDecimal getInterval1() {
        return interval1;
    }

    public void setInterval1(BigDecimal interval1) {
        this.interval1 = interval1;
    }

    public BigDecimal getInterval2() {
        return interval2;
    }

    public void setInterval2(BigDecimal interval2) {
        this.interval2 = interval2;
    }

    public BigDecimal getInterval3() {
        return interval3;
    }

    public void setInterval3(BigDecimal interval3) {
        this.interval3 = interval3;
    }

    public BigDecimal getInterval4() {
        return interval4;
    }

    public void setInterval4(BigDecimal interval4) {
        this.interval4 = interval4;
    }

    public BigDecimal getInterval5() {
        return interval5;
    }

    public void setInterval5(BigDecimal interval5) {
        this.interval5 = interval5;
    }

    public BigDecimal getInterval6() {
        return interval6;
    }

    public void setInterval6(BigDecimal interval6) {
        this.interval6 = interval6;
    }

    public static List<WindLookupDataDTO> createDTOs(List<WindLookupData> entities) {
        List<WindLookupDataDTO> dtos = new ArrayList<WindLookupDataDTO>(entities.size());
        for (WindLookupData entity : entities) {
            dtos.add(entity.copyInto(new WindLookupDataDTO()));
        }
        return dtos;
    }
    
    public static WindLookupData createEmpty() {
    	WindLookupData data = new WindLookupData();
    	data.setInterval1(BigDecimal.ZERO);
    	data.setInterval2(BigDecimal.ZERO);
    	data.setInterval3(BigDecimal.ZERO);
    	data.setInterval4(BigDecimal.ZERO);
    	data.setInterval5(BigDecimal.ZERO);
    	data.setInterval6(BigDecimal.ZERO);
    	return data;
    }
    
    @Override
    public String toString() {
        return "windDataCalcsDTO{" +
                "longitudeAndLatitude='" + longitudeAndLatitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", interval1=" + interval1 +
                ", interval2=" + interval2 +
                ", interval3=" + interval3 +
                ", interval4=" + interval4 +
                ", interval5=" + interval5 +
                ", interval6=" + interval6 +
                '}';
    }
}
