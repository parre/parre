/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AttackStepDTO;
import org.parre.shared.PathAnalysisDTO;
import org.parre.shared.ResponseStepDTO;

import java.util.*;

/**
 * The Class PathAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 1/12/12
 */
@Entity
@Table(name = "PATH_ANALYSIS")
public class PathAnalysis extends NameDescription {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    @Column(name = "RESPONSE_TIME")
    private Integer responseTime;
    @OneToMany(targetEntity = AttackStep.class, cascade = {CascadeType.REMOVE})
    @JoinColumn(name = "PATH_ANALYSIS_ID")
    private Set<AttackStep> attackSteps = Collections.emptySet();

    @OneToMany(targetEntity = ResponseStep.class, cascade = {CascadeType.REMOVE})
    @JoinColumn(name = "PATH_ANALYSIS_ID")
    private Set<ResponseStep> responseSteps = Collections.emptySet();

    @ManyToMany(targetEntity = CounterMeasure.class)
    @JoinTable(name = "PATH_ANALYSIS_COUNTER_MEASURE",
            joinColumns = {@JoinColumn(name = "PATH_ANALYSIS_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COUNTER_MEASURE_ID")})
    private Set<CounterMeasure> pathAnalysisCounterMeasures = Collections.<CounterMeasure>emptySet();

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public Integer getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(Integer responseTime) {
        this.responseTime = responseTime;
    }

    public Set<AttackStep> getAttackSteps() {
        return attackSteps;
    }

    public void setAttackSteps(Set<AttackStep> steps) {
        this.attackSteps = steps;
    }

    public void addAttackStep(AttackStep analysisPathStep) {
        if (attackSteps.isEmpty()) {
            attackSteps = new HashSet<AttackStep>();
        }
        attackSteps.add(analysisPathStep);
    }

    public Set<ResponseStep> getResponseSteps() {
        return responseSteps;
    }

    public void setResponseSteps(Set<ResponseStep> responseSteps) {
        this.responseSteps = responseSteps;
    }

    public void addResponseStep(ResponseStep responseStep) {
        if (responseSteps.isEmpty()) {
            responseSteps = new HashSet<ResponseStep>();
        }
        responseSteps.add(responseStep);
    }

    public PathAnalysisDTO createDTO() {
        return copyInto(new PathAnalysisDTO());
    }

    public PathAnalysisDTO buildPathDTO() {
        PathAnalysisDTO pathDTO = createDTO();
        List<AttackStepDTO> attackStepDTOs = new ArrayList<AttackStepDTO>(attackSteps.size());
        for (AttackStep attackStep : attackSteps) {
            attackStepDTOs.add(attackStep.copyInto(new AttackStepDTO(getId())));
        }
        pathDTO.setAttackStepDTOs(attackStepDTOs);
        pathDTO.updateAttackSteps();
        List<ResponseStepDTO> responseStepDTOs = new ArrayList<ResponseStepDTO>(getResponseSteps().size());
        for (ResponseStep responseStep : responseSteps) {
            responseStepDTOs.add(responseStep.copyInto(new ResponseStepDTO(getId())));
        }
        pathDTO.setResponseStepDTOs(responseStepDTOs);
        pathDTO.sortResponseStepDTOs();
        return pathDTO;
    }

    public PathAnalysis makeCopy() {
        PathAnalysis copy = new PathAnalysis();
        copy.copyFrom(this);
        copy.setName(copy.getName() + " - Copy");
        copy.setId(null);
        copy.setAttackSteps(Collections.<AttackStep>emptySet());
        copy.setResponseSteps(Collections.<ResponseStep>emptySet());
        copy.setPathAnalysisCounterMeasures(Collections.<CounterMeasure>emptySet());

        for (AttackStep attackStep : attackSteps) {
            copy.addAttackStep(attackStep.makeCopy());
        }

        for (ResponseStep responseStep : responseSteps) {
            copy.addResponseStep(responseStep.makeCopy());
        }
        
        for(CounterMeasure counterMeasure : pathAnalysisCounterMeasures){
        	copy.addCounterMeasure(counterMeasure);  // Should re-use counter measures by default...
        }
        
        return copy;
    }

    public static List<PathAnalysisDTO> createDTOs(List<PathAnalysis> entities) {
        List<PathAnalysisDTO> dtos = new ArrayList<PathAnalysisDTO>(entities.size());
        for (PathAnalysis entity : entities) {
            dtos.add(entity.createDTO());
        }
        return dtos;

    }

    public Set<CounterMeasure> getPathAnalysisCounterMeasures() {
        return pathAnalysisCounterMeasures;
    }

    public void setPathAnalysisCounterMeasures(Set<CounterMeasure> pathAnalysisCounterMeasures) {
        this.pathAnalysisCounterMeasures = pathAnalysisCounterMeasures;
    }

    public void addCounterMeasure(CounterMeasure counterMeasure) {
        if(pathAnalysisCounterMeasures.isEmpty()) {
            pathAnalysisCounterMeasures = new HashSet<CounterMeasure>();
        }
        pathAnalysisCounterMeasures.add(counterMeasure);
        counterMeasure.addPathAnalysis(this);
    }

    public void addCounterMeasureSimple(CounterMeasure counterMeasure) {
    	if(pathAnalysisCounterMeasures.isEmpty()) {
    		pathAnalysisCounterMeasures = new HashSet<CounterMeasure>();
    	}
    	pathAnalysisCounterMeasures.add(counterMeasure);
    }
}
