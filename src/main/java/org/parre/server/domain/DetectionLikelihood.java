/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import org.apache.log4j.Logger;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The Class DetectionLikelihood.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/6/12
 */
@Entity
@Table(name = "DETECTION_LIKELIHOOD")
public class DetectionLikelihood extends BaseEntity {
    @OneToOne(targetEntity = Threat.class)
    @JoinColumn(name = "THREAT_ID")
    private Threat threat;
    @Column(name = "LIKELIHOOD", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal likelihood;

    public Threat getThreat() {
        return threat;
    }

    public void setThreat(Threat threat) {
        this.threat = threat;
    }

    public BigDecimal getLikelihood() {
        return likelihood;
    }

    public void setLikelihood(BigDecimal likelihood) {
        this.likelihood = likelihood;
    }

	@Override
	public String toString() {
		return "DetectionLikelihood [threat=" + threat + ", likelihood="
				+ likelihood + "]";
	}
}
