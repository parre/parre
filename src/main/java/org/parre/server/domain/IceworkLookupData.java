/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.BaseDTO;
import org.parre.shared.IceworkLookupDataDTO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * User: keithjones
 * Date: 2/22/12
 * Time: 2:11 PM
*/
@Entity
@Table(name = "ICEWORK_LOOKUP_DATA")
public class IceworkLookupData extends BaseEntity {
    @Column(name = "STATE")
    private String state;
    @Column(name = "INITIALS")
    private String initials;
    @Column(name = "INTERVAL1", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval1;   //.1  <= X < .25
    @Column(name = "INTERVAL2", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval2;   //.25 <= X < .5
    @Column(name = "INTERVAL3", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval3;   //.5  <= X < .75
    @Column(name = "INTERVAL4", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval4;   //.75 <= X < 1
    @Column(name = "INTERVAL5", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval5;   //1   <= X < 1.5
    @Column(name = "INTERVAL6", columnDefinition = "DECIMAL(10,9)")
    private BigDecimal interval6;   //1.5 <= X


    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public BigDecimal getInterval1() {
        return interval1;
    }

    public void setInterval1(BigDecimal interval1) {
        this.interval1 = interval1;
    }

    public BigDecimal getInterval2() {
        return interval2;
    }

    public void setInterval2(BigDecimal interval2) {
        this.interval2 = interval2;
    }

    public BigDecimal getInterval3() {
        return interval3;
    }

    public void setInterval3(BigDecimal interval3) {
        this.interval3 = interval3;
    }

    public BigDecimal getInterval4() {
        return interval4;
    }

    public void setInterval4(BigDecimal interval4) {
        this.interval4 = interval4;
    }

    public BigDecimal getInterval5() {
        return interval5;
    }

    public void setInterval5(BigDecimal interval5) {
        this.interval5 = interval5;
    }

    public BigDecimal getInterval6() {
        return interval6;
    }

    public void setInterval6(BigDecimal interval6) {
        this.interval6 = interval6;
    }

    public static List<IceworkLookupDataDTO> createDTOs(List<IceworkLookupData> entities) {
        List<IceworkLookupDataDTO> dtos = new ArrayList<IceworkLookupDataDTO>(entities.size());
        for (IceworkLookupData entity : entities) {
            dtos.add(entity.copyInto(new IceworkLookupDataDTO()));
        }
        return dtos;
    }

    @Override
    public String toString() {
        return "IceworkLookupData{" +
                "state='" + state + '\'' +
                ", initials='" + initials + '\'' +
                ", interval1=" + interval1 +
                ", interval2=" + interval2 +
                ", interval3=" + interval3 +
                ", interval4=" + interval4 +
                ", interval5=" + interval5 +
                ", interval6=" + interval6 +
                '}';
    }
}