/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.parre.server.domain.util.DataObject;
import org.parre.server.domain.util.EntityObject;
import org.parre.shared.BaseDTO;


/**
 * The Class BaseEntity.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/14/11
 */
@MappedSuperclass
public abstract class BaseEntity extends DataObject implements EntityObject {
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    public BaseEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

     /**
     * determines that the passed dto has the same id. User of this
     * method must take care to only pass the relevant dto.
     *
     * @param dto
     * @return true if dto has same id, false otherwise
     */
    public boolean hasSameIdentity(BaseDTO dto) {
        if (dto == null) {
            return false;
        }
        if (isNew() || dto.isNew()) {
            return false;
        }

        if (dto.getId().getClass() != getId().getClass()) {
            return false;
        }

        return getId().equals(dto.getId());
    }

    @Override
    public boolean equals(Object obj) {
        return getClass().isInstance(obj) && !isNew() && ((EntityObject) obj).getId().equals(getId());
    }

    @Override
    public int hashCode() {
        if (isNew()) {
            return super.hashCode();
        }
        else {
            return getId().hashCode();
        }
    }

    public boolean isNew() {
        Object id = getId();
        if (id == null) {
            return true;
        }
        else if (id instanceof Number) {
            return ((Number) id).intValue() == 0;
        }
        else if (id instanceof String) {
            return ((String) id).length() == 0;
        }
        else if (id instanceof EntityObject) {
            return ((EntityObject) id).isNew();
        }
        return false;
    }

    public boolean hasIdentity(Long id) {
        return this.id.equals(id);
    }

    public static <T> Set<T> add(T value, Set<T> set) {
        if (set == null || set.isEmpty()) {
            set = new HashSet<T>();
        }
        set.add(value);
        return set;
    }
    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append("[" + getId() + "]");
        return sb.toString();
    }

    public static List<Long> extractIds(Collection<? extends BaseEntity> entities) {
        List<Long> ids = new ArrayList<Long>(entities.size());
        for (BaseEntity entity : entities) {
            ids.add(entity.getId());
        }
        return ids;
    }
}
