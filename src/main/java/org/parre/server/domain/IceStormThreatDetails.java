/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The Class IceStormThreatDetails.
 */
@Entity
@Table(name = "ICE_STORM_THREAT_DETAILS")
public class IceStormThreatDetails extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	public IceStormThreatDetails() {
		super();
	}
	
	public IceStormThreatDetails(NaturalThreat naturalThreat) {
		super();
		this.naturalThreat = naturalThreat;
	}
	
	@OneToOne(targetEntity = NaturalThreat.class)
    @JoinColumn(name = "NATURAL_THREAT_ID")
    private NaturalThreat naturalThreat;

	@Column (name = "LATITUDE", columnDefinition = "DECIMAL(10,5)")
	private BigDecimal latitude;
	
	@Column (name = "LONGITUDE", columnDefinition = "DECIMAL(10,5)")
    private BigDecimal longitude;
	
	@Column (name = "STATE_CODE", length = 2)
    private String stateCode;
    
	@Column (name ="DAYS_OF_BACKUP_POWER")
    private BigDecimal daysOfBackupPower = BigDecimal.ZERO;
	
	@Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "DAILY_LOSS_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
            @AttributeOverride(name = "unit", column = @Column(name = "DAILY_LOSS_UNIT"))
    })
    private Amount dailyOperationalLoss = Amount.ZERO;
    
	@Column (name = "INDEX3_RECOVERY_TIME", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal index3RecoveryTime = BigDecimal.ZERO;
	
	@Column (name = "INDEX3_FATALITIES")
	private Integer index3Fatalities = 0;
	
	@Column (name = "INDEX3_SERIOUS_INJURIES")
	private Integer index3SeriousInjuries = 0;
	
	@Column (name = "INDEX4_RECOVERY_TIME", columnDefinition = "DECIMAL(8,2)")
	private BigDecimal index4RecoveryTime = BigDecimal.ZERO;
	
	@Column (name = "INDEX4_FATALITIES")
	private Integer index4Fatalities = 0;
	
	@Column (name = "INDEX4_SERIOUS_INJURIES")
	private Integer index4SeriousInjuries = 0;
	
	@Column (name = "INDEX5_RECOVERY_TIME", columnDefinition = "DECIMAL(8,2)")
	private BigDecimal index5RecoveryTime = BigDecimal.ZERO;
	
	@Column (name = "INDEX5_FATALITIES")
	private Integer index5Fatalities = 0;
	
	@Column (name = "INDEX5_SERIOUS_INJURIES")
	private Integer index5SeriousInjuries = 0;

	public NaturalThreat getNaturalThreat() {
		return naturalThreat;
	}

	public void setNaturalThreat(NaturalThreat naturalThreat) {
		this.naturalThreat = naturalThreat;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public BigDecimal getDaysOfBackupPower() {
		return daysOfBackupPower;
	}

	public void setDaysOfBackupPower(BigDecimal daysOfBackupPower) {
		this.daysOfBackupPower = daysOfBackupPower;
	}

	public Amount getDailyOperationalLoss() {
		return dailyOperationalLoss;
	}

	public void setDailyOperationalLoss(Amount dailyOperationalLoss) {
		this.dailyOperationalLoss = dailyOperationalLoss;
	}

	public BigDecimal getIndex3RecoveryTime() {
		return index3RecoveryTime;
	}

	public void setIndex3RecoveryTime(BigDecimal index3RecoveryTime) {
		this.index3RecoveryTime = index3RecoveryTime;
	}

	public Integer getIndex3Fatalities() {
		return index3Fatalities;
	}

	public void setIndex3Fatalities(Integer index3Fatalities) {
		this.index3Fatalities = index3Fatalities;
	}

	public Integer getIndex3SeriousInjuries() {
		return index3SeriousInjuries;
	}

	public void setIndex3SeriousInjuries(Integer index3SeriousInjuries) {
		this.index3SeriousInjuries = index3SeriousInjuries;
	}

	public BigDecimal getIndex4RecoveryTime() {
		return index4RecoveryTime;
	}

	public void setIndex4RecoveryTime(BigDecimal index4RecoveryTime) {
		this.index4RecoveryTime = index4RecoveryTime;
	}

	public Integer getIndex4Fatalities() {
		return index4Fatalities;
	}

	public void setIndex4Fatalities(Integer index4Fatalities) {
		this.index4Fatalities = index4Fatalities;
	}

	public Integer getIndex4SeriousInjuries() {
		return index4SeriousInjuries;
	}

	public void setIndex4SeriousInjuries(Integer index4SeriousInjuries) {
		this.index4SeriousInjuries = index4SeriousInjuries;
	}

	public BigDecimal getIndex5RecoveryTime() {
		return index5RecoveryTime;
	}

	public void setIndex5RecoveryTime(BigDecimal index5RecoveryTime) {
		this.index5RecoveryTime = index5RecoveryTime;
	}

	public Integer getIndex5Fatalities() {
		return index5Fatalities;
	}

	public void setIndex5Fatalities(Integer index5Fatalities) {
		this.index5Fatalities = index5Fatalities;
	}

	public Integer getIndex5SeriousInjuries() {
		return index5SeriousInjuries;
	}

	public void setIndex5SeriousInjuries(Integer index5SeriousInjuries) {
		this.index5SeriousInjuries = index5SeriousInjuries;
	}
	
	
	
}
