/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AmountDTO;
import org.parre.shared.AssetDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.types.AssetType;
import org.parre.shared.types.PhysicalAssetType;

import java.math.BigDecimal;

/**
 * The Class PhysicalResource.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/22/11
 */
@Entity
@DiscriminatorValue(value = "PhysicalResource")
public class PhysicalResource extends Asset {
    @Column (name = "YEAR_BUILT")
    private Integer yearBuilt;
    @Column (name = "POSTAL_CODE")
    private String postalCode;
    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="REPL_COST_QUANTITY", columnDefinition = "DECIMAL(16,2)") ),
            @AttributeOverride(name="unit", column = @Column(name="REPL_COST_UNIT") )
    } )
    private Amount replacementCost;
    @Column (name = "DAMAGE_FACTOR", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal damageFactor;
    @Column (name = "LATITUDE", columnDefinition = "DECIMAL(10,5)")
    private BigDecimal latitude;
    @Column (name = "LONGITUDE", columnDefinition = "DECIMAL(10, 5)")
    private BigDecimal longitude;
    @Column (name = "PHYSICAL_ASSET_TYPE") @Enumerated(EnumType.STRING)
    private PhysicalAssetType physicalAssetType;
    
    
    public PhysicalResource() {
    }

    public PhysicalResource(int yearBuilt, AmountDTO replacementCostDTO, String postalCode, BigDecimal damageFactor) {
        this.yearBuilt = yearBuilt;
        this.postalCode = postalCode;
        this.damageFactor = damageFactor;
        setReplacementCostDTO(replacementCostDTO);
    }

    public AssetType getAssetType() {
        return AssetType.PHYSICAL_ASSET;
    }

    public Amount getReplacementCost() {
        return replacementCost;
    }

    public void setReplacementCost(Amount replacementCost) {
        this.replacementCost = replacementCost;
    }

    public AmountDTO getReplacementCostDTO() {
        return replacementCost.createDTO();
    }

    public void setReplacementCostDTO(AmountDTO amountDTO) {
        this.replacementCost = new Amount(amountDTO);
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public Integer getYearBuilt() {
        return yearBuilt;
    }

    public void setYearBuilt(Integer yearBuilt) {
        this.yearBuilt = yearBuilt;
    }

    @Override
    public String toString() {
        return "PhysicalResource{" +
                "yearBuilt=" + yearBuilt +
                ", postalCode='" + postalCode + '\'' +
                ", replacementCost=" + replacementCost +
                ", damageFactor=" + damageFactor +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }

    @Override
    protected AssetDTO createDTO() {
        return new PhysicalResourceDTO();
    }

    public BigDecimal getDamageFactor() {
        return damageFactor;
    }

    public void setDamageFactor(BigDecimal damageFactor) {
        this.damageFactor = damageFactor;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

	public PhysicalAssetType getPhysicalAssetType() {
		return physicalAssetType;
	}

	public void setPhysicalAssetType(PhysicalAssetType physicalAssetType) {
		this.physicalAssetType = physicalAssetType;
	}


}
