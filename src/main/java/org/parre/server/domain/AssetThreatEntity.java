/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;



import javax.persistence.JoinColumn;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

import org.parre.shared.AssetDTO;
import org.parre.shared.AssetThreatDTO;

/**
 * The Class AssetThreatEntity.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/21/12
 */
@MappedSuperclass
public abstract class AssetThreatEntity extends BaseEntity {
    @OneToOne(targetEntity = AssetThreat.class)
    @JoinColumn(name = "ASSET_THREAT_ID")
    private AssetThreat assetThreat;
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;

    protected AssetThreatEntity() {
    }

    public AssetThreatEntity(AssetThreat assetThreat) {
        this.assetThreat = assetThreat;
    }

    public AssetThreat getAssetThreat() {
        return assetThreat;
    }

    public void setAssetThreat(AssetThreat assetThreat) {
        this.assetThreat = assetThreat;
    }

    public Asset getAsset() {
        return assetThreat.getAsset();
    }

    public Threat getThreat() {
        return assetThreat.getThreat();
    }

    public Boolean getRemoved() {
        return assetThreat.getRemoved();
    }

    public boolean isFor(AssetThreat assetThreat) {
        return this.assetThreat.equals(assetThreat);
    }

    public void populate(AssetThreatDTO dto) {
        assetThreat.populate(dto);
    }

    public boolean hasIdenticalAssetThreat(AssetThreatEntity assetThreatEntity) {
        return assetThreat.equals(assetThreatEntity.assetThreat);
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }
}
