/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.parre.shared.types.ParreAnalysisStatusType;

@Entity
@Table(name = "PARRE_ANALYSIS_REPORT")
public class ParreAnalysisReport extends NameDescription {
	private static final long serialVersionUID = 1L;

	@OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;
    
    @Column(name = "CREATED_TIME", insertable = true, updatable = false) @Temporal(TemporalType.TIMESTAMP)
    private Date createdTime;
    
    @Column(name = "STATUS") @Enumerated(EnumType.STRING)
    private ParreAnalysisStatusType status;
    
    @OneToMany(mappedBy = "parreAnalysisReport", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<ParreAnalysisReportSection> sections;
    
	public ParreAnalysisReport() {
	}

	public ParreAnalysis getParreAnalysis() {
		return parreAnalysis;
	}

	public void setParreAnalysis(ParreAnalysis parreAnalysis) {
		this.parreAnalysis = parreAnalysis;
	}

	public Date getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	public ParreAnalysisStatusType getStatus() {
		return status;
	}

	public void setStatus(ParreAnalysisStatusType status) {
		this.status = status;
	}

	public List<ParreAnalysisReportSection> getSections() {
		return sections;
	}

	public void setSections(List<ParreAnalysisReportSection> sections) {
		this.sections = sections;
	}

	@Override
	public String toString() {
		return "ParreAnalysisReport [parreAnalysis=" + parreAnalysis
				+ ", createdTime=" + createdTime + ", status=" + status
				+ ", sections=" + sections + "]";
	}
}
