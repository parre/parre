/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.AmountDTO;
import org.parre.shared.RiskBenefitOptionDTO;

import java.math.BigDecimal;
import java.util.Set;

/**
 * The Class RiskBenefitOption.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
@Entity
@Table(name = "RISK_BENEFIT_OPTION")
public class RiskBenefitOption extends BaseEntity {
    @OneToOne(targetEntity = ParreAnalysis.class)
    @JoinColumn(name = "PARRE_ANALYSIS_ID")
    private ParreAnalysis parreAnalysis;

    @ManyToOne(targetEntity = RiskBenefit.class)
    @JoinColumn(name = "RISK_BENEFIT_ID")
    private RiskBenefit riskBenefit;

    @Embedded
    @AttributeOverrides( {
            @AttributeOverride(name="quantity", column = @Column(name="OPTION_RISK_Q", columnDefinition = "DECIMAL(10,2)") ),
            @AttributeOverride(name="unit", column = @Column(name="OPTION_RISK_U") )
    })
    private Amount optionRisk;

    public RiskBenefitOption() {
    }

    public RiskBenefitOption(ParreAnalysis optionAnalysis, Amount optionRisk) {
        this.parreAnalysis = optionAnalysis;
        this.optionRisk = optionRisk;
    }

    public ParreAnalysis getParreAnalysis() {
        return parreAnalysis;
    }

    public void setParreAnalysis(ParreAnalysis parreAnalysis) {
        this.parreAnalysis = parreAnalysis;
    }

    public RiskBenefit getRiskBenefit() {
        return riskBenefit;
    }

    public void setRiskBenefit(RiskBenefit riskBenefit) {
        this.riskBenefit = riskBenefit;
    }

    public Amount getOptionRisk() {
        return optionRisk;
    }

    public void setOptionRisk(Amount optionRisk) {
        this.optionRisk = optionRisk;
    }

    public AmountDTO getOptionRiskDTO() {
        return optionRisk.createDTO();
    }

    public RiskBenefitOptionDTO createDTO() {
        RiskBenefitOptionDTO optionDTO = new RiskBenefitOptionDTO();
        optionDTO.setParreAnalysisId(parreAnalysis.getId());
        optionDTO.updateId();
        optionDTO.setDisplayOrder(parreAnalysis.getDisplayOrder());
        optionDTO.setOptionAmountDTO(getOptionRiskDTO());
        return optionDTO;
    }
}
