/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;


import javax.persistence.*;

import org.parre.shared.NameDescriptionDTO;

import java.util.Collections;
import java.util.Set;

/**
 * The Class DirectedThreatAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/15/11
 */
@Entity
@Table(name = "DIRECTED_THREAT_ANALYSIS")
public class DirectedThreatAnalysis extends Analysis {
    @OneToMany(mappedBy = "analysis")
    private Set<DirectedThreat> directedThreats = Collections.<DirectedThreat>emptySet();

    public DirectedThreatAnalysis() {

    }

    public DirectedThreatAnalysis(NameDescriptionDTO dto) {
        super(dto.getName(), dto.getDescription());
    }

    public DirectedThreatAnalysis(String name, String description) {
        super(name, description);
    }

    @Override
    public Set<? extends AssetThreatEntity> getAssetThreats() {
        return directedThreats;
    }

    @Override
    public void addAssetThreat(AssetThreatEntity assetThreat) {
        addDirectedThreat((DirectedThreat) assetThreat);
    }


    public Set<DirectedThreat> getDirectedThreats() {
        return directedThreats;
    }

    public void setDirectedThreats(Set<DirectedThreat> consequences) {
        this.directedThreats = consequences;
    }

    public void addDirectedThreat(DirectedThreat consequence) {
        directedThreats = add(consequence, directedThreats);
        consequence.setAnalysis(this);
    }

    public boolean hasDirectedThreatFor(AssetThreat assetThreat) {
        for (DirectedThreat consequence : directedThreats) {
            if (consequence.isFor(assetThreat)) {
                return true;
            }
        }
        return false;
    }
}
