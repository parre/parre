/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetNote;
import org.parre.server.domain.ZipLookupData;
import org.parre.server.util.GeneralUtil;
import org.parre.server.util.LoggingUtil;
import org.parre.shared.AssetSearchDTO;


/**
 * The Class AssetManager.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/23/11
 */
@SuppressWarnings("unchecked")
public class AssetManager extends ParreManager {
    private static transient final Logger log = Logger.getLogger(AssetManager.class);

    public static List<Asset> searchAssets(AssetSearchDTO searchDTO, EntityManager entityManager) {
        String queryString = "select asset from Asset asset where asset.removed = :removed" +
                " and asset.parreAnalysis.id = :parreAnalysisId";
        StringBuilder builder = new StringBuilder(queryString);
        boolean hasWhere = true;
        boolean hasAssetId = !GeneralUtil.isNullOrEmpty(searchDTO.getAssetId());
        if (hasAssetId) {
            hasWhere = addCriteria("asset", "assetId", searchDTO.getAssetId(), hasWhere, builder);
        }

        boolean hasAssetName = !GeneralUtil.isNullOrEmpty(searchDTO.getAssetName());
        if (hasAssetName) {
            hasWhere = addCriteria("asset", "name", searchDTO.getAssetName(), hasWhere, builder);
        }

        queryString = builder.toString();
        LoggingUtil.debugLogMessage(log, "Query=", queryString);
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", searchDTO.getRemoved());
        query.setParameter("parreAnalysisId", searchDTO.getParreAnalysisId());
        List<Asset> resultList = query.getResultList();
        return resultList;
    }

    public static Asset findAsset(String assetId, Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select asset from Asset asset where asset.assetId = :assetId" +
                " and asset.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("assetId", assetId);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<Asset> resultList = query.getResultList();
        return getSingleResultWithNull(resultList);
    }

    public static List<Asset> getAssets(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select asset from Asset asset" +
                " where asset.removed = :removed and asset.parreAnalysis.id = :parreAnalysisId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("removed", Boolean.FALSE);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        return query.getResultList();
    }

    public static List<AssetNote> getAssetNotes(Long assetId, EntityManager entityManager) {
        String queryString = "select assetNote from AssetNote assetNote" +
                " where assetNote.asset.id = :assetId";
        Query query = entityManager.createQuery(queryString);
        query.setParameter("assetId", assetId);
        return query.getResultList();
    }
    
    public static List<ZipLookupData> getZipLookupData(EntityManager entityManager) {
    	String queryString = "select zip from ZipLookupData zip";
    	return entityManager.createQuery(queryString).getResultList();
    }

    public static ZipLookupData getSpecificZipLookupData(String lookupValue, EntityManager entityManager) {
        String queryString = "select zip from ZipLookupData zip where zip.zipCode = :lookupValue";
        try {
            return (ZipLookupData) entityManager.createQuery(queryString).setParameter("lookupValue", lookupValue).getSingleResult();
        } catch (NoResultException e) {
            log.error("No ZIP Code found.");
            return new ZipLookupData();
        } catch (NonUniqueResultException e) {
            List<ZipLookupData> data = (List<ZipLookupData>) entityManager.createQuery(queryString).setParameter("lookupValue", lookupValue).getResultList();
            return data.get(0);
        }

    }
    
    public static void deleteAsset(Asset asset, EntityManager entityManager) {
    	log.info("Removing asset: " + asset.getId());
    	
    	asset.getAssetNotes().clear();
        entityManager.persist(asset);        
        
    	int result = 0;
    	
    	result = entityManager.createQuery("delete from NaturalThreat nt where nt.assetThreat in (select at from AssetThreat at where at.asset = :asset)")
    							.setParameter("asset", asset)
    							.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " NaturalThreat."); }
    	
    	result = entityManager.createQuery("delete from Dp dp where dp.assetThreat in (select at from AssetThreat at where at.asset = :asset)")
    			.setParameter("asset", asset)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " Dp entities."); }
    	
    	result = entityManager.createQuery("delete from DirectedThreat dt where dt.assetThreat in (select at from AssetThreat at where at.asset = :asset)")
    			.setParameter("asset", asset)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " DirectedThreats."); }
    	
    	result = entityManager.createQuery("delete from RiskBenefit rb where rb.assetThreat in (select at from AssetThreat at where at.asset = :asset)")
    			.setParameter("asset", asset)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " RiskBenefit."); }
    	
    	result = entityManager.createQuery("delete from RiskResilience rr where rr.assetThreat in (select at from AssetThreat at where at.asset = :asset)")
    			.setParameter("asset", asset)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " RiskResiliences."); }

        result = entityManager.createQuery("delete from AssetThreatLevel atl where atl.assetId = :assetId")
    							.setParameter("assetId", asset.getId())
    							.executeUpdate();
        if (log.isDebugEnabled()) { log.debug("Removed : " + result + " asset threat levels."); }

    	result = entityManager.createQuery("delete from AssetThreat at where at.asset = :asset")
    							.setParameter("asset", asset)
    							.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed : " + result + " asset threats."); }
        entityManager.remove(asset);
    }

    public static void deleteAssetThreatPairsWithParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        log.info("Removing asset/threat pairs from parreAnalysisId = " + parreAnalysisId);

        int result = 0;

        result = entityManager.createQuery("delete from IceStormThreatDetails istd where istd.naturalThreat.id in " +
                "(select ata.id from AssetThreatAnalysis ata where ata.parreAnalysis.id = :parreAnalysisId)")
                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();
        if (log.isDebugEnabled()) { log.debug("Removed " + result + " Ice Storm Threat Details."); }

    	result = entityManager.createQuery("delete from NaturalThreat nt where nt.parreAnalysis.id = :parreAnalysisId")
    							.setParameter("parreAnalysisId", parreAnalysisId)
    							.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " NaturalThreat."); }

    	result = entityManager.createQuery("delete from Dp dp where dp.parreAnalysis.id = :parreAnalysisId")
    			.setParameter("parreAnalysisId", parreAnalysisId)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " Dp entities."); }

        entityManager.createNativeQuery("delete from public.directed_threat_counter_measure where directed_threat_id in " +
                "(select id from public.asset_threat_analysis where parre_analysis_id = :parreAnalysisId)")
                .setParameter("parreAnalysisId", parreAnalysisId).executeUpdate();

    	result = entityManager.createQuery("delete from DirectedThreat dt where dt.parreAnalysis.id = :parreAnalysisId")
    			.setParameter("parreAnalysisId", parreAnalysisId)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " DirectedThreats."); }

    	result = entityManager.createQuery("delete from RiskBenefit rb where rb.parreAnalysis.id = :parreAnalysisId")
    			.setParameter("parreAnalysisId", parreAnalysisId)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " RiskBenefit."); }

    	result = entityManager.createQuery("delete from RiskResilience rr where rr.parreAnalysis.id = :parreAnalysisId")
    			.setParameter("parreAnalysisId", parreAnalysisId)
    			.executeUpdate();
    	if (log.isDebugEnabled()) { log.debug("Removed " + result + " RiskResiliences."); }
    }

    public static void deleteAssetWithParreAnalysis(Long parreAnalysisId, EntityManager entityManager) {
        int result = 0;

        result = entityManager.createQuery("delete from AssetThreatLevel atl where atl.parreAnalysisId = :parreAnalysisId")
                                .setParameter("parreAnalysisId", parreAnalysisId)
                                .executeUpdate();
        if (log.isDebugEnabled()) { log.debug("Removed : " + result + " asset threat levels."); }

        result = entityManager.createQuery("delete from AssetThreat at where at.parreAnalysis.id = :parreAnalysisId")
    							.setParameter("parreAnalysisId", parreAnalysisId)
    							.executeUpdate();
        if (log.isDebugEnabled()) { log.debug("Removed : " + result + " asset threats."); }
        
        result = entityManager.createQuery("delete from AssetNote an where an.asset in (select a from Asset a where a.parreAnalysis.id = :parreAnalysisId)")
        				.setParameter("parreAnalysisId", parreAnalysisId)
        				.executeUpdate();
        
        if (log.isDebugEnabled()) { log.debug("Removed " + result + " asset notes."); }

        result = entityManager.createQuery("delete from Asset a where a.parreAnalysis.id = :parreAnalysisId")
    							.setParameter("parreAnalysisId", parreAnalysisId)
    							.executeUpdate();
        if (log.isDebugEnabled()) { log.debug("Removed : " + result + " assets."); }
    }
    
    public static List<String> getAssetSystemTypes(Long parreAnalysisId, EntityManager entityManager) {
        String queryString = "select distinct asset.systemType from Asset asset where asset.parreAnalysis.id = :parreAnalysisId" +
		" and asset.systemType NOT IN ('Physical Asset','External Asset','Human Asset') ORDER BY asset.systemType" ;
        Query query = entityManager.createQuery(queryString);
        query.setParameter("parreAnalysisId", parreAnalysisId);
        List<String> assetSystemTypes =  query.getResultList();
        return assetSystemTypes;
    }
    
}
