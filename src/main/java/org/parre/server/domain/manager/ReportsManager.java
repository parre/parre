/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.parre.client.messaging.ReportsService;
import org.parre.client.messaging.RiskBenefitService;
import org.parre.server.domain.Asset;
import org.parre.server.domain.CounterMeasure;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.ParreAnalysisReport;
import org.parre.server.domain.ParreAnalysisReportSection;
import org.parre.server.domain.ReportAssessmentTask;
import org.parre.server.domain.ReportInvestmentOption;
import org.parre.server.domain.ReportSectionLookup;
import org.parre.server.domain.RiskResilienceAnalysis;
import org.parre.server.domain.Threat;
import org.parre.server.messaging.service.ReportsServiceImpl;
import org.parre.server.messaging.service.RiskBenefitServiceImpl;
import org.parre.server.util.GeneralUtil;
import org.parre.shared.BenefitCostDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.ReportAssessmentTaskDTO;
import org.parre.shared.ReportInvestmentOptionDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptCountermeasureDTO;
import org.parre.shared.RptFatalitiesDTO;
import org.parre.shared.RptInjuriesDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptNaturalThreatsDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptResilienceDTO;
import org.parre.shared.RptRiskDTO;
import org.parre.shared.RptThreatDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptTop20Risks;
import org.parre.shared.RptVulnerabilityDTO;
import org.parre.shared.UriQuestionDTO;
import org.parre.shared.UriResponseDTO;
import org.parre.shared.VulnerabilityDTO;

/**
 * This class ..
 * 
 * @author Raju Kanumuri
 * @version 1.0 10/29/2013
 */
@SuppressWarnings("unchecked")
public class ReportsManager extends ParreManager {
	private static transient final Logger log = Logger.getLogger(ReportsManager.class);
	private static ReportsService reportsService = ReportsServiceImpl.getInstance();
	
	public static List<RptAssetDTO> getCriticalAssets(Long parreAnalysisId,
			EntityManager entityManager) {
		String queryString = "select asset from Asset asset where asset.removed = :removed and "
				+ " asset.critical = :critical and asset.parreAnalysis.id = :parreAnalysisId "
				+ " ORDER BY asset.systemType " ;
		Query query = entityManager.createQuery(queryString);
		query.setParameter("removed", Boolean.FALSE);
		query.setParameter("critical", Boolean.TRUE);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		List<Asset> results = query.getResultList();

		List<RptAssetDTO> criticalAssets = new ArrayList<RptAssetDTO>();
		for (Asset asset : results) {
			criticalAssets.add(new RptAssetDTO(String.valueOf(asset.getSystemType()), asset.getName()));
		}
		return criticalAssets;
	}
	
	public static ParreAnalysisReport getAnalysisReport(Long parreAnalysisId, EntityManager entityManager) {
		try {
			return (ParreAnalysisReport) entityManager.createQuery(
					"select par from ParreAnalysisReport par" +
					" where par.parreAnalysis.id = :parreAnalysisId")
				.setParameter("parreAnalysisId", parreAnalysisId)
				.getSingleResult();
		} catch (NoResultException nre) {
			ParreAnalysisReport report = new ParreAnalysisReport();
			ParreAnalysis parreAnalysis = entityManager.find(ParreAnalysis.class, parreAnalysisId);
			report.setParreAnalysis(parreAnalysis);
			report.setCreatedTime(new Date());
			report.setStatus(parreAnalysis.getStatus());
			report.setName("Report for: " + parreAnalysis.getName());
			entityManager.persist(report);
			entityManager.flush();
			return report;
		}
	}
	
	
	public static List<ReportInvestmentOptionDTO> getRptInvOptions(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select options from ReportInvestmentOption options" +
		" where options.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		List<ReportInvestmentOption> resultList = query.getResultList();
		List<ReportInvestmentOptionDTO> dtos = new ArrayList<ReportInvestmentOptionDTO>(resultList.size());
        for (ReportInvestmentOption record : resultList) {
        	ReportInvestmentOptionDTO dto = new ReportInvestmentOptionDTO();
            dto.setReportId(record.getParreAnalysisReport().getParreAnalysis().getId());
            dto.setInvestOption(record.getInvestOption());
            dto.setCountermeasure(record.getCountermeasure());
            dto.setSitesApplied(record.getSitesApplied());
           // dto.setSiteCost(record.getSiteCost());
           // dto.setTotalInvestment(record.getTotalInvestment());
           // log.info("record.getSiteCost() = "+ record.getSiteCost());
           // log.info("record.getTotalInvestment() = "+ record.getTotalInvestment());
            
            if(record.getSiteCost() != null && !record.getSiteCost().isEmpty() && !record.getSiteCost().equals("-")){
            	dto.setSiteCost(GeneralUtil.displayFormat(new BigDecimal(record.getSiteCost())));
            } else {
            	dto.setSiteCost(record.getSiteCost());
            }
            
            
            if(record.getTotalInvestment() != null && !record.getTotalInvestment().isEmpty() && !record.getTotalInvestment().equals("-")){
            	dto.setTotalInvestment(GeneralUtil.displayFormat(new BigDecimal(record.getTotalInvestment())));
            } else{
            	dto.setTotalInvestment(record.getTotalInvestment());
            }
            
            dto.setId(record.getId());
            dtos.add(dto);
        }
        return dtos;
	}
	
	
	public static List<ReportInvestmentOptionDTO> getReportInvestmentOptions(Long parreAnalysisId, EntityManager entityManager){
		
		List<ReportInvestmentOptionDTO> rptDtos = getRptInvOptions(parreAnalysisId, entityManager);
		List<ReportInvestmentOptionDTO> deleteRecords = new ArrayList<ReportInvestmentOptionDTO>();
		List<BenefitCostDTO> insertRecords = new ArrayList<BenefitCostDTO>();

		RiskBenefitService benefitService = RiskBenefitServiceImpl.getInstance();
		List<BenefitCostDTO> benefitCostDtos = benefitService.getBenefitCostAnalysis();
		
		//log.info("No of Option Records = " + benefitCostDtos.size());
		//log.info("Investment Option Records = " + rptDtos.size());

		// Delete the Investment Option Record if the Option Records were deleted
		for (ReportInvestmentOptionDTO rptDto : rptDtos) {
			boolean foundRptRow = false;
			for (BenefitCostDTO benefitCostDTO : benefitCostDtos) {
				if (rptDto.getInvestOption().equalsIgnoreCase(benefitCostDTO.getOptionName())) {
					foundRptRow = true;
				}
			}
			if (!foundRptRow) {
				deleteRecords.add(rptDto);
			}
		}

		//log.info("Investment Option Records to be Deleted = " + deleteRecords.size());
		if(deleteRecords.size()>0){
			for (ReportInvestmentOptionDTO optDto : deleteRecords) {
				deleteRptInvestmentOption(optDto, entityManager);
			}
		}

		// Insert Blank Investment Option Records when the new Options were found
		for (BenefitCostDTO benefitCostDTO : benefitCostDtos) {
			boolean foundBenifitRow = false;
			for (ReportInvestmentOptionDTO rptDto : rptDtos) {
				if (benefitCostDTO.getOptionName().equalsIgnoreCase(rptDto.getInvestOption())) {
					foundBenifitRow = true;
				}
			}
			if (!foundBenifitRow) {
				insertRecords.add(benefitCostDTO);
			}
		}
		
		//log.info("Investment Option Records to be Added = " + insertRecords.size());
		if(insertRecords.size()>0){
			for (BenefitCostDTO benefitCostDTO : insertRecords) {
				insertRptInvestmentOption(benefitCostDTO, parreAnalysisId, entityManager);
			}
		}

		return getRptInvOptions(parreAnalysisId, entityManager);
		}
	
	
	
	private static void insertRptInvestmentOption(BenefitCostDTO benefitCostDTO, Long parreAnalysisId, EntityManager entityManager){
		ReportInvestmentOption investmentOption = new ReportInvestmentOption();
		ParreAnalysisReport parreAnalysisReport = getAnalysisReport(parreAnalysisId, entityManager);
		investmentOption.setInvestOption(benefitCostDTO.getOptionName());
		investmentOption.setParreAnalysisReport(parreAnalysisReport);
		investmentOption.setName(parreAnalysisReport.getName());
		investmentOption.setDescription(parreAnalysisReport.getDescription());
		investmentOption.setCountermeasure("-");
		investmentOption.setSitesApplied("-");
		investmentOption.setSiteCost("-");
		investmentOption.setTotalInvestment("-");
		entityManager.persist(investmentOption);
		entityManager.flush();
	}
	
	private static void deleteRptInvestmentOption(ReportInvestmentOptionDTO invOptDTO, EntityManager entityManager){
		entityManager.remove(invOptDTO);
		entityManager.flush();
	}
	
	
	public static ReportAssessmentTask getAssessmentTask(Long parreAnalysisId, EntityManager entityManager) {
		try {
			return (ReportAssessmentTask) entityManager.createQuery(
					"select task from ReportAssessmentTask task" +
					" where task.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId")
				.setParameter("parreAnalysisId", parreAnalysisId)
				.getSingleResult();
		} catch (NoResultException nre) {
			ReportAssessmentTask task = new ReportAssessmentTask();
			ParreAnalysisReport parreAnalysisReport = getAnalysisReport(parreAnalysisId, entityManager);
			task.setParreAnalysisReport(parreAnalysisReport);
			task.setName("Assessment Task " + parreAnalysisReport.getName());
			return task;
		} catch(NonUniqueResultException nure){
			String queryString = "select task from ReportAssessmentTask task" +
					" where task.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("parreAnalysisId", parreAnalysisId);
			List<ReportAssessmentTask> results = query.getResultList();
			return results.get(0);
		}
	}
	
	public static ParreAnalysisReportSection getReportSection(Long parreAnalysisId, Long reportSectionLookupId, EntityManager entityManager) {
		try {
			return (ParreAnalysisReportSection) entityManager.createQuery(
					"select pars from ParreAnalysisReportSection pars" +
							" where pars.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId" +
							" and pars.reportSectionLookup.id = :reportSectionLookupId")
					.setParameter("parreAnalysisId", parreAnalysisId)
					.setParameter("reportSectionLookupId", reportSectionLookupId)
					.getSingleResult();
		} catch (NoResultException nre) {
			return new ParreAnalysisReportSection();
		}
	}
	
	
	
	public static List<ReportAssessmentTaskDTO> getReportAssessmentTasks(Long parreAnalysisId, EntityManager entityManager){
		String queryString = "select tasks from ReportAssessmentTask tasks" +
									" where tasks.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		List<ReportAssessmentTask> resultList = query.getResultList();
		
		List<ReportAssessmentTaskDTO> dtos = new ArrayList<ReportAssessmentTaskDTO>(resultList.size());
	        for (ReportAssessmentTask record : resultList) {
	        	ReportAssessmentTaskDTO dto = new ReportAssessmentTaskDTO();
	            dto.setReportId(record.getParreAnalysisReport().getParreAnalysis().getId());
	            dto.setTaskNumber(record.getTaskNumber());
	            dto.setTitle(record.getTitle());
	            dto.setComments(record.getComments());
	            dto.setId(record.getId());
	            dtos.add(dto);
	        }
	        return dtos;
	}
	
	
	public static ReportAssessmentTask getReportTask(Long taskId, EntityManager entityManager) {
		return (ReportAssessmentTask) entityManager.createQuery(
				"select task from ReportAssessmentTask task" +
						" where task.id = :taskId")
				.setParameter("taskId", taskId)
				.getSingleResult();	
	}
	
	public static ReportInvestmentOption getReportOption(Long optionId, EntityManager entityManager) {
		return (ReportInvestmentOption) entityManager.createQuery(
				"select opts from ReportInvestmentOption opts" +
						" where opts.id = :optionId")
				.setParameter("optionId", optionId)
				.getSingleResult();	
	}
	

	public static List<RptThreatDTO> getMostProbableThreats(
			Long parreAnalysisId, EntityManager entityManager) {

		List<Threat> results = ThreatManager.getActiveThreats(parreAnalysisId,
				entityManager);

		List<RptThreatDTO> mostProbableThreats = new ArrayList<RptThreatDTO>();
		for (Threat threat : results) {
			mostProbableThreats.add(new RptThreatDTO(threat.getHazardType(),
					threat.getDescription()));
		}
		return mostProbableThreats;
	}

	public static List<RptCountermeasureDTO> getExistingCountermeasures(Long parreAnalysisId, EntityManager entityManager) {
	
		String query = "select d from DirectedThreat d where d.parreAnalysis.id = :parreAnalysisId";
		List<DirectedThreat> directedThreats = entityManager.createQuery(query).setParameter("parreAnalysisId", parreAnalysisId).getResultList();
	    Map<Long, RptCountermeasureDTO> rollup = new HashMap<Long, RptCountermeasureDTO>();
	    
	    for (DirectedThreat dt : directedThreats) {
	    	RptCountermeasureDTO dto = rollup.get(dt.getAsset().getId()) != null 
	    			? rollup.get(dt.getAsset().getId()) : new RptCountermeasureDTO(dt.getAsset().getName());
    
	    	for (CounterMeasure cm : dt.getExpertElicitationCounterMeasures()) {
	    		dto.getCounterMeasures().add(cm.getName());
	    	}
		    
		    if (dt.getPathAnalysis() != null) {
		    	for (CounterMeasure cm : dt.getPathAnalysis().getPathAnalysisCounterMeasures()) {
		    		dto.getCounterMeasures().add(cm.getName());
		    	}
		    }

		    if (dt.getDiagramAnalysis() != null) {
		    	for (CounterMeasure cm : dt.getDiagramAnalysis().getDiagramAnalysisCounterMeasures()) {
		    	dto.getCounterMeasures().add(cm.getName());
		    	}
		    }

		    rollup.put(dt.getAsset().getId(), dto);
	    }
	    return new ArrayList<RptCountermeasureDTO>(rollup.values());
	}
	

	public static List<RptConsequenceDTO> getFinancialConsequences(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = getRRDTOsForConsequenceRpt(parreAnalysisId, entityManager);
		List<RptConsequenceDTO> financialConsequences = new ArrayList<RptConsequenceDTO>();
		for (RiskResilienceDTO riskResilience : results) {
			financialConsequences.add(new RptConsequenceDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-"	+ riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getFinancialTotalQuantity())));
		}
		return financialConsequences;
	}

	public static List<RptFatalitiesDTO> getFatalities(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = getRRDTOsForFatalitiesRpt(parreAnalysisId, entityManager);
		List<RptFatalitiesDTO> fatalities = new ArrayList<RptFatalitiesDTO>();
		for (RiskResilienceDTO riskResilience : results) {
			fatalities.add(new RptFatalitiesDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getFatalities())));
		}
		return fatalities;
	}

	public static List<RptInjuriesDTO> getSeriousInjuries(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = getRRDTOsForInjuriesRpt(parreAnalysisId, entityManager);
		List<RptInjuriesDTO> injuries = new ArrayList<RptInjuriesDTO>();
		for (RiskResilienceDTO riskResilience : results) {
			injuries.add(new RptInjuriesDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getSeriousInjuries())));
		}
		return injuries;
	}

	public static List<RptVulnerabilityDTO> getVulnerabilities(Long parreAnalysisId, EntityManager entityManager) {
		List<VulnerabilityDTO> results = getVulnerabilityDTOs(parreAnalysisId, entityManager);
		List<RptVulnerabilityDTO> vulnerabilities = new ArrayList<RptVulnerabilityDTO>();
		for (VulnerabilityDTO dto : results) {
			vulnerabilities.add(new RptVulnerabilityDTO(dto.getAssetName(), dto.getThreatName() + "-" + dto.getThreatDescription(), dto.getVulnerabilityAnalysisType(), GeneralUtil.displayFormat(dto.getVulnerability())));
		}
		return vulnerabilities;
	}

	public static List<RptThreatLikelihoodDTO> getThreatLikelihoods(Long parreAnalysisId, EntityManager entityManager) {
		List<LotDTO> results = getThreatLikelihoodDTOs(parreAnalysisId, entityManager);
		List<RptThreatLikelihoodDTO> threatLikelihood = new ArrayList<RptThreatLikelihoodDTO>();
		for (LotDTO dto : results) {
			threatLikelihood.add(new RptThreatLikelihoodDTO(dto.getAssetName(), dto.getThreatName() + "-" + dto.getThreatDescription(), dto.getLotAnalysisType(), GeneralUtil.displayFormat(dto.getLot())));
		}
		return threatLikelihood;
	}

	public static List<RptRiskDTO> getRisks(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = getRRDTOsForRiskRpt(parreAnalysisId, entityManager);
		List<RptRiskDTO> risks = new ArrayList<RptRiskDTO>(results.size());
		
		for (RiskResilienceDTO riskResilience : results) {
			if(riskResilience.getRisk().compareTo(BigDecimal.ONE) > 0 ){
				risks.add(new RptRiskDTO(riskResilience.getAssetName(),	riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), GeneralUtil.displayFormatForRisk(riskResilience.getRisk())));
			}
		}
		// Sort the Results on the Risk Amount
		Collections.sort(risks);		
		return risks;
	}

	public static List<RptAssessmentDTO> getInvAssessment(Long parreAnalysisId, EntityManager entityManager) {
		RiskBenefitService benefitService = RiskBenefitServiceImpl.getInstance();
		List<BenefitCostDTO> benefitCostDtos = benefitService.getBenefitCostAnalysis();
		List<RptAssessmentDTO> assessments = new ArrayList<RptAssessmentDTO>();
		
		for (BenefitCostDTO dto : benefitCostDtos) {
			assessments.add(new RptAssessmentDTO(dto.getOptionName(), GeneralUtil.displayFormat(dto.getTotalGrossBenefits()), GeneralUtil.displayFormat(dto.getPresentValueCost()), GeneralUtil.displayFormat(dto.getNetBenefits()), GeneralUtil.displayFormatForRatio(dto.getBenefitCostRatio())));
		}
		return assessments;
	}

	public static List<RptResilienceDTO> getResilience(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = reportsService.getRptRiskResilienceDTOs(parreAnalysisId);
		List<RptResilienceDTO> resilience = new ArrayList<RptResilienceDTO>();
		for (RiskResilienceDTO riskResilience : results) {
			resilience.add(new RptResilienceDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-"	+ riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getOwnerResilienceQuantity())));
		}
		Collections.sort(resilience);
		return resilience;

	}
	
	public static List<RptTop20Risks> getTop20Risks(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = reportsService.getRptTop20RisksDTOs(parreAnalysisId);
		List<RptTop20Risks> allRisks = new ArrayList<RptTop20Risks>();
		for (RiskResilienceDTO riskResilience : results) {
			if(riskResilience.getRisk().compareTo(BigDecimal.ONE) > 0 ){
				allRisks.add(new RptTop20Risks(riskResilience.getAssetName(), "Threat Class" ,riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getFatalities()), GeneralUtil.displayFormat(riskResilience.getSeriousInjuries()), GeneralUtil.displayFormat(riskResilience.getOwnersFinancialImpact()), GeneralUtil.displayFormat(riskResilience.getFinancialTotalQuantity()), GeneralUtil.displayFormat(riskResilience.getVulnerability()), GeneralUtil.displayFormat(riskResilience.getLot()), GeneralUtil.displayFormatForRisk(riskResilience.getRisk()), GeneralUtil.displayFormat(riskResilience.getOwnerResilienceQuantity())));
			}
		}
		Collections.sort(allRisks);		
		List<RptTop20Risks> top20Risks = new ArrayList<RptTop20Risks>();
		top20Risks = getSafeSubList(allRisks, 20);
		return top20Risks;
	}
	
	public static List<RptNaturalThreatsDTO> getNaturalThreats(Long parreAnalysisId, EntityManager entityManager) {
		List<RiskResilienceDTO> results = reportsService.getRptTop10NatThreatDTOs(parreAnalysisId);
		List<RptNaturalThreatsDTO> natThreats = new ArrayList<RptNaturalThreatsDTO>();
		for (RiskResilienceDTO riskResilience : results) {
			//Field that needs Multiplication of Vulnerability and Threat Likelihood
			BigDecimal tVul = riskResilience.getVulnerability();
			BigDecimal tLot = riskResilience.getLot();
			BigDecimal vulLot = tVul.multiply(tLot);
			if(riskResilience.getRisk().compareTo(BigDecimal.ONE) > 0 ){
				natThreats.add(new RptNaturalThreatsDTO(riskResilience.getAssetName(), "Threat Class" ,riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), GeneralUtil.displayFormat(riskResilience.getFatalities()), GeneralUtil.displayFormat(riskResilience.getSeriousInjuries()), GeneralUtil.displayFormat(riskResilience.getOwnersFinancialImpact()), GeneralUtil.displayFormat(vulLot), GeneralUtil.displayFormatForRisk(riskResilience.getRisk()), GeneralUtil.displayFormat(riskResilience.getOwnerResilienceQuantity())));
			}
		}
		Collections.sort(natThreats);		
		List<RptNaturalThreatsDTO> top10Natthreats = null;		
		top10Natthreats = getSafeSubList(natThreats, 10);
		return top10Natthreats;
	}

	
	public static List<RiskResilienceDTO> getRRDTOsForConsequenceRpt(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name"
				+ ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description"
				+ ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd"
				+ ", ata.economicImpact, ata.financialTotal "
				+ ", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk"
				+ " from RiskResilience rr, AssetThreatAnalysis ata"
				+ " where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed and ata.financialTotal.quantity > 0.0 "
				+ " and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId "
				+ " ORDER BY ata.financialTotal.quantity DESC";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query, entityManager);
	}

	public static List<RiskResilienceDTO> getRRDTOsForFatalitiesRpt(
			Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name"
				+ ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description"
				+ ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd"
				+ ", ata.economicImpact, ata.financialTotal "
				+ ", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk"
				+ " from RiskResilience rr, AssetThreatAnalysis ata"
				+ " where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed and ata.fatalities > 0.0 "
				+ " and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId "
				+ " order by ata.fatalities DESC";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query,
				entityManager);
	}

	public static List<RiskResilienceDTO> getRRDTOsForInjuriesRpt(
			Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name"
				+ ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description"
				+ ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd"
				+ ", ata.economicImpact, ata.financialTotal "
				+ ", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk"
				+ " from RiskResilience rr, AssetThreatAnalysis ata"
				+ " where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed and ata.seriousInjuries > 0.0 "
				+ " and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId "
				+ " order by ata.seriousInjuries DESC";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query,
				entityManager);
	}

	public static List<VulnerabilityDTO> getVulnerabilityDTOs(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select e.id, e.assetThreat.id, e.assetThreat.asset.assetId, e.assetThreat.asset.name"
				+ ", e.assetThreat.threat.name, e.assetThreat.threat.description"
				+ ", e.vulnerabilityAnalysisType, e.vulnerability"
				+ " from DirectedThreat e"
				+ " where e.assetThreat.removed = :removed and e.vulnerability > 0.0 "
				+ " and e.parreAnalysis.id = :analysisId"
				+ " order by e.vulnerability DESC";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("removed", Boolean.FALSE);
		query.setParameter("analysisId", parreAnalysisId);

		return DirectedThreatManager.createVulnerabilityDTOs(query);

	}

	public static List<LotDTO> getThreatLikelihoodDTOs(Long parreAnalysisId,
			EntityManager entityManager) {
		String queryString = "select e.id, e.assetThreat.id, e.assetThreat.asset.assetId, e.assetThreat.asset.name"
				+ ", e.assetThreat.threat.name, e.assetThreat.threat.description"
				+ ", e.lotAnalysisType, e.lot"
				+ " from DirectedThreat e"
				+ " where e.assetThreat.removed = :removed and e.lot > 0.0 "
				+ " and e.parreAnalysis.id = :analysisId "
				+ " order by e.lot DESC";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("removed", Boolean.FALSE);
		query.setParameter("analysisId", parreAnalysisId);

		return DirectedThreatManager.createLotDTOs(query);

	}

	public static List<RiskResilienceDTO> getRRDTOsForRiskRpt(
			Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name"
				+ ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description"
				+ ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd"
				+ ", ata.economicImpact, ata.financialTotal "
				+ ", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk"
				+ " from RiskResilience rr, AssetThreatAnalysis ata"
				+ " where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed "
				+ " and ata.financialTotal.quantity >= 0.0 and ata.vulnerability > 0.0 and ata.lot > 0.0 "
				+ " and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId " 
				+ " order by ata.financialTotal.quantity, ata.vulnerability, ata.lot DESC";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query,
				entityManager);
	}
	
	public static List<RiskResilienceDTO> getRRDTOsForResilienceRpt(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name"
				+ ", rr.assetThreat.threat.name,  rr.assetThreat.threat.description"
				+ ", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd"
				+ ", ata.economicImpact, ata.financialTotal "
				+ ", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk"
				+ " from RiskResilience rr, AssetThreatAnalysis ata"
				+ " where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed "
				+ " and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId "
				+ " and ata.vulnerability > 0.0 and ata.durationDays > 0.0 and ata.severityMgd > 0.0 "
				+ " ORDER BY ata.vulnerability, ata.durationDays, ata.severityMgd DESC ";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query, entityManager);
	}
	
	public static List<RiskResilienceDTO> getRRDTOsForTop20RisksRpt(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name" +
    			", rr.assetThreat.threat.name,  rr.assetThreat.threat.description" +
    			", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd" +
    			", ata.economicImpact, ata.financialTotal " +
    			", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk" +
    			" from RiskResilience rr, AssetThreatAnalysis ata" +
    			" where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed" +
    			" and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId " +
    			" order by rr.assetThreat.id";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query, entityManager);
	}
	
	public static List<RiskResilienceDTO> getRRDTOsForTop10NatThreatsRpt(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name" +
    			", rr.assetThreat.threat.name,  rr.assetThreat.threat.description" +
    			", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd" +
    			", ata.economicImpact, ata.financialTotal " +
    			", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk" +
    			" from RiskResilience rr, AssetThreatAnalysis ata" +
    			" where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed" +
    			" and rr.assetThreat.threat.threatCategory = 'NATURAL_HAZARD' " +
    			" and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId " +
    			" order by rr.assetThreat.id";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query, entityManager);
	}
	

	
	public static List<RiskResilienceDTO> getFinancialImpact(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rr.id, rr.assetThreat.id, rr.assetThreat.asset.assetId, rr.assetThreat.asset.name" +
    			", rr.assetThreat.threat.name,  rr.assetThreat.threat.description" +
    			", ata.vulnerability, ata.lot, ata.durationDays, ata.severityMgd" +
    			", ata.economicImpact, ata.financialTotal " +
    			", ata.fatalities, ata.seriousInjuries, ata.financialImpact, ata.totalRisk" +
    			" from RiskResilience rr, AssetThreatAnalysis ata" +
    			" where ata.assetThreat = rr.assetThreat and rr.assetThreat.removed = :removed" +
    			" and ata.financialImpact.quantity > 0.0 " +
    			" and rr.parreAnalysis.id = :parreAnalysisId and ata.parreAnalysis.id = :parreAnalysisId " +
    			" order by ata.financialImpact.quantity";

		Query query = entityManager.createQuery(queryString);
		return RiskResilienceManager.createDTOs(parreAnalysisId, query, entityManager);
	}
	
	
	
	public static List<RptRankingsDTO> getSummaryRankings(Long parreAnalysisId, EntityManager entityManager) {
				
		List<RptRankingsDTO> risks = getRiskRanking(parreAnalysisId, entityManager);
		List<RptRankingsDTO> fatalities = getFatalityRanking(parreAnalysisId, entityManager);
		List<RptRankingsDTO> injuries = getInjuryRanking(parreAnalysisId, entityManager);
		List<RptRankingsDTO> resilience = getResilienceRanking(parreAnalysisId, entityManager);
		List<RptRankingsDTO> impact = getImpactRanking(parreAnalysisId, entityManager);
		
		if (log.isDebugEnabled()) { log.debug("Risks # " + risks.size() + "Fatalities # " + fatalities.size() + "Injuries # " + injuries.size() + "Relience # " + resilience.size() + "Impact # " + impact.size() ); }
		
		List<RptRankingsDTO> markedFatal = new ArrayList<RptRankingsDTO>();
		List<RptRankingsDTO> markedInjury = new ArrayList<RptRankingsDTO>();	
		List<RptRankingsDTO> markedResilience = new ArrayList<RptRankingsDTO>();	
		List<RptRankingsDTO> markedImpact = new ArrayList<RptRankingsDTO>();	
			
		//Compare Fatality collection against  Risk collection and remove the duplicates from Fatality collection after copying their Ranks into Risk collection.
		for (RptRankingsDTO riskDTO : risks) {
			for (RptRankingsDTO fatalDTO : fatalities) {
				if(riskDTO.getAssetThreatId().equals(fatalDTO.getAssetThreatId())){
					riskDTO.setFatalityRank(fatalDTO.getFatalityRank());
					markedFatal.add(fatalDTO);
				}
			}			
		}		
		
		//log.info("Duplicate Fatalities = " + markedFatal.size());
		fatalities.removeAll(markedFatal);
		risks.addAll(fatalities);
		
		//Compare Injury collection against Risk(+Fatality) collection and remove the duplicates from Injury collection after copying their Ranks into Risk collection.
		for (RptRankingsDTO riskDTO : risks) {
			for (RptRankingsDTO injuryDTO : injuries) {
				if(riskDTO.getAssetThreatId().equals(injuryDTO.getAssetThreatId())){
					riskDTO.setInjuryRank(injuryDTO.getInjuryRank());
					markedInjury.add(injuryDTO);
				}
			}			
		}		
		//log.info("Duplicate Injuries = " + markedInjury.size());
		injuries.removeAll(markedInjury);
		risks.addAll(injuries);
		
		//Compare Resilience collection against Risk(+Fatality+Injury) collection and remove the duplicates from Resilience collection after copying their Ranks into Risk collection.
		for (RptRankingsDTO riskDTO : risks) {
			for (RptRankingsDTO resilienceDTO : resilience) {
				if(riskDTO.getAssetThreatId().equals(resilienceDTO.getAssetThreatId())){
					riskDTO.setResilienceRank(resilienceDTO.getResilienceRank());
					markedResilience.add(resilienceDTO);
				}
			}			
		}		
		//log.info("Duplicate Resilience = " + markedResilience.size());
		resilience.removeAll(markedResilience);
		risks.addAll(resilience);
		
		//Compare Impact collection against Risk(+Fatality+Injury+Resilience) collection and remove the duplicates from Impact collection after copying their Ranks into Risk collection.
		for (RptRankingsDTO riskDTO : risks) {
			for (RptRankingsDTO impactDTO : impact) {
				if(riskDTO.getAssetThreatId().equals(impactDTO.getAssetThreatId())){
					riskDTO.setImpactRank(impactDTO.getImpactRank());
					markedImpact.add(impactDTO);
				}
			}			
		}		
		//log.info("Duplicate Impact = " + markedImpact.size());
		impact.removeAll(markedImpact);
		risks.addAll(impact);
				
		removeNullRanks(risks);
		return risks;
				
	}
	
	private static List<RptRankingsDTO> removeNullRanks(List<RptRankingsDTO> risks){
		
		for (RptRankingsDTO rptRankingsDTO : risks) {
			if(rptRankingsDTO.getRiskRank() == null){
				rptRankingsDTO.setRiskRank(0);
			} if(rptRankingsDTO.getFatalityRank() == null){
				rptRankingsDTO.setFatalityRank(0);
			} if(rptRankingsDTO.getImpactRank() == null){
				rptRankingsDTO.setImpactRank(0);
			} if(rptRankingsDTO.getInjuryRank() == null){
				rptRankingsDTO.setInjuryRank(0);
			} if(rptRankingsDTO.getResilienceRank() == null){
				rptRankingsDTO.setResilienceRank(0);
			}
		}
		return risks;
	}
	
	private static List<RptRankingsDTO> getRiskRanking(Long parreAnalysisId, EntityManager entityManager){
		
		List<RptRankingsDTO> top10Risks = new ArrayList<RptRankingsDTO>();		
		int i = 1;
		BigDecimal bd = new BigDecimal("0");
		
		List<RiskResilienceDTO> riskResults = getRRDTOsForRiskRpt(parreAnalysisId, entityManager);
		List<RptRankingsDTO> risks = new ArrayList<RptRankingsDTO>(riskResults.size());
		for (RiskResilienceDTO riskResilience : riskResults) {
			risks.add(new RptRankingsDTO(riskResilience.getAssetName(),	riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), riskResilience.getAssetThreatId() ,riskResilience.getRisk(), bd, bd, bd, bd));
		}
		
		Collections.sort(risks, RptRankingsDTO.Comparators.RISK);			
		top10Risks = getSafeSubList(risks, 10);
		for (RptRankingsDTO rptRankingsDTO : top10Risks) {		
			rptRankingsDTO.setRiskRank(i);
			i=i+1;
		}
		return top10Risks;
	}
	
	private static <T> List<T> getSafeSubList(List<T> results, int max) {
		if (results == null || results.size() <= max) { return results; }
		
		List<T> safeList = new ArrayList<T>();
		
		for (int i = 0; i < max; i++) {
			safeList.add(results.get(i));
		}
		
		return safeList;
	}
	
	
	private static List<RptRankingsDTO> getFatalityRanking(Long parreAnalysisId, EntityManager entityManager){
		
		List<RptRankingsDTO> top10Fatalities = new ArrayList<RptRankingsDTO>();
		int i = 1;
		BigDecimal bd = new BigDecimal("0"); 
		List<RiskResilienceDTO> fatalityResults = getRRDTOsForFatalitiesRpt(parreAnalysisId, entityManager);
	
		List<RptRankingsDTO> fatalities = new ArrayList<RptRankingsDTO>(fatalityResults.size());
		for (RiskResilienceDTO riskResilience : fatalityResults) {
			fatalities.add(new RptRankingsDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), riskResilience.getAssetThreatId(), bd, bd, riskResilience.getFatalities(), bd, bd));
		}
		
		Collections.sort(fatalities, RptRankingsDTO.Comparators.FATALITY);
		top10Fatalities = getSafeSubList(fatalities, 10);
		for (RptRankingsDTO rptRankingsDTO : top10Fatalities) {
			rptRankingsDTO.setFatalityRank(i);
			i=i+1;
		}
		return top10Fatalities;
		
	}
	
	private static List<RptRankingsDTO> getInjuryRanking(Long parreAnalysisId, EntityManager entityManager){
		
		List<RptRankingsDTO> top10Injuries = new ArrayList<RptRankingsDTO>();
		int i = 1;
		BigDecimal bd = new BigDecimal("0"); 
		List<RiskResilienceDTO> injuryResults = getRRDTOsForInjuriesRpt(parreAnalysisId, entityManager);
	
		List<RptRankingsDTO> injuries = new ArrayList<RptRankingsDTO>(injuryResults.size());
		for (RiskResilienceDTO riskResilience : injuryResults) {
			injuries.add(new RptRankingsDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), riskResilience.getAssetThreatId(), bd, bd, bd, riskResilience.getSeriousInjuries(), bd));
		}
		
		Collections.sort(injuries, RptRankingsDTO.Comparators.INJURY);	
		top10Injuries = getSafeSubList(injuries, 10);
		for (RptRankingsDTO rptRankingsDTO : top10Injuries) {
			rptRankingsDTO.setInjuryRank(i);
			i=i+1;
		}
		return top10Injuries;
		
	}
	
	private static List<RptRankingsDTO> getResilienceRanking(Long parreAnalysisId, EntityManager entityManager){
		
		List<RptRankingsDTO> top10Resilience = new ArrayList<RptRankingsDTO>();
		int i = 1;
		BigDecimal bd = new BigDecimal("0"); 
		
		List<RiskResilienceDTO> resilienceResults = reportsService.getRptRiskResilienceDTOs(parreAnalysisId);
		
		List<RptRankingsDTO> resilience = new ArrayList<RptRankingsDTO>(resilienceResults.size());
		for (RiskResilienceDTO riskResilience : resilienceResults) {
			resilience.add(new RptRankingsDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), riskResilience.getAssetThreatId(), bd, riskResilience.getOwnerResilienceQuantity(), bd, bd, bd));
		}
		
		Collections.sort(resilience, RptRankingsDTO.Comparators.RESILIENCE);	
		top10Resilience = getSafeSubList(resilience, 10);
		for (RptRankingsDTO rptRankingsDTO : top10Resilience) {
			rptRankingsDTO.setResilienceRank(i);
			i=i+1;
		}
		return top10Resilience;
		
	}
	
	private static List<RptRankingsDTO> getImpactRanking(Long parreAnalysisId, EntityManager entityManager){
		
		List<RptRankingsDTO> top10Impact = new ArrayList<RptRankingsDTO>();
		int i = 1;
		BigDecimal bd = new BigDecimal("0"); 
		
		List<RiskResilienceDTO> impactResults = getFinancialImpact(parreAnalysisId, entityManager);
		
		List<RptRankingsDTO> impact = new ArrayList<RptRankingsDTO>(impactResults.size());
		for (RiskResilienceDTO riskResilience : impactResults) {
			impact.add(new RptRankingsDTO(riskResilience.getAssetName(), riskResilience.getThreatName() + "-" + riskResilience.getThreatDescription(), riskResilience.getAssetThreatId(), bd, bd, bd, bd, riskResilience.getOwnersFinancialImpact()));
		}
		
		Collections.sort(impact, RptRankingsDTO.Comparators.IMPACT);	
		top10Impact = getSafeSubList(impact, 10);
		for (RptRankingsDTO rptRankingsDTO : top10Impact) {
			rptRankingsDTO.setImpactRank(i);
			i=i+1;
		}
		return top10Impact;
		
	}
	
	public static RptMainDTO setReportStaticContent(RptMainDTO rptMain, Long parreAnalysisId, EntityManager entityManager) {
		
		StringBuffer staticContent = new StringBuffer();
		
		String queryString = "select rsl from ReportSectionLookup rsl";
		Query query = entityManager.createQuery(queryString);
		List<ReportSectionLookup> results = query.getResultList();
        
		for (ReportSectionLookup rsl : results) {
			if(rsl.getName().equalsIgnoreCase("Introduction"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsIntroduction(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("RiskAssessment"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsRiskAssessment(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ComplaintAssessment"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsComplaintAssessment(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("SafetyAct"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsSafetyAct(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("SupportTool"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsSupportTool(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Assessment Team Members"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsAssessmentTeamMembers(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("AssessmentProcess"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsAssessmentProcess(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Mission Statement"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsMissionStatement(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Critical Assets"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsCriticalAssets(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ProbableThreats"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsProbableThreats(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Asset Threat Pairs"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsAssetThreatPairs(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Countermeasure"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsCountermeasure(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Calculation of Consequences"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsCalOfConsequences(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("StatisticalValue"))
			{	
				//TODO: Get Life and Serious Injury Dollar Amounts and Add in here!
				ParreAnalysisDTO paDto = getStatValues(parreAnalysisId, entityManager);
				BigDecimal bdZero1 = new BigDecimal("0.000000");
				BigDecimal fatalityModalValue = new BigDecimal(paDto.getStatValuesDTO().getFatalityQuantity().toPlainString());
				BigDecimal fatalityDispValue = fatalityModalValue.setScale(3, RoundingMode.HALF_EVEN);
				BigDecimal injuryModalValue = new BigDecimal(paDto.getStatValuesDTO().getSeriousInjuryQuantity().toPlainString());
				BigDecimal injuryDispValue = injuryModalValue.setScale(3, RoundingMode.HALF_EVEN);
				
				if(fatalityModalValue.equals(bdZero1) && injuryModalValue.equals(bdZero1) ){
					rptMain.setsStatisticalValue("The team choose not to include the statictical values of life and serious injuries in this assessment.");
				}else {
					staticContent.append(rsl.getDescription());		
					staticContent.append("$"+fatalityDispValue+" million for life and $"+injuryDispValue+" million for a serious injury. ");
					staticContent.append(rsl.getAdditionalDescription());
					rptMain.setsStatisticalValue(staticContent.toString());
				}
					
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("AssetVulnerabilities"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsAssetVulnerabilities(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Path Analysis Discussion"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsPathAnalysisDiscussion(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Event Tree Discussion"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsEventTreeDiscussion(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ThreatLikelihood"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsThreatLikelihood(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Best Estimate"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsBestEstimate(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Proxy Analysis"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsProxyAnalysis(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("RiskDetermination"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsRiskDetermination(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Baseline"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsBaseline(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Risk Management"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsRiskManagement(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Resilience"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsResilience(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("PairResilience"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsPairResilience(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("SystemResilience"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsSystemResilience(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("SystemResilienceSummary"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsSystemResilienceSummary(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("FRI1"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsFRI1(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("FRI2"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsFRI2(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("FRI3"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsFRI3(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("FRI4"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsFRI4(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("FRI5"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsFRI5(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI1"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI1(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI2"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI2(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI3"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI3(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI4"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI4(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI5"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI5(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI6"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI6(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("ORI7"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsORI7(staticContent.toString());
				staticContent.setLength(0);
			}else if(rsl.getName().equalsIgnoreCase("Appendix"))
			{	
				staticContent.append(rsl.getDescription());
				staticContent.append(rsl.getAdditionalDescription());
				rptMain.setsAppendix(staticContent.toString());
				staticContent.setLength(0);
			}
		}
		
		
		return rptMain;
		
	}
	
	public static RptMainDTO setReportUserInputData(RptMainDTO rptUserInput, Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select rsl.name, parc.userInput from ParreAnalysisReportSection parc, ReportSectionLookup rsl"
				+ " where parc.reportSectionLookup.id = rsl.id and parc.parreAnalysisReport.parreAnalysis.id = :parreAnalysisId";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		 List<Object[]> records = query.getResultList();

		for (Object[] record : records) {
			if(record[0].toString().equalsIgnoreCase("Executive Summary")){
				rptUserInput.setExeSummary(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Assessment Team")){
				rptUserInput.setAssesTeam(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Assessment Team Members")){
				rptUserInput.setAssesTeamOf(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Assessment Approach")){
				rptUserInput.setAssesApproach(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Assessment Tasks")){
				rptUserInput.setAssesTasks(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Work Sessions")){
				rptUserInput.setWorkSessions(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Description of Utility")){
				rptUserInput.setDescUtility(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Mission Statement")){
				rptUserInput.setMissionStmt(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Critical Assets")){
				rptUserInput.setCriticalAssets(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Asset Threat Pairs")){
				rptUserInput.setAssetThreatPairs(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Calculation of Consequences")){
				rptUserInput.setCalConsequences(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Expert Elicitation")){
				rptUserInput.setExpertElicitation(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Path Analysis Discussion")){
				rptUserInput.setPathAnalysisDisc(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Event Tree Discussion")){
				rptUserInput.setTreeEventDisc(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Best Estimate")){
				rptUserInput.setBestEstimate(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Proxy Analysis")){
				rptUserInput.setProxyAnalysis(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Conditional Likelihood")){
				rptUserInput.setCondLikelihood(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Risk Management")){
				rptUserInput.setRiskManagement(record[1].toString());
			} else if(record[0].toString().equalsIgnoreCase("Risk Reduction Options")){
				rptUserInput.setRiskReductionOpt(record[1].toString());
			} 
		}
		return rptUserInput;
	}
	
	public static RptMainDTO setIndexContent(RptMainDTO rptMain, Long parreAnalysisId, EntityManager entityManager) {
		
		int counterFRI = 1;
		int counterORI = 1;
		
		RiskResilienceAnalysis analysis = RiskResilienceManager.getCurrentAnalysis(parreAnalysisId, entityManager);
		Long riskResilienceAnalysisId = analysis.getId();
		List<UriResponseDTO> resilienceIndex = RiskResilienceManager.getUriResponseDTOs(riskResilienceAnalysisId, entityManager);
		
		for (UriResponseDTO uriResponseDTO : resilienceIndex) {
			UriQuestionDTO questionDTO = RiskResilienceManager.getUriQuestionDTO(uriResponseDTO.getUriQuestionId(), entityManager);
			
			if(questionDTO.getType().isFri()){
				if(counterFRI==1){
					rptMain.setsFRI1(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterFRI = counterFRI+1;
				}else if(counterFRI==2){
					rptMain.setsFRI2(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterFRI = counterFRI+1;
				}else if(counterFRI==3){
					rptMain.setsFRI3(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterFRI = counterFRI+1;
				}else if(counterFRI==4){
					rptMain.setsFRI4(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterFRI = counterFRI+1;
				}else if(counterFRI==5){
					rptMain.setsFRI5(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterFRI = counterFRI+1;
				}
			
			} else if(questionDTO.getType().isOri()){
				if(counterORI==1){
					rptMain.setsORI1(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==2){
					rptMain.setsORI2(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==3){
					rptMain.setsORI3(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==4){
					rptMain.setsORI4(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==5){
					rptMain.setsORI5(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==6){
					rptMain.setsORI6(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}else if(counterORI==7){
					rptMain.setsORI7(uriResponseDTO.getQuestionName()+"("+uriResponseDTO.getValue()+"-"+uriResponseDTO.getWeight()+") :"+uriResponseDTO.getOptionName());
					counterORI = counterORI+1;
				}
			}
		}
		
		
		return rptMain;
	}
	
	
	public static ParreAnalysisDTO getStatValues(Long parreAnalysisId, EntityManager entityManager) {
		String queryString = "select pa from ParreAnalysis pa where pa.id = :parreAnalysisId";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		ParreAnalysis result = (ParreAnalysis) query.getSingleResult();
		ParreAnalysisDTO dto = new ParreAnalysisDTO();
		//log.info("Stat Values = "+ result.getStatValues());
		dto.setStatValuesDTO(result.getStatValuesDTO());
		return dto;
	}
	
	
	public static Long getParreAnalysisBaselineId(Long parreAnalysisId, EntityManager entityManager) {
		Long paId = null;
		String queryString = "select pa from ParreAnalysis pa where pa.id = :parreAnalysisId";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parreAnalysisId", parreAnalysisId);
		ParreAnalysis result = (ParreAnalysis) query.getSingleResult();
		
		if(result.getBaseline() != null){
			paId = result.getBaseline().getId();
		} else
		{
			paId = parreAnalysisId;
		}
		return paId;
	}

	public static void deleteReport(Long parreAnalysisId, EntityManager entityManager) {
		
		try {
			int assessmentTaskCount = 
					entityManager.createQuery("delete ReportAssessmentTask rat where rat.parreAnalysisReport in (select par from ParreAnalysisReport par where par.parreAnalysis.id = :parreAnalysisId)")
										.setParameter("parreAnalysisId", parreAnalysisId)
										.executeUpdate();
			
			if (log.isDebugEnabled()) { log.debug("Removed " + assessmentTaskCount + " report assessment tasks for parre analysis id: " + parreAnalysisId); }
			
			List<ParreAnalysisReport> reports = entityManager.createQuery("select par from ParreAnalysisReport par where par.parreAnalysis.id = :parreAnalysisId")
				.setParameter("parreAnalysisId", parreAnalysisId)
				.getResultList();
			
			
			for (ParreAnalysisReport report : reports) {
				entityManager.remove(report);
			}
			
			if (log.isDebugEnabled()) { log.debug("Removed " + reports.size() + " reports for parre analysis id: " + parreAnalysisId); }
			
		} catch (EntityNotFoundException enfe) {
			// ignore
		}
	}
	
}
