/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;



import javax.persistence.*;

import org.parre.shared.AmountDTO;
import org.parre.shared.SharedCalculationUtil;

import java.math.BigDecimal;

/**
 * The Class AssetThreatAnalysis.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/19/11
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ANALYSIS_TYPE", discriminatorType = DiscriminatorType.STRING)
@Table(name = "ASSET_THREAT_ANALYSIS")
public abstract class AssetThreatAnalysis extends AssetThreatEntity {
    @Column(name = "FATALITIES")
    private Integer fatalities = Integer.valueOf(0);
    @Column(name = "SERIOUS_INJURIES")
    private Integer seriousInjuries = Integer.valueOf(0);
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "FI_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
            @AttributeOverride(name = "unit", column = @Column(name = "FI_UNIT"))
    })
    private Amount financialImpact = Amount.ZERO;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "EI_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
            @AttributeOverride(name = "unit", column = @Column(name = "EI_UNIT"))
    })
    private Amount economicImpact = Amount.ZERO;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "quantity", column = @Column(name = "FT_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
            @AttributeOverride(name = "unit", column = @Column(name = "FT_UNIT"))
    })
    private Amount financialTotal = Amount.ZERO;
    
    // Total Risk added just for Naturals for now, so we don't have to recalculate each time we read out
    @Embedded
    @AttributeOverrides({
    	@AttributeOverride(name = "quantity", column = @Column(name = "TR_QUANTITY", columnDefinition = "DECIMAL(19,6)")),
    	@AttributeOverride(name = "unit", column = @Column(name = "TR_UNIT"))
    })
    private Amount totalRisk = Amount.ZERO;

    @Column(name = "VULNERABILITY", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal vulnerability = BigDecimal.ZERO;

    @Column(name = "LOT", columnDefinition = "DECIMAL(21,20)")
    private BigDecimal lot = BigDecimal.ZERO;

    @Column(name = "DURATION_DAYS", columnDefinition = "DECIMAL(8,2)")
    private BigDecimal durationDays;
    @Column(name = "SEVERITY_MGD", columnDefinition = "DECIMAL(19,6)")
    private BigDecimal severityMgd;

    protected AssetThreatAnalysis() {
    }

    protected AssetThreatAnalysis(ParreAnalysis parreAnalysis, AssetThreat assetThreat) {
        this(assetThreat);
        setParreAnalysis(parreAnalysis);
    }
    protected AssetThreatAnalysis(AssetThreat assetThreat) {
        super(assetThreat);
        fatalities = 0;
        seriousInjuries = 0;
        financialImpact = new Amount(BigDecimal.ZERO);
        economicImpact = new Amount(BigDecimal.ZERO);
        financialTotal = new Amount(BigDecimal.ZERO);
        vulnerability = BigDecimal.ZERO;
        lot = BigDecimal.ZERO;
        this.durationDays = BigDecimal.ZERO;
        this.severityMgd = BigDecimal.ZERO;
    }

    public abstract Analysis getAnalysis();


    public Integer getFatalities() {
        return fatalities;
    }

    public void setFatalities(Integer fatalities) {
        this.fatalities = fatalities;
    }

    public void setSeriousInjuries(Integer seriousInjuries) {
        this.seriousInjuries = seriousInjuries;
    }

    public Integer getSeriousInjuries() {
        return seriousInjuries;
    }

    public void setFinancialImpact(Amount financialImpact) {
        this.financialImpact = financialImpact;
    }

    public Amount getFinancialImpact() {
        return financialImpact;
    }

    public void setFinancialImpactDTO(AmountDTO financialImpact) {
        this.financialImpact = new Amount(financialImpact);
    }

    public AmountDTO getFinancialImpactDTO() {
        return financialImpact.createDTO();
    }

    public void setEconomicImpact(Amount economicImpact) {
        this.economicImpact = economicImpact;
    }

    public Amount getEconomicImpact() {
        return economicImpact;
    }

    public void setEconomicImpactDTO(AmountDTO economicImpact) {
        this.economicImpact = new Amount(economicImpact);
    }

    public AmountDTO getEconomicImpactDTO() {
        return economicImpact.createDTO();
    }


    public void setFinancialTotal(Amount financialTotal) {
        this.financialTotal = financialTotal;
    }

    public Amount getFinancialTotal() {
        return financialTotal;
    }

    public BigDecimal getVulnerability() {
        return vulnerability;
    }

    public void setVulnerability(BigDecimal vulnerability) {
        this.vulnerability = vulnerability;
    }

    public BigDecimal getLot() {
        return lot;
    }

    public void setLot(BigDecimal lot) {
        this.lot = lot;
    }

    public void updateFinancialTotal() {
        ParreAnalysis parreAnalysis = getAnalysis().getParreAnalysis();
        if (!parreAnalysis.getUseStatValues()) {
            this.financialTotal = this.financialImpact;
        } else {
            BigDecimal ftQ = SharedCalculationUtil.
                    calculateFinancialTotal(fatalities, seriousInjuries, financialImpact.getQuantity(),
                            parreAnalysis.getStatValues());
            this.financialTotal = new Amount(ftQ, financialImpact.getUnit());
        }
    }

    public AmountDTO getFinancialTotalDTO() {
        return financialTotal.createDTO();
    }

    public BigDecimal getDurationDays() {
        return durationDays;
    }

    public void setDurationDays(BigDecimal durationDays) {
        this.durationDays = durationDays;
    }

    public BigDecimal getSeverityMgd() {
        return severityMgd;
    }

    public void setSeverityMgd(BigDecimal severityMgd) {
        this.severityMgd = severityMgd;
    }

	public Amount getTotalRisk() {
		return totalRisk;
	}

	public void setTotalRisk(Amount totalRisk) {
		this.totalRisk = totalRisk;
	}
    
}
