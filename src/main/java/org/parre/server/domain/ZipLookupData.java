/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.parre.shared.ZipLookupDataDTO;


/**
 * The Class ZipLookupData.
 */
@Entity
@Table(name = "ZIP_LOOKUP_DATA")
public class ZipLookupData extends BaseEntity {
	private static final long serialVersionUID = 1L;

	@Column(name = "ZIP_CODE") 
	private String zipCode;
	
	@Column(name = "COUNTY")
	private String county;
	
	@Column(name = "STATE_CODE")
	private String stateCode;
	
	@Column(name = "LATITUDE", columnDefinition = "DECIMAL(12,9)")
    private BigDecimal latitude;
	
    @Column(name = "LONGITUDE", columnDefinition = "DECIMAL(12,9)")
    private BigDecimal longitude;

	public ZipLookupData() {
		super();
	}

	public ZipLookupData(String zipCode, String county, String stateCode,
			BigDecimal latitude, BigDecimal longitude) {
		super();
		this.zipCode = zipCode;
		this.county = county;
		this.stateCode = stateCode;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public static List<ZipLookupDataDTO> createDTOs(List<ZipLookupData> entities) {
        List<ZipLookupDataDTO> dtos = new ArrayList<ZipLookupDataDTO>(entities.size());
        for (ZipLookupData entity : entities) {
            dtos.add(entity.copyInto(new ZipLookupDataDTO()));
        }
        return dtos;
    }
	
	@Override
	public String toString() {
		return "ZipLookupData [zipCode=" + zipCode + ", county=" + county
				+ ", stateCode=" + stateCode + ", latitude=" + latitude
				+ ", longitude=" + longitude + ", getId()=" + getId() + "]";
	}
    
    
}
