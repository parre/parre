/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.parre.server.persistence;

import org.apache.log4j.Logger;
import org.parre.server.util.LoggingUtil;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 Feb 20, 2006
 */
public class PersistenceContextRegistry {
    private transient static final Logger log = Logger
            .getLogger(PersistenceContextRegistry.class);

    private static Map<String, EntityManagerRegistry> entityManagerAdaptorMap =
            new HashMap<String, EntityManagerRegistry>(5);

    public static EntityManagerRegistry getEntityManagerAdaptor(String persistenceUnit) {
        return getInstance(persistenceUnit);
    }

    public static EntityManager entityManager(String persitentUnit) {
        return getInstance(persitentUnit).entityManager();
    }

    public static void closeEntityManager(String persistenceUnit) {
        EntityManagerRegistry instance = getInstance(persistenceUnit);
        if (instance == null) {
            log.warn("Attempt to close non-existent entity manager ignored : " + persistenceUnit);
            return;
        }
        instance.closeEntityManager();
    }

    public static boolean isTransactionActive(String persistentUnit) {
        return getInstance(persistentUnit).isTransactionActive();
    }

    /**
     * begins a transaction in the context of current thread for the entity manager for the given persistentContext.
     *
     * @param persistenceUnit -- the persistent unit for which the transaction is to be started.
     * @return -- returns true if a new transaction was started, returns false if a transaction was already in progress
     */

    public static boolean beginTransaction(String persistenceUnit) {
        EntityManagerRegistry instance = getInstance(persistenceUnit);
        return instance.beginTransaction();
    }

    public static void commitTransaction(String persistenceUnit) {
        LoggingUtil.debugLogMessage(log, "Committing transaction for :", persistenceUnit);
        getInstance(persistenceUnit).commitTransaction();
    }

    public static void rollbackTransaction(String persistenceUnit) {
        getInstance(persistenceUnit).rollbackTransaction();
    }


    private static EntityManagerRegistry getInstance(String persistenceUnit) {
        EntityManagerRegistry entityManagerAdaptor = entityManagerAdaptorMap.get(persistenceUnit);
        if (entityManagerAdaptor == null) {
            log.info("Creating EntityManger factory for : " + persistenceUnit);
            entityManagerAdaptor = createEntityManagerAdaptor(persistenceUnit);
            log.info("EntityManger factory created for : " + persistenceUnit);
        }
        return entityManagerAdaptor;
    }

    private synchronized static EntityManagerRegistry createEntityManagerAdaptor(
            String persistenceUnit) {
        EntityManagerRegistry entityManagerAdaptor = entityManagerAdaptorMap.get(persistenceUnit);
        if (entityManagerAdaptor != null) {
            return entityManagerAdaptor;
        }
        entityManagerAdaptor = new EntityManagerRegistry(persistenceUnit);
        entityManagerAdaptorMap.put(persistenceUnit, entityManagerAdaptor);
        return entityManagerAdaptor;
    }

    public static void shutdown() {
        log.info("Shutting down EntityManager factories");
        Collection<EntityManagerRegistry> managerRegistries = entityManagerAdaptorMap.values();
        for (EntityManagerRegistry managerRegistry : managerRegistries) {
            managerRegistry.shutdown();
        }
        entityManagerAdaptorMap.clear();
        log.info("EntityManager factories shutdown..");
    }

}
