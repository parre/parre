/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.apache.log4j.Logger;
import org.parre.client.messaging.RiskBenefitService;
import org.parre.server.ParreDatabase;
import org.parre.server.domain.*;
import org.parre.server.domain.manager.RiskBenefitManager;
import org.parre.server.domain.manager.RiskResilienceManager;
import org.parre.shared.*;
import org.parre.shared.types.MoneyUnitType;
import org.parre.shared.types.RiskBenefitMetricType;

import java.math.BigDecimal;
import java.util.*;

/**
 * The Class RiskBenefitServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/2/12
 */
public class RiskBenefitServiceImpl extends ParreBaseService implements RiskBenefitService {
    private static transient final Logger log = Logger.getLogger(RiskBenefitServiceImpl.class);

    private static RiskBenefitService instance = new RiskBenefitServiceImpl();

    public static RiskBenefitService getInstance() {
        return instance;
    }

    private RiskBenefitServiceImpl() {

    }

    public RiskBenefitAnalysisDTO getAnalysis(RiskBenefitMetricType metricType) {
        ParreAnalysis baseline = getCurrentBaseline();
        RiskBenefitAnalysis analysis = RiskBenefitManager.getRiskBenefitAnalysis(baseline, entityManager());
        boolean isNew = (analysis == null);
        if (isNew) {
            analysis = initRiskBenefitAnalysis(baseline);
        }
        RiskBenefitAnalysisDTO dto = analysis.createDTO();
        dto.setRiskBenefitDTOs(getMetricAnalysis(metricType));
        populateOptionAnalysisDTOs(baseline, dto);
        if (dto.hasOptions()) {
            dto.setBenefitCostDTOs(getBenefitCostAnalysis());
        }
        return dto;
    }

    public List<BenefitCostDTO> getBenefitCostAnalysis() {
        List<RiskBenefitDTO> metricAnalysis = getMetricAnalysis(RiskBenefitMetricType.RISK);
        Map<Long, BigDecimal> optionGrossBenefitMap = createOptionGrossBenefitMap(metricAnalysis);
        ParreAnalysis currentBaseline = getCurrentBaseline();
        List<ParreAnalysis> options = new ArrayList<ParreAnalysis>(currentBaseline.getOptionParreAnalyses());
        //Sometimes the options are out of order, this reorders them
        for(int i = 0; i < options.size() - 1; i++) {
            for(int j = i + 1; j < options.size(); j++) {
                if(options.get(i).getId() > options.get(j).getId()) {
                    ParreAnalysis temp = options.get(j);
                    options.set(j, options.get(i));
                    options.set(i, temp);
                }
            }
        }
        List<BenefitCostDTO> benefitCostDTOs = new ArrayList<BenefitCostDTO>(options.size());
        for (ParreAnalysis option : options) {
            BigDecimal presentValueOfOption = BigDecimal.ZERO;
            Set<ProposedMeasure> proposedMeasures = option.getProposedMeasures();
            for (ProposedMeasure proposedMeasure : proposedMeasures) {
                BigDecimal presentValueOfPm = calculatePresentValueOfOption(proposedMeasure);
                presentValueOfOption = presentValueOfOption.add(presentValueOfPm);
            }
            BenefitCostDTO dto = createBenefitCostDTO(option, presentValueOfOption, optionGrossBenefitMap);
            benefitCostDTOs.add(dto);
        }
        Collections.sort(benefitCostDTOs);
        return benefitCostDTOs;
    }

    protected BenefitCostDTO createBenefitCostDTO(ParreAnalysis option, BigDecimal presentValueOfOption, Map<Long, BigDecimal> optionGrossBenefitMap) {
        Long optionId = option.getId();
        BenefitCostDTO dto = new BenefitCostDTO(optionId, option.getName(), option.getDisplayOrder());
        BigDecimal grossBenefits = optionGrossBenefitMap.get(optionId);
        dto.setTotalGrossBenefits(grossBenefits);
        dto.setPresentValueCost(presentValueOfOption);
        dto.setNetBenefits(dto.getTotalGrossBenefits().subtract(dto.getPresentValueCost()));
        dto.setBenefitCostRatio(SharedCalculationUtil.divideSafely(dto.getNetBenefits(), dto.getPresentValueCost()));
        return dto;
    }

    protected BigDecimal calculatePresentValueOfOption(ProposedMeasure proposedMeasure) {
        BigDecimal c = proposedMeasure.getCapitalCost().createDollarValue();
        BigDecimal sv = proposedMeasure.getSalvageValue().createDollarValue();
        BigDecimal oAndM = proposedMeasure.getoAndMCostPerYear().createDollarValue();
        Integer ls = proposedMeasure.getEffectiveLife();
        BigDecimal i = proposedMeasure.getInflationRate();
        BigDecimal i_plus_1 = i.add(BigDecimal.ONE);
        BigDecimal subexp1 = SharedCalculationUtil.divideSafely(BigDecimal.ONE, i_plus_1.pow(ls));//(1+i)-LS
        BigDecimal exp1 = sv.multiply(subexp1); //SV[(1+i)-LS]

        BigDecimal subexp21 = i_plus_1.pow(ls).subtract(BigDecimal.ONE); //((1+i)LS - 1)
        BigDecimal subexp22 = SharedCalculationUtil.divideSafely(subexp21,
                i.multiply(i_plus_1.pow(ls)));   //((1+i)LS - 1)/(i(1+i)LS)
        BigDecimal exp2 = oAndM.multiply(subexp22);//O&M[((1+i)LS - 1)/(i(1+i)LS)]
        return c.subtract(exp1.add(exp2));
    }

    private Map<Long, BigDecimal> createOptionGrossBenefitMap(List<RiskBenefitDTO> metricAnalysis) {
        Map<Long, BigDecimal> optionBenefitMap = new HashMap<Long, BigDecimal>();
        for (RiskBenefitDTO benefitDTO : metricAnalysis) {
            BigDecimal baselineQuantity = benefitDTO.getBaselineQuantity();
            List<RiskBenefitOptionDTO> optionDTOs = benefitDTO.getOptionDTOs();
            for (RiskBenefitOptionDTO optionDTO : optionDTOs) {
                Long optionId = optionDTO.getParreAnalysisId();
                BigDecimal optionTotal = optionBenefitMap.get(optionId);
                if (optionTotal == null) {
                    optionTotal = BigDecimal.ZERO;
                }
                BigDecimal optionBenefit;
                try {
                    optionBenefit = baselineQuantity
                        .subtract(optionDTO.getOptionAmountDTO().getQuantity());
                } catch (NullPointerException n) {
                    optionBenefit = BigDecimal.ZERO;
                }

                optionTotal = optionTotal.add(optionBenefit);
                optionBenefitMap.put(optionId, optionTotal);
            }
        }
        return optionBenefitMap;
    }

    private ParreAnalysis getCurrentBaseline() {
        ParreAnalysis currentAnalysis = getCurrentUser().getCurrentAnalysis();
        return currentAnalysis.isBaseline() ? currentAnalysis : currentAnalysis.getBaseline();
    }

    public List<RiskBenefitDTO> getMetricAnalysis(RiskBenefitMetricType metricType) {
        ParreAnalysis baseline = getCurrentBaseline();
        switch (metricType) {
            case FATALITIES:
                return populateDeltas(getFatalities(baseline));
            case SERIOUS_INJURIES:
                return populateDeltas(getSeriousInjuries(baseline));
            case FINANCIAL_IMPACT:
                return populateDeltas(getFinancialImpact(baseline));
            case FINANCIAL_TOTAL:
                return populateDeltas(getFinancialTotal(baseline));
            case ECONOMIC_IMPACT:
                return populateDeltas(getEconomicImact(baseline));
            case VULNERABILITY:
                return populateDeltas(getVulnerability(baseline));
            case LOT:
                return populateDeltas(getLot(baseline));
            case RISK:
                return populateDeltas(getRisk(baseline));
            case OWNER_RESILIENCE:
                return populateDeltas(getOwnerResilience(baseline));
            case ECONOMIC_RESILIENCE:
                return populateDeltas(getEconomicResilience(baseline));
            default:
                throw new IllegalArgumentException("Unknown metric type : " + metricType);
        }

    }

    protected List<RiskBenefitDTO> populateDeltas(List<RiskBenefitDTO> dtos) {
        for (RiskBenefitDTO dto : dtos) {
            dto.populateOptionDeltas();
        }
        return dtos;
    }

    private List<RiskBenefitDTO> getEconomicResilience(ParreAnalysis baseline) {
        return getCalculatedRiskResilienceValues(baseline, ErValueGetter.INSTANCE);
    }

    private List<RiskBenefitDTO> getOwnerResilience(ParreAnalysis baseline) {
        return getCalculatedRiskResilienceValues(baseline, FrValueGetter.INSTANCE);
    }

    private List<RiskBenefitDTO> getRisk(ParreAnalysis baseline) {
        return getCalculatedRiskResilienceValues(baseline, RiskValueGetter.INSTANCE);
    }

    private List<RiskBenefitDTO> getLot(ParreAnalysis baseline) {
        Map<Long, Map<Long, BigDecimal>> valueMap = RiskResilienceManager.getLot(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new QuantityMap(valueMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getVulnerability(ParreAnalysis baseline) {
        Map<Long, Map<Long, BigDecimal>> valueMap = RiskResilienceManager.getVulnerability(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new QuantityMap(valueMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getFinancialImpact(ParreAnalysis baseline) {
        Map<Long, Map<Long, AmountDTO>> fiMap = RiskResilienceManager.getFinancialImpact(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new MoneyMap(fiMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getFinancialTotal(ParreAnalysis baseline) {
        Map<Long, Map<Long, AmountDTO>> fiMap;
        if(baseline.getUseStatValues()) {
            fiMap = RiskResilienceManager.getFinancialTotalWithStatValues(baseline.getAllIds(), entityManager());
        }
        else {
            fiMap = RiskResilienceManager.getFinancialTotal(baseline.getAllIds(), entityManager());
        }

        AmountMap amountMap = new MoneyMap(fiMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getEconomicImact(ParreAnalysis baseline) {
        Map<Long, Map<Long, AmountDTO>> fiMap = RiskResilienceManager.getEconomicImpact(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new MoneyMap(fiMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }


    private List<RiskBenefitDTO> getFatalities(ParreAnalysis baseline) {
        Map<Long, Map<Long, BigDecimal>> fatalitiesMap = RiskResilienceManager.getFatalities(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new QuantityMap(fatalitiesMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getSeriousInjuries(ParreAnalysis baseline) {
        Map<Long, Map<Long, BigDecimal>> fatalitiesMap = RiskResilienceManager.getSeriousInjuries(baseline.getAllIds(), entityManager());
        AmountMap amountMap = new QuantityMap(fatalitiesMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private List<RiskBenefitDTO> getCalculatedRiskResilienceValues(ParreAnalysis baseline, RrValueGetter valueGetter) {
        Map<Long, List<RiskResilienceDTO>> rrMap = getCalculateRiskResilienceDTOs(baseline);
        Map<Long, Map<Long, AmountDTO>> erMap = new HashMap<Long, Map<Long, AmountDTO>>(rrMap.size());
        for (Map.Entry<Long, List<RiskResilienceDTO>> entry : rrMap.entrySet()) {
            List<RiskResilienceDTO> values = entry.getValue();
            Map<Long, AmountDTO> valueMap = new HashMap<Long, AmountDTO>(values.size());
            for (RiskResilienceDTO value : values) {
                valueMap.put(value.getAssetThreatId(), valueGetter.getValue(value));
                if (log.isDebugEnabled()) { log.debug("ASSET THREAT ID: "+value.getAssetThreatId()); }
            }
            
            if (log.isDebugEnabled()) { log.debug("ENTRY KEY: "+entry.getKey()); }
            
            erMap.put(entry.getKey(), valueMap);

        }
        AmountMap amountMap = new MoneyMap(erMap);
        return createRiskBenefitDTOs(baseline, amountMap);
    }

    private Map<Long, List<RiskResilienceDTO>> getCalculateRiskResilienceDTOs(ParreAnalysis baseline) {
        List<Long> allIds = baseline.getAllIds();
        Map<Long, List<RiskResilienceDTO>> rrMap = RiskResilienceManager.getRiskResilienceDTOs(allIds, entityManager());
        Map<Long, UnitPriceDTO> unitPriceDTOs = RiskResilienceManager.getParreAnalysisUnitPrices(allIds, entityManager());
        Set<Long> parreAnalysisIds = rrMap.keySet();
        for (Long parreAnalysisId : parreAnalysisIds) {
            List<RiskResilienceDTO> riskResilienceDTOs = rrMap.get(parreAnalysisId);
            UnitPriceDTO unitPriceDTO = unitPriceDTOs.get(parreAnalysisId);
            riskResilienceDTOs = updateRiskResilienceDTOs(unitPriceDTO, riskResilienceDTOs, parreAnalysisId);
            rrMap.put(parreAnalysisId, riskResilienceDTOs);
        }
        return rrMap;
    }

    private List<RiskBenefitDTO> createRiskBenefitDTOs(ParreAnalysis baseline, AmountMap amountMap) {
        List<AssetThreat> assetThreats = baseline.getActiveAssetThreats();
        List<RiskBenefitDTO> riskBenefitDTOs = new ArrayList<RiskBenefitDTO>(assetThreats.size());
        List<ParreAnalysis> optionAnalyses = new ArrayList<ParreAnalysis>(baseline.getOptionParreAnalyses());
        //Sometimes the options are out of order, this reorders them
        for(int i = 0; i < optionAnalyses.size() - 1; i++) {
            for(int j = i + 1; j < optionAnalyses.size(); j++) {
                if(optionAnalyses.get(i).getId() > optionAnalyses.get(j).getId()) {
                    ParreAnalysis temp = optionAnalyses.get(j);
                    optionAnalyses.set(j, optionAnalyses.get(i));
                    optionAnalyses.set(i, temp);
                }
            }
        }
        for (AssetThreat assetThreat : assetThreats) {
            RiskBenefitDTO riskBenefitDTO = new RiskBenefitDTO();
            assetThreat.populate(riskBenefitDTO);
            riskBenefitDTO.updateId();
            
            if (log.isDebugEnabled()) { 
            	log.debug("\n\n\n\nBASELINE ID: "+baseline.getId());
            	log.debug("ASSET THREAT ID: "+assetThreat.getId()+ "\n\n\n\n");
            }
            
            AmountDTO amount = amountMap.getAmount(baseline.getId(), assetThreat.getId());
            riskBenefitDTO.setBaselineAmountDTO(amount);
            for (ParreAnalysis optionAnalysis : optionAnalyses) {
                RiskBenefitOptionDTO optionDTO = new RiskBenefitOptionDTO();
                optionDTO.setParreAnalysisId(optionAnalysis.getId());
                optionDTO.updateId();
                optionDTO.setDisplayOrder(optionAnalysis.getDisplayOrder());
                optionDTO.setOptionAmountDTO(amountMap.getAmount(optionAnalysis.getId(), assetThreat.getId()));
                riskBenefitDTO.addRiskBenefitOptionDTO(optionDTO);
            }
            riskBenefitDTOs.add(riskBenefitDTO);
        }
        return riskBenefitDTOs;
    }

    private void populateOptionAnalysisDTOs(ParreAnalysis parreAnalysis, RiskBenefitAnalysisDTO dto) {
        dto.setOptionAnalysisDTOs(parreAnalysis.createOptionDTOs());
    }

    public void updateBudget(Long riskBenefitAnalysisId, AmountDTO budgetAmount) {
        RiskBenefitAnalysis analysis = find(RiskBenefitAnalysis.class, riskBenefitAnalysisId);
        analysis.setBudgetDTO(budgetAmount);
        saveEntity(analysis);
    }

    public ParreAnalysisDTO startOptionAnalysis(Long riskBenefitAnalysisId, NameDescriptionDTO dto) {
        RiskBenefitAnalysis analysis = find(RiskBenefitAnalysis.class, riskBenefitAnalysisId);
        ParreAnalysis baseline = analysis.getParreAnalysis();
        ParreAnalysis optionAnalysis = copyParreAnalysis(dto, baseline, false);
        baseline.addOptionParreAnalysis(optionAnalysis);
        optionAnalysis.setDisplayOrder(baseline.getOptionParreAnalyses().size());
        saveEntity(optionAnalysis);
        return optionAnalysis.createDTO();
    }


    private RiskBenefitAnalysis initRiskBenefitAnalysis(ParreAnalysis parreAnalysis) {
        RiskBenefitAnalysis analysis;
        analysis = new RiskBenefitAnalysis(parreAnalysis);
        analysis.setBudget(new Amount(BigDecimal.ZERO, MoneyUnitType.DOLLAR));
        saveEntityWithFlush(analysis);
        return analysis;
    }

}
