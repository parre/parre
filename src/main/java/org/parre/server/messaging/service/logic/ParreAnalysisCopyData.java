/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service.logic;


import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.parre.server.domain.*;

/**
 * The Class ParreAnalysisCopyData.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/8/12
 */
public class ParreAnalysisCopyData {
    public static final ParreAnalysisCopyData EMPTY = new ParreAnalysisCopyData
            (Collections.<Long, AssetThreat>emptyMap(), new HashMap<Long, CounterMeasure>(),
            Collections.<Long, EarthquakeProfile>emptyMap(), Collections.<Long, HurricaneProfile>emptyMap(), new HashMap<Long, DiagramAnalysis>(),
                    new HashMap<Long, PathAnalysis>(), new HashMap<Long, ProxyIndication>());
    private final Map<Long, AssetThreat> assetThreatCopies;
    private HashMap<Long, CounterMeasure> counterMeasureCopies;
    private final Map<Long, EarthquakeProfile> earthquakeProfileCopies;
    private final Map<Long, HurricaneProfile> hurricaneProfileCopies;
    private HashMap<Long, DiagramAnalysis> diagramAnalysisCopies;
    private HashMap<Long, PathAnalysis> pathAnalysisCopies;
    private HashMap<Long, ProxyIndication> proxyIndicationCopies;
    
    public ParreAnalysisCopyData(Map<Long, AssetThreat> assetThreatCopies, HashMap<Long, CounterMeasure> counterMeasureCopies) {
    	this(assetThreatCopies, counterMeasureCopies,
                Collections.<Long, EarthquakeProfile>emptyMap(), Collections.<Long, HurricaneProfile>emptyMap(),
                new HashMap<Long, DiagramAnalysis>(), new HashMap<Long, PathAnalysis>(), new HashMap<Long, ProxyIndication>());
    }

    public ParreAnalysisCopyData(Map<Long, AssetThreat> assetThreatCopies, HashMap<Long, CounterMeasure> counterMeasureCopies,
                                 Map<Long, EarthquakeProfile> earthquakeProfileCopies, Map<Long, HurricaneProfile> hurricaneProfileCopies,
                                 HashMap<Long, DiagramAnalysis> diagramAnalysisCopies, HashMap<Long, PathAnalysis> pathAnalysisCopies,
                                 HashMap<Long, ProxyIndication> proxyIndicationCopies) {
        this.assetThreatCopies = assetThreatCopies;
        this.counterMeasureCopies = counterMeasureCopies;
        this.earthquakeProfileCopies = earthquakeProfileCopies;
        this.hurricaneProfileCopies = hurricaneProfileCopies;
        this.diagramAnalysisCopies = diagramAnalysisCopies;
        this.pathAnalysisCopies = pathAnalysisCopies;
        this.proxyIndicationCopies = proxyIndicationCopies;
    }

    public boolean isEmpty() {
        return assetThreatCopies.isEmpty();
    }
    public Map<Long, AssetThreat> getAssetThreatCopies() {
        return assetThreatCopies;
    }

    public HashMap<Long, CounterMeasure> getCounterMeasureCopies() {
        return counterMeasureCopies;
    }

	public Map<Long, EarthquakeProfile> getEarthquakeProfileCopies() {
		return earthquakeProfileCopies;
	}

	public Map<Long, HurricaneProfile> getHurricaneProfileCopies() {
		return hurricaneProfileCopies;
	}

    public HashMap<Long, DiagramAnalysis> getDiagramAnalysisCopies() {
        return diagramAnalysisCopies;
    }

    public HashMap<Long, PathAnalysis> getPathAnalysisCopies() {
        return pathAnalysisCopies;
    }

    public HashMap<Long, ProxyIndication> getProxyIndicationCopies() {
        return proxyIndicationCopies;
    }

	public void setCounterMeasureCopies(HashMap<Long, CounterMeasure> copyCounterMeasures) {
		this.counterMeasureCopies = copyCounterMeasures;
	}

    public void setDiagramAnalysisCopies(HashMap<Long, DiagramAnalysis> longDiagramAnalysisHashMap) {
        this.diagramAnalysisCopies = longDiagramAnalysisHashMap;
    }

    public void setPathAnalysisCopies(HashMap<Long, PathAnalysis> longPathAnalysisHashMap) {
        this.pathAnalysisCopies = longPathAnalysisHashMap;
    }

    public void setProxyindicationCopies(HashMap<Long, ProxyIndication> proxyIndicationHashMap) {
        this.proxyIndicationCopies = proxyIndicationHashMap;
    }
}
