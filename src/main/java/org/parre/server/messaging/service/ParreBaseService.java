/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.parre.server.AssetThresholdSpec;
import org.parre.server.FilterUtil;
import org.parre.server.ParreDatabase;
import org.parre.server.domain.Analysis;
import org.parre.server.domain.AnalysisTreeNode;
import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetNote;
import org.parre.server.domain.AssetThreat;
import org.parre.server.domain.AssetThreatEntity;
import org.parre.server.domain.AssetThreatLevel;
import org.parre.server.domain.AttackStep;
import org.parre.server.domain.CounterMeasure;
import org.parre.server.domain.DetectionLikelihood;
import org.parre.server.domain.DiagramAnalysis;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.DirectedThreatAnalysis;
import org.parre.server.domain.Dp;
import org.parre.server.domain.DpAnalysis;
import org.parre.server.domain.EarthquakeProfile;
import org.parre.server.domain.HurricaneProfile;
import org.parre.server.domain.IceStormThreatDetails;
import org.parre.server.domain.InactiveThreat;
import org.parre.server.domain.NaturalThreat;
import org.parre.server.domain.NaturalThreatAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.PathAnalysis;
import org.parre.server.domain.ProxyIndication;
import org.parre.server.domain.ProxyTier;
import org.parre.server.domain.ResponseStep;
import org.parre.server.domain.RiskResilienceAnalysis;
import org.parre.server.domain.Threat;
import org.parre.server.domain.User;
import org.parre.server.domain.manager.AssetManager;
import org.parre.server.domain.manager.DirectedThreatManager;
import org.parre.server.domain.manager.DpManager;
import org.parre.server.domain.manager.LookupDataManager;
import org.parre.server.domain.manager.LotManager;
import org.parre.server.domain.manager.NaturalThreatManager;
import org.parre.server.domain.manager.RiskResilienceManager;
import org.parre.server.domain.manager.ParreManager;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.domain.util.EntityObject;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.messaging.service.logic.AssetThreatAnalysisState;
import org.parre.server.messaging.service.logic.AssetThreatLevelState;
import org.parre.server.messaging.service.logic.ParreAnalysisCopyData;
import org.parre.shared.AmountDTO;
import org.parre.shared.AnalysisDTO;
import org.parre.shared.BaseDTO;
import org.parre.shared.DpAnalysisDTO;
import org.parre.shared.LoginInfo;
import org.parre.shared.NameDescriptionDTO;
import org.parre.shared.NaturalThreatAnalysisDTO;
import org.parre.shared.ProxyCityDTO;
import org.parre.shared.ProxyIndicationDTO;
import org.parre.shared.RiskResilienceAnalysisDTO;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.SharedCalculationUtil;
import org.parre.shared.ThreatDTO;
import org.parre.shared.UnitPriceDTO;
import org.parre.shared.types.ParreAnalysisStatusType;
import org.parre.shared.types.ThreatCategoryType;
import org.parre.shared.util.ThreatTypeUtil;


/**
 * The Class ParreBaseService.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/3/11
 */
public abstract class ParreBaseService {
    private static transient final Logger log = Logger.getLogger(ParreBaseService.class);

    protected <T> T find(Class<T> entityClass, Long id) {
        return entityManager().find(entityClass, id);
    }

    protected EntityManager entityManager() {
        return ServiceInvocationContext.getCurrentInstance().getEntityManager();
    }

    protected User getCurrentUser() {
        LoginInfo loginInfo = ServiceInvocationContext.getCurrentInstance().getLoginInfo();
        return ParreManager.findUser(loginInfo.getEmailAddress(), entityManager());
    }

    protected void saveEntityWithFlush(EntityObject baseEntity) {
        entityManager().persist(baseEntity);
        entityManager().flush();
    }

    protected void saveEntity(EntityObject baseEntity) {
        entityManager().persist(baseEntity);
    }

    protected List<ThreatDTO> getDirectedThreatAnalysisDTOs(Long parreAnalysisId) {
        List<Threat> activeThreats = getDirectedAnalysisThreats(parreAnalysisId);
        return Threat.createDTOs(activeThreats);
    }

    protected List<Threat> getDirectedAnalysisThreats(Long parreAnalysisId) {
        return ThreatManager.getActiveThreats(parreAnalysisId, ThreatCategoryType.MAN_MADE_HAZARD, entityManager());
    }

    public ParreAnalysis findParreAnalysis(Long parreAnalysisId) {
        return entityManager().find(ParreAnalysis.class, parreAnalysisId);
    }

    protected List<Asset> getAssetsForAnalysis(Long parreAnalysisId) {
        Integer assetThreshold = findParreAnalysis(parreAnalysisId).getAssetThreshold();
        List<Asset> allAssets = AssetManager.getAssets(parreAnalysisId, entityManager());
        return FilterUtil.filter(new AssetThresholdSpec(assetThreshold), allAssets);
    }

    protected Set<? extends AssetThreatEntity> initAssetThreatAnalyses(ParreAnalysis parreAnalysis, ThreatCategoryType threatCategory, AssetThreatAnalysisFactory<AssetThreatEntity> factory) {
        List<AssetThreat> assetThreats = ParreManager.getAllAssetThreats(parreAnalysis.getId(), entityManager());
        List<Asset> assets = getAssetsForAnalysis(parreAnalysis.getId());
        List<Threat> threats = ThreatManager.getActiveThreats(parreAnalysis.getId(), entityManager());
        AssetThreatAnalysisState state = createAssetThreatAnalysisState(parreAnalysis.getId());

        List<AssetThreat> newAssetThreats = new ArrayList<AssetThreat>();
        for (Asset asset : assets) {
            for (Threat threat : threats) {
                boolean found = false;
                for (AssetThreat assetThreat : assetThreats) {
                    if (assetThreat.isFor(asset, threat)) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    if (state.analysisRequired(asset, threat)) {
                        AssetThreat newAssetThreat = new AssetThreat(parreAnalysis, asset, threat);
                        saveEntityWithFlush(newAssetThreat);
                        newAssetThreats.add(newAssetThreat);
                    }
                }
            }
        }
        List<AssetThreat> newList = new ArrayList<AssetThreat>(assetThreats.size() + newAssetThreats.size());
        newList.addAll(assetThreats);
        newList.addAll(newAssetThreats);

        return initAssetThreatAnalyses(parreAnalysis, threatCategory, newList, factory);
    }

    protected List<AssetThreat> initAssetThreats(ParreAnalysis parreAnalysis, List<Asset> assets, List<Threat> threats) {
        AssetThreatAnalysisState state = createAssetThreatAnalysisState(parreAnalysis.getId());
        List<AssetThreat> assetThreats = new ArrayList<AssetThreat>(assets.size() * threats.size());
        for (Asset asset : assets) {
            for (Threat threat : threats) {
                if (state.analysisRequired(asset, threat)) {
                    AssetThreat assetThreat = new AssetThreat(parreAnalysis, asset, threat);
                    saveEntityWithFlush(assetThreat);
                    assetThreats.add(assetThreat);
                }
            }
        }
        return assetThreats;
    }

    protected Set<? extends AssetThreatEntity> initAssetThreatAnalyses(ParreAnalysis parreAnalysis, ThreatCategoryType threatCategory, List<AssetThreat> assetThreats, AssetThreatAnalysisFactory<AssetThreatEntity> factory) {
        AssetThreatAnalysisState state = createAssetThreatAnalysisState(parreAnalysis.getId());

        Set<AssetThreatEntity> analyses = new HashSet<AssetThreatEntity>();
        for (AssetThreat assetThreat : assetThreats) {
            if (assetThreat.getRemoved()) {
                continue;
            }
            ThreatCategoryType currentThreatCategory = assetThreat.getThreat().getThreatCategory();
            if (!(threatCategory.isNone() || currentThreatCategory == threatCategory)) {
                continue;
            }
            if (threatCategory.isNone() || state.analysisRequired(assetThreat)) {
                AssetThreatEntity assetThreatEntity = factory.create(assetThreat);
                assetThreatEntity.setParreAnalysis(parreAnalysis);
                analyses.add(assetThreatEntity);
            }
        }
        return analyses;
    }

    protected AssetThreatLevelState createAssetThreatLevelState(Long parreAnalysisId) {
        List<AssetThreatLevel> levels = ParreManager.getAssetThreatLevels(parreAnalysisId, entityManager());
        return new AssetThreatLevelState(levels);
    }

    protected AssetThreatAnalysisState createAssetThreatAnalysisState(Long parreAnalysisId) {
        List<AssetThreatLevel> levels = ParreManager.getAssetThreatLevels(parreAnalysisId, entityManager());
        Integer assetThreatThreshold = findParreAnalysis(parreAnalysisId).getAssetThreatThreshold();
        List<Long> inactiveThreatIds = ThreatManager.getInactiveThreatIds(parreAnalysisId, entityManager());
        return new AssetThreatAnalysisState(assetThreatThreshold, levels, inactiveThreatIds);
    }


    protected void saveEntities(Set<? extends EntityObject> entityObjects) {
        for (EntityObject entity : entityObjects) {
            saveEntityWithFlush(entity);
        }
    }

    protected void assertNotNew(BaseDTO baseDTO) {
        if (baseDTO.isNew()) {
            throw new IllegalArgumentException("Not new dto expected : " + baseDTO);
        }
    }

    protected void assertNew(BaseDTO dto) {
        if (!dto.isNew()) {
            throw new IllegalArgumentException("New Object expected : " + dto);
        }
    }

    protected LoginInfo getLoginInfo() {
        return ServiceInvocationContext.getCurrentInstance().getLoginInfo();
    }

    protected <T> T getCurrentDirectedThreatAnalysisDTO(Long parreAnalysisId, T analysisDTO) {
        DirectedThreatAnalysis currentAnalysis = DirectedThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
        return currentAnalysis != null ? currentAnalysis.copyInto(analysisDTO) : null;
    }

    protected <T> T startDirectedThreatAnalysis(T dto) {
        AnalysisDTO analysisDTO = (AnalysisDTO) dto;
        analysisDTO.setName("Directed Threat Analysis");
        DirectedThreatAnalysis analysis = startDtAnalysis(analysisDTO);
        return analysis.copyInto(dto);
    }

    protected DirectedThreatAnalysis startDtAnalysis(AnalysisDTO analysisDTO) {
        ParreAnalysis parreAnalysis = findParreAnalysis(analysisDTO.getParreAnalysisId());
        DirectedThreatAnalysis analysis = new DirectedThreatAnalysis(analysisDTO);
        analysis.setParreAnalysis(parreAnalysis);
        analysis.setStartedDate(new Date());
        saveEntityWithFlush(analysis);
        analysis.addAssetThreats(initDirectedThreats(parreAnalysis));
        saveEntityWithFlush(analysis);
        return analysis;
    }

    private Set<? extends AssetThreatEntity> initDirectedThreats(ParreAnalysis parreAnalysis) {
        Set<? extends AssetThreatEntity> analyses = initAssetThreatAnalyses(parreAnalysis, ThreatCategoryType.MAN_MADE_HAZARD,
                DirectedThreatFactory.getInstance());
        saveEntities(analyses);
        return analyses;
    }

    protected DirectedThreatAnalysis getCurrentDirectedThreatAnalysis(Long parreAnalysisId) {
        return DirectedThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    }

    protected NaturalThreatAnalysis getCurrentNaturalThreatAnalysis(Long parreAnalysisId) {
        return NaturalThreatManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    }

    protected DpAnalysis getCurrentDpAnalysis(Long parreAnalysisId) {
        return DpManager.getCurrentAnalysis(parreAnalysisId, entityManager());
    }

    protected List<RiskResilienceDTO> createRiskResilienceDTOs(RiskResilienceAnalysis analysis, Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = analysis.getParreAnalysis();
        UnitPriceDTO unitPriceDTO = analysis.getUnitPriceDTO();
        List<RiskResilienceDTO> dtos = RiskResilienceManager.getRiskResilienceDTOs(parreAnalysis.getId(), entityManager());
        return updateRiskResilienceDTOs(unitPriceDTO, dtos, parreAnalysisId);
    }

    protected List<RiskResilienceDTO> updateRiskResilienceDTOs(UnitPriceDTO unitPriceDTO, List<RiskResilienceDTO> dtos, Long parreAnalysisId) {
    	dtos = rollupRiskResilienceDTOs(dtos);
        for (int i = 0; i<dtos.size();i++) {
        	if (!ThreatTypeUtil.isNaturalThreat(dtos.get(i).getThreatName()) && dtos.get(i).getRisk().equals(BigDecimal.ZERO)) {
        		dtos.get(i).calculateRisk();
        	}
            SharedCalculationUtil.calculateFinancialTotal(dtos.get(i).getFatalities().intValue(),
                                                                dtos.get(i).getSeriousInjuries().intValue(),
                                                                dtos.get(i).getOwnersFinancialImpact(),
                                                                ParreDatabase.get().getStatisticalValuesDTO());
            dtos.get(i).setUnitPriceDTO(unitPriceDTO);
            dtos.get(i).calculateOwnerResilience();
        }
        return dtos;
    }
    
    protected List<RiskResilienceDTO> rollupRiskResilienceDTOs(List<RiskResilienceDTO> dtos) {
        for (int i = 0; i<dtos.size()-1;i++) {
            if(dtos.get(i).getThreatName().equals("D(S)") || dtos.get(i).getThreatName().equals("D(U)")) {
            	RiskResilienceDTO rollup = dtos.get(i);
            	BigDecimal fA = rollup.getFatalities();
                BigDecimal sI = rollup.getSeriousInjuries();
                BigDecimal ofi = rollup.getOwnersFinancialImpact();
                BigDecimal vul = rollup.getVulnerability();
                BigDecimal lot = rollup.getLot();
                BigDecimal fT = rollup.getFinancialTotalQuantity();
                BigDecimal eR = rollup.getEconomicResilienceQuantity();
                BigDecimal tR = rollup.getRisk();
                BigDecimal dD = rollup.getDurationDays();
                BigDecimal smgd= rollup.getSeverityMgd();
                int count = 1;
                for(int j = i + 1; j<dtos.size();j++) {
                    if(dtos.get(i).getAssetName().equals(dtos.get(j).getAssetName()) && 
                    	dtos.get(i).getThreatName().equals(dtos.get(j).getThreatName())) {
                    		count++;
                    		RiskResilienceDTO child = dtos.get(j);
                    		fA = fA.add(child.getFatalities());
                            sI = sI.add(child.getSeriousInjuries());
                            dD = dD.add(child.getDurationDays());
                            smgd=smgd.add(child.getSeverityMgd());
                            ofi = ofi.add(child.getOwnersFinancialImpact());
                            vul = vul.add(child.getVulnerability());
                            lot = lot.add(child.getLot());
                            fT = fT.add(child.getFinancialTotalQuantity());
                            eR = eR.add(child.getEconomicResilienceQuantity());
                            tR = tR.add(child.getRisk());
                        	dtos.remove(j--);
                    }
                }
                
                if (count > 1) {
                	rollup.setFatalities(fA);
                	rollup.setSeriousInjuries(sI);
                	rollup.setDurationDays(dD);
                	rollup.setSeverityMgd(smgd);
                	rollup.setOwnersFinancialImpact(ofi);
                	rollup.setVulnerability(SharedCalculationUtil.divideSafely(vul, BigDecimal.valueOf(count - 1)));
                	rollup.setLot(SharedCalculationUtil.divideSafely(lot, BigDecimal.valueOf(count - 1)));
                     
                	rollup.setEconomicResilience(new AmountDTO(eR));
                	rollup.setFinancialTotal(new AmountDTO(fT));
                    rollup.setRisk(tR);
                }
            }
        }
        return dtos;
    }

    protected NaturalThreatAnalysis startNaturalThreatAnalysis(NaturalThreatAnalysisDTO dto) {
        ParreAnalysis parreAnalysis = findParreAnalysis(dto.getParreAnalysisId());
        NaturalThreatAnalysis analysis = new NaturalThreatAnalysis(dto);
        analysis.setParreAnalysis(parreAnalysis);
        analysis.setStartedDate(new Date());
        saveEntityWithFlush(analysis);
        analysis.addAssetThreats(initNaturalThreats(parreAnalysis));
        saveEntityWithFlush(analysis);
        return analysis;
    }

    private Set<? extends AssetThreatEntity> initNaturalThreats(ParreAnalysis parreAnalysis) {
        Set<? extends AssetThreatEntity> naturalThreats = initAssetThreatAnalyses(parreAnalysis, ThreatCategoryType.NATURAL_HAZARD, NaturalThreatFactory.getInstance());
        saveEntities(naturalThreats);
        return naturalThreats;
    }

    protected DpAnalysis startDpAnalysis(DpAnalysisDTO dto) {
        ParreAnalysis parreAnalysis = findParreAnalysis(dto.getParreAnalysisId());
        DpAnalysis analysis = new DpAnalysis(dto);
        analysis.setParreAnalysis(parreAnalysis);
        analysis.setStartedDate(new Date());
        saveEntityWithFlush(analysis);
        analysis.addAssetThreats(initDps(parreAnalysis));
        saveEntityWithFlush(analysis);
        return analysis;
    }

    private Set<? extends AssetThreatEntity> initDps(ParreAnalysis parreAnalysis) {
        Set<? extends AssetThreatEntity> assetThreatEntities = initAssetThreatAnalyses
                (parreAnalysis, ThreatCategoryType.DEPENDENCY_PROXIMITY, DpFactory.getInstance());
        saveEntities(assetThreatEntities);
        return assetThreatEntities;
    }

    protected RiskResilienceAnalysis startRiskResilienceAnalysis(RiskResilienceAnalysisDTO analysisDTO) {
        ParreAnalysis parreAnalysis = findParreAnalysis(analysisDTO.getParreAnalysisId());
        RiskResilienceAnalysis analysis = new RiskResilienceAnalysis(analysisDTO);
        analysis.setParreAnalysis(parreAnalysis);
        analysis.setStartedDate(new Date());
        saveEntityWithFlush(analysis);
        analysis.addAssetThreats(initRiskResiliences(parreAnalysis));
        saveEntityWithFlush(analysis);
        return analysis;
    }

    private Set<? extends AssetThreatEntity> initRiskResiliences(ParreAnalysis parreAnalysis) {
        Set<? extends AssetThreatEntity> analyses = initAssetThreatAnalyses(parreAnalysis, ThreatCategoryType.NONE, RiskResilienceFactory.getInstance());
        saveEntities(analyses);
        return analyses;

    }

    protected Long getBaselineId(Long parreAnalysisId) {
        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);
        return parreAnalysis.isOption() ? parreAnalysis.getBaseline().getId() : parreAnalysisId;
    }

    protected void updateProbabilities(ProxyIndication proxyIndication) {
    	if (log.isDebugEnabled()) { log.debug("Updating probability for ProxyIndication : " + proxyIndication.getName()); }
        List<DirectedThreat> lotsWithProxyIndication = LotManager.getLotsWithProxyIndication(proxyIndication, entityManager());
        if (log.isDebugEnabled()) { log.debug("Number of Lots with this ProxyIndication : " + lotsWithProxyIndication.size()); }
        for (DirectedThreat directedThreat : lotsWithProxyIndication) {
            ProxyIndicationDTO proxyIndicationDTO = calculateProbability(proxyIndication, directedThreat);
            directedThreat.setLot(proxyIndicationDTO.getCalculatedProbability());
            saveEntity(directedThreat);
        }
    }

    protected ProxyIndicationDTO calculateProbability(ProxyIndication proxyIndication, DirectedThreat lot) {
        ProxyIndicationDTO dto = proxyIndication.createDTO();
        if (dto.getProxyCityDTO() == null) {
            Integer tierNumber = CalculationUtil.calculateTierFromPopulation(dto.getPopulation());
            ProxyTier proxyTier = LotManager.findProxyTier(tierNumber, entityManager());
            ProxyCityDTO proxyCityDTO = new ProxyCityDTO("", proxyTier.getTierNumber(), proxyTier.getNumberOfCities());
            proxyCityDTO.setId(-1L);
            dto.setProxyCityDTO(proxyCityDTO);
        }
        ParreAnalysis parreAnalysis = lot.getAnalysis().getParreAnalysis();
        List<DirectedThreat> directedThreats = DirectedThreatManager.getDirectedThreats(parreAnalysis, entityManager());
        List<DetectionLikelihood> detectionLikelihoods = LookupDataManager.getDetectionLikelihoods(entityManager());
        Map<Threat, BigDecimal> dlMap = new HashMap<Threat, BigDecimal>(detectionLikelihoods.size());
        for (DetectionLikelihood detectionLikelihood : detectionLikelihoods) {
            dlMap.put(detectionLikelihood.getThreat(), detectionLikelihood.getLikelihood());
        }

        BigDecimal sum = BigDecimal.ZERO;
        for (DirectedThreat directedThreat : directedThreats) {
            BigDecimal c_v_dl = calcCVOneMinusDL(directedThreat, dlMap);
            sum = sum.add(c_v_dl);
        }
        BigDecimal numerator = calcCVOneMinusDL(lot, dlMap);
        BigDecimal node6 = SharedCalculationUtil.divideSafely(numerator, sum);
        dto.setNode6Value(node6);
        BigDecimal calculatedProbability = CalculationUtil.calculateProbability(dto);
        dto.setCalculatedProbability(calculatedProbability);
        return dto;
    }

    private BigDecimal calcCVOneMinusDL(DirectedThreat directedThreat, Map<Threat, BigDecimal> dlMap) {
        BigDecimal c = directedThreat.getFinancialImpact().getQuantity();
        BigDecimal v = directedThreat.getVulnerability();
        BigDecimal oneMinusDL = BigDecimal.ZERO;
        try {
            oneMinusDL = BigDecimal.ONE.subtract(dlMap.get(directedThreat.getThreat()));
        } catch (Exception ex) {
            if(log.isDebugEnabled()) {
                log.debug("Null detection likelihood found, setting as .5: ParreBaseService:459");
            }
            oneMinusDL = BigDecimal.valueOf(.5);
        }
        BigDecimal calculatedValue = c.multiply(v).multiply(oneMinusDL);
        return calculatedValue;
    }

    protected ParreAnalysis copyParreAnalysis(NameDescriptionDTO dto, ParreAnalysis srcParreAnalysis, boolean makeFullyCopy) {
        ParreAnalysis copiedAnalysis = new ParreAnalysis();
        copiedAnalysis.copyFrom(srcParreAnalysis);
        copiedAnalysis.setBaseline(null);
        copiedAnalysis.setCreatedTime(new Date());
        copiedAnalysis.setStatus(ParreAnalysisStatusType.ACTIVE);
        copiedAnalysis.setOptionParreAnalyses(Collections.<ParreAnalysis>emptySet());
        copiedAnalysis.setAssetThreats(Collections.<AssetThreat>emptySet());
        copiedAnalysis.setEarthquakeProfiles(Collections.<EarthquakeProfile>emptySet());
        copiedAnalysis.setHurricaneProfiles(Collections.<HurricaneProfile>emptySet());
        copiedAnalysis.setDefaultEarthquakeProfile(null);
        copiedAnalysis.setDefaultHurricaneProfile(null);
        copiedAnalysis.copyFrom(dto);
        copiedAnalysis.setId(null);
        
        saveEntityWithFlush(copiedAnalysis);

        ParreAnalysisCopyData copyData = ParreAnalysisCopyData.EMPTY;
        if (makeFullyCopy) {
            copyData = copyRelatedItems(copiedAnalysis, srcParreAnalysis);
        } else {
        	copyData.setCounterMeasureCopies(copyCounterMeasures(copiedAnalysis, srcParreAnalysis));
            copyData.setDiagramAnalysisCopies(copyEachDiagramAnalysis(copiedAnalysis, srcParreAnalysis, copyData.getCounterMeasureCopies()));
            copyData.setPathAnalysisCopies(copyEachPathAnalysis(copiedAnalysis, srcParreAnalysis, copyData.getCounterMeasureCopies()));
            copyData.setProxyindicationCopies(copyProxyIndications(copiedAnalysis,  srcParreAnalysis));
        }
        addNaturalThreatProfilesToCopy(copiedAnalysis, srcParreAnalysis, copyData);
        
        copyIndividualOptionAnalyses(copiedAnalysis, srcParreAnalysis, copyData);
        return copiedAnalysis;
    }

    private void saveTree(AnalysisTreeNode analysisTreeRoot) {
        saveEntity(analysisTreeRoot);
        for(AnalysisTreeNode analysisTreeNode : analysisTreeRoot.getChildren()) {
            saveLeaves(analysisTreeNode, analysisTreeRoot);
        }
    }

    private void saveLeaves(AnalysisTreeNode analysisTreeNode, AnalysisTreeNode parent) {
        analysisTreeNode.setParent(parent);
        saveEntity(analysisTreeNode);
        for(AnalysisTreeNode treeNode : analysisTreeNode.getChildren()) {
            saveLeaves(treeNode, analysisTreeNode);
        }
    }

    private void addNaturalThreatProfilesToCopy(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis, ParreAnalysisCopyData copyData) {
    	Map<Long, EarthquakeProfile> earthquakeProfileCopies = makeEarthquakeProfileCopies(copiedAnalysis, srcParreAnalysis);
        Map<Long, HurricaneProfile> hurricaneProfileCopies = makeHurricaneProfileCopies(copiedAnalysis, srcParreAnalysis);
        
        if (srcParreAnalysis.getDefaultEarthquakeProfile() != null && earthquakeProfileCopies.containsKey(srcParreAnalysis.getDefaultEarthquakeProfile().getId())) {
        	copiedAnalysis.setDefaultEarthquakeProfile(earthquakeProfileCopies.get(srcParreAnalysis.getDefaultEarthquakeProfile().getId()));
        	saveEntityWithFlush(copiedAnalysis);
        }
        
        if (srcParreAnalysis.getDefaultHurricaneProfile() != null && hurricaneProfileCopies.containsKey(srcParreAnalysis.getDefaultHurricaneProfile().getId())) {
        	copiedAnalysis.setDefaultHurricaneProfile(hurricaneProfileCopies.get(srcParreAnalysis.getDefaultHurricaneProfile().getId()));
        	saveEntityWithFlush(copiedAnalysis);
        }
    }

    private ParreAnalysisCopyData copyRelatedItems(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis) {
        Map<Long, Asset> copiedAssetMap = makeAssetCopies(copiedAnalysis, srcParreAnalysis);
        copyInactiveThreats(copiedAnalysis, srcParreAnalysis);
        copyAssetThreatLevels(copiedAnalysis, srcParreAnalysis, copiedAssetMap);
        HashMap<Long, CounterMeasure> counterMeasureCopies = copyCounterMeasures(copiedAnalysis, srcParreAnalysis);
        Map<Long, AssetThreat> assetThreatCopies = makeAssetThreatCopies(srcParreAnalysis, copiedAssetMap);
        
        ParreAnalysisCopyData copyData = new ParreAnalysisCopyData(assetThreatCopies, counterMeasureCopies);
        copyData.setDiagramAnalysisCopies(copyEachDiagramAnalysis(copiedAnalysis, srcParreAnalysis, copyData.getCounterMeasureCopies()));
        copyData.setPathAnalysisCopies(copyEachPathAnalysis(copiedAnalysis, srcParreAnalysis, counterMeasureCopies));
        copyData.setProxyindicationCopies(copyProxyIndications(copiedAnalysis,  srcParreAnalysis));
        return copyData;
    }

    private HashMap<Long, DiagramAnalysis> copyEachDiagramAnalysis(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis, Map<Long, CounterMeasure> countermeasureCopies) {
        List<DiagramAnalysis> diagramAnalysises = ParreManager.getAllDiagramAnalysis(srcParreAnalysis.getId(), entityManager());
        HashMap<Long, DiagramAnalysis> diagramAnalysisCopies = new HashMap<Long, DiagramAnalysis>(diagramAnalysises.size());
        for(DiagramAnalysis diagramAnalysis : diagramAnalysises) {
            DiagramAnalysis copy = diagramAnalysis.copyInto(new DiagramAnalysis());
            copy.setId(null);
            copy.setDiagramAnalysisCounterMeasures(Collections.<CounterMeasure>emptySet());
            for(CounterMeasure counterMeasure : diagramAnalysis.getDiagramAnalysisCounterMeasures()) {
                copy.addCounterMeasureSimple(countermeasureCopies.get(counterMeasure.getId()));
            }
            copy.setParreAnalysis(copiedAnalysis);
            saveDiagramAnalysis(copy);
            diagramAnalysisCopies.put(diagramAnalysis.getId(), copy);
        }
        return diagramAnalysisCopies;
    }

    private HashMap<Long, PathAnalysis> copyEachPathAnalysis(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis, Map<Long, CounterMeasure> counterMeasureCopies) {
        List<PathAnalysis> pathAnalysises = ParreManager.getAllPathAnalysisAnalysis(srcParreAnalysis.getId(), entityManager());
        HashMap<Long, PathAnalysis> pathAnalysisCopies = new HashMap<Long, PathAnalysis>(pathAnalysises.size());
        for(PathAnalysis pathAnalysis : pathAnalysises) {
            PathAnalysis copy = pathAnalysis.makeCopy();
            copy.setParreAnalysis(copiedAnalysis);
            copy.setPathAnalysisCounterMeasures(Collections.<CounterMeasure>emptySet());
            for(CounterMeasure counterMeasure : pathAnalysis.getPathAnalysisCounterMeasures()) {
                copy.addCounterMeasureSimple(counterMeasureCopies.get(counterMeasure.getId()));
            }
            for(AttackStep attackStep : copy.getAttackSteps()) {
            	if (attackStep.getCounterMeasure() != null) {
            		attackStep.setCounterMeasure(counterMeasureCopies.get(attackStep.getCounterMeasure().getId()));
            	}
            }
            savePathAnalysis(copy);
            pathAnalysisCopies.put(pathAnalysis.getId(), copy);
        }
        return pathAnalysisCopies;
    }

    private HashMap<Long, ProxyIndication> copyProxyIndications(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis) {
        List<ProxyIndication> proxyIndications = ParreManager.getProxyIndications(srcParreAnalysis.getId(), entityManager());
        HashMap<Long, ProxyIndication> proxyIndicationCopies = new HashMap<Long, ProxyIndication>(proxyIndications.size());
        for(ProxyIndication pathAnalysis : proxyIndications) {
            ProxyIndication copy = pathAnalysis.makeCopy();
            copy.setParreAnalysis(copiedAnalysis);
            saveEntityWithFlush(copy);
            proxyIndicationCopies.put(pathAnalysis.getId(), copy);
        }
        return proxyIndicationCopies;
    }
    private HashMap<Long, CounterMeasure> copyCounterMeasures(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis) {
        List<CounterMeasure> counterMeasures = ParreManager.getCounterMeasures(srcParreAnalysis.getId(), entityManager());
        HashMap<Long, CounterMeasure> counterMeasureCopies = new HashMap<Long, CounterMeasure>(counterMeasures.size());
        for (CounterMeasure counterMeasure : counterMeasures) {
            CounterMeasure copy = counterMeasure.createCopy();
            copy.setParreAnalysis(copiedAnalysis);
            saveEntityWithFlush(copy);
            counterMeasureCopies.put(counterMeasure.getId(), copy);
        }
        return counterMeasureCopies;
    }

    private void copyAssetThreatLevels(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis, Map<Long, Asset> copiedAssetMap) {
        List<AssetThreatLevel> levels = ParreManager.getAssetThreatLevels(srcParreAnalysis.getId(), entityManager());
        for (AssetThreatLevel level : levels) {
            Long assetId = copiedAssetMap.get(level.getAssetId()).getId();
            AssetThreatLevel copy = level.createCopy();
            copy.setAssetId(assetId);
            copy.setParreAnalysisId(copiedAnalysis.getId());
            saveEntityWithFlush(copy);
        }
    }
    
    private Map<Long, EarthquakeProfile> makeEarthquakeProfileCopies(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis) {
    	Map<Long, EarthquakeProfile> profiles = new HashMap<Long, EarthquakeProfile>();
    	if (srcParreAnalysis.getEarthquakeProfiles() != null) {
    		for (EarthquakeProfile profile : srcParreAnalysis.getEarthquakeProfiles()) {
    			EarthquakeProfile copyProfile = new EarthquakeProfile();
    			copyProfile.setParreAnalysis(copiedAnalysis);
    			copyProfile.copyFrom(profile);
    			copyProfile.setId(null);
    			saveEntityWithFlush(copyProfile);
    			profiles.put(profile.getId(), copyProfile);
    		}
    	}
    	return profiles;
    }
    
    private Map<Long, HurricaneProfile> makeHurricaneProfileCopies(ParreAnalysis copiedAnalysis, ParreAnalysis srcParreAnalysis) {
    	Map<Long, HurricaneProfile> profiles = new HashMap<Long, HurricaneProfile>();
    	
    	if (srcParreAnalysis.getHurricaneProfiles() != null) {
    		for (HurricaneProfile profile : srcParreAnalysis.getHurricaneProfiles()) {
    			HurricaneProfile copyProfile = new HurricaneProfile();
    			copyProfile.setParreAnalysis(copiedAnalysis);
    			copyProfile.copyFrom(profile);
                copyProfile.setName(profile.getName());
                copyProfile.setDescription(profile.getDescription());
                saveEntityWithFlush(copyProfile);
    			profiles.put(profile.getId(), copyProfile);
    		}
    	}
    	
    	return profiles;
    }

    private Map<Long, AssetThreat> makeAssetThreatCopies(ParreAnalysis srcParreAnalysis, Map<Long, Asset> copiedAssetMap) {
        List<AssetThreat> baselineAnalyses = srcParreAnalysis.getActiveAssetThreats();
        Map<Long, AssetThreat> copiedAssetThreatMap = new HashMap<Long, AssetThreat>();
        for (AssetThreat assetThreat : baselineAnalyses) {
            Asset assetCopy = copiedAssetMap.get(assetThreat.getAsset().getId());
            AssetThreat assetThreatCopy = new AssetThreat(assetCopy.getParreAnalysis(), assetCopy, assetThreat.getThreat());
            saveEntityWithFlush(assetThreatCopy);
            copiedAssetThreatMap.put(assetThreat.getId(), assetThreatCopy);
        }
        return copiedAssetThreatMap;
    }

    protected Map<Long, Asset> makeAssetCopies(ParreAnalysis copiedAnalysis, ParreAnalysis srcAnalysis) {
        List<Asset> activeAssets = srcAnalysis.getActiveAssets();
        Map<Long, Asset> copiedAssetMap = new HashMap<Long, Asset>();
        for (Asset asset : activeAssets) {
            Asset assetCopy = Asset.cloneAsset(asset);
            assetCopy.setParreAnalysis(copiedAnalysis);
            Set<AssetNote> notes = assetCopy.getAssetNotes();
            assetCopy.setAssetNotes(Collections.<AssetNote>emptySet());
            saveEntityWithFlush(assetCopy);
            if(assetCopy.getAssetNotes() != null) {
                for(AssetNote note : notes) {
                    note.setAsset(assetCopy);
                    saveEntityWithFlush(note);
                    assetCopy.addAssetNote(note);
                }
                saveEntityWithFlush(assetCopy);
            }
            copiedAssetMap.put(asset.getId(), assetCopy);
        }
        return copiedAssetMap;
    }

    public void copyIndividualOptionAnalyses(ParreAnalysis optionAnalysis, ParreAnalysis baseline, ParreAnalysisCopyData copyAssets) {
        copyNaturalThreatAnalysis(optionAnalysis, baseline, copyAssets);
        copyDpAnalysis(optionAnalysis, baseline, copyAssets);
        copyDirectedThreatAnalysis(optionAnalysis, baseline, copyAssets);
        copyRiskResilienceAnalysis(optionAnalysis, baseline, copyAssets);
    }

    private void copyNaturalThreatAnalysis(ParreAnalysis optionAnalysis, ParreAnalysis baseline, ParreAnalysisCopyData copyAssets) {
        NaturalThreatAnalysis newAnalysis = new NaturalThreatAnalysis(optionAnalysis.getName(), optionAnalysis.getDescription());
        NaturalThreatAnalysis baselineAnalysis = getCurrentNaturalThreatAnalysis(baseline.getId());
        copyAnalysis(newAnalysis, optionAnalysis, baselineAnalysis, NaturalThreatFactory.getInstance(), copyAssets);

    }

    private void copyDpAnalysis(ParreAnalysis optionAnalysis, ParreAnalysis baseline, ParreAnalysisCopyData copyAssets) {
        DpAnalysis newAnalysis = new DpAnalysis(optionAnalysis.getName(), optionAnalysis.getDescription());
        DpAnalysis baselineAnalysis = getCurrentDpAnalysis(baseline.getId());
        copyAnalysis(newAnalysis, optionAnalysis, baselineAnalysis, DpFactory.getInstance(), copyAssets);
        updateParentIds(newAnalysis, baselineAnalysis, copyAssets.getAssetThreatCopies());
    }

    private void copyDirectedThreatAnalysis(ParreAnalysis optionAnalysis, ParreAnalysis baseline, ParreAnalysisCopyData copyAssets) {
        DirectedThreatAnalysis newAnalysis = new DirectedThreatAnalysis(optionAnalysis.getName(), optionAnalysis.getDescription());
        DirectedThreatAnalysis baselineAnalysis = getCurrentDirectedThreatAnalysis(baseline.getId());
        copyAnalysis(newAnalysis, optionAnalysis, baselineAnalysis, DirectedThreatFactory.getInstance(), copyAssets);
    }

    private void copyRiskResilienceAnalysis(ParreAnalysis optionAnalysis, ParreAnalysis baseline, ParreAnalysisCopyData copyAssets) {
        RiskResilienceAnalysis newAnalysis = new RiskResilienceAnalysis(optionAnalysis.getName(), optionAnalysis.getDescription());
        RiskResilienceAnalysis baselineAnalysis = getCurrentRiskResilienceAnalysis(baseline);
        if (baselineAnalysis != null) {
        	newAnalysis.setUnitPrice(baselineAnalysis.getUnitPrice());
        }
        copyAnalysis(newAnalysis, optionAnalysis, baselineAnalysis, RiskResilienceFactory.getInstance(), copyAssets);
    }

    private void copyAnalysis(Analysis newAnalysis, ParreAnalysis copiedAnalysis, Analysis baselineAnalysis,
                              AssetThreatAnalysisFactory<AssetThreatEntity> factory, ParreAnalysisCopyData copiedAssetThreats) {
        newAnalysis.setParreAnalysis(copiedAnalysis);
        saveEntityWithFlush(newAnalysis);
        
        if (baselineAnalysis != null) {
	        Set<? extends AssetThreatEntity> baselineAnalyses = baselineAnalysis.getAssetThreats();
	
	        for (AssetThreatEntity atAnalysis : baselineAnalyses) {
	            if (atAnalysis.getRemoved()) {
	                continue;
	            }
	            AssetThreatEntity assetThreatEntity = factory.create(); 
	            
	            if (assetThreatEntity instanceof DirectedThreat) {
	            	assetThreatEntity.setParreAnalysis(copiedAnalysis);
	            	assetThreatEntity = ((DirectedThreat)atAnalysis).makeCopy((DirectedThreat)assetThreatEntity, copiedAssetThreats);
                    if(((DirectedThreat)assetThreatEntity).getVulnerabilityAnalysisType().isEventTree()) {
                        saveTree(((DirectedThreat)assetThreatEntity).getDiagramAnalysis().getAnalysisTreeRoot());
                    }
	            } else {
	            	assetThreatEntity = atAnalysis.copyInto(assetThreatEntity);
	            }
	            
	            assetThreatEntity.setParreAnalysis(copiedAnalysis);
	            replaceAssetThreat(assetThreatEntity, copiedAssetThreats.getAssetThreatCopies());
	            assetThreatEntity.setId(null);
                assetThreatEntity.setParreAnalysis(copiedAnalysis);
	            newAnalysis.addAssetThreat(assetThreatEntity);

	            if (assetThreatEntity instanceof NaturalThreat) {
	            	NaturalThreat nt = (NaturalThreat)assetThreatEntity;
	            	if (nt.getEarthquakeProfile() != null) {
	            		nt.setEarthquakeProfile(copiedAssetThreats.getEarthquakeProfileCopies().get(nt.getEarthquakeProfile().getId()));
	            	}
	            	if (nt.getHurricaneProfile() != null) {
	            		nt.setHurricaneProfile(copiedAssetThreats.getHurricaneProfileCopies().get(nt.getHurricaneProfile().getId()));
	            	}
	            }
	            saveEntityWithFlush(assetThreatEntity);
	            
		        // Copy over ice storm detail if it exists...
            	//TODO: is there a better way to test sub type here?
	            if (assetThreatEntity instanceof NaturalThreat) {
	            	NaturalThreat nt = (NaturalThreat)assetThreatEntity;
	            	if ("N(I)".equals(nt.getThreat().getName()) && !nt.getManuallyEntered()) {
		            	try {
		            		NaturalThreat copiedNt = (NaturalThreat) atAnalysis;
		            		IceStormThreatDetails origDetails = (IceStormThreatDetails)entityManager().createQuery("select istd from IceStormThreatDetails istd where istd.naturalThreat = :naturalThreat")
		            															.setParameter("naturalThreat", copiedNt)
		            															.getSingleResult();
		            		IceStormThreatDetails copyDetails = new IceStormThreatDetails();
		            		copyDetails.copyFrom(origDetails);
		            		copyDetails.setId(null);
		            		copyDetails.setNaturalThreat(nt);
		            		saveEntityWithFlush(copyDetails);
		            	} catch (NoResultException nre) {
		            		// gulp 
		            	} catch (Exception ex) {
		            		log.error("Could not copy over ice storm calc. details when making option for parre anlaysis: " + copiedAnalysis.getId(), ex);
		            	}
	            	}
	            }
	        }
        }
    }
    
    private void saveDiagramAnalysis(DiagramAnalysis diagramAnalysis) {
    	if (diagramAnalysis != null) {
	    	saveTree(diagramAnalysis.getAnalysisTreeRoot());
	    	saveEntityWithFlush(diagramAnalysis);
    	}
    }

    private void savePathAnalysis(PathAnalysis pathAnalysis) {
    	if (pathAnalysis != null) {
	        Set<AttackStep> attackSteps = pathAnalysis.getAttackSteps();
	        Set<ResponseStep> responseSteps = pathAnalysis.getResponseSteps();
	        pathAnalysis.setAttackSteps(null);
	        pathAnalysis.setResponseSteps(null);
	        saveEntityWithFlush(pathAnalysis);
	        for (AttackStep attackStep : attackSteps) {
	            saveEntityWithFlush(attackStep);
	        }
	        for (ResponseStep responseStep : responseSteps) {
	            saveEntityWithFlush(responseStep);
	        }
	        pathAnalysis.setAttackSteps(attackSteps);
	        pathAnalysis.setResponseSteps(responseSteps);
    	}
    }

    private void replaceAssetThreat(AssetThreatEntity assetThreatEntity, Map<Long, AssetThreat> copiedAssetThreatMap) {
        if (!copiedAssetThreatMap.isEmpty()) {
            AssetThreat assetThreatCopy = copiedAssetThreatMap.get(assetThreatEntity.getAssetThreat().getId());
            assetThreatEntity.setAssetThreat(assetThreatCopy);
        }
    }

    private void updateParentIds(DpAnalysis newAnalysis, DpAnalysis baselineAnalysis, Map<Long, AssetThreat> copyAssets) {
    	if (baselineAnalysis != null) {
	        Set<Dp> newDps = newAnalysis.getDps();
	        Set<Dp> baselineDps = baselineAnalysis.getDps();
	        for (Dp newDp : newDps) {
	            Long parentId = newDp.getParentId();
	            if (parentId != null) {
	                Dp parentDp = findParentDp(parentId, newDps, baselineDps, copyAssets);
	                newDp.setParentId(parentDp.getId());
	                saveEntity(newDp);
	            }
	        }
    	}
    }

    private Dp findParentDp(Long parentId, Set<Dp> newDps, Set<Dp> baselineDps, Map<Long, AssetThreat> copyAssets) {
        for (Dp baselineDp : baselineDps) {
            if (baselineDp.hasIdentity(parentId)) {
                AssetThreat baselineAssetThreat = getBaselineAssetThreat(baselineDp, copyAssets);
                return findMatchingDp(baselineAssetThreat, newDps);
            }
        }
        return null;
    }

    private AssetThreat getBaselineAssetThreat(Dp baselineDp, Map<Long, AssetThreat> copyAssets) {
        AssetThreat baselineAssetThreat = baselineDp.getAssetThreat();
        if (!copyAssets.isEmpty()) {
            baselineAssetThreat = copyAssets.get(baselineAssetThreat.getId());
        }
        return baselineAssetThreat;
    }

    private Dp findMatchingDp(AssetThreat baselineAssetThreat, Set<Dp> newDps) {
        for (Dp dp : newDps) {
            if (dp.isFor(baselineAssetThreat) && dp.getType().isMainWithSub()) {
                return dp;
            }
        }
        return null;
    }

    RiskResilienceAnalysis getCurrentRiskResilienceAnalysis(ParreAnalysis parreAnalysis) {
        return RiskResilienceManager.getCurrentAnalysis(parreAnalysis.getId(), entityManager());
    }

    protected void copyInactiveThreats(ParreAnalysis copiedParreAnalysis, ParreAnalysis parreAnalysis) {
        List<InactiveThreat> inactiveThreats = ThreatManager.getInactiveThreats(parreAnalysis.getId(), entityManager());
        for (InactiveThreat inactiveThreat : inactiveThreats) {
            InactiveThreat copy = inactiveThreat.createCopy();
            copy.setParreAnalysis(copiedParreAnalysis);
            saveEntityWithFlush(copy);
        }
    }
}
