/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.io.File;

import org.apache.log4j.Logger;
import org.parre.server.NaturalThreatLookupData;
import org.parre.server.ParreDatabase;
import org.parre.server.domain.manager.ParreManager;


/**
 * The Class DbInitServiceImpl.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 4/24/12
 */
public class DbInitServiceImpl extends ParreBaseService implements DbInitService {
    private static transient final Logger log = Logger.getLogger(DbInitServiceImpl.class);

    private static DbInitService instance = new DbInitServiceImpl();

    public static DbInitService getInstance() {
        return instance;
    }

    public boolean isDbInitialized() {
        return ParreManager.haveUsers(entityManager());
    }
    public void initData(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initData(dataFile);
    }
    public void initUsers(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initUsers(dataFile);
    }
    public void initLookupValues(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initLookupValues(dataFile);
    }

    public void initQuestions(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initQuestions(dataFile);
    }

    public void initCityTierTotals(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initCityTierTotals(dataFile);
    }

    public void initCityTiers(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initCityTiers(dataFile);
    }

    public void initTargetTypes(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initTargetTypes(dataFile);
    }

    public void initDetectionLikelihood(File dataFile) {
        ParreDatabase db = ParreDatabase.get();
        db.initDetectionLikelihood(dataFile);
    }
    public void initEarthquakeLookupData(File dataFile) {
        NaturalThreatLookupData.get().initEarthquakeLookupData(dataFile);
    }

    public void initHurricaneLookupData(File dataFile) {
    	NaturalThreatLookupData.get().initHurricaneCalc(dataFile);
    }

    public void initIceworkLookupData(File dataFile) {
    	NaturalThreatLookupData.get().initIceworkCalc(dataFile);
    }

    public void initTornadoLookupData(File dataFile) {
    	NaturalThreatLookupData.get().initTornadoCalc(dataFile);
    }

    public void initWindLookupData(File dataFile) {
    	NaturalThreatLookupData.get().initWindLookupData(dataFile);
    }

}
