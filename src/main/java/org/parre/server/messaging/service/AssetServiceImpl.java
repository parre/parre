/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;
import org.parre.client.messaging.AssetService;
import org.parre.server.domain.Asset;
import org.parre.server.domain.AssetNote;
import org.parre.server.domain.AssetThreatLevel;
import org.parre.server.domain.LookupValue;
import org.parre.server.domain.Org;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.Threat;
import org.parre.server.domain.manager.AssetManager;
import org.parre.server.domain.manager.ParreManager;
import org.parre.server.domain.manager.ThreatManager;
import org.parre.server.messaging.service.async.AssetAdded;
import org.parre.server.messaging.service.async.AssetCountChanged;
import org.parre.server.messaging.service.async.ParreCommandExecutor;
import org.parre.server.messaging.service.logic.AssetThreatLevelState;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.NoteDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.ZipLookupDataDTO;
import org.parre.shared.exception.DuplicateAssetIdException;
import org.parre.shared.types.AssetType;
import org.parre.shared.types.LookupType;


/**
 * The server side implementation of the RPC service.
 */
public class AssetServiceImpl extends ParreBaseService implements AssetService {
    private static transient Logger log = Logger.getLogger(AssetServiceImpl.class);

    private static AssetServiceImpl instance = new AssetServiceImpl();

    public static AssetService getInstance() {
        return instance;
    }

    public List<AssetDTO> getAssetSearchResults(AssetSearchDTO searchDTO) {
    	if (log.isDebugEnabled()) { log.debug("Searching using : " + searchDTO); }
        List<Asset> assets = AssetManager.searchAssets(searchDTO, entityManager());
        for(Asset asset : assets) {
            asset.setAssetNotes(new HashSet<AssetNote>(AssetManager.getAssetNotes(asset.getId(), entityManager())));
        }
        return Asset.createDTOs(assets);
    }

    public List<LabelValueDTO> getStateList() {
        List<LookupValue> lookupValues = ParreManager.getLookupValues(LookupType.STATE_CODES, entityManager());
        return LookupValue.createDTOs(lookupValues);
    }

    public AssetDTO saveAsset(AssetDTO assetDTO, Long parreAnalysisId) throws DuplicateAssetIdException {
        boolean isNewProductService = assetDTO.isNew() && assetDTO.getAssetType().isProductService();
        if (isNewProductService) {
            return createProductService(assetDTO);
        }
        if (parreAnalysisId == null) {
            throw new IllegalArgumentException("Only ProductService can be created without ParreAnalysis");
        }
        if (log.isDebugEnabled()) { log.debug("Updating Asset : " + assetDTO); }
        Asset existingAsset = AssetManager.findAsset(assetDTO.getAssetId(), parreAnalysisId, entityManager());

        if (existingAsset != null) {
            if (!existingAsset.getId().equals(assetDTO.getId())) {
                throw new DuplicateAssetIdException(assetDTO.getAssetId());
            }
        }

        ParreAnalysis parreAnalysis = findParreAnalysis(parreAnalysisId);

        Asset asset = null;
        boolean isNewAsset = assetDTO.isNew();
        if (isNewAsset) {
            assetDTO.setRemoved(Boolean.FALSE);
            asset = Asset.create(assetDTO.getAssetType());
            asset.setParreAnalysis(parreAnalysis);
            
            if(asset.getAssetType().equals(AssetType.HUMAN_ASSET)){
            	assetDTO.setSystemType("Human Asset");
            } else if(asset.getAssetType().equals(AssetType.EXTERNAL_ASSET)){
            	assetDTO.setSystemType("External Asset");
           }
            
        } else {
            asset = existingAsset != null ? existingAsset : getAsset(assetDTO);
        }
        asset.copyFrom(assetDTO);
        saveEntityWithFlush(asset);
        assetDTO.setId(asset.getId());

        for(NoteDTO noteDTO : assetDTO.getNoteDTOs()) {
            if(noteDTO.isNew()) {
                saveAssetNote(noteDTO, asset);
            }
        }

        if (isNewAsset) {        	
            ParreCommandExecutor.getInstance().submitCommand(new AssetAdded(getLoginInfo(), parreAnalysisId, asset.getId()));
        } else {
            ParreCommandExecutor.getInstance().submitCommand(new AssetCountChanged(getLoginInfo(), parreAnalysisId));
        }
        return assetDTO;
    }

    private void saveAssetNote(NoteDTO noteDTO, Asset asset) {

        log.debug("AssetNoteDTO.id = " + noteDTO.toString());
        AssetNote assetNote = null;
        if(noteDTO.getId() ==  null) {
            assetNote = new AssetNote();
            assetNote.setAsset(asset);
        } else {
            assetNote = entityManager().find(AssetNote.class, noteDTO.getId());
        }

        assetNote.copyFrom(noteDTO);
        saveEntityWithFlush(assetNote);

    }

    private AssetDTO createProductService(AssetDTO assetDTO) throws DuplicateAssetIdException {
        Org org = getCurrentUser().getOrg();
        Asset asset = org.findAsset(assetDTO.getAssetId());
        if (asset != null) {
            throw new DuplicateAssetIdException("A product service with this asset id already exists");
        }

        asset = Asset.create(assetDTO.getAssetType());
        assetDTO.setRemoved(Boolean.FALSE);
        asset.copyFrom(assetDTO);
        saveEntityWithFlush(asset);
        org.addAsset(asset);
        saveEntityWithFlush(org);
        assetDTO.setId(asset.getId());
        return assetDTO;
    }

    private Asset getAsset(AssetDTO assetDTO) {
        return entityManager().find(Asset.class, assetDTO.getId());
    }

    public List<ThreatDTO> getActiveThreats(Long parreAnalysisId) {
        return getDirectedThreatAnalysisDTOs(parreAnalysisId);
    }

    public AssetDTO deleteAsset(AssetDTO assetDTO, Long parreAnalysisId) {
    	if (log.isDebugEnabled()) { log.debug("Removing assetId: " + assetDTO.getId() + " from analysis id: " + parreAnalysisId); }
    	if (assetDTO == null || assetDTO.isNew()) { return assetDTO; }
        Asset asset = getAsset(assetDTO);
        AssetManager.deleteAsset(asset, entityManager());
        assetDTO.setId(null);
        return assetDTO;
    }

    public ZipLookupDataDTO getZipLookupData(String zipCode) {
        return AssetManager.getSpecificZipLookupData(zipCode, entityManager()).copyInto(new ZipLookupDataDTO());
    }


    public List<AssetThreatLevelsDTO> getAssetThreatLevels(Long baselineId) {
        baselineId = getBaselineId(baselineId);
        AssetThreatLevelState levelState = createAssetThreatLevelState(baselineId);
        List<Asset> analysis = getAssetsForAnalysis(baselineId);
        List<Threat> activeThreats = getDirectedAnalysisThreats(baselineId);
        List<AssetThreatLevelsDTO> dtos = new ArrayList<AssetThreatLevelsDTO>(analysis.size());
        for (Asset assetDTO : analysis) {
            AssetThreatLevelsDTO dto = new AssetThreatLevelsDTO();
            Long assetId = assetDTO.getId();
            dto.setId(assetId);
            dto.setAssetId(assetDTO.getAssetId());
            dto.setAssetName(assetDTO.getName());
            for (Threat activeThreat : activeThreats) {
                Long threatId = activeThreat.getId();
                AssetThreatLevel assetThreatLevel = levelState.findAssetThreatLevel(assetId, threatId);
                Integer level = assetThreatLevel != null ? assetThreatLevel.getLevel() : 0;
                dto.putThreatLevel(threatId, level);
            }
            dtos.add(dto);
        }
        return dtos;
    }

    public boolean hasAssets(Long parreAnalysisId) {
        return AssetManager.getAssets(parreAnalysisId, entityManager()).size() != 0;
    }

    public boolean hasActiveThreats(Long parreAnalysisId) {
    	if (log.isDebugEnabled()) { log.debug("Checking for active threats for parre analysis id: " + parreAnalysisId); }
    	if (!hasAssets(parreAnalysisId)) {
    		return false;
    	}
    	ParreAnalysis parreAnalysis = entityManager().find(ParreAnalysis.class, parreAnalysisId);
    	Long id = parreAnalysis.getBaseline() != null ? parreAnalysis.getBaseline().getId() : parreAnalysisId;
    	return ThreatManager.getActiveThreatCountBaseline(id, entityManager()) > 0;
    }

	@Override
	public List<String> getSystemTypes(Long parreAnalysisId) {
		List<String> systemTypes = AssetManager.getAssetSystemTypes(parreAnalysisId, entityManager());
		return systemTypes;
	}

}
