/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;

import org.parre.client.messaging.LotService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;

/*
	Date			Author				Changes
    12/1/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class LotServlet.
 */
public class LotServlet extends RemoteServiceServlet implements LotService {
    private LotService service = ParreServiceFactory.createLotService();
    public LotDTO save(LotDTO vulnerabilityDTO) {
        return service.save(vulnerabilityDTO);
    }

    public List<LotDTO> getAssetSearchResults(AssetSearchDTO dto, Long parreAnalysisId) {
        return service.getAssetSearchResults(dto, parreAnalysisId);
    }

    public List<LotDTO> getThreatSearchResults(ThreatDTO dto, Long parreAnalysisId) {
        return service.getThreatSearchResults(dto, parreAnalysisId);
    }

    public List<LabelValueDTO> getAnalysisTypes() {
        return service.getAnalysisTypes();
    }

    public LotAnalysisDTO getCurrentAnalysis(Long parreAnalysisId) {
        return service.getCurrentAnalysis(parreAnalysisId);
    }

    public LotAnalysisDTO startAnalysis(LotAnalysisDTO dto) {
        return service.startAnalysis(dto);
    }

    public LotDTO saveWithProxyIndication(LotDTO lotDTO) {
        return service.saveWithProxyIndication(lotDTO);
    }

    public List<LabelValueDTO> getProxyCities() {
        return service.getProxyCities();
    }

    public List<LabelValueDTO> getProxyTargetTypes() {
        return service.getProxyTargetTypes();
    }

    public ProxyIndicationDTO getProxyIndication(Long lotId) {
        return service.getProxyIndication(lotId);
    }

    public ProxyIndicationDTO saveProxyIndication(ProxyIndicationDTO proxyIndicationDTO) {
        return service.saveProxyIndication(proxyIndicationDTO);
    }

    public List<ProxyIndicationDTO> getExistingProxyIndications(Long parreAnalysisId) {
        return service.getExistingProxyIndications(parreAnalysisId);
    }

    public ProxyIndicationDTO associateProxyIndication(Long lotId, Long proxyIndicationId, boolean makeCopy) {
        return service.associateProxyIndication(lotId, proxyIndicationId, makeCopy);
    }
}