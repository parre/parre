/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.List;
import java.util.Set;

import org.parre.client.messaging.ParreService;
import org.parre.server.domain.Contact;
import org.parre.server.domain.manager.ParreManager;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.*;
import org.parre.shared.exception.DuplicateItemException;
import org.parre.shared.types.AssetThreatAnalysisType;

/*
	Date			Author				Changes
    11/29/11	Swarn S. Dhaliwal	Created
*/
/**
 * The Class ParreServlet.
 */
public class ParreServlet extends RemoteServiceServlet implements ParreService {
    private ParreService service = ParreServiceFactory.createParreService();
    public List<LabelValueDTO> getContacts() {
        return service.getContacts();
    }

    @Override
    public ContactDTO getContact(Long contactId) {
        return service.getContact(contactId);
    }

    @Override
    public SiteLocationDTO getSiteLocation(Long locationId) {
        return service.getSiteLocation(locationId);
    }

    @Override
    public List<Long> deleteParreAnalysis(Long parreAnalysisId) {
        return service.deleteParreAnalysis(parreAnalysisId);
    }

    public List<LabelValueDTO> getProductServices() {
        return service.getProductServices();
    }

    public ParreAnalysisDTO saveParreAnalysis(ParreAnalysisDTO dto) throws DuplicateItemException {
        return service.saveParreAnalysis(dto);
    }

    public AppInitData getAppInitData() {
        return service.getAppInitData();
    }

    public ParreAnalysisDTO getParreAnalysis(Long parreAnalysisId) {
        return service.getParreAnalysis(parreAnalysisId);
    }

    public void setCurrentParreAnalysis(Long parreAnalysisId) {
        service.setCurrentParreAnalysis(parreAnalysisId);
    }

    public Integer updateAssetThreatData(Integer thresholdValue, Set<AssetThreatLevelDTO> levelDTOs, Long parreAnalysisId) {
        return service.updateAssetThreatData(thresholdValue, levelDTOs, parreAnalysisId);
    }

    public void updateAssetThreatLevel(AssetThreatLevelDTO levelDTO, Long parreAnalysisId) {
        service.updateAssetThreatLevel(levelDTO, parreAnalysisId);
    }

    public Integer updateAssetThreshold(Integer threshold, Long parreAnalysisId) {
        return service.updateAssetThreshold(threshold, parreAnalysisId);
    }

    public Integer getAssetThreatAnalysisCount(Long parreAnalysisId, AssetThreatAnalysisType analysisType) {
        return service.getAssetThreatAnalysisCount(parreAnalysisId, analysisType);
    }

    public void lockCurrentAnalysis(Long parreAnalysisId) {
        service.lockCurrentAnalysis(parreAnalysisId);
    }

    public List<ProposedMeasureDTO> getProposedMeasures(Long parreAnalysisId) {
        return service.getProposedMeasures(parreAnalysisId);
    }

    public List<ProposedMeasureDTO> getBaselineProposedMeasures(Long baselineParreAnalysisId) {
        return service.getBaselineProposedMeasures(baselineParreAnalysisId);
    }

    public void associateProposedMeasure(Long proposedMeasureId, Long parreAnalysisId) {
        service.associateProposedMeasure(proposedMeasureId, parreAnalysisId);
    }

    public ProposedMeasureDTO createAndAssociateProposedMeasure(ProposedMeasureDTO dto) {
        return service.createAndAssociateProposedMeasure(dto);
    }

    public void disassociateProposedMeasure(Long pmId, Long parreAnalysisId) {
        service.disassociateProposedMeasure(pmId, parreAnalysisId);
    }

    public SiteLocationDTO saveSiteLocation(SiteLocationDTO dto) {
        return service.saveSiteLocation(dto);
    }

    public List<LabelValueDTO> getSiteLocationList() {
        return service.getSiteLocationList();
    }

    public void unlockCurrentAnalysis(Long parreAnalysisId) {
        service.unlockCurrentAnalysis(parreAnalysisId);
    }

    public ContactDTO saveContact(ContactDTO contactDTO) {
        return service.saveContact(contactDTO);
    }

    public ProposedMeasureDTO saveProposedMeasure(ProposedMeasureDTO dto) {
        return service.saveProposedMeasure(dto);
    }

    public ParreAnalysisDTO copyParreAnalysis(Long parreAnalysisId) {
        return service.copyParreAnalysis(parreAnalysisId);
    }

}