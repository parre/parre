/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.servlet;

import java.util.List;

import org.parre.client.messaging.AssetService;
import org.parre.server.messaging.ParreServiceFactory;
import org.parre.shared.AssetDTO;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.AssetThreatLevelsDTO;
import org.parre.shared.LabelValueDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.ZipLookupDataDTO;
import org.parre.shared.exception.DuplicateAssetIdException;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class AssetServlet extends RemoteServiceServlet implements AssetService {
    private AssetService service = ParreServiceFactory.createAssetService();

    public List<AssetDTO> getAssetSearchResults(AssetSearchDTO searchDTO) throws IllegalArgumentException {
        return service.getAssetSearchResults(searchDTO);
	}

    public List<LabelValueDTO> getStateList() {
        return service.getStateList();
    }


    public AssetDTO saveAsset(AssetDTO assetDTO, Long parreAnalysisId) throws DuplicateAssetIdException {
        return service.saveAsset(assetDTO, parreAnalysisId);
    }

    public List<AssetThreatLevelsDTO> getAssetThreatLevels(Long baselineId) {
        return service.getAssetThreatLevels(baselineId);
    }

    public List<ThreatDTO> getActiveThreats(Long parreAnalysisId) {
        return service.getActiveThreats(parreAnalysisId);
    }

    public AssetDTO deleteAsset(AssetDTO assetDTO, Long parreAnalysisId) {
        return service.deleteAsset(assetDTO, parreAnalysisId);
    }

    public ZipLookupDataDTO getZipLookupData(String zipCode) {
        return service.getZipLookupData(zipCode);
    }

    @Override
    public boolean hasAssets(Long parreAnalysisId) {
        return service.hasAssets(parreAnalysisId);
    }

    @Override
    public boolean hasActiveThreats(Long baselineId) {
        return service.hasActiveThreats(baselineId);
    }

	@Override
	public List<String> getSystemTypes(Long parreAnalysisId) {
		return service.getSystemTypes(parreAnalysisId);
	}

}
