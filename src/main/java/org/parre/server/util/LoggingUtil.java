/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import org.apache.log4j.Logger;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

/**
 * This class contains a number of utility functions for logging information in an efficient manner.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 Apr 30, 2008
 */
public class LoggingUtil {
    private static transient final Logger myLogger = Logger.getLogger(LoggingUtil.class);

    /**
     * This method creates and logs a message for a method call only if Debug is enabled on the
     * passed logger.  This method can be very useful for debugging method calls as it can create an
     * informative log for runtime information that was passed to a method. It also minimized the
     * performance cost in production environments because no processing is done if debug is not
     * enabled.
     *
     * @param logger -- logger to be used for writing the message to the log
     * @param methodName -- name of the method to be logged.
     * @param params -- variable list of method arguments.
     */
    public static void debugLogMethod(Logger logger, String methodName, Object... params) {
        if (logger.isDebugEnabled()) {
            logger.debug(createMethodLog(methodName, params).toString());
        }
    }

    /**
     * This method creates and logs a message for a method call only if Info is enabled on the
     * passed logger.
     *
     * @param logger -- logger to be used for writing the message to the log
     * @param methodName -- name of the method to be logged.
     * @param params -- variable list of method arguments.
     * @see LoggingUtil#debugLogMethod(org.apache.log4j.Logger, String, Object[])
     */

    public static void infoLogMethod(Logger logger, String methodName, Object... params) {
        if (logger.isInfoEnabled()) {
            logger.info(createMethodLog(methodName, params));
        }
    }

    /**
     * This method creates and logs a message by concatenating message components. No processing is
     * done is debug is not enabled on the logger.
     * @param logger -- logger to be used by writing the message to a log
     * @param messageComponents -- variable arguments that constitutue the individual parts of the
     * message.
     */
    public static void debugLogMessage(Logger logger, Object... messageComponents) {
        if (logger.isDebugEnabled()) {
            logger.debug(createMessageLog(messageComponents));
        }
    }

    /**
     * @see LoggingUtil#debugLogMessage(org.apache.log4j.Logger, Object[])
     * @param logger
     * @param messageComponents
     */
    public static void infoLogMessage(Logger logger, Object... messageComponents) {
        if (logger.isInfoEnabled()) {
            logger.info(createMessageLog(messageComponents));
        }
    }

    /**
     * This method creates and logs a message consisting of detailed information about an
     * object. All components of complex objects are recursively queried until a primitive field is
     * found which is then printed by calling toString on it.  This in effect creates a snapshot of
     * object data at a given points and prints it to the log in a human readable format. Although using
     * this method can be extremely useful in development environment, it incurs no performance
     * cost in a production environment as no processing happens if debug is not enabled on the
     * logger.
     * @param logger
     * @param obj
     */
    public static void debugLogObjectDump(Logger logger, Object obj) {
        if (logger.isDebugEnabled()) {
            String logMessage = createObjectLog(obj);
            logger.debug(logMessage);
        }

    }

    /**
     * @see LoggingUtil#debugLogObjectDump(org.apache.log4j.Logger, Object)
     * @param logger
     * @param obj
     */
    public static void infoLogObjectDump(Logger logger, Object obj) {
        if (logger.isInfoEnabled()) {
            String logMessage = createObjectLog(obj);
            logger.info(logMessage);
        }

    }

    private static String createObjectLog(Object obj) {
        StringBuilder builder = new StringBuilder();
        if (obj == null) {
            builder.append("{null}");
        } else {
            Set circularRefSet = new HashSet();
            circularRefSet.add(obj);
            builder.append(obj.getClass().getSimpleName()).append("{");
            builder.append(createFieldLog(obj, circularRefSet));
            builder.append("}");
        }
        return builder.toString();
    }

    protected static String createFieldLog(Object obj, Set circularRefSet) {
        if (obj == null) {
            return "null";
        }
        if (isSimpleType(obj.getClass())) {
            return obj.toString();
        }
        StringBuilder builder = new StringBuilder();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        Map<String, Object> complexObjMap = new TreeMap<String, Object>();
        for (Field declaredField : declaredFields) {
            myLogger.debug("Processing field : " + declaredField.getName());
            declaredField.setAccessible(true);
            if (!loggingRequired(declaredField)) {
                continue;
            }
            if (isCollectionType(declaredField)) {
                addFieldSeparator(builder);
                Object fieldValue = getFieldValue(obj, declaredField);
                String collectionValue = createCollectionLog(fieldValue, circularRefSet);
                builder.append(declaredField.getName()).append("[").append
                        (collectionValue).append("]");
            } else if (isSimpleType(declaredField)) {
                addFieldSeparator(builder);
                Object simpleValue = getFieldValue(obj, declaredField);
                builder.append(declaredField.getName()).append("='").append(simpleValue).append("'");
            } else {
                Object complexValue = getFieldValue(obj, declaredField);
                complexObjMap.put(declaredField.getName(), complexValue);
            }

        }
        Set<Map.Entry<String, Object>> entries = complexObjMap.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            Object complexValue = entry.getValue();
            String fieldLog;
            if (circularRefSet.contains(complexValue)) {
                fieldLog = "CircularReference";
                addFieldSeparator(builder);
            } else {
                addObjectSeparator(builder);
                circularRefSet.add(complexValue);
                fieldLog = createFieldLog(complexValue, circularRefSet);
            }
            builder.append(entry.getKey()).append("{").append(fieldLog).append("}");

        }
        return builder.toString();
    }

    private static void addObjectSeparator(StringBuilder builder) {
        if (builder.length() > 0) {
            builder.append(", ").append(System.getProperty("line.separator"));
        }
    }

    private static void addFieldSeparator(StringBuilder builder) {
        if (builder.length() > 0) {
            builder.append(", ");
        }
    }

    protected static String createCollectionLog(Object fieldValue, Set circularRefMap) {
        if (fieldValue == null) {
            return "null";
        }
        StringBuilder builder = new StringBuilder();
        if (fieldValue.getClass().isArray()) {
            Object[] array = (Object[]) fieldValue;
            if (array.length > 0) {
                builder.append(createFieldLog(array[0], circularRefMap));
                for (int i = 0; i < array.length; i++) {
                    builder.append(", ").append(createFieldLog(array[i], circularRefMap));
                }
            }
        } else {
            Iterator coll = ((Collection) fieldValue).iterator();
            if (coll.hasNext()) {
                Object obj = coll.next();
                builder.append(createFieldLog(obj, circularRefMap));
                while (coll.hasNext()) {
                    obj = (Object) coll.next();
                    builder.append(", ").append(createFieldLog(obj, circularRefMap));
                }
            }
        }
        return builder.toString();
    }

    protected static boolean isCollectionType(Field declaredField) {
        Class<?> type = declaredField.getType();
        return type.isArray() || Collection.class.isAssignableFrom(type);
    }

    protected static Object getFieldValue(Object obj, Field declaredField) {
        try {
            return declaredField.get(obj);
        } catch (IllegalAccessException e) {
            return e.getMessage();
        }
    }

    protected static boolean isSimpleType
            (Field
                    declaredField) {
        Class<?> type = declaredField.getType();
        return isSimpleType(type);
    }

    protected static boolean isSimpleType(Class<?> type) {
        String fieldType = type.getName();
        return fieldType.startsWith("java.") || fieldType.startsWith("javax.");
    }

    protected static boolean loggingRequired(Field declaredField) {
        String fieldName = declaredField.getName();
        Class<?> fieldType = declaredField.getType();
        return !("serialVersionUID".equals(fieldName) || fieldType == Class.class || fieldType == Logger.class
                || fieldType == java.util.logging.Logger.class);
    }

    private static String createMessageLog
            (Object[] messageComponents) {
        if (messageComponents.getClass().getComponentType() != Object.class) {
            return createArrayLog(messageComponents);
        }
        StringBuilder builder = new StringBuilder();
        if (messageComponents == null) {
            builder.append("null");
        } else if (messageComponents.length == 0) {
            builder.append("empty");
        } else {
            builder.append(createMessageComponentLog(messageComponents[0]));
            for (int i = 1; i < messageComponents.length; i++) {
                builder.append(createMessageComponentLog(messageComponents[i]));
            }
        }
        return builder.toString();
    }

    private static Object createMessageComponentLog(Object messageComponent) {
        if (messageComponent != null && messageComponent.getClass().isArray()) {
            messageComponent = createArrayLog(messageComponent);
        }
        return messageComponent;
    }


    protected static String createArrayLog(Object array) {
        Class<?> type = array.getClass().getComponentType();
        StringBuilder builder = new StringBuilder();
        builder.append(type.getSimpleName()).append("[");
        int length = Array.getLength(array);
        if (length > 0) {
            builder.append(Array.get(array, 0));
            for (int i = 1; i < length; i++) {
                builder.append(", ").append(Array.get(array, i));
            }
        }
        builder.append("]");
        return builder.toString();
    }

    private static String createMethodLog(String methodName, Object[] params) {
        StringBuilder builder = new StringBuilder();
        builder.append(methodName).append("(");
        if (params != null && params.length > 0) {
            builder.append(createMessageComponentLog(params[0]));
            for (int i = 1; i < params.length; i++) {
                Object param = params[i];
                builder.append(", ").append(createMessageComponentLog(param));
            }
        }
        builder.append(")");
        return builder.toString();
    }

}
