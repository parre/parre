/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.parre.server.NaturalThreatLookupData;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.messaging.service.NaturalThreatServiceImpl;
import org.parre.shared.AmountDTO;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.HurricaneLookupDataDTO;
import org.parre.shared.HurricaneProfileDTO;
import org.parre.shared.IceStormIndexWorkDTO;
import org.parre.shared.IceStormWorkDTO;
import org.parre.shared.IceworkLookupDataDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.SharedCalculationUtil;
import org.parre.shared.WindLookupDataDTO;
import org.parre.shared.types.MoneyUnitType;


/**
 * The Class NaturalThreatCalculatorImpl.
 */
public class NaturalThreatCalculatorImpl implements NaturalThreatCalculator {
	private final static transient Logger log = Logger.getLogger(NaturalThreatCalculatorImpl.class);
	
	private static final BigDecimal THREE = new BigDecimal("3");
	private static Map<Integer, Integer> INDEX_DAYS_MAP = new HashMap<Integer, Integer>();
	
	static {
		INDEX_DAYS_MAP.put(3, NaturalThreatCalculator.INDEX3_DAYS);
		INDEX_DAYS_MAP.put(4, NaturalThreatCalculator.INDEX4_DAYS);
		INDEX_DAYS_MAP.put(5, NaturalThreatCalculator.INDEX5_DAYS);
	}
	
	@Override
	public void calculateEarthquakeRisk(NaturalThreatCalculationDTO calc) {
		calc.getAnalysis().setVulnerability(BigDecimal.ONE);
		calc.getAnalysis().setLot(calculateEarthquakeLot(calc));
		calculateConsequence(calc, true);
	}
	
	private BigDecimal calculateEarthquakeLot(NaturalThreatCalculationDTO calc) {
		BigDecimal totalLot = BigDecimal.ZERO;
		
		BigDecimal nextFrequency = BigDecimal.ZERO;
		for (EarthquakeMagnitudeProbabilityDTO magnitudeProbability : EarthquakeProfileDTO.getMagnitudeProbabilitiesReverseOrder(calc.getEarthquakeProfile())) {
			BigDecimal vulnerability = NaturalThreatLookupData.get().getEarthquakeVulnerability(magnitudeProbability.getMagnitude(), calc.getAsset().getYearBuilt());
			
			BigDecimal currentFrequency = BigDecimal.ZERO;
			if (magnitudeProbability.getProbability() != null && magnitudeProbability.getProbability().compareTo(BigDecimal.ZERO) > 0) {
				currentFrequency = calc.getEarthquakeProfile().getReturnValue().divide(magnitudeProbability.getProbability(), 10, RoundingMode.HALF_UP);
				currentFrequency = BigDecimal.ONE.divide(currentFrequency, 10, RoundingMode.HALF_UP);
			}
			BigDecimal currentCalculatedValue = currentFrequency.subtract(nextFrequency);
			currentCalculatedValue = vulnerability.multiply(currentFrequency.subtract(nextFrequency));
			totalLot = totalLot.add(currentCalculatedValue);
			nextFrequency = currentFrequency;
		}
		
		log.debug("Returning totalLot for Earthquake: " + totalLot);
		return totalLot;
	}
	
	@Override
	public void calculateTornadoRisk(NaturalThreatCalculationDTO calc) {
		// Threat likelihood based on location of physical resource
		try {
			if (calc.getAnalysis().getUseDefaultValues()) {
				calc.getAnalysis().setLot(NaturalThreatLookupData.get().getTornadoVulnerability(calc.getAsset().getPostalCode()));
			} else {
				calc.getAnalysis().setLot(NaturalThreatLookupData.get().getTornadoVulnerability(calc.getAnalysis().getCountyOverride(), calc.getAnalysis().getStateCodeOverride()));
			}
		} catch (IllegalArgumentException e) {
			calc.setCalculationSuccess(false);
			calc.setResultMessage("Tornado statistics not available for " + calc.getAnalysis().getCountyOverride() + ", " + calc.getAnalysis().getStateCodeOverride());
			return;
		}
		calc.getAnalysis().setVulnerability(BigDecimal.ONE);
		calculateConsequence(calc, true);
	}
	
	@Override
	public void calculateFloodRisk(NaturalThreatCalculationDTO calc) {
		calc.getAnalysis().setVulnerability(calc.getAnalysis().getFloodRisk() ? BigDecimal.ONE : BigDecimal.ZERO);
		calc.getAnalysis().setLot(new BigDecimal("0.01"));
		calculateConsequence(calc, false);
	}

	@Override
	public void calculateHurricaneRisk(NaturalThreatCalculationDTO calc) {
		BigDecimal lot = BigDecimal.ZERO;
		
		// Calculate likelihood based on design speed of asset
		try {
			List<HurricaneLookupDataDTO> lookupData = NaturalThreatLookupData.get().getHurricaneLookupDataDTOs();
			boolean first = true;
			int i = -1; // Skipping tropical depressions, 0 is tropical storm
			for (HurricaneLookupDataDTO dto : lookupData) {
				if (i >= 0 && calc.getAnalysis().getDesignSpeed().compareTo(dto.getLowerWindSpeed()) == -1) {
					BigDecimal probability = BigDecimal.ZERO;
					BigDecimal returnSpeed = getCategoryReturnPeriod(calc.getHurricaneProfile(), i);
					if (BigDecimal.ZERO.compareTo(returnSpeed) == -1) { 
						probability = BigDecimal.ONE.divide(returnSpeed, 10, RoundingMode.HALF_UP);
					}
					
					if (first) {
						probability = probability.multiply(new BigDecimal("0.5"));
						first = false;
					}
					
					lot = lot.add(probability);
				}
				i++;
			}
		} catch (Exception ex) {
			log.error("Could not calculate hurricane risk.", ex);
			calc.setCalculationSuccess(false);
			calc.setResultMessage("Hurricane risk could not be calculated: " + ex.getMessage());
			return;
		}
		
		calc.getAnalysis().setVulnerability(BigDecimal.ONE);
		lot = lot.divide(BigDecimal.ONE, 4, RoundingMode.HALF_UP);  // Matching precision of original model
		calc.getAnalysis().setLot(lot);
		calculateConsequence(calc, true);
		
	}
	
	private BigDecimal getCategoryReturnPeriod(HurricaneProfileDTO profile, int category) {
		if (log.isDebugEnabled()) { log.debug("Calling category return period for: " + category +", Profile: " + String.valueOf(profile)); }
		
		if (profile == null) {
			return BigDecimal.ZERO;
		}
		
		if (category >= 0 && category <= 5) {
			try {
				Method m = profile.getClass().getMethod("getCat" + category + "ReturnPeriod");
				return (BigDecimal) m.invoke(profile);
			} catch (Exception e) {
				log.error("Could not dynamically generate call to get return speed for category: " + category, e);
				throw new IllegalStateException();
			}
		}
		throw new IllegalArgumentException("Category must be between 0 and 5 inclusive.");
	}

	private void calculateConsequence(NaturalThreatCalculationDTO calc, boolean usesDamageFactor) {
		BigDecimal damageFactor = usesDamageFactor ? calc.getAsset().getDamageFactor() : BigDecimal.ONE;
		
		BigDecimal adjustedReplacementCost = calc.getAsset().getReplacementCostDTO().getQuantity().multiply(damageFactor);
		BigDecimal consequence = calc.getAnalysis().getOperationalLossDTO().getQuantity();
		consequence = consequence.add(adjustedReplacementCost);
		
		AmountDTO consequenceAmount = new AmountDTO(consequence, MoneyUnitType.DOLLAR);
		calc.getAnalysis().setFinancialImpactDTO(consequenceAmount);
		ParreAnalysis parreAnalysis = NaturalThreatServiceImpl.getInstance().findParreAnalysis(calc.getAsset().getParreAnalysisId());
		if (parreAnalysis.getUseStatValues()) {
			BigDecimal updatedFinancialTotal = SharedCalculationUtil.calculateFinancialTotal(calc.getAnalysis().getFatalities(), calc.getAnalysis().getSeriousInjuries(), 
					calc.getAnalysis().getFinancialImpactDTO().getQuantity(), 
					parreAnalysis.getStatValues());
			calc.getAnalysis().setFinancialTotalDTO(new AmountDTO(updatedFinancialTotal, calc.getAnalysis().getFinancialImpactDTO().getUnit()));
		} else {
			calc.getAnalysis().setFinancialTotalDTO(new AmountDTO(
					calc.getAnalysis().getFinancialImpactDTO().getQuantity(),
					calc.getAnalysis().getFinancialImpactDTO().getUnit()));
		}

		if (log.isDebugEnabled()) { log.debug("Calculate Risk: Financial Total = " + calc.getAnalysis().getFinancialImpactDTO().getQuantity() +
                " LOT = " + calc.getAnalysis().getLot() +  " Vulnerability = " + calc.getAnalysis().getVulnerability()); }
		calc.getAnalysis().setTotalRiskDTO(SharedCalculationUtil.calculateNaturalThreatTotalRisk(calc.getAnalysis()));
	}

	@Override
	public void calculateIceStormRisk(NaturalThreatCalculationDTO calc) {
		calculateIceDamageProbabilities(calc.getIceStormWorkDTO());
		
		ParreAnalysis parreAnalysis = NaturalThreatServiceImpl.getInstance().findParreAnalysis(calc.getAsset().getParreAnalysisId());
		if (log.isDebugEnabled()) { log.debug("ParreAnalysisId = " + calc.getAsset().getParreAnalysisId()); }
		BigDecimal averageConsequence = BigDecimal.ZERO;
		BigDecimal averageFinancialImpact = BigDecimal.ZERO;
		BigDecimal averageVulnerability = BigDecimal.ZERO;
		BigDecimal averageLOT = BigDecimal.ZERO;
		BigDecimal totalRisk = BigDecimal.ZERO;
		
		if (calc.getIceStormWorkDTO().getDailyOperationalLoss().longValue() > 0 || calc.getIceStormWorkDTO().getDaysOfBackupPower().longValue() > 0) {
			for (int i = 3; i <= 5; i++) {
				calculateIceStormIndexThreat(calc.getIceStormWorkDTO(), i, parreAnalysis);
				
				IceStormIndexWorkDTO indexWork = calc.getIceStormWorkDTO().getIceStormIndex(i);
				if (log.isDebugEnabled()) { log.debug("Calculated for index " + i + " : " + indexWork); }
				
				averageConsequence = averageConsequence.add(indexWork.getConsequence());
				averageFinancialImpact = averageFinancialImpact.add(indexWork.getFinancialImpact());
				averageVulnerability = averageVulnerability.add(indexWork.getVulnerability());
				averageLOT = averageLOT.add(indexWork.getLot());
				
				totalRisk = totalRisk.add(indexWork.getConsequence().multiply(indexWork.getVulnerability()).multiply(indexWork.getLot()));
			}
		}
		calc.getAnalysis().setFinancialImpactDTO(new AmountDTO(averageFinancialImpact.divide(THREE, RoundingMode.HALF_UP), MoneyUnitType.DOLLAR));
		calc.getAnalysis().setFinancialTotalDTO(new AmountDTO(averageConsequence.divide(THREE, RoundingMode.HALF_UP), MoneyUnitType.DOLLAR));
		calc.getAnalysis().setLot(averageLOT.divide(THREE, RoundingMode.HALF_UP));
		calc.getAnalysis().setVulnerability(averageVulnerability.divide(THREE, RoundingMode.HALF_UP));
		
		calc.getAnalysis().setTotalRiskDTO(new AmountDTO(totalRisk, MoneyUnitType.DOLLAR));
	}
	
	private void calculateIceStormIndexThreat(IceStormWorkDTO work, Integer daysIndex, ParreAnalysis parreAnalysis) {
		IceStormIndexWorkDTO indexWork = work.getIceStormIndex(daysIndex);
		if (work.getDaysOfBackupPower().intValue() >= INDEX_DAYS_MAP.get(daysIndex)) {
			indexWork.setConsequence(BigDecimal.ZERO);
			indexWork.setVulnerability(BigDecimal.ZERO);
			indexWork.setLot(BigDecimal.ZERO);
		} else {
			// Consequence = (DaysLimit - DaysOfBackupPower + RecoveryTime) * OperationalLossPerDay + LossOfLifeAdjusted + SeriousInjuryAdjusted
			BigDecimal daysLimit = new BigDecimal(INDEX_DAYS_MAP.get(daysIndex));
			BigDecimal consequence = daysLimit.subtract(work.getDaysOfBackupPower());
			consequence = consequence.add(indexWork.getRecoveryTime());
			consequence = consequence.multiply(work.getDailyOperationalLoss());
			indexWork.setFinancialImpact(consequence);
			if (parreAnalysis.getUseStatValues()) {
				consequence = SharedCalculationUtil.calculateFinancialTotal(indexWork.getFatalities(), indexWork.getSeriousInjuries(), consequence, parreAnalysis.getStatValues());
			}
			indexWork.setConsequence(consequence);
			
			
			// Vulnerability = (DaysLimit - DaysOfBackupPower)/DaysLimit * ProbabilityOfPowerOutage
			BigDecimal vulnerability = daysLimit.subtract(work.getDaysOfBackupPower());
			vulnerability = vulnerability.divide(daysLimit, 10, RoundingMode.HALF_UP);
			vulnerability = vulnerability.multiply(indexWork.getPowerOutageProbability());
			indexWork.setVulnerability(vulnerability);
			
			// Likelihood of threat = Calculated probability of ice storm
			indexWork.setLot(indexWork.getIceDamageProbability());
		}
	}

	@Override
	public void calculateIceDamageProbabilities(IceStormWorkDTO work) {
		IceworkLookupDataDTO iceLookup = NaturalThreatLookupData.get().getIceworkLookupDataDTO(work.getStateCode());
		WindLookupDataDTO windLookup = NaturalThreatLookupData.get().getWindLookup(work.getLatitude(), work.getLongitude());
		
		IceStormIndexWorkDTO indexWork = work.getIceStormIndex(3);
		if (indexWork == null) {
			indexWork = new IceStormIndexWorkDTO();
		}
		indexWork.setPowerOutageProbability(NaturalThreatCalculator.INDEX3_POWER_OUTAGE_PROBABLILITY);
		work.addIceStormIndex(3, indexWork);
		
		indexWork = work.getIceStormIndex(4);
		if (indexWork == null) {
			indexWork = new IceStormIndexWorkDTO();
		}
		indexWork.setPowerOutageProbability(NaturalThreatCalculator.INDEX4_POWER_OUTAGE_PROBABLILITY);
		work.addIceStormIndex(4, indexWork);
		
		indexWork = work.getIceStormIndex(5);
		if (indexWork == null) {
			indexWork = new IceStormIndexWorkDTO();
		}
		indexWork.setPowerOutageProbability(NaturalThreatCalculator.INDEX5_POWER_OUTAGE_PROBABLILITY);
		work.addIceStormIndex(5, indexWork);
		
		for (int i = 3; i <=5; i++) {
			work.getIceStormIndex(i).setIceDamageProbability(BigDecimal.ZERO);
			int k = i -2;
			for (int j = 4; j >= 1; j--, k++) {
				BigDecimal iceThicknessProbability = work.getIceStormIndex(i).getIceDamageProbability();
				BigDecimal temp = iceLookup == null ? BigDecimal.ZERO : iceLookup.getIntervalByIndex(k).multiply(windLookup.getIntervalByIndex(j));
				temp = temp.add(iceThicknessProbability);
				work.getIceStormIndex(i).setIceDamageProbability(temp);
			}
		}
	}
}
