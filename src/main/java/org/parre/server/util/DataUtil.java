/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * The Class DataUtil.
 */
public class DataUtil {
	private static transient Logger log = Logger.getLogger(DataUtil.class);
	
    public static List<String[]> createRecords(File dataFile) {
        return DataUtil.createRecords(dataFile, "\t");
    }
	
	public static List<String[]> createRecords(File dataFile, String separator) {
        List<String[]> dataRecords;
        InputStream inputStream = null;
        InputStreamReader in = null;
        BufferedReader bReader = null;
        try {
            inputStream = new FileInputStream(dataFile);
            in = new InputStreamReader(inputStream);
            bReader = new BufferedReader(in);
            List<String> records = new ArrayList<String>();
            String record;
            while ((record = bReader.readLine()) != null) {
                records.add(record);
            }
            dataRecords = new ArrayList<String[]>(records.size());
            for (String dataRecord : records) {
                dataRecords.add(dataRecord.split(separator));
            }
        } catch (Exception e) {
            String message = "Error Initializing DB : " + e.getMessage();
            log.error(message, e);
            throw new RuntimeException(message);
        } finally {
            try {
                if (bReader != null) {
                    bReader.close();
                }
                if (in != null) {
                    in.close();
                }
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException e) {
                log.error("Error closing streams : " + e.getMessage());
            }
        }
        return dataRecords;
    }
	
	public static void removeQuotes(List<String[]> dataRecords) {
        for (String[] dataRecord : dataRecords) {
            int index = 0;
            for (String value : dataRecord) {
                if (value.startsWith("\"")) {
                    value = value.substring(1, value.length() - 1);
                }
                dataRecord[index] = value;
                index++;
            }
        }
    }
}
