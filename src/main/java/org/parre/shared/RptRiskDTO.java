/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;
import java.util.Comparator;

import org.parre.shared.util.ThreatTypeUtil;



/**
 * @author Raju Kanumuri
 * @date 10/30/2013
 */
public class RptRiskDTO extends BaseDTO implements Comparable<RptRiskDTO>{
    private String asset;
    private String threat;
    private String risk;
    
    
    public RptRiskDTO(String asset, String threat, String risk){
		this.asset=asset;
		this.threat=threat;
		this.risk=risk;
	}

	public String getAsset() {
		return asset;
	}

	public void setAsset(String asset) {
		this.asset = asset;
	}

	public String getThreat() {
		return threat;
	}

	public void setThreat(String threat) {
		this.threat = threat;
	}

	public String getRisk() {
		return risk;
	}

	public void setRisk(String risk) {
		this.risk = risk;
	}

	@Override
	public int compareTo(RptRiskDTO o) {
		return Comparators.RISK.compare(this, o);
	}
	
	public static class Comparators {
		public static Comparator<RptRiskDTO> RISK = new Comparator<RptRiskDTO>() {
            @Override
            public int compare(RptRiskDTO o1, RptRiskDTO o2) {
            	BigDecimal firstValue = o1 != null && o1.risk != null ? new BigDecimal(ThreatTypeUtil.removeCommas(o1.risk)) : BigDecimal.ZERO;
            	BigDecimal secondValue = o2 != null && o2.risk != null ? new BigDecimal(ThreatTypeUtil.removeCommas(o2.risk)) : BigDecimal.ZERO;
                return secondValue.compareTo(firstValue);
            }
        };
	}
	
	
}
