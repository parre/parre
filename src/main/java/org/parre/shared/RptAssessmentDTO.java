/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;

/**
 * @author Raju Kanumuri
 * @date 11/04/2013
 */
public class RptAssessmentDTO extends BaseDTO implements Serializable {

	private static final long serialVersionUID = 457309028718202073L;
	private String optionName;  
    private String gross;
    private String value;
    private String net;
    private String ratio;
   
    
    
    public RptAssessmentDTO(String optionName, String gross, String value, String net, String ratio){
		this.optionName=optionName;		
		this.gross=gross;
		this.value=value;
		this.net=net;
		this.ratio=ratio;		
	}

    public RptAssessmentDTO(){
    }

	public String getOptionName() {
		return optionName;
	}



	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}



	public String getGross() {
		return gross;
	}



	public void setGross(String gross) {
		this.gross = gross;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public String getNet() {
		return net;
	}



	public void setNet(String net) {
		this.net = net;
	}



	public String getRatio() {
		return ratio;
	}



	public void setRatio(String ratio) {
		this.ratio = ratio;
	}

	
}
