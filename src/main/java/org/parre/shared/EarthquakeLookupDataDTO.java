/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: keithjones
 * Date: 1/26/12
 * Time: 11:06 AM
*/
public class EarthquakeLookupDataDTO extends BaseDTO {

    private String seismicZone;
    private BigDecimal lowerGBoundry;
    private BigDecimal upperGBoundry;
    private BigDecimal lowerRichter;
    private BigDecimal upperRichter;
    private BigDecimal pre1988Vulnerability;
    private BigDecimal post1988Vulnerability;
    private BigDecimal other;

    public String getSeismicZone() {
        return seismicZone;
    }

    public void setSeismicZone(String seismicZone) {
        this.seismicZone = seismicZone;
    }

    public BigDecimal getLowerGBoundry() {
        return lowerGBoundry;
    }

    public void setLowerGBoundry(BigDecimal lowerGBoundry) {
        this.lowerGBoundry = lowerGBoundry;
    }

    public BigDecimal getUpperGBoundry() {
        return upperGBoundry;
    }

    public void setUpperGBoundry(BigDecimal upperGBoundry) {
        this.upperGBoundry = upperGBoundry;
    }

    public BigDecimal getLowerRichter() {
        return lowerRichter;
    }

    public void setLowerRichter(BigDecimal lowerRichter) {
        this.lowerRichter = lowerRichter;
    }

    public BigDecimal getUpperRichter() {
        return upperRichter;
    }

    public void setUpperRichter(BigDecimal upperRichter) {
        this.upperRichter = upperRichter;
    }

    public BigDecimal getPre1988Vulnerability() {
        return pre1988Vulnerability;
    }

    public void setPre1988Vulnerability(BigDecimal pre1988Vulnerability) {
        this.pre1988Vulnerability = pre1988Vulnerability;
    }

    public BigDecimal getPost1988Vulnerability() {
        return post1988Vulnerability;
    }

    public void setPost1988Vulnerability(BigDecimal post1988Vulnerability) {
        this.post1988Vulnerability = post1988Vulnerability;
    }

    public BigDecimal getOther() {
        return other;
    }

    public void setOther(BigDecimal other) {
        this.other = other;
    }

    @Override
    public String toString() {
        return "EarthquakeCalcs{" +
                "seismicZone='" + seismicZone + '\'' +
                ", lowerGBoundry=" + lowerGBoundry +
                ", upperGBoundry=" + upperGBoundry +
                ", lowerRichter=" + lowerRichter +
                ", upperRichter=" + upperRichter +
                ", pre1988Vulnerability=" + pre1988Vulnerability +
                ", post1988Vulnerability=" + post1988Vulnerability +
                ", other=" + other +
                '}';
    }
}
