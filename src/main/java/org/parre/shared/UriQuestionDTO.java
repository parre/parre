/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.parre.shared.types.UriQuestionType;


/**
 * The Class UriQuestionDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/15/12
 */
public class UriQuestionDTO extends NameDescriptionDTO {
    private UriQuestionType type;
    private List<UriOptionDTO> optionDTOs = Collections.emptyList();

    public UriQuestionType getType() {
        return type;
    }

    public void setType(UriQuestionType type) {
        this.type = type;
    }

    public List<UriOptionDTO> getOptionDTOs() {
        return optionDTOs;
    }

    public void setOptionDTOs(List<UriOptionDTO> optionDTOs) {
        this.optionDTOs = optionDTOs;
    }

    public void addOptionDTO(UriOptionDTO dto) {
        if (optionDTOs.isEmpty()) {
            optionDTOs = new ArrayList<UriOptionDTO>();
        }
        optionDTOs.add(dto);
    }
}