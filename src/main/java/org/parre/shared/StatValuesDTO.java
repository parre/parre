/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


import java.io.Serializable;
import java.math.BigDecimal;

import org.parre.shared.types.MoneyUnitType;

/**
 * The Class StatValuesDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/4/11
 */
public class StatValuesDTO implements Serializable, StatValueQuantities {
    private AmountDTO fatalityDTO = AmountDTO.createZero();
    private AmountDTO seriousInjuryDTO = AmountDTO.createZero();

    public StatValuesDTO() {
    }

    public StatValuesDTO(StatValuesDTO statValues) {
        this.fatalityDTO = statValues.fatalityDTO;
        this.seriousInjuryDTO = statValues.seriousInjuryDTO;
    }

    public AmountDTO getFatalityDTO() {
        return fatalityDTO;
    }

    public void setFatalityDTO(AmountDTO fatalityDTO) {
        this.fatalityDTO = fatalityDTO;
    }

    public AmountDTO getSeriousInjuryDTO() {
        return seriousInjuryDTO;
    }

    public void setSeriousInjuryDTO(AmountDTO seriousInjuryDTO) {
        this.seriousInjuryDTO = seriousInjuryDTO;
    }

    public BigDecimal getFatalityQuantity() {
        return fatalityDTO != null ? fatalityDTO.getQuantity() : BigDecimal.ZERO;
    }

    public BigDecimal getSeriousInjuryQuantity() {
        return seriousInjuryDTO.getQuantity() != null ? seriousInjuryDTO.getQuantity() : BigDecimal.ZERO;
    }
    
    public static StatValuesDTO createEmptyStatValues() {
    	StatValuesDTO dto = new StatValuesDTO();
    	dto.setFatalityDTO(new AmountDTO(BigDecimal.ZERO, MoneyUnitType.DOLLAR));
    	dto.setSeriousInjuryDTO(new AmountDTO(BigDecimal.ZERO, MoneyUnitType.DOLLAR));
    	return dto;
    }

	@Override
	public String toString() {
		return "StatValuesDTO [fatalityDTO=" + fatalityDTO
				+ ", seriousInjuryDTO=" + seriousInjuryDTO + "]";
	}
}
