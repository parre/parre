/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.util.Date;

/**
 * The Class AnalysisDTO.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
public class AnalysisDTO extends NameDescriptionDTO {
    private Long parreAnalysisId;
    private Date startedDate;

    public AnalysisDTO() {
    }

    public AnalysisDTO(NameDescriptionDTO dto) {
        super(dto);
    }

    public AnalysisDTO(NameDescriptionDTO dto, Long parreAnalysisId) {
        super(dto);
        setParreAnalysisId(parreAnalysisId);
    }

    public Date getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(Date startedDate) {
        this.startedDate = fixDate(startedDate);
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }
}
