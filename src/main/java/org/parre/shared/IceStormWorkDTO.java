/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * The Class IceStormWorkDTO.
 */
public class IceStormWorkDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private BigDecimal latitude;
    private BigDecimal longitude;
    private String stateCode;
    
    private BigDecimal daysOfBackupPower = BigDecimal.ZERO;
    private BigDecimal dailyOperationalLoss = BigDecimal.ZERO;
    
    Map<Integer, IceStormIndexWorkDTO> indexMap = new HashMap<Integer, IceStormIndexWorkDTO>();
    
    
    public IceStormWorkDTO() { }


	public BigDecimal getLatitude() {
		return latitude;
	}


	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}


	public BigDecimal getLongitude() {
		return longitude;
	}


	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}


	public String getStateCode() {
		return stateCode;
	}


	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}


	public BigDecimal getDaysOfBackupPower() {
		return daysOfBackupPower;
	}


	public void setDaysOfBackupPower(BigDecimal daysOfBackupPower) {
		this.daysOfBackupPower = daysOfBackupPower == null ? BigDecimal.ZERO : daysOfBackupPower;
	}


	public BigDecimal getDailyOperationalLoss() {
		return dailyOperationalLoss;
	}


	public void setDailyOperationalLoss(BigDecimal dailyOperationalLoss) {
		this.dailyOperationalLoss = dailyOperationalLoss == null ? BigDecimal.ZERO : dailyOperationalLoss;
	}


	public Map<Integer, IceStormIndexWorkDTO> getIndexMap() {
		return indexMap;
	}


	public void setIndexMap(Map<Integer, IceStormIndexWorkDTO> indexMap) {
		this.indexMap = indexMap;
	}

	public void addIceStormIndex(Integer index, IceStormIndexWorkDTO work) {
		indexMap.put(index, work);
	}
	
	public IceStormIndexWorkDTO getIceStormIndex(Integer index) {
		return indexMap.get(index);
	}
	
	public Integer getIndexFatalityCount(int index) {
		return (getIceStormIndex(index) != null && getIceStormIndex(index).getFatalities() != null)
				? getIceStormIndex(index).getFatalities()
			    : 0;
	}
	
	public Integer getIndexSeriousInjuryCount(int index) {
		return (getIceStormIndex(index) != null && getIceStormIndex(index).getSeriousInjuries() != null)
				? getIceStormIndex(index).getSeriousInjuries()
						: 0;
	}


	@Override
	public String toString() {
		String iceIndexOutput = "";
		
		for (int i = 3; i <= 5; i++) {
			iceIndexOutput += String.valueOf(getIceStormIndex(i));
		}
		return "IceStormWorkDTO [latitude=" + latitude + ", longitude="
				+ longitude + ", stateCode=" + stateCode
				+ ", daysOfBackupPower=" + daysOfBackupPower
				+ ", dailyOperationalLoss=" + dailyOperationalLoss 
				+ ", indexes: " + iceIndexOutput + "]";
	}
	
	
}
