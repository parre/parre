/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.io.Serializable;


/**
 * .
 * User: Swarn S. Dhaliwal
 * Date: 8/31/11
 * Time: 3:56 PM
 */
public class LabelValueDTO implements Comparable<LabelValueDTO>, Serializable {
    private String label;
    private String value;

    public LabelValueDTO() {
    }

    public LabelValueDTO(String label, String value) {
        this.label = label;
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LabelValueDTO that = (LabelValueDTO) o;

        if (value != null ? !value.equals(that.value) : that.value != null) return false;

        return true;
    }

    public int compareTo(LabelValueDTO o) {
        return label.compareTo(o.label);
    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "LabelValueDTO{" +
                "label='" + label + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
