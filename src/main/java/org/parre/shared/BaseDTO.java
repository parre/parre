/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import com.google.gwt.user.client.rpc.IsSerializable;

import java.util.*;


/**
 * User: Swarn S. Dhaliwal
 * Date: 7/9/11
 * Time: 10:27 AM
*/
public class BaseDTO implements IsSerializable {
    private Long id;

    public BaseDTO() {
    }

    public BaseDTO(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public BaseDTO copyInto(BaseDTO dto) {
        dto.setId(getId());
        return dto;
    }

    public void copyFrom(BaseDTO dto) {
        setId(dto.getId());
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean hasId(Long id) {
        return this.id.equals(id);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseDTO baseDTO = (BaseDTO) o;

        if (id != null ? !id.equals(baseDTO.id) : baseDTO.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public boolean isNew() {
        return id == null || id == 0;
    }

    protected Date fixDate(Date date) {
        if (date == null) {
            return null;
        }
        if (date.getClass() != Date.class) {
            return new Date(date.getTime());
        }
        return date;
    }

    public static <T> Set<T> add(T value, Set<T> set) {
        if (set == null || set.isEmpty()) {
            set = new HashSet<T>();
        }
        set.add(value);
        return set;
    }

    public static <T> List<T> add(T value, List<T> list) {
        if (list == null || list.isEmpty()) {
            list = new ArrayList<T>();
        }
        list.add(value);
        return list;
    }

}
