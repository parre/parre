/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: Daniel
 * Date: 1/12/12
 * Time: 4:18 PM
*/
public class ProxyIndicationDTO extends NameDescriptionDTO {
    private Long parreAnalysisId;
    private BigDecimal node6Value;
    private BigDecimal calculatedProbability;
    private Integer attacksPerYear = 4;
    private ProxyCityDTO proxyCityDTO;
    private ProxyTargetTypeDTO targetTypeDTO;
    private Integer targetTypeInRegionNum;
    private Integer facilitiesInRegionNum;
    private Integer facilityCapacity;
    private Integer regionalCapacity;
    private Integer population;

    public ProxyIndicationDTO() {
    }

    public ProxyIndicationDTO(Integer attacksPerYear, ProxyCityDTO cityDTO, ProxyTargetTypeDTO targetType, Integer facilitiesInRegionNum, Integer targetTypeInRegionNum, Integer facilityCapacity, Integer regionalCapacity, Integer population) {
        this.attacksPerYear = attacksPerYear;
        this.proxyCityDTO = cityDTO;
        this.targetTypeDTO = targetType;
        this.targetTypeInRegionNum = targetTypeInRegionNum;
        this.facilitiesInRegionNum = facilitiesInRegionNum;
        this.facilityCapacity = facilityCapacity;
        this.regionalCapacity = regionalCapacity;
        this.population = population;
    }
    
    public ProxyIndicationDTO(Integer attacksPerYear, ProxyCityDTO cityDTO, ProxyTargetTypeDTO targetType, Integer facilitiesInRegionNum, Integer targetTypeInRegionNum, Integer facilityCapacity, Integer regionalCapacity, Integer population, BigDecimal node6Value) {
    	this.attacksPerYear = attacksPerYear;
    	this.proxyCityDTO = cityDTO;
    	this.targetTypeDTO = targetType;
    	this.targetTypeInRegionNum = targetTypeInRegionNum;
    	this.facilitiesInRegionNum = facilitiesInRegionNum;
    	this.facilityCapacity = facilityCapacity;
    	this.regionalCapacity = regionalCapacity;
    	this.population = population;
    	this.node6Value = node6Value;
    }

    public Long getParreAnalysisId() {
        return parreAnalysisId;
    }

    public void setParreAnalysisId(Long parreAnalysisId) {
        this.parreAnalysisId = parreAnalysisId;
    }

    public BigDecimal getNode6Value() {
        return node6Value;
    }

    public void setNode6Value(BigDecimal node6Value) {
        this.node6Value = node6Value;
    }

    public BigDecimal getCalculatedProbability() {
        return calculatedProbability;
    }

    public void setCalculatedProbability(BigDecimal calculatedProbability) {
        this.calculatedProbability = calculatedProbability;
    }

    public Integer getAttacksPerYear() {
        return attacksPerYear;
    }

    public ProxyCityDTO getProxyCityDTO() {
        return proxyCityDTO;
    }

    public ProxyTargetTypeDTO getTargetTypeDTO() {
        return targetTypeDTO;
    }

    public Integer getTargetTypeInRegionNum() {
        return targetTypeInRegionNum;
    }

    public Integer getFacilitiesInRegionNum() {
        return facilitiesInRegionNum;
    }

    public Integer getFacilityCapacity() {
        return facilityCapacity;
    }

    public Integer getRegionalCapacity() {
        return regionalCapacity;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setAttacksPerYear(Integer attacksPerYear) {
        this.attacksPerYear = attacksPerYear;
    }

    public void setProxyCityDTO(ProxyCityDTO proxyCityDTO) {
        this.proxyCityDTO = proxyCityDTO;
    }

    public void setTargetTypeDTO(ProxyTargetTypeDTO targetTypeDTO) {
        this.targetTypeDTO = targetTypeDTO;
    }

    public void setTargetTypeInRegionNum(Integer targetTypeInRegionNum) {
        this.targetTypeInRegionNum = targetTypeInRegionNum;
    }

    public void setFacilitiesInRegionNum(Integer facilitiesInRegionNum) {
        this.facilitiesInRegionNum = facilitiesInRegionNum;
    }

    public void setFacilityCapacity(Integer facilityCapacity) {
        this.facilityCapacity = facilityCapacity;
    }

    public void setRegionalCapacity(Integer regionalCapacity) {
        this.regionalCapacity = regionalCapacity;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }
}
