/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

/**
 * The Enum AssetThreatAnalysisType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/23/12
 */
public enum AssetThreatAnalysisType {
    CONSEQUENCE("Consequence"), DP("Dependency and Proximity"),
    LOT("Threat Likelihood"), NATURAL_THREAT("Natural Hazard"), RISK_RESILIENCE("Risk and Resilience"),
    VULNERABILITY("Vulnerability");
    private String value;

    AssetThreatAnalysisType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isDirected() {
        switch (this) {
            case CONSEQUENCE:
            case LOT:
            case VULNERABILITY:
            return true;
        default:
            return false;
        }
    }

    public boolean isNatural() {
        return NATURAL_THREAT == this;
    }

    public boolean isDependencyProximity() {
        return DP == this;
    }

    public boolean isRiskResilience() {
        return RISK_RESILIENCE == this;
    }

    public ThreatCategoryType getThreatCategoryType() {
        switch (this) {
            case CONSEQUENCE:
            case LOT:
            case VULNERABILITY:
                return ThreatCategoryType.MAN_MADE_HAZARD;
            case NATURAL_THREAT:
                return ThreatCategoryType.NATURAL_HAZARD;
            case DP:
                return ThreatCategoryType.DEPENDENCY_PROXIMITY;
            default:
                return ThreatCategoryType.NONE;
        }
    }
}
