/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared.types;

import org.parre.shared.*;

/**
 * The Enum AssetType.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 9/23/11
 */
public enum AssetType {
    PHYSICAL_ASSET("Physical Asset"), HUMAN_ASSET("Human Asset"),
    EXTERNAL_ASSET("External Asset"), SCADA("Scada"), PRODUCT_SERVICE("Product/Service");
    private String value;

    AssetType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
    public AssetDTO createAssetDTO() {
        switch (this) {
            case HUMAN_ASSET:
                return new HumanResourceDTO();
            case EXTERNAL_ASSET:
                return new ExternalResourceDTO();
            case PHYSICAL_ASSET:
                return new PhysicalResourceDTO();
            case SCADA:
                return new ScadaDTO();
            case PRODUCT_SERVICE:
                return new ProductServiceDTO();
            default:
                throw new IllegalArgumentException("Unknown AssetType : " + this);
        }
    }

    public boolean isProductService() {
        return PRODUCT_SERVICE == this;
    }
}
