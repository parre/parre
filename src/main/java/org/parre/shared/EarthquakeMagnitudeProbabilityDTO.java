/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import java.math.BigDecimal;

/**
 * User: keithjones
 * Date: 5/7/12
 * Time: 1:11 PM
*/
public class EarthquakeMagnitudeProbabilityDTO extends BaseDTO implements Comparable<EarthquakeMagnitudeProbabilityDTO> {

	private BigDecimal magnitude;
    private BigDecimal probability;
    
    public EarthquakeMagnitudeProbabilityDTO(BigDecimal magnitude) {
        this.magnitude = magnitude;
        this.probability = BigDecimal.ZERO;
    }

    public EarthquakeMagnitudeProbabilityDTO() {
        this.magnitude = BigDecimal.ZERO;
        this.probability = BigDecimal.ZERO;
    }

	public EarthquakeMagnitudeProbabilityDTO(BigDecimal magnitude,
			BigDecimal probability) {
		super();
		this.magnitude = magnitude;
		this.probability = probability;
	}

	public BigDecimal getProbability() {
        return probability;
    }

    public void setProbability(BigDecimal probability) {
        this.probability = probability;
    }

    public BigDecimal getMagnitude() {
        return magnitude;
    }

    public void setMagnitude(BigDecimal magnitude) {
        this.magnitude = magnitude;
    }

	@Override
	public String toString() {
		return "EarthquakeMagnitudeProbabilityDTO [magnitude=" + magnitude
				+ ", probability=" + probability + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((magnitude == null) ? 0 : magnitude.hashCode());
		result = prime * result
				+ ((probability == null) ? 0 : probability.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		EarthquakeMagnitudeProbabilityDTO other = (EarthquakeMagnitudeProbabilityDTO) obj;
		if (magnitude == null) {
			if (other.magnitude != null)
				return false;
		} else if (!magnitude.equals(other.magnitude))
			return false;
		if (probability == null) {
			if (other.probability != null)
				return false;
		} else if (!probability.equals(other.probability))
			return false;
		return true;
	}

	@Override
	public int compareTo(EarthquakeMagnitudeProbabilityDTO other) {
		if (magnitude != null && other.getMagnitude() != null) {
			return magnitude.compareTo(other.getMagnitude());
		}
		return 0;
	}
}
