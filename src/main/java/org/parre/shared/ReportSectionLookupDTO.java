/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;


public class ReportSectionLookupDTO extends BaseDTO {
	
	private String name;
	private String description;
	private String additionalDescription;
	private String instructions;
	private Integer displayOrder;
	private String reportSectionGroup;
	private Boolean displayTable;
	private Boolean inputTable;
	
	public ReportSectionLookupDTO() {
		super();
	}

	public ReportSectionLookupDTO(Long id, String name, String description,
			String additionalDescription, String instructions,
			Integer displayOrder, String reportSectionGroup, Boolean displayTable, Boolean inputTable) {
		super(id);
		this.name = name;
		this.description = description;
		this.additionalDescription = additionalDescription;
		this.instructions = instructions;
		this.displayOrder = displayOrder;
		this.reportSectionGroup = reportSectionGroup;
		this.displayTable = displayTable;
		this.inputTable = inputTable;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAdditionalDescription() {
		return additionalDescription;
	}

	public void setAdditionalDescription(String additionalDescription) {
		this.additionalDescription = additionalDescription;
	}

	public String getInstructions() {
		return instructions;
	}

	public void setInstructions(String instructions) {
		this.instructions = instructions;
	}

	public Integer getDisplayOrder() {
		return displayOrder;
	}

	public void setDisplayOrder(Integer displayOrder) {
		this.displayOrder = displayOrder;
	}

	public String getReportSectionGroup() {
		return reportSectionGroup;
	}

	public void setReportSectionGroup(String reportSectionGroup) {
		this.reportSectionGroup = reportSectionGroup;
	}

	public Boolean getDisplayTable() {
		return displayTable;
	}

	public void setDisplayTable(Boolean displayTable) {
		this.displayTable = displayTable;
	}

	public Boolean getInputTable() {
		return inputTable;
	}

	public void setInputTable(Boolean inputTable) {
		this.inputTable = inputTable;
	}

	@Override
	public String toString() {
		return "ReportSectionLookupDTO [name=" + name + ", description="
				+ description + ", additionalDescription="
				+ additionalDescription + ", instructions=" + instructions
				+ ", displayOrder=" + displayOrder + ", reportSectionGroup=" + reportSectionGroup 
				+ ", getId()=" + getId() + ", displayTable=" + displayTable + ", inputTable=" + inputTable 
				+ "]";
	}

	
}
