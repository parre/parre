/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.shared;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.shared.RiskResilienceDTO;
import org.parre.shared.UnitPriceDTO;

import java.math.BigDecimal;

/**
 * The Class CalculationTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/17/12
 */
public class CalculationTest {
    private static transient final Logger log = Logger.getLogger(CalculationTest.class);
    @Test
    public void calculateFinancialImpact() {
        int numberOfDays = 10;
        int mgd = 10;
        int millionGallons = numberOfDays * mgd;
        long gallons = millionGallons * 1000 * 1000;

        BigDecimal unitPricePkg = new BigDecimal("3.50");
        BigDecimal totalPriceDollars = unitPricePkg.multiply(new BigDecimal(gallons)).divide(new BigDecimal(1000));
        log.info("Total Prices (Dollars) = " + totalPriceDollars);
        BigDecimal totalImpactDollars = totalPriceDollars.multiply(new BigDecimal("0.05")).multiply(new BigDecimal("0.05"));
        log.info("Total Impact (Dollars) = " + totalImpactDollars);
        log.info("Total Impact (Million Dollars) = " + totalImpactDollars.divide(new BigDecimal(1000000)));

        RiskResilienceDTO dto = new RiskResilienceDTO();
        dto.setSeverityMgd(new BigDecimal(10));
        dto.setDurationDays(new BigDecimal(10));
        dto.setUnitPriceDTO(new UnitPriceDTO(new BigDecimal("3.50"), 1000));
        dto.setVulnerability(new BigDecimal("0.05"));
        dto.setLot(new BigDecimal("0.05"));
        log.info("Total Impact : " + dto.getOwnerResilienceQuantity());
    }
}
