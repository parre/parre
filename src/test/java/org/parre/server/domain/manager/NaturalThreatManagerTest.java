/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.NaturalThreatAnalysis;
import org.parre.server.domain.manager.NaturalThreatManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ThreatDTO;

import java.util.List;

/**
 * The Class NaturalThreatManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/27/12
 */
public class NaturalThreatManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(NaturalThreatManagerTest.class);
    @Test
    public void searchUsingThreat() {
        List<NaturalThreatDTO> dtos = NaturalThreatManager.search(new ThreatDTO(), 1L, 1L, entityManager());
        log.info("Number of dtos : " + dtos.size());
        for (NaturalThreatDTO dto : dtos) {
        	log.info("Found: " + dto.toString());
        }
    }
    
    @Test
    public void testGetNaturalThreatAnalysis() {
    	NaturalThreatAnalysis analysis = NaturalThreatManager.getCurrentAnalysis(6L, entityManager());
    	log.info("Analysis: " + String.valueOf(analysis));
    }
    
    @Test
    public void testGetNaturalThreatByAssetThreatId() {
    	NaturalThreatDTO dto = NaturalThreatManager.buildByAssetThreatId(169L, 6L, entityManager());
    	log.info("Found: " + dto.toString());
    }
}
