/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.AssetThreatAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.UriQuestion;
import org.parre.server.domain.manager.RiskResilienceManager;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.AmountDTO;
import org.parre.shared.RiskResilienceDTO;


/**
 * The Class RiskResilienceManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 2/16/12
 */
public class RiskResilienceManagerTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(RiskResilienceManagerTest.class);

    @Test
    public void getRiskResilienceDTOs() {
        ParreAnalysis parreAnalysis = entityManager().find(ParreAnalysis.class, 6L);
        List<RiskResilienceDTO> riskResilienceDTOs = RiskResilienceManager.getRiskResilienceDTOs(parreAnalysis.getId(), entityManager());
        log.info("Number of Risk Resilences = " + riskResilienceDTOs.size());
        for (RiskResilienceDTO assetThreatRra : riskResilienceDTOs) {
            log.debug(assetThreatRra.toString());
        }
    }
    @Test
    public void getRiskResilienceDTOMaps() {
        Map<Long, List<RiskResilienceDTO>> riskResilienceDTOs = RiskResilienceManager.getRiskResilienceDTOs(Arrays.asList(1L, 2L), entityManager());
        List<RiskResilienceDTO> firstList = riskResilienceDTOs.get(1L);
        List<RiskResilienceDTO> secondList = riskResilienceDTOs.get(2L);
        if (secondList != null) {
	        Iterator<RiskResilienceDTO> secondIt = secondList.iterator();
	        for (RiskResilienceDTO riskResilienceDTO : firstList) {
	            log.info(riskResilienceDTO);
	            log.info(secondIt.next());
	            log.info("");
	        }
        }
    }
    @Test
    public void getRiskResilienceDTOMap() {
    	Map<Long, List<RiskResilienceDTO>> riskResilienceDTOs = RiskResilienceManager.getRiskResilienceDTOs(Arrays.asList(6L), entityManager());
    	List<RiskResilienceDTO> dtos = riskResilienceDTOs.get(6L);
    	System.err.println("Found " + dtos.size() + " RR dtos for parre analysis id:6");
		for (RiskResilienceDTO riskResilienceDTO : dtos) {
			log.info(riskResilienceDTO);
		}
    }
    @Test
    public void getFatalities() throws Exception {
        Map<Long,Map<Long,BigDecimal>> fatalities = RiskResilienceManager.getFatalities(Arrays.asList(1L, 2L), entityManager());
        log.info("fatalities=" + fatalities);
    }
    @Test
    public void testGetAssetThreatAnalsysis() {
    	List<AssetThreatAnalysis> results = entityManager().createQuery("select ata from AssetThreatAnalysis ata").getResultList();
    	
    	for (AssetThreatAnalysis result : results) {
    		log.info("Result: " + result);
    	}
    }
    
    @Test
    public void testGetSeriousInjuryMap() {
    	List<Long> parreAnalysisIds = Arrays.asList(22L, 23L, 24L);
    	Map<Long,Map<Long,BigDecimal>> seriousInjuryMap = RiskResilienceManager.getSeriousInjuries(parreAnalysisIds, entityManager());
    	for (Long parreAnalysisId : seriousInjuryMap.keySet()) {
    		for (Long assetThreatId : seriousInjuryMap.get(parreAnalysisId).keySet()) {
    			log.info("ParreAnalysis: " + parreAnalysisId + ", AssetThreatId: " + assetThreatId + ", Serious Injury: " + seriousInjuryMap.get(parreAnalysisId).get(assetThreatId));
    		}
    	}
    }
    
    @Test
    public void testGetLotMap() {
    	List<Long> parreAnalysisIds = Arrays.asList(22L, 23L, 24L);
    	Map<Long,Map<Long,BigDecimal>> seriousInjuryMap = RiskResilienceManager.getLot(parreAnalysisIds, entityManager());
    	for (Long parreAnalysisId : seriousInjuryMap.keySet()) {
    		for (Long assetThreatId : seriousInjuryMap.get(parreAnalysisId).keySet()) {
    			log.info("ParreAnalysis: " + parreAnalysisId + ", AssetThreatId: " + assetThreatId + ", Lot: " + seriousInjuryMap.get(parreAnalysisId).get(assetThreatId));
    		}
    	}
    }
    
    @Test
    public void testGetVulnerabilityMap() {
    	List<Long> parreAnalysisIds = Arrays.asList(22L, 23L, 24L);
    	Map<Long,Map<Long,BigDecimal>> seriousInjuryMap = RiskResilienceManager.getVulnerability(parreAnalysisIds, entityManager());
    	for (Long parreAnalysisId : seriousInjuryMap.keySet()) {
    		for (Long assetThreatId : seriousInjuryMap.get(parreAnalysisId).keySet()) {
    			log.info("ParreAnalysis: " + parreAnalysisId + ", AssetThreatId: " + assetThreatId + ", Lot: " + seriousInjuryMap.get(parreAnalysisId).get(assetThreatId));
    		}
    	}
    }

    private void print(Map<Long, Map<Long, AmountDTO>> financialImpact) {
        Set<Long> keys = financialImpact.keySet();
        List<Long> keyList = new ArrayList<Long>(keys);
        Long firstEntry = keyList.get(0);
        Map<Long, AmountDTO> firstEntryList = financialImpact.get(firstEntry);
        for (Map.Entry<Long, AmountDTO> entry : firstEntryList.entrySet()) {
            StringBuilder entryBuilder = new StringBuilder();
            entryBuilder.append(entry.getKey()).append(" ").append(entry.getValue());
            for (int i = 1; i < keyList.size(); i++) {
                Long id = keyList.get(i);
                entryBuilder.append(" ").append(financialImpact.get(id).get(entry.getKey()));
            }
            log.info(entryBuilder.toString());
        }
    }

    @Test
    public void testGetURIQuestions() {
        List<UriQuestion> list = RiskResilienceManager.getUriQuestions(entityManager());
        for(UriQuestion entities : list) {
            log.debug("Value = " + entities.getOptions().toString());
        }
    }
}
