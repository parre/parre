/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.DirectedThreat;
import org.parre.server.domain.DirectedThreatAnalysis;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.manager.DirectedThreatManager;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.AssetSearchDTO;
import org.parre.shared.ConsequenceDTO;
import org.parre.shared.LotDTO;
import org.parre.shared.ThreatDTO;
import org.parre.shared.VulnerabilityDTO;
import org.parre.shared.types.ThreatCategoryType;


/**
 * The Class DirectedThreatManagerTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/30/11
 */
public class DirectedThreatManagerTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(DirectedThreatManagerTest.class);
    @Test
    public void getCurrentConsequenceAnalysis() {
        DirectedThreatAnalysis analysis = DirectedThreatManager.getCurrentAnalysis(1L, entityManager());
        log.info("Current Analysis=" + analysis);
    }

    @Test
    public void searchConsequencesUsingAsset() {
        List<ConsequenceDTO> consequenceDTOs = DirectedThreatManager.searchConsequences(new AssetSearchDTO("p*", "plant*"), 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
        for (ConsequenceDTO consequenceDTO : consequenceDTOs) {
            log.info(consequenceDTO.getFinancialImpactDTO() + ", " + consequenceDTO.getEconomicImpactDTO() + ", " + consequenceDTO.getFinancialTotalDTO());
        }
    }

    @Test
    public void searchConsequencesUsingThreat() {
        ThreatDTO searchDTO = new ThreatDTO();
        searchDTO.setThreatCategory(ThreatCategoryType.MAN_MADE_HAZARD);
        List<ConsequenceDTO> consequenceDTOs = DirectedThreatManager.searchConsequences(searchDTO, 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
    }
    @Test
    public void searchVulnerabilityUsingAsset() {
        AssetSearchDTO searchDTO = new AssetSearchDTO("p*", "plant*");
        List<VulnerabilityDTO> consequenceDTOs = DirectedThreatManager.searchVulnerabilities(searchDTO, 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
        for (VulnerabilityDTO consequenceDTO : consequenceDTOs) {
            log.info(consequenceDTO.getVulnerability() + ", " + consequenceDTO.getVulnerabilityAnalysisType());
        }
    }

    @Test
    public void searchVulnerabilityUsingThreat() {
        ThreatDTO searchDTO = new ThreatDTO();
        searchDTO.setThreatCategory(ThreatCategoryType.MAN_MADE_HAZARD);
        List<VulnerabilityDTO> consequenceDTOs = DirectedThreatManager.searchVulnerabilities(searchDTO, 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
        for (VulnerabilityDTO consequenceDTO : consequenceDTOs) {
            log.info(consequenceDTO.getVulnerability() + ", " + consequenceDTO.getVulnerabilityAnalysisType());
        }

    }
    @Test
    public void searchLotUsingAsset() {
        List<LotDTO> consequenceDTOs = DirectedThreatManager.searchLots(new AssetSearchDTO("p*", "plant*"), 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
        for (LotDTO consequenceDTO : consequenceDTOs) {
            log.info(consequenceDTO.getLot() + ", " + consequenceDTO.getLotAnalysisType());
        }
    }

    @Test
    public void searchLotUsingThreat() {
        ThreatDTO searchDTO = new ThreatDTO();
        searchDTO.setThreatCategory(ThreatCategoryType.MAN_MADE_HAZARD);
        List<LotDTO> consequenceDTOs = DirectedThreatManager.searchLots(searchDTO, 1L, entityManager());
        log.info("Number of dtos = " + consequenceDTOs.size());
    }

    @Test
    public void testGetDirectedThreats() {
        ParreAnalysis parreAnalysis = new ParreAnalysis();
        parreAnalysis.setId(2L);
        List<DirectedThreat> directedThreats = DirectedThreatManager.getDirectedThreats(parreAnalysis, entityManager());
        for(DirectedThreat directedThreat : directedThreats) {
            log.info(directedThreat.getThreat().getName());
        }
    }

}
