/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.domain.manager;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;

import org.parre.server.domain.ReportAssessmentTask;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.RptAssessmentDTO;
import org.parre.shared.RptAssetDTO;
import org.parre.shared.RptConsequenceDTO;
import org.parre.shared.RptCountermeasureDTO;
import org.parre.shared.RptFatalitiesDTO;
import org.parre.shared.RptInjuriesDTO;
import org.parre.shared.RptMainDTO;
import org.parre.shared.RptNaturalThreatsDTO;
import org.parre.shared.RptRankingsDTO;
import org.parre.shared.RptResilienceDTO;
import org.parre.shared.RptRiskDTO;
import org.parre.shared.RptThreatDTO;
import org.parre.shared.RptThreatLikelihoodDTO;
import org.parre.shared.RptTop20Risks;
import org.parre.shared.RptVulnerabilityDTO;

public class ReportsManagerTest extends ServiceTest {
	private static transient final Logger log = Logger.getLogger(ReportsManager.class);
	
	@Test
	public void testGetParreAnalysisBaselineId() {
		Long PAID = ReportsManager.getParreAnalysisBaselineId(4L, entityManager());
		log.info("Parre Analysis ID (For Options) : " + PAID);
	}
	
	@Test
	public void testSetReportStaticContent() {
		RptMainDTO rptMain = new RptMainDTO();
		RptMainDTO result = ReportsManager.setReportStaticContent(rptMain, 5L, entityManager());
		log.info("Report Static Content Sample = : " + result.getsStatisticalValue());
	}
	
	//setReportUserInputData
	/*@Test
	public void testSetReportUserInputData() {
		RptMainDTO result = ReportsManager.setReportUserInputData(1L, entityManager());
		log.info("Report User Input Content Sample = : " + result.toString());
	}*/
	
	
	@Test
	public void testGetStatValues() {
		ParreAnalysisDTO result = ReportsManager.getStatValues(5L, entityManager());
		log.info("Fatality Dollar Amount =: " + result.getStatValuesDTO().getFatalityQuantity());
		
	}
	
	@Test
	public void testSetIndexContent() {
		RptMainDTO rptMain = new RptMainDTO();
		RptMainDTO result = ReportsManager.setIndexContent(rptMain, 1L, entityManager());
		log.info("Fri Index Data = : " + result.getsFRI1());
		log.info("Ori Index Data = : " + result.getsORI1());
		
	}
	
	
	@Test
	public void testGetAssessmentTask() {
		ReportAssessmentTask result = ReportsManager.getAssessmentTask(1L, entityManager());
		log.info("Found: " + result + " Assessment Task!");
		
	}
	
	
	@Test
	public void testGetCriticalAssets() {
		List<RptAssetDTO> results = ReportsManager.getCriticalAssets(1L, entityManager());
		log.info("Found: " + results.size() + " critical assets!");
		for (RptAssetDTO rptAssetDTO : results) {
			log.info("Asset System = " + rptAssetDTO.getAssetSystem() + " : Critical Asset = " + rptAssetDTO.getCriticalAsset());
		}
	}
	
	@Test
	public void testGetMostProbableThreats() {
		List<RptThreatDTO> results = ReportsManager.getMostProbableThreats(1L, entityManager());
		log.info("Found: " + results.size() + " Most Probale Threats!");
		for (RptThreatDTO rptThreatDTO : results) {
			log.info("Threat Class = " + rptThreatDTO.getThreatClass() + " : Threat = " + rptThreatDTO.getThreat());
		}
	}
	
	@Test
	public void testGetExistingCountermeasures() {
		List<RptCountermeasureDTO> results = ReportsManager.getExistingCountermeasures(1L, entityManager());
		log.info("Found: " + results.size() + " Existing Countermeasures!");
		for (RptCountermeasureDTO counterDTO : results) {
			log.info("Asset = " + counterDTO.getAssetName() + " : Countermeasures = " + counterDTO.getCounterMeasures());
		}
	}
	
	@Test
	public void testGetFinancialConsequences() {
		List<RptConsequenceDTO> results = ReportsManager.getFinancialConsequences(1L, entityManager());
		log.info("Found: " + results.size() + " Financial Consequences!");
		for (RptConsequenceDTO rptConseqDTO : results) {
			log.info("Asset = " + rptConseqDTO.getAsset() + " : Threat = " + rptConseqDTO.getThreat() + ": Consequence = " + rptConseqDTO.getConsequence());
		}
	}
	
	@Test
	public void testGetFatalities() {
		List<RptFatalitiesDTO> results = ReportsManager.getFatalities(1L, entityManager());
		log.info("Found: " + results.size() + " Fatalities!");
		for (RptFatalitiesDTO rptFatalDTO : results) {
			log.info("Asset = " + rptFatalDTO.getAsset() + " : Threat = " + rptFatalDTO.getThreat() + ": Fatalities = " + rptFatalDTO.getFatalities());
		}
	}
	
	@Test
	public void testGetInjuries() {
		List<RptInjuriesDTO> results = ReportsManager.getSeriousInjuries(1L, entityManager());
		log.info("Found: " + results.size() + " Serious Injuries!");
		for (RptInjuriesDTO rptInjuryDTO : results) {
			log.info("Asset = " + rptInjuryDTO.getAsset() + " : Threat = " + rptInjuryDTO.getThreat() + ": Injuries = " + rptInjuryDTO.getInjuries());
		}
	}
	
	
	@Test
	public void testGetVulnerabilities() {
		List<RptVulnerabilityDTO> results = ReportsManager.getVulnerabilities(1L, entityManager());
		log.info("Found: " + results.size() + " Vulnerabilities!");
		for (RptVulnerabilityDTO rptVulDTO : results) {
			log.info("Asset = " + rptVulDTO.getAsset() + " : Threat = " + rptVulDTO.getThreat() + ": Type Analysis = " + rptVulDTO.getTypeAnalysis() +": Vulnerabilities"+ rptVulDTO.getVulnerability());
		}
	}
	
	@Test
	public void testGetThreatLikelihoods() {
		List<RptThreatLikelihoodDTO> results = ReportsManager.getThreatLikelihoods(1L, entityManager());
		log.info("Found: " + results.size() + " Threat Likelihoods!");
		for (RptThreatLikelihoodDTO rptLikeDTO : results) {
			log.info("Asset = " + rptLikeDTO.getAsset() + " : Threat = " + rptLikeDTO.getThreat() + ": Type Analysis = " + rptLikeDTO.getTypeAnalysis() +": Likelihood"+ rptLikeDTO.getLikelihood());
		}
	}
	
	@Test
	public void testGetRisks() {
		List<RptRiskDTO> results = ReportsManager.getRisks(47L, entityManager());
		log.info("Found: " + results.size() + " Risks!");
		for (RptRiskDTO rptRiskDTO : results) {
			log.info("Asset = " + rptRiskDTO.getAsset() + " : Threat = " + rptRiskDTO.getThreat() + ": Risks = " + rptRiskDTO.getRisk());
		}
	}
	
	
	@Test
	public void testGetInvAssessment() {
		List<RptAssessmentDTO> results = ReportsManager.getInvAssessment(44L, entityManager());
		log.info("Found: " + results.size() + " Investment Assessment Results!");
		for (RptAssessmentDTO rptAssessmentDTO : results) {
			log.info("Option Name = " + rptAssessmentDTO.getOptionName() + " : Total Gross = " + rptAssessmentDTO.getGross() + ": Annual Value = " + rptAssessmentDTO.getValue() + ": Net Annual = " + rptAssessmentDTO.getNet() + ": B/C Ratio = " + rptAssessmentDTO.getRatio());
		}
	}
	
	@Test
	public void testGetResilience() {
		List<RptResilienceDTO> results = ReportsManager.getResilience(1L, entityManager());
		log.info("Found: " + results.size() + " Least Financially Resilience Results!");
		for (RptResilienceDTO rptResilienceDTO : results) {
			log.info("Asset = " + rptResilienceDTO.getAsset() + " : Threat = " + rptResilienceDTO.getThreat() + ": Financial Resilience = " + rptResilienceDTO.getResilience());
		}
	}
	
	
	@Test
	public void testGetTop20Risks() {
		List<RptTop20Risks> results = ReportsManager.getTop20Risks(44L, entityManager());
		log.info("Found: " + results.size() + " Top 20 Risks!");
		for (RptTop20Risks t20DTO : results) {
			log.info("Asset = " + t20DTO.getAsset() + " : Threat = " + t20DTO.getThreat() + " : Risk =" + t20DTO.getRisk() + ": Financial Impact = " + t20DTO.getFinImpact() + ": Vulnerability = " + t20DTO.getVulnerability() + ": Serious Injuries = " + t20DTO.getInjuries());
		}
	}
	
	@Test
	public void testGetNaturalThreats() {
		List<RptNaturalThreatsDTO> results = ReportsManager.getNaturalThreats(1L, entityManager());
		log.info("Found: " + results.size() + " Top 10 Natural Threats!");
		for (RptNaturalThreatsDTO natThreatDTO : results) {
			log.info("Asset = " + natThreatDTO.getAsset() + " : Threat = " + natThreatDTO.getThreat() + " : Risk =" + natThreatDTO.getRisk() + ": Financial Impact = " + natThreatDTO.getFinImpact() + ": Vul*Lot = " + natThreatDTO.getVulnerabilityLot());
		}
	}
	
	
	@Test
	public void testGetSummaryRankings() {
		List<RptRankingsDTO> results = ReportsManager.getSummaryRankings(1L, entityManager());
		log.info("Found: " + results.size() + " Summary Ranks!");
		for (RptRankingsDTO rptRankDTO : results) {
			//log.info("Asset = " + rptRankDTO.getAsset() + " : Threat = " + rptRankDTO.getThreat() + " : AssetThreatId = " + rptRankDTO.getAssetThreatId() + ": Risk =" + rptRankDTO.getRisk() + ": Risk Rank = " + rptRankDTO.getRiskRank() + " : Fatalities = " + rptRankDTO.getFatality() + ": Fatality Rank = " + rptRankDTO.getFatalityRank() );
			log.info("Results = " + rptRankDTO.toString());
		}
	}
	
	
}
