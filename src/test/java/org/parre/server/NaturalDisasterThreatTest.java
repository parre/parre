/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.parre.client.ClientSingleton;
import org.parre.server.NaturalThreatLookupData;
import org.parre.server.domain.Amount;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.NaturalThreatServiceImpl;
import org.parre.server.util.NaturalThreatCalculator;
import org.parre.server.util.NaturalThreatCalculatorImpl;
import org.parre.shared.AmountDTO;
import org.parre.shared.AppInitData;
import org.parre.shared.EarthquakeLookupDataDTO;
import org.parre.shared.EarthquakeMagnitudeProbabilityDTO;
import org.parre.shared.EarthquakeProfileDTO;
import org.parre.shared.HurricaneProfileDTO;
import org.parre.shared.IceStormIndexWorkDTO;
import org.parre.shared.IceStormWorkDTO;
import org.parre.shared.NaturalThreatCalculationDTO;
import org.parre.shared.NaturalThreatDTO;
import org.parre.shared.ParreAnalysisDTO;
import org.parre.shared.PhysicalResourceDTO;
import org.parre.shared.StatValuesDTO;
import org.parre.shared.types.MoneyUnitType;


/**
 * User: keithjones
 * Date: 2/6/12
 * Time: 3:03 PM
*/
public class NaturalDisasterThreatTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(DataAnalysisTest.class);
    
    private NaturalThreatCalculator calculator = new NaturalThreatCalculatorImpl();
    private static final Long TEST_ANALYSIS_ID = 1L;
    
    private boolean testDataExists = false;
    
    @Before
    public void setup() {
    	ParreAnalysis parreAnalysis = NaturalThreatServiceImpl.getInstance().findParreAnalysis(TEST_ANALYSIS_ID);
		parreAnalysis.setUseStatValues(true);
		parreAnalysis.getStatValues().setFatality(new Amount(new BigDecimal("6.2")));
		parreAnalysis.getStatValues().setSeriousInjury(new Amount(new BigDecimal("0.651")));
		entityManager().persist(parreAnalysis);
		
    	StatValuesDTO statValues = new StatValuesDTO();
    	statValues.setFatalityDTO(new AmountDTO(new BigDecimal("6.2"), MoneyUnitType.MILLION));
    	statValues.setSeriousInjuryDTO(new AmountDTO(new BigDecimal("0.651"), MoneyUnitType.MILLION));
    	
    	ParreAnalysisDTO testAnalysis = new ParreAnalysisDTO();
    	testAnalysis.setStatValuesDTO(statValues);
    	testAnalysis.setUseStatValues(true);
    	
    	AppInitData appInitData = new AppInitData();
    	appInitData.setParreAnalysisDTO(testAnalysis);
    	
    	ClientSingleton.setAppInitData(appInitData);
    	
    	testDataExists = NaturalThreatServiceImpl.getInstance().findParreAnalysis(TEST_ANALYSIS_ID) != null;
    }

    @Test
    public void tornadoCalculationTest() {
    	if (testDataExists) {
	    	NaturalThreatCalculationDTO calc = generateTornadoCalcDTO();
	    	calculator.calculateTornadoRisk(calc);
	    	compareResults(calc, 0.0006562d, 210000d, 9129.31d);
    	} else {
    		log.info("Test data does not exist for tornado calculation test.");
    	}
    }

    @Test
    public void floodCalculationTest() {
    	if (testDataExists) {
	    	NaturalThreatCalculationDTO calc = generateFloodCalcDTO();
	    	calc.getAnalysis().setFloodRisk(true);
	    	calculator.calculateFloodRisk(calc);
	    	compareResults(calc, 0.01d,  1002000.00d, 91550.00d);
    	} else {
    		log.info("Test data does not exist for flood calculation test.");
    	}
    }

    @Test
    public void earthquakeLookupTest() {
    	for (EarthquakeLookupDataDTO dto : NaturalThreatLookupData.get().getEarthquakeLookupDataDTOs()) {
    		log.debug(dto);
    	}
    }

    @Test
    public void magnitudeReverseOrderTest() {
    	NaturalThreatCalculationDTO calc = generateEarthquakeCalcDTO();
    	log.debug("Unordered set (" + calc.getEarthquakeProfile().getMagnitudeProbabilityDTOs().size() + " items):");
    	for (EarthquakeMagnitudeProbabilityDTO dto : calc.getEarthquakeProfile().getMagnitudeProbabilityDTOs()) {
    		log.debug(dto);
    	}

    	log.debug("Reverse order list:");
    	for (EarthquakeMagnitudeProbabilityDTO dto : EarthquakeProfileDTO.getMagnitudeProbabilitiesReverseOrder(calc.getEarthquakeProfile())) {
    		log.debug(dto);
    	}
    }

    @Test
    public void earthquakeCalculationTest() {
    	if (testDataExists) {
	    	NaturalThreatCalculationDTO calc = generateEarthquakeCalcDTO();
	    	calc.getAsset().setYearBuilt(1958);
	    	calculator.calculateEarthquakeRisk(calc);
	    	compareResults(calc, 0.002d, 650000d, 17606d);
    	} else {
    		log.info("Test data does not exist for earthquake calculation test.");
    	}
    }

    @Test
    public void hurricaneCalculationTest() {
    	if (testDataExists) {
    		NaturalThreatCalculationDTO calc = generateHurricaneCalcDTO();
    		calculator.calculateHurricaneRisk(calc);
    		compareResults(calc, 0.0779d, 310000d, 760693.5d);
    	} else {
    		log.info("Test data does not exist for hurricane calculation test.");
    	}
    }

    @Test
    public void iceStormCalculationTest() {
    	if (testDataExists) {
    		NaturalThreatCalculationDTO calc = generateIceStormCalcDTO();
    		calculator.calculateIceStormRisk(calc);
    		
    		compareResults(calc, 0.07808029622241637d,  9648667d,  188669.64d);
    		
    		log.info("Calculated total risk: " + calc.getAnalysis().getTotalRiskDTO().toString());
    		log.info("Calculated financial total: " + calc.getAnalysis().getFinancialTotalDTO().toString());
    	} else {
    		log.info("Test data does not exist for ice storm calculation test.");
    	}
    }
    
    @Test
    public void blankIceStormCalculationTest() {
    	if (testDataExists) {
    		NaturalThreatCalculationDTO calc = generateIceStormCalcDTO();
    		for (int i = 3; i <= 5; i++) {
    			calc.getIceStormWorkDTO().getIceStormIndex(i).setFatalities(0);
    			calc.getIceStormWorkDTO().getIceStormIndex(i).setSeriousInjuries(0);
    		}
    		calc.getIceStormWorkDTO().setDailyOperationalLoss(BigDecimal.ZERO);
    		calc.getIceStormWorkDTO().setDaysOfBackupPower(BigDecimal.ZERO);
    		calculator.calculateIceStormRisk(calc);
    		
    		log.info("Results from blank calculation: " + String.valueOf(calc));
    	} else {
    		log.info("Test data does not exist for ice storm calculation test.");
    	}
    }
    
    @Test
    public void row2IceStormTest() {
    	// from the excel spreadsheet "Natural Disaster Risk 2-15-2012.xlsx"
    	NaturalThreatCalculationDTO calc = generateIceStormCalcDTO();
    	calc.getIceStormWorkDTO().setDailyOperationalLoss(new BigDecimal("250000"));
    	calc.getIceStormWorkDTO().setDaysOfBackupPower(new BigDecimal("7"));
    	calc.getIceStormWorkDTO().getIceStormIndex(3).setRecoveryTime(new BigDecimal("0.5"));
    	calc.getIceStormWorkDTO().getIceStormIndex(3).setFatalities(0);
    	calc.getIceStormWorkDTO().getIceStormIndex(3).setSeriousInjuries(0);
    	
    	calc.getIceStormWorkDTO().getIceStormIndex(4).setRecoveryTime(new BigDecimal("1"));
    	calc.getIceStormWorkDTO().getIceStormIndex(4).setFatalities(0);
    	calc.getIceStormWorkDTO().getIceStormIndex(4).setSeriousInjuries(0);
    	
    	calc.getIceStormWorkDTO().getIceStormIndex(5).setRecoveryTime(new BigDecimal("3"));
    	calc.getIceStormWorkDTO().getIceStormIndex(5).setFatalities(0);
    	calc.getIceStormWorkDTO().getIceStormIndex(5).setSeriousInjuries(0);
    	
    	calculator.calculateIceStormRisk(calc);
    	log.info("Results from calculation: " + String.valueOf(calc));
    	
    	for (int i = 3; i <= 5; i++) {
    		log.info("Index work (" + i + ") : " + String.valueOf(calc.getIceStormWorkDTO().getIceStormIndex(i)));
    	}
    }
    
    private void compareResults(NaturalThreatCalculationDTO calc, double expectedThreatLikelihood, double expectedFinancialImpact, double expectedTotalRisk) {
    	log.debug("Calculated threat likelihood: " + calc.getAnalysis().getLot().doubleValue());
    	Assert.assertEquals(expectedThreatLikelihood, calc.getAnalysis().getLot().doubleValue(), 0);

    	log.debug("Calculated financial impact: " + calc.getAnalysis().getFinancialImpactDTO().getQuantity().doubleValue());
        Assert.assertEquals(expectedFinancialImpact, calc.getAnalysis().getFinancialImpactDTO().getQuantity().doubleValue(), expectedFinancialImpact*.01);
    	
        log.debug("Calculated financial total: " + calc.getAnalysis().getFinancialTotalDTO().getQuantity().doubleValue());
        log.debug("Calculated total risk: " + calc.getAnalysis().getTotalRiskDTO().getQuantity().doubleValue());
        Assert.assertEquals(expectedTotalRisk, calc.getAnalysis().getTotalRiskDTO().getQuantity().doubleValue(), expectedTotalRisk * .001);
    }
    
    @Test
    public void testTornadoLookup() {
    	NaturalThreatLookupData ntld = NaturalThreatLookupData.get();
    	BigDecimal fairfaxResult = ntld.getTornadoVulnerability("22027");
    	log.debug("Result: " + fairfaxResult);
    }
    
    private NaturalThreatCalculationDTO generateEarthquakeCalcDTO() {
    	PhysicalResourceDTO asset = new PhysicalResourceDTO();
    	asset.setReplacementCostDTO(new AmountDTO(new BigDecimal("1000000")));
    	asset.setDamageFactor(new BigDecimal("0.2"));
    	asset.setParreAnalysisId(TEST_ANALYSIS_ID);

    	NaturalThreatDTO analysis = new NaturalThreatDTO();
    	analysis.setFatalities(1);
    	analysis.setSeriousInjuries(3);
    	
    	analysis.setSeverityMgd(new BigDecimal("0.45"));
    	analysis.setDurationDays(BigDecimal.ONE);
    	
    	analysis.setOperationalLossDTO(new AmountDTO(new BigDecimal("450000")));

    	EarthquakeProfileDTO profile = new EarthquakeProfileDTO();
    	profile.setPercentG(new BigDecimal("2.5"));
    	profile.setReturnValue(new BigDecimal("50"));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("7.5"), new BigDecimal("0.07")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("8"), new BigDecimal("0.06")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("5"), new BigDecimal("0.12")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("5.5"), new BigDecimal("0.11")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("6"), new BigDecimal("0.1")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("6.5"), new BigDecimal("0.09")));
    	profile.addMagnitudeProbabilityDTO(new EarthquakeMagnitudeProbabilityDTO
    			(new BigDecimal("7"), new BigDecimal("0.08")));
    	
    	return new NaturalThreatCalculationDTO(asset, analysis, profile);
    }
    
    private NaturalThreatCalculationDTO generateTornadoCalcDTO() {
    	PhysicalResourceDTO asset = new PhysicalResourceDTO();
    	asset.setPostalCode("22027"); // fairfax county, va
    	asset.setReplacementCostDTO(new AmountDTO(new BigDecimal("1000000")));
    	asset.setDamageFactor(new BigDecimal("0.2"));
    	asset.setParreAnalysisId(TEST_ANALYSIS_ID);

    	NaturalThreatDTO analysis = new NaturalThreatDTO();
    	analysis.setFatalities(2);
    	analysis.setSeriousInjuries(2);
    	
    	analysis.setSeverityMgd(new BigDecimal("0.01"));
    	analysis.setDurationDays(BigDecimal.ONE);
    	
    	analysis.setOperationalLossDTO(new AmountDTO(new BigDecimal("10000")));
    	
    	analysis.setUseDefaultValues(true);
    	
    	return new NaturalThreatCalculationDTO(asset, analysis);
    }
    	
    private NaturalThreatCalculationDTO generateFloodCalcDTO() {
    	PhysicalResourceDTO asset = new PhysicalResourceDTO();
    	asset.setReplacementCostDTO(new AmountDTO(new BigDecimal("1000000")));
    	asset.setDamageFactor(BigDecimal.ONE);
    	asset.setParreAnalysisId(TEST_ANALYSIS_ID);
    
    	NaturalThreatDTO analysis = new NaturalThreatDTO();
    	analysis.setFatalities(1);
    	analysis.setSeriousInjuries(3);

    	analysis.setSeverityMgd(new BigDecimal("0.002"));
    	analysis.setDurationDays(BigDecimal.ONE);

    	analysis.setOperationalLossDTO(new AmountDTO(new BigDecimal("2000")));
    	
    	return new NaturalThreatCalculationDTO(asset, analysis);
    }

    private NaturalThreatCalculationDTO generateHurricaneCalcDTO() {
    	PhysicalResourceDTO asset = new PhysicalResourceDTO();
    	asset.setReplacementCostDTO(new AmountDTO(new BigDecimal("1000000")));
    	asset.setDamageFactor(new BigDecimal("0.3"));
    	asset.setParreAnalysisId(TEST_ANALYSIS_ID);
    	
    	NaturalThreatDTO analysis = new NaturalThreatDTO();
    	analysis.setFatalities(1);
    	analysis.setSeriousInjuries(5);
    	
    	analysis.setSeverityMgd(new BigDecimal("0.002"));
    	analysis.setDurationDays(BigDecimal.ONE);
    	
    	analysis.setOperationalLossDTO(new AmountDTO(new BigDecimal("10000")));
    	
    	analysis.setDesignSpeed(new BigDecimal("100"));
    	HurricaneProfileDTO profile = new HurricaneProfileDTO();
    	profile.setCat3ReturnPeriod(new BigDecimal("16"));
    	profile.setCat4ReturnPeriod(new BigDecimal("30"));
    	profile.setCat5ReturnPeriod(new BigDecimal("75"));
    	

    	return new NaturalThreatCalculationDTO(asset, analysis, profile);
    }
    
    private NaturalThreatCalculationDTO generateIceStormCalcDTO() {
    	PhysicalResourceDTO asset = new PhysicalResourceDTO();
    	asset.setReplacementCostDTO(new AmountDTO(new BigDecimal("1000000")));
    	asset.setDamageFactor(new BigDecimal("0.3"));
    	asset.setParreAnalysisId(TEST_ANALYSIS_ID);
    	
    	IceStormWorkDTO iceWork = new IceStormWorkDTO();
    	iceWork.setDailyOperationalLoss(new BigDecimal("10000"));
    	iceWork.setDaysOfBackupPower(new BigDecimal("4"));
    	iceWork.setStateCode("VA");
    	iceWork.setLatitude(new BigDecimal("39.1234"));
    	iceWork.setLongitude(new BigDecimal("-77.1234"));
    	
    	IceStormIndexWorkDTO index = new IceStormIndexWorkDTO();
    	index.setRecoveryTime(new BigDecimal("0"));
    	index.setFatalities(1);
    	index.setSeriousInjuries(1);
    	iceWork.addIceStormIndex(3, index);
    	
    	index = new IceStormIndexWorkDTO();
    	index.setRecoveryTime(new BigDecimal("0"));
    	index.setFatalities(1);
    	index.setSeriousInjuries(2);
    	iceWork.addIceStormIndex(4, index);
    	
    	index = new IceStormIndexWorkDTO();
    	index.setRecoveryTime(new BigDecimal("0"));
    	index.setFatalities(2);
    	index.setSeriousInjuries(3);
    	iceWork.addIceStormIndex(5, index);
    	
    
    	NaturalThreatDTO analysis = new NaturalThreatDTO();
    	
    	
    	return new NaturalThreatCalculationDTO(asset, analysis, iceWork);
    }
}
