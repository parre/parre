/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.invocation;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.parre.server.messaging.invocation.ServiceInvocationContext;
import org.parre.server.persistence.PersistenceTest;
import org.parre.shared.LoginInfo;

/**
 * The Class ServiceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 11/17/11
 */
public class ServiceTest extends PersistenceTest {
    private static transient final Logger log = Logger.getLogger(ServiceTest.class);
    @Before
    public void initServiceContext() {
    	System.setProperty("test.mode", "true");
        ServiceInvocationContext context = new ServiceInvocationContext(createLoginInfo());
        context.setEntityManager(entityManager());
        ServiceInvocationContext.init(context);
    }

    protected LoginInfo createLoginInfo() {
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setEmailAddress("sdhaliwal@aemcorp.com");
        return loginInfo;
    }

    @After
    public void closeServiceContext() {
        ServiceInvocationContext.destroy();
    }
}
