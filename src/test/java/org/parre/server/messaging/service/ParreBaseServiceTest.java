/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.ProposedMeasure;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.ParreServiceImpl;
import org.parre.shared.NameDescriptionDTO;


/**
 * User: keithjones
 * Date: 7/23/12
 * Time: 12:18 PM
*/
public class ParreBaseServiceTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(ParreBaseServiceTest.class);

    @Test
    public void testCopyParreAnalysis() {
        Long analysisId = 1L; //ParreServiceImpl.getInstance().getAppInitData().getParreAnalysisDTO().getId();
        NameDescriptionDTO dto = new NameDescriptionDTO();
        dto.setName("Option A");
        log.debug("Looking up Parre Analysis by id: " + analysisId);
        ParreAnalysis srcParreAnalysis =  ParreServiceImpl.getInstance().findParreAnalysis(analysisId);
        
        if (srcParreAnalysis != null) {
	        log.debug("Found analysis named: " + srcParreAnalysis.getName());        
	        boolean makeFullCopy = false;
	        
	        ParreAnalysis copy = ParreServiceImpl.getInstance().copyParreAnalysis(dto, srcParreAnalysis, makeFullCopy);
	        printAnalysisData(copy);
	        ParreAnalysis original = ParreServiceImpl.getInstance().findParreAnalysis(analysisId);
	        printAnalysisData(original);
	        
	        entityManager().getTransaction().rollback();
        } else {
        	log.debug("Parre analysis with id: " + analysisId + " does not exist");
        }
    }
    
    private void printAnalysisData(ParreAnalysis analysis) {
    	log.debug("Analysis name: " + analysis.getName());
    	StringBuffer sb = new StringBuffer();
    	for (ProposedMeasure pm : analysis.getProposedMeasures()) {
    		sb.append(pm.getId()).append(", ");
    	}
    	log.debug(" (" + analysis.getProposedMeasures().size() + ") Proposed Measures, ids: " + sb.toString());
    	
    }
}
