/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.domain.ParreAnalysis;
import org.parre.server.domain.RiskBenefitAnalysis;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.RiskBenefitServiceImpl;
import org.parre.shared.BenefitCostDTO;
import org.parre.shared.RiskBenefitAnalysisDTO;
import org.parre.shared.RiskBenefitDTO;
import org.parre.shared.types.RiskBenefitMetricType;


/**
 * The Class RiskBenefitServiceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 3/5/12
 */
public class RiskBenefitServiceTest extends ServiceTest {
    private static transient final Logger log = Logger.getLogger(RiskBenefitServiceTest.class);

    private RiskBenefitServiceImpl service = (RiskBenefitServiceImpl) RiskBenefitServiceImpl.getInstance();

    @Test
    public void initRiskBenefitOptions() {
        entityManager().find(RiskBenefitAnalysis.class, 8L);
        entityManager().find(ParreAnalysis.class, 1L);
        entityManager().find(ParreAnalysis.class, 2L);
        //service.initRiskBenefitOptions(riskBenefitAnalysis, optionAnalysis, baselineAnalysis);
    }
    
    //TODO: Fix these to work when there is no analysis data
    @Test
    public void getBenefitAnalysis() {
    	RiskBenefitAnalysisDTO benefitAnalysisDTO = service.getAnalysis(RiskBenefitMetricType.RISK);
    	log.info("Benefit analysis description: " + benefitAnalysisDTO.getDescription());
    }
    
    @Test
    public void getMetricAnalysis() {
    	List<RiskBenefitDTO> metricAnalysis = service.getMetricAnalysis(RiskBenefitMetricType.RISK);
    	if (metricAnalysis != null) {
    		for (RiskBenefitDTO metricAnalysi : metricAnalysis) {
    			log.info(metricAnalysi.getBaselineQuantity());
    		}
    	}
    }
    
    @Test
    public void getBenefitRiskAnalysis() {
        List<BenefitCostDTO> benefitAnalysis = service.getBenefitCostAnalysis();
        if (benefitAnalysis != null) {
	        for (BenefitCostDTO analysis : benefitAnalysis) {
	            log.info(analysis);
	        }
        }
    }


}
