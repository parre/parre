/*
 *   Copyright 2013, Applied Engineering Management Corporation 
 *
 *   This file is part of Parre.
 *
 *   Parre is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Parre is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Parre.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.parre.server.messaging.service;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.parre.server.messaging.invocation.ServiceTest;
import org.parre.server.messaging.service.AssetThreatServiceImpl;

/**
 * The Class AssetThreatServiceTest.
 *
 * @author Swarn S. Dhaliwal
 * @version 1.0 5/2/12
 */
public class AssetThreatServiceTest extends ServiceTest {
    private AssetThreatServiceImpl service = (AssetThreatServiceImpl) AssetThreatServiceImpl.getInstance();
    @Test
    public void testAssetCountChanged() throws Exception {

    }

    @Test
    public void testThreatAdded() throws Exception {

    }

    @Test
    public void testThreatRemoved() throws Exception {

    }

    @Test
    public void testStatValuesChanged() throws Exception {

    }

    @Test
    public void testConsequenceUpdated() throws Exception {
        service.consequenceUpdated(1L);
    }

    @Test
    public void testVulnerabilityUpdated() throws Exception {

    }

    @Test
    public void testAssetThreatCountChanged() throws Exception {

    }

    @Test
    public void testAssetAdded() throws Exception {

    }

    @Test
    public void testAssetRemoved() throws Exception {

    }
}
